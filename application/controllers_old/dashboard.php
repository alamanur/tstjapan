<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function index()
    {
        $this->common_model->frontUserCheck();
        $this->load->view('front_end/includes/header');
        $this->load->view('dashboard/dashboard_left_menu');
        $this->load->view('dashboard/dashboard');
        $this->load->view('dashboard/dashboard_footer');
        $this->load->view('front_end/includes/footer_index');
    }

    public function notice()
    {
        $this->common_model->frontUserCheck();
        $this->load->view('front_end/includes/header');
        $this->load->view('dashboard/dashboard_left_menu');
        $this->load->view('dashboard/notice');
        $this->load->view('dashboard/dashboard_footer');
        $this->load->view('front_end/includes/footer_index');
    }

    public function favorite()
    {
        $this->common_model->frontUserCheck();
        $this->load->view('front_end/includes/header');
        $this->load->view('dashboard/dashboard_left_menu');
        $this->load->view('dashboard/bookmark');
        $this->load->view('dashboard/dashboard_footer');
        $this->load->view('front_end/includes/footer_index');
    }

    public function customer_profile()
    {
        $this->common_model->frontUserCheck();
        $this->load->view('front_end/includes/header');
        $this->load->view('dashboard/dashboard_left_menu');
        $this->load->view('dashboard/customer_profile');
        $this->load->view('dashboard/dashboard_footer');
        $this->load->view('front_end/includes/footer_index');
    }


    public function logout()
    {
        $this->session->unset_userdata('fuser_id');
        $this->session->unset_userdata('fusername');
        $this->session->unset_userdata('fuser_last_name');
        $this->session->unset_userdata('fuser_email');
        redirect(base_url()."en/start");
    }
}