<?php

class Select_Model extends CI_Model
{
	public function __construct()
    {
        parent::__construct();
    }
    function Select_box($table_name)
    {
        $query="SELECT * FROM $table_name";
        $result=$this->db->query($query);
        $option="<option value=''>Select One</option>";
        foreach ($result->result_array() as $data)
        {
            $option.='<option value="'.$data['make_id'].'" '.set_select("txtMakeId",$data['make_id']).'>'.$data['name'].'</option>';
        }
        return $option;
    }

    function Select_car_category()
    {
        $query="SELECT * FROM tbl_category";
        $result=$this->db->query($query);
        $option="<option value=''>Select One</option>";
        foreach ($result->result_array() as $data)
        {
            $option.='<option value="'.$data['category_id'].'"'. set_select("txtCategory",$data["category_id"]).'>'.$data['name'].'</option>';
        }
        return $option;
    }/*
    function Select_transmission()
    {
        $query="SELECT * FROM tbl_transmission";
        $result=$this->db->query($query);
        $option="<option value=''>Select One</option>";
        foreach ($result->result_array() as $data)
        {
            $option.='<option value="'.$data['category_id'].'"'. set_select("txtCategory",$data["category_id"]).'>'.$data['name'].'</option>';
        }
        return $option;
    }*/
    function Select_country()
    {
        $query="SELECT * FROM tbl_country";
        $result=$this->db->query($query);
        $option="<option value=''>Select One</option>";
        foreach ($result->result_array() as $data)
        {
            $option.='<option value="'.$data['id'].'"'. set_select("txtMadeIn",$data["id"]).'>'.$data['country_name'].'</option>';
        }
        return $option;
    }

    function Select_car_seller($table_name)
    {
        $query="SELECT * FROM $table_name";
        $result=$this->db->query($query);
        $option="<option value=''>Select One</option>";
        foreach ($result->result_array() as $data)
        {
            $option.='<option value="'.$data['company_id'].'">'.$data['company_name'].'</option>';
        }
        return $option;
    }


    public function Select_User()
    {
        $sql="SELECT a.admin_user_id, a.admin_first_name,a.admin_last_name, a.admin_phone, a.status, a.created, b.role_name FROM tst_admin_user AS a INNER JOIN admin_user_role AS b ON a.admin_role=b.role_id ORDER BY a.admin_user_id DESC";
        $query=$this->db->query($sql);
        return $query;

    }

    public  function Select_Car_By_Model($parameter){


        $sql='SELECT tbl_make.make_id,tbl_make.name,tbl_model.model_id,tbl_model.model_name,tbl_product.product_id,tbl_product.price,tbl_product.Mileage,tbl_product.reference_no,tbl_product.feature_image,tbl_category.name,tbl_product.manufacture_year FROM `tbl_make` INNER JOIN tbl_model ON tbl_make.make_id = tbl_model.make_id INNER JOIN tbl_product ON tbl_model.model_id = tbl_product.model_id LEFT JOIN tbl_category ON tbl_product.category_id = tbl_category.category_id'.$parameter;
      /*  print($sql);
        die;*/
        $query=$this->db->query($sql);
        return $query;
    }

    public function Select_Car_List()
    {
        //print_r($this->input->post());die;
        $txtMakeId = $this->input->post('txtMakeId');
        $min_price = $this->input->post('min_price');
        $max_price = $this->input->post('max_price');
        $txtModel = $this->input->post('txtModel');
        $manufactureYearfrom = $this->input->post('manufactureYearfrom');
        $manufactureMonthfrom = $this->input->post('manufactureMonthfrom');
        $manufactureYearto = $this->input->post('manufactureYearto');
        $manufactureMonthto = $this->input->post('manufactureMonthto');
        //$dpfrom = $this->input->post('dpfrom');
        //$dpto = $this->input->post('dpto');
       // $left_hand = $this->input->post('left_hand');
       // $right_hand = $this->input->post('right_hand');
       // $special = $this->input->post('special');
        
        $this->db->select("p.product_id,p.Mileage,p.reference_no,p.price,p.feature_image,p.manufacture_year");
        $this->db->select("mk.name");
        $this->db->select("md.model_name");
        $this->db->from("tbl_product p");
        $this->db->join("tbl_make mk","p.make_id=mk.make_id","left");
        $this->db->join("tbl_model md","p.model_id=md.model_id","left");
        if($txtMakeId!="")
        {
            $this->db->where("p.make_id",$txtMakeId);
        }
        if($min_price!="")
        {
            $this->db->where("p.price >=",$min_price);
        }
        if($max_price!="")
        {
            $this->db->where("p.price <=",$max_price);
        }
        if($txtModel!="")
        {
            $this->db->where("p.model_id",$txtModel);
        }
        if($manufactureYearfrom!="")
        {
            $this->db->where("p.manufacture_year >=",$manufactureYearfrom);
        }
        if($manufactureYearto!="")
        {
            $this->db->where("p.manufacture_year <=",$manufactureYearto);
        }
        if($manufactureMonthfrom!="")
        {
            $this->db->where("p.manufacture_month >=",$manufactureMonthfrom);
        }
        if($manufactureMonthto!="")
        {
            $this->db->where("p.manufacture_month <=",$manufactureMonthto);
        }
       
        $query = $this->db->get();
        return $query;

    }

    public function select_new_car($limit,$offset){
        $sql ="SELECT `tbl_product`.`price`,`tbl_product`.`product_id`,`tbl_product`.`manufacture_year`,`tbl_product`.`feature_image`,`tbl_make`.`name` ,`tbl_model`.`model_name` FROM `tbl_product`INNER JOIN `tbl_make` ON `tbl_make`.`make_id`= `tbl_product`.`make_id` INNER JOIN `tbl_model` ON `tbl_model`.`model_id`=`tbl_product`.`model_id` ORDER BY `tbl_product`.`product_id` DESC LIMIT $limit,$offset";
        $query=$this->db->query($sql);
        return $query;
    }
    public function count_by_range($min,$max){
        $sql ="SELECT count(`product_id`) as no FROM `tbl_product` WHERE `price` BETWEEN $min AND $max";
        $query=$this->db->query($sql);
        return $query;
    }

    public function count_by_category($category){
        $sql ="SELECT count(`product_id`) as no FROM `tbl_product` WHERE `category_id`= '$category'";
        $query=$this->db->query($sql);
        return $query;
    }

    public function Select_model()
    {
        $sql="SELECT a.model_id,a.model_name,a.created,b.name FROM tbl_model AS a INNER JOIN tbl_make AS b ON a.make_id=b.make_id ORDER BY a.model_id DESC";
        $query=$this->db->query($sql);
        return $query;

    }
    public function Select_body_style()
    {
        $sql="SELECT c.id, c.body_style,a.model_id,a.model_name,c.remarks,b.name FROM tbl_body_style AS c inner join tbl_model AS a ON c.model_id = a.model_id  INNER JOIN tbl_make AS b ON a.make_id=b.make_id ORDER BY a.model_id DESC";
        $query=$this->db->query($sql);
        return $query;

    }
    public function Select_transmission()
    {
        $sql="SELECT c.id, c.transmission,a.model_id,a.model_name,c.remarks,b.name FROM tbl_transmission AS c inner join tbl_model AS a ON c.model_id = a.model_id  INNER JOIN tbl_make AS b ON a.make_id=b.make_id ORDER BY a.model_id DESC";
        $query=$this->db->query($sql);
        return $query;

    }
    public function Select_engine_type()
    {
        $sql="SELECT c.id, c.engine_type,a.model_id,a.model_name,c.remarks,b.name FROM tbl_engine_type AS c inner join tbl_model AS a ON c.model_id = a.model_id  INNER JOIN tbl_make AS b ON a.make_id=b.make_id ORDER BY a.model_id DESC";
        $query=$this->db->query($sql);
        return $query;

    }

    public function Select_model_by_id($id)
    {
        $sql="SELECT tbl_make.name, tbl_model.model_id,tbl_model.model_name, COUNT(tbl_product.model_id) AS no FROM tbl_make LEFT JOIN tbl_model ON tbl_make.make_id = tbl_model.make_id LEFT JOIN tbl_product ON tbl_model.model_id=tbl_product.model_id WHERE tbl_model.make_id = '$id' GROUP BY tbl_model.model_name";
        $query=$this->db->query($sql);
        return $query;

    }
    public function Select_transportation_cost()
    {
        $sql="SELECT tbl_transportation_cost.*,tbl_country.country_name,tbl_country_port.port_name FROM `tbl_transportation_cost` INNER JOIN tbl_country ON tbl_country.id=tbl_transportation_cost.country_id INNER JOIN tbl_country_port ON tbl_country_port.id=tbl_transportation_cost.port_id ";
        $query=$this->db->query($sql);
        return $query;

    }

    function Select_Single_Employee_Info($admin_id)
    {
        $query="SELECT a.`admin_user_id`,a.admin_first_name,a.admin_last_name,a.admin_email,a.admin_address,a.admin_phone,a.status,a.last_login,a.profile_picture,a.created,a.modified,b.role_name FROM tst_admin_user AS a INNER JOIN admin_user_role AS b ON a.admin_role=b.role_id WHERE a.admin_user_id=$admin_id";
        $result=mysql_query($query);
        $row=mysql_fetch_array($result);
       /* print_r($row);
        die*/;
        //$date_of_birth=date('d-m-Y',strtotime($row['created']));
        //$date_of_joining=date('d-m-Y',strtotime($row[7]));
        if($row['status'] == 1){
            $status= "Active";
        }else{
            $status= "Inactive";
        }

        $feedback='<div class="col-md-2">
                <div class="left-div">
                    <img style="width:70%; height: auto; margin-left: 5%;" src="'.base_url().'resource/images/admin/'.$row['profile_picture'].'" alt="'.$row['admin_first_name'].'" />
                </div>
            </div>
            <div class="col-md-10">
                <div class="right-div">
                    <div class="heading-div">Personal Info</div>
                    <p>Name : '.$row['admin_first_name'].' '.$row['admin_last_name'] .'</p>
                    <p>Email Address : '.$row['admin_email'].'</p>
                    <p>Phone : '.$row['admin_phone'].'</p>
                    <p>User Role : '.$row['role_name'].'</p>
                    <p>Status : '.$status.'</p>
                    <p>Last Login: '.$row['last_login'].'</p>
                    <p>Created : '.$row['created'].'</p>
                    <p>Modified : '.$row['modified'].'</p>
                </div>
            </div>';
        return $feedback;
    }

    public function select_single_car_details($car_id)
    {
        $sql='SELECT * FROM tbl_product WHERE product_id ='.$car_id;
        $query=$this->db->query($sql);
        $result=$query->row();
        return $result;
    }

    public function select_single_car_details_front_end($car_id){
        $sql ="SELECT tbl_product.*,tbl_body_style.body_style,tbl_category.name as category_name, tbl_engine_type.engine_type , tbl_make.name AS make_name, tbl_model.model_name,tbl_transmission.transmission,tbl_company_information.* , tbl_country.country_name FROM `tbl_product` INNER JOIN tbl_body_style ON tbl_product.body_style_id=tbl_body_style.id INNER JOIN tbl_category ON tbl_category.category_id=tbl_product.category_id INNER JOIN tbl_engine_type ON tbl_engine_type.id=tbl_product.engine_type_id INNER JOIN tbl_make ON tbl_make.make_id=tbl_product.make_id INNER JOIN tbl_model ON tbl_model.model_id=tbl_product.model_id INNER JOIN tbl_transmission ON tbl_transmission.id=tbl_product.transmission_id INNER JOIN tbl_company_information ON tbl_company_information.company_id = tbl_product.seller_id INNER JOIN tbl_country ON tbl_country.id=tbl_product.made_in WHERE tbl_product.product_id = $car_id";
        $query=$this->db->query($sql);
        $result=$query->row();
        return $result;
    }


    public function select_bookmark_product($customer_id)
    {
        $sql="SELECT tbl_product.*,tbl_favorite.* ,tbl_category.*,tbl_make.*,tbl_model.* from tbl_product INNER JOIN tbl_favorite on tbl_favorite.product_id=tbl_product.product_id INNER JOIN tbl_category ON tbl_category.category_id=tbl_product.category_id INNER JOIN tbl_make ON tbl_make.make_id =tbl_product.make_id INNER JOIN tbl_model ON tbl_model.model_id=tbl_product.model_id where tbl_favorite.customer_id='$customer_id' and tbl_favorite.status='1'";
        $query=$this->db->query($sql);
        return $query;

    }

    function select_all($table=null,$order=null,$id_field=null,$where_field=null,$where_val =null)
    {
        $statement='';
        if($order != null && $id_field != null){
            $statement .= " ORDER BY $id_field $order";
        }
        if($where_field != null && $where_val != null){
            $statement = " WHERE ".$where_field."=".$where_val;
        }

        $sql="SELECT * FROM $table $statement";
        $query=$this->db->query($sql);
        return $query;

    }


	 public function  Select_Single_Row_product($id,$table,$id_field)
    {
        $result=$this->db->query("SELECT * FROM `$table` WHERE `$id_field`='$id'");
        $data=$result->result_array();
        return $data;
    }

    public function  Select_All_Row_Where($id,$table,$id_field)
    {
        $result=$this->db->query("SELECT * FROM `$table` WHERE `$id_field`='$id'");
        return $result;
    }

    public function  Select_Single_Row($id,$table,$id_field)
        {
            $result=$this->db->query("SELECT * FROM `$table` WHERE `$id_field`='$id'");
            $data=$result->row_array();
            return $data;
        }


    public function check_favorite($product_id,$customer_id){
        $sql = "SELECT * FROM `tbl_favorite` WHERE `product_id`= $product_id AND `customer_id`= $customer_id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0)
        {
            return true;
        }else{
            return false;
        }
    }
    public function check_order($product_id,$customer_id){
        $sql = "SELECT * FROM `tbl_order_list` WHERE `car_id`= $product_id AND `customer_id`= $customer_id";
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0)
        {
            return true;
        }else{
            return false;
        }
    }

    public function Select_single_product_basic_info($id){
        $sql= "SELECT tbl_product.product_id, tbl_make.name as make_name, tbl_model.model_name FROM tbl_product INNER JOIN tbl_make ON tbl_make.make_id=tbl_product.make_id INNER JOIN tbl_model ON tbl_model.model_id=tbl_product.product_id WHERE tbl_product.product_id='$id'";
        $result = $this->db->query($sql);
        $data=$result->row_array();
        return $data;

    }

    public function Select_all_images(){
        $sql = "SELECT tbl_attachment.*,tbl_make.name as make_name,tbl_model.model_name FROM `tbl_attachment` INNER JOIN tbl_product ON tbl_attachment.product_id= tbl_product.product_id INNER JOIN tbl_make ON tbl_product.make_id=tbl_make.make_id INNER JOIN tbl_model ON tbl_model.model_id=tbl_product.model_id";
        $query = $this->db->query($sql);
        //$data=$query->result();
        return $query;
    }


    public function Select_seller_list(){
        $sql="SELECT tbl_company_information.*,tbl_country.country_name FROM `tbl_company_information` INNER JOIN tbl_country ON tbl_country.id=tbl_company_information.country";
        $query = $this->db->query($sql);
        return $query;
    }

    public function select_dollar_price(){
        $sql= "SELECT * FROM `tbl_dollar_price`";
        $result = $this->db->query($sql);
        $data=$result->row_array();
        return $data;

    }



    public function select_order_list($customer_id)
    {
        $sql="SELECT tbl_product.*,tbl_order_list.id as order_list_id,tbl_order_list.customer_id,tbl_order_list.car_id ,tbl_order_list.country_id,tbl_order_list.port_id ,tbl_category.*,tbl_make.*,tbl_model.* from tbl_product INNER JOIN tbl_order_list on tbl_order_list.car_id=tbl_product.product_id INNER JOIN tbl_category ON tbl_category.category_id=tbl_product.category_id INNER JOIN tbl_make ON tbl_make.make_id =tbl_product.make_id INNER JOIN tbl_model ON tbl_model.model_id=tbl_product.model_id where tbl_order_list.customer_id='$customer_id' and tbl_order_list.status='1'";
        $query=$this->db->query($sql);
        return $query;

    }
    public function select_transport_cost($country_id,$port_id)
    {
        $sql="SELECT * FROM `tbl_transportation_cost` where country_id='$country_id' and port_id='$port_id'";
        $query=$this->db->query($sql);
        return $query->row();

    }

    /*---------End------*/
}