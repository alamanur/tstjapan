<div class="col-md-9">
    <div class="faq-help">
        <p>How much is shipping charge to my port of destination?</p>
        <div class="basic-help-child">
            Prices appearing on TST JAPAN are all free on board (FOB) prices.
            FOB does not include shipping charge to the port of destination.
            If you add your destination port and country,
            the C&F price (shipping cost inclusive price) will appear automatically.
            Shipping costs vary according to the way of shipping (ro-ro or container),
            the shipping company, the departure port etc... You can ask the
            Seller directly to know the shipping cost details.
            Shipping.
        </div>
    </div>
</div>