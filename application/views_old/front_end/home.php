<section id="main-content">
    <div class="container">
            <div class="col-md-3 col-sm-3" style="background: rgba(0,0,0,.05);">
                <div class="varticalmenu">
                    <h3>Browse by Make</h3>
                    <?php echo $vertical_menu;?><!--for vertical menu-->
                </div>
                <div class="left-text"><!--left bar link strat-->
                    <div class="browse-price-dealer">
                        <div class="browse-price">
                            <h4>Browse by FOB Price</h4>
                            <a href="<?php echo base_url();?>en/car/max-price/0-1000"><i class="fa fa-arrow-right"></i> Under US $1,000 <span>
                                    <?php
                                    $query = $this->select_model->count_by_range('0','1000');
                                    foreach ($query->result() as $row) {
                                        echo "(".$row->no.")";
                                    }
                                    ?>
                                </span></a><br />
                            <a href="<?php echo base_url();?>en/car/max-price/1001-2000"><i class="fa fa-arrow-right"></i> Under US $2,000 <span>
                                    <?php
                                    $query = $this->select_model->count_by_range('1001','2000');
                                    foreach ($query->result() as $row) {
                                        echo "(".$row->no.")";
                                    }
                                    ?>
                                </span></a><br />
                            <a href="<?php echo base_url();?>en/car/max-price/2001-5000"><i class="fa fa-arrow-right"></i> Under US $5,000 <span>
                                    <?php
                                    $query = $this->select_model->count_by_range('2001','5000');
                                    foreach ($query->result() as $row) {
                                        echo "(".$row->no.")";
                                    }
                                    ?>
                                </span></a><br />
                            <a href="<?php echo base_url();?>en/car/max-price/5001-10000"><i class="fa fa-arrow-right"></i> Under US $10,000 <span>
                                    <?php
                                    $query = $this->select_model->count_by_range('5001','10000');
                                    foreach ($query->result() as $row) {
                                        echo "(".$row->no.")";
                                    }
                                    ?>
                                </span></a><br />
                            <a href="<?php echo base_url();?>en/car/max-price/10001-20000"><i class="fa fa-arrow-right"></i> Under US $20,000 <span>
                                    <?php
                                    $query = $this->select_model->count_by_range('10001','20000');
                                    foreach ($query->result() as $row) {
                                        echo "(".$row->no.")";
                                    }
                                    ?>
                                </span></a><br />
                            <a href="<?php echo base_url();?>en/car/max-price/20001-50000"><i class="fa fa-arrow-right"></i> Under US $50,000 <span>
                                    <?php
                                    $query = $this->select_model->count_by_range('20001','50000');
                                    foreach ($query->result() as $row) {
                                        echo "(".$row->no.")";
                                    }
                                    ?>
                                </span></a><br />
                            <a href="<?php echo base_url();?>en/car/max-price/50001-10000"><i class="fa fa-arrow-right"></i> Under US $100,000 <span>
                                    <?php
                                    $query = $this->select_model->count_by_range('50001','100000');
                                    foreach ($query->result() as $row) {
                                        echo "(".$row->no.")";
                                    }
                                    ?>
                                </span></a><br />
                            <a href="<?php echo base_url();?>en/car/max-price/100001-500000"><i class="fa fa-arrow-right"></i> Under US $500,000 <span>
                                    <?php
                                    $query = $this->select_model->count_by_range('100001','500000');
                                    foreach ($query->result() as $row) {
                                        echo "(".$row->no.")";
                                    }
                                    ?>
                                </span></a><br />
                            <a href="<?php echo base_url();?>en/car/max-price/500001-1000000"><i class="fa fa-arrow-right"></i> Under US $1000,000 <span>
                                    <?php
                                    $query = $this->select_model->count_by_range('500001','1000000');
                                    foreach ($query->result() as $row) {
                                        echo "(".$row->no.")";
                                    }
                                    ?>
                                </span></a><br />
                        </div>
                        <hr/>

                        <div class="browse-category">
                            <h4>Browse by Category</h4>
                            <a href="<?php echo base_url();?>en/car/category/suv"><i class="fa fa-car"></i> SUV<span>
                                   <?php
                                   $query = $this->select_model->count_by_category(2);
                                   foreach ($query->result() as $row) {
                                       echo "(".$row->no.")";
                                   }
                                   ?>
                                </span></a><br />
                            <a href="<?php echo base_url();?>en/car/category/car"><i class="fa fa-car"></i> Car<span>
                                    <?php
                                    $query = $this->select_model->count_by_category('1');
                                    foreach ($query->result() as $row) {
                                        echo "(".$row->no.")";
                                    }
                                    ?>
                                </span></a><br />
                            <a href="<?php echo base_url();?>en/car/category/truck"><i class="fa fa-truck"></i> Truck<span>
                                    <?php
                                    $query = $this->select_model->count_by_category('3');
                                    foreach ($query->result() as $row) {
                                        echo "(".$row->no.")";
                                    }
                                    ?>
                                </span></a><br />
                            <a href="<?php echo base_url();?>en/car/category/bus"><i class="fa fa-bus"></i> Bus<span>
                                    <?php
                                    $query = $this->select_model->count_by_category('7');
                                    foreach ($query->result() as $row) {
                                        echo "(".$row->no.")";
                                    }
                                    ?>
                                </span></a><br />
                            <a href="<?php echo base_url();?>en/car/category/motorcycle"><i class="fa fa-motorcycle"></i> Motorcycle<span>
                                    <?php
                                    $query = $this->select_model->count_by_category('5');
                                    foreach ($query->result() as $row) {
                                        echo "(".$row->no.")";
                                    }
                                    ?>
                                </span></a><br />
                            <a href="<?php echo base_url();?>en/car/category/machinery"><i class="fa fa-tachometer"></i> Machinery<span>
                                    <?php
                                    $query = $this->select_model->count_by_category('4');
                                    foreach ($query->result() as $row) {
                                        echo "(".$row->no.")";
                                    }
                                    ?>
                                </span></a><br />
                            <a href="<?php echo base_url();?>en/car/category/parts"><i class="fa fa-cog"></i> Parts<span>
                                    <?php
                                    $query = $this->select_model->count_by_category('6');
                                    foreach ($query->result() as $row) {
                                        echo "(".$row->no.")";
                                    }
                                    ?>
                                </span></a><br />
                        </div>
                    </div>
                </div><!--left bar link end-->
            </div>
            <div class="col-md-9 main-content-right">
                <div class="row slider_section">
                    <div class="col-md-3 col-sm-3">
                        <div class="stock">
                            <p>Total cars stock<br />
                                <span class="stock-body">61,358</span>
                            </p>
                            <p>
                                Updated today<br />
                                <span class="stock-body">1,372</span>
                            </p>
                            <p>
                                No. of Exporters<br />
                                <span class="stock-body">1,001</span>
                            </p><br />

                        </div>
                    </div>
                    <div class="col-md-9 col-sm-9"><!--for slider-->
                        <div class="slider">
                            <?php /*include ("slider.php");*/ echo $slide; ?>

                        </div>
                    </div>

                    <section id="search-form"><!-- search-form section strat-->
                        <div class="col-md-12">
                            <div class="row">
                                <div class="search-form-body">
                                    <form class="form-horizontal" method="post" action="<?php echo base_url().'en/used_car'; ?>">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Keywords</label>
                                            <div class="col-md-4">
                                                <input name="keyword" type="text" class="form-control" placeholder="Keywords">
                                            </div>

                                            <label class="col-md-1 control-label">Make</label>
                                            <div class="col-md-5">
                                                <select name="txtMakeId" id="txtMakeId" class="form-control">
                                                    <?php
                                                    echo $this->select_model->Select_box($table='tbl_make');
                                                    ?>
                                                </select>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-md-2 control-label">FOB Price</label>
                                            <div class="col-md-2">
                                                <select class="form-control" name="min_price">
                                                    <option value="">Any</option>
                                                    <option value="1000">US $ 1,000</option>
                                                    <option value="2000">US $ 2,000</option>
                                                    <option value="5000">US $ 5,000</option>
                                                    <option value="10000">US $ 10,000</option>
                                                    <option value="20000">US $ 20,000</option>
                                                </select>
                                            </div>
                                            <div class="col-md-2">
                                                <select class="form-control" name="max_price">
                                                    <option value="">Any</option>
                                                    <option value="1000">US $ 1,000</option>
                                                    <option value="2000">US $ 2,000</option>
                                                    <option value="5000">US $ 5,000</option>
                                                    <option value="10000">US $ 10,000</option>
                                                    <option value="20000">US $ 20,000</option>
                                                </select><br />
                                            </div>
                                            <label class="col-md-1 control-label">Model</label>
                                            <div class="col-md-5">
                                                <select name="txtModel" id="txtModel" class="form-control">
                                                    <option value="">Select One</option>
                                                </select>
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Year/Month</label>
                                            <div class="col-sm-2">
                                                <select name="manufactureYearfrom" class="form-control" >
                                                    <option value="">Any</option>
                                                    <?php
                                                    $y= date("Y")+1;
                                                    for ($i = 0;$i < 50;$i++)
                                                    {
                                                        $y -=1;
                                                        echo "<option value='$y'>$y</option>";
                                                    }
                                                    ?>

                                                </select>
                                            </div>
                                            <div class="col-sm-3">
                                                <select name="manufactureMonthfrom" class="form-control" >
                                                    <option value="">Any</option>
                                                    <option value="January">January</option>
                                                    <option value="February">February</option>
                                                    <option value="March">March</option>
                                                    <option value="April">April</option>
                                                    <option value="May">May</option>
                                                    <option value="June">June</option>
                                                    <option value="July">July</option>
                                                    <option value="August">August</option>
                                                    <option value="September">September</option>
                                                    <option value="October">October</option>
                                                    <option value="November">November</option>
                                                    <option value="December">December</option>
                                                </select>
                                            </div>

                                            <div class="col-sm-2">
                                                <select name="manufactureYearto" class="form-control" >
                                                    <option value="">Any</option>
                                                    <?php
                                                    $y= date("Y")+1;
                                                    for ($i = 0;$i < 50;$i++)
                                                    {
                                                        $y -=1;
                                                        echo "<option value='$y'>$y</option>";
                                                    }
                                                    ?>

                                                </select>
                                            </div>
                                            <div class="col-sm-3">
                                                <select name="manufactureMonthto" class="form-control" >
                                                    <option value="">Any</option>
                                                    <option value="January">January</option>
                                                    <option value="February">February</option>
                                                    <option value="March">March</option>
                                                    <option value="April">April</option>
                                                    <option value="May">May</option>
                                                    <option value="June">June</option>
                                                    <option value="July">July</option>
                                                    <option value="August">August</option>
                                                    <option value="September">September</option>
                                                    <option value="October">October</option>
                                                    <option value="November">November</option>
                                                    <option value="December">December</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-sm-2 control-label">Displacement</label>
                                            <div class="col-sm-5">
                                                <select name="dpfrom" class="form-control">
                                                    <option value="">Any</option>
                                                    <option value="550">550</option>
                                                    <option value="660">660</option>
                                                    <option value="800">800</option>
                                                    <option value="1000">1000</option>
                                                    <option value="1100">1100(1.1L)</option>
                                                    <option value="1200">1200(1.2L)</option>
                                                    <option value="1300">1300(1.3L)</option>
                                                    <option value="1400">1400(1.4L)</option>
                                                    <option value="1500">1500(1.5L)</option>
                                                    <option value="1600">1600(1.6L)</option>
                                                    <option value="1700">1700(1.7L)</option>
                                                    <option value="1800">1800(1.8L)</option>
                                                    <option value="1900">1900(1.9L)</option>
                                                    <option value="2000">2000(2.0L)</option>
                                                    <option value="2100">2100(2.1L)</option>
                                                    <option value="2200">2200(2.2L)</option>
                                                    <option value="2300">2300(2.3L)</option>
                                                    <option value="2400">2400(2.4L)</option>
                                                    <option value="2500">2500(2.5L)</option>
                                                    <option value="2600">2600(2.6L)</option>
                                                    <option value="2700">2700(2.7L)</option>
                                                    <option value="2800">2800(2.8L)</option>
                                                    <option value="2900">2900(2.9L)</option>
                                                    <option value="3000">3000(3.0L)</option>
                                                    <option value="3500">3500(3.5L)</option>
                                                    <option value="4000">4000(4.0L)</option>
                                                    <option value="4500">4500(4.5L)</option>
                                                    <option value="5000">5000(3.5L)</option>
                                                    <option value="5500">5500(5.5L)</option>
                                                    <option value="6000">6000(6.0L)</option>
                                                </select>
                                            </div>

                                            <div class="col-sm-5">
                                                <select name="dpto" class="form-control">
                                                    <option value="">Any</option>
                                                    <option value="550">550</option>
                                                    <option value="660">660</option>
                                                    <option value="800">800</option>
                                                    <option value="1000">1000</option>
                                                    <option value="1100">1100(1.1L)</option>
                                                    <option value="1200">1200(1.2L)</option>
                                                    <option value="1300">1300(1.3L)</option>
                                                    <option value="1400">1400(1.4L)</option>
                                                    <option value="1500">1500(1.5L)</option>
                                                    <option value="1600">1600(1.6L)</option>
                                                    <option value="1700">1700(1.7L)</option>
                                                    <option value="1800">1800(1.8L)</option>
                                                    <option value="1900">1900(1.9L)</option>
                                                    <option value="2000">2000(2.0L)</option>
                                                    <option value="2100">2100(2.1L)</option>
                                                    <option value="2200">2200(2.2L)</option>
                                                    <option value="2300">2300(2.3L)</option>
                                                    <option value="2400">2400(2.4L)</option>
                                                    <option value="2500">2500(2.5L)</option>
                                                    <option value="2600">2600(2.6L)</option>
                                                    <option value="2700">2700(2.7L)</option>
                                                    <option value="2800">2800(2.8L)</option>
                                                    <option value="2900">2900(2.9L)</option>
                                                    <option value="3000">3000(3.0L)</option>
                                                    <option value="3500">3500(3.5L)</option>
                                                    <option value="4000">4000(4.0L)</option>
                                                    <option value="4500">4500(4.5L)</option>
                                                    <option value="5000">5000(3.5L)</option>
                                                    <option value="5500">5500(5.5L)</option>
                                                    <option value="6000">6000(6.0L)</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class=" form-group">
                                            <label class="col-md-4 control-label">
                                                <input name="left_hand" type="checkbox" id="blankCheckbox" value="Left" aria-label="...">Left Hand Drive
                                            </label>
                                            <label class="col-md-4 control-label">
                                                <input name="right_hand" type="checkbox" id="blankCheckbox" value="Right" aria-label="...">Right Hand Drive
                                            </label>
                                            <label class="col-md-4 control-label">
                                                <input name="special" type="checkbox" id="blankCheckbox" value="Special" aria-label="...">Special Price
                                            </label><br/><br/>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-12 text-center">
                                                <input type="submit" class="btn btn-primary btn-lg" value="Search">
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </section><!-- search-form section end-->

                    <section id="new-arrivals">
                        <h3>New Arrivals</h3>
                        <div class="col-md-12">
                            <div class="row">
                                <?php

                                $feedback=$this->select_model->select_new_car('0','6');
                                foreach ($feedback->result() as $row) {
                                    ?>
                                    <div class="col-md-2">
                                        <a href="<?php echo base_url(); ?>en/car_details/<?php echo $row->product_id;?>">
                                            <div class="arrivals-body">
                                                <img class="car_list" src="<?php echo base_url(); ?>resource/images/car/<?php echo $row->feature_image;?>"
                                                     alt="<?php echo $row->feature_image;?>"/>

                                                <p>
                                                    <?php echo $row->name;?> <?php echo $row->model_name;?><br/>
                                                    US$<?php echo $row->price;?>
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                <?php
                                }
                                ?>


                            </div>
                            <div class="row">
                                <?php

                                $feedback1=$this->select_model->select_new_car('4','6');
                                foreach ($feedback1->result() as $row) {
                                    ?>
                                    <div class="col-md-2">
                                        <a href="<?php echo base_url(); ?>en/car_details/<?php echo $row->product_id;?>">
                                            <div class="arrivals-body">
                                                <img class="car_list" src="<?php echo base_url(); ?>resource/images/car/<?php echo $row->feature_image;?>"
                                                     alt="<?php echo $row->feature_image;?>"/>

                                                <p>
                                                    <?php echo $row->name;?> <?php echo $row->model_name;?><br/>
                                                    US$<?php echo $row->price;?>
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                <?php
                                }
                                ?><hr/>
                        </div>
                    </section>


                    <section id="popular-vehicles">
                        <h3>Popular vehicles in your country</h3>
                        <div class="col-md-12">
                            <div class="row">
                                <?php

                                $feedback2=$this->select_model->select_new_car('7','6');
                                foreach ($feedback2->result() as $row) {
                                    ?>
                                    <div class="col-md-2">
                                        <a href="<?php echo base_url(); ?>en/car_details/<?php echo $row->product_id;?>">
                                            <div class="arrivals-body">
                                                <img class="car_list" src="<?php echo base_url(); ?>resource/images/car/<?php echo $row->feature_image;?>"
                                                     alt="<?php echo $row->feature_image;?>"/>

                                                <p>
                                                    <?php echo $row->name;?> <?php echo $row->model_name;?><br/>
                                                    US$<?php echo $row->price;?>
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                <?php
                                }
                                ?>
                            </div>
                            <div class="row">
                                <?php

                                $feedback3=$this->select_model->select_new_car('2','6');
                                foreach ($feedback3->result() as $row) {
                                    ?>
                                    <div class="col-md-2">
                                        <a href="<?php echo base_url(); ?>en/car_details/<?php echo $row->product_id;?>">
                                            <div class="arrivals-body">
                                                <img class="car_list" src="<?php echo base_url(); ?>resource/images/car/<?php echo $row->feature_image;?>"
                                                     alt="<?php echo $row->feature_image;?>"/>

                                                <p>
                                                    <?php echo $row->name;?> <?php echo $row->model_name;?><br/>
                                                    US$<?php echo $row->price;?>
                                                </p>
                                            </div>
                                        </a>
                                    </div>
                                <?php
                                }
                                ?>
                            </div>
                        </div>
                    </section>
                </div>
            </div>

            <section id="contry-flag">
                <div class="container">
                    <div class="flag-body">
                        <h4>Check your local page</h4>
                        <p>Select the country to see local Import procedure and regulation.</p>
                        <div class="col-md-1">
                            <a href="#" data-toggle="tooltip" title="Antigua and Barbuda"><img src="<?php echo base_url();?>resource/images/flag/Antigua and Barbuda.png" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Aruba"><img src="<?php echo base_url();?>resource/images/flag/Aruba-flag.png" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Australia"><img src="<?php echo base_url();?>resource/images/flag/Australia.png" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Bahamas"><img src="<?php echo base_url();?>resource/images/flag/bahamas.png" height="30" width="50" /></a>
                        </div>

                        <div class="col-md-1">
                            <a href="#" data-toggle="tooltip" title="Bangladesh"><img src="<?php echo base_url();?>resource/images/flag/bangladesh.png" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Botswana"><img src="<?php echo base_url();?>resource/images/flag/Botswana.jpg" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Burundi"><img src="<?php echo base_url();?>resource/images/flag/Burundi.png" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Canada"><img src="<?php echo base_url();?>resource/images/flag/Canada.png" height="30" width="50" /></a>
                        </div>

                        <div class="col-md-1">
                            <a href="#" data-toggle="tooltip" title="Cayman Islands"><img src="<?php echo base_url();?>resource/images/flag/Cayman Islands.png" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Chile"><img src="<?php echo base_url();?>resource/images/flag/Chile.png" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Curacao"><img src="<?php echo base_url();?>resource/images/flag/Curacao.png" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Dominica"><img src="<?php echo base_url();?>resource/images/flag/Dominica.png" height="30" width="50" /></a>
                        </div>

                        <div class="col-md-1">
                            <a href="#" data-toggle="tooltip" title="DRC"><img src="<?php echo base_url();?>resource/images/flag/DRC.png" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Georgia"><img src="<?php echo base_url();?>resource/images/flag/Georgia.jpg" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Ghana"><img src="<?php echo base_url();?>resource/images/flag/Ghana.png" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Guatemala"><img src="<?php echo base_url();?>resource/images/flag/Guatemala.jpg" height="30" width="50" /></a>
                        </div>

                        <div class="col-md-1">
                            <a href="#" data-toggle="tooltip" title="Guyana"><img src="<?php echo base_url();?>resource/images/flag/Guyana.png" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Jamaica"><img src="<?php echo base_url();?>resource/images/flag/Jamaica.png" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Kenya"><img src="<?php echo base_url();?>resource/images/flag/Kenya.png" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Lesotho"><img src="<?php echo base_url();?>resource/images/flag/Lesotho.gif" height="30" width="50" /></a>
                        </div>

                        <div class="col-md-1">
                            <a href="#" data-toggle="tooltip" title="Malawi"><img src="<?php echo base_url();?>resource/images/flag/Malawi.jpg" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Mongolia"><img src="<?php echo base_url();?>resource/images/flag/Mongolia.png" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Mozambique"><img src="<?php echo base_url();?>resource/images/flag/Mozambique.jpg" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Myanmar"><img src="<?php echo base_url();?>resource/images/flag/Myanmar.gif" height="30" width="50" /></a>
                        </div>

                        <div class="col-md-1">
                            <a href="#" data-toggle="tooltip" title="Namibia"><img src="<?php echo base_url();?>resource/images/flag/namibia-flag.gif" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Nauru"><img src="<?php echo base_url();?>resource/images/flag/Nauru.png" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="New Zealand"><img src="<?php echo base_url();?>resource/images/flag/New Zealand.png" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Nigeria"><img src="<?php echo base_url();?>resource/images/flag/Nigeria.png" height="30" width="50" /></a>
                        </div>

                        <div class="col-md-1">
                            <a href="#" data-toggle="tooltip" title="Pakistan"><img src="<?php echo base_url();?>resource/images/flag/Pakistan.png" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Papua New Guinea"><img src="<?php echo base_url();?>resource/images/flag/Papua New Guinea.jpg" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Russian Federation"><img src="<?php echo base_url();?>resource/images/flag/Russian Federation.jpg" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Rwanda"><img src="<?php echo base_url();?>resource/images/flag/Rwanda.png" height="30" width="50" /></a>
                        </div>

                        <div class="col-md-1">
                            <a href="#" data-toggle="tooltip" title="Saint Kitts and Nevis"><img src="<?php echo base_url();?>resource/images/flag/Saint Kitts and Nevis.jpg" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Saint Vincent and the Grenadines"><img src="<?php echo base_url();?>resource/images/flag/Saint Vincent and the Grenadines.png" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Samoa"><img src="<?php echo base_url();?>resource/images/flag/Samoa.png" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Singapore"><img src="<?php echo base_url();?>resource/images/flag/Singapore.png" height="30" width="50" /></a>
                        </div>

                        <div class="col-md-1">
                            <a href="#" data-toggle="tooltip" title="Solomon Islands"><img src="<?php echo base_url();?>resource/images/flag/Solomon Islands.png" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="South Sudan"><img src="<?php echo base_url();?>resource/images/flag/South Sudan.png" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Sri Lanka"><img src="<?php echo base_url();?>resource/images/flag/Sri Lanka.png" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Suriname"><img src="<?php echo base_url();?>resource/images/flag/Suriname.png" height="30" width="50" /></a>
                        </div>

                        <div class="col-md-1">
                            <a href="#" data-toggle="tooltip" title="Swaziland"><img src="<?php echo base_url();?>resource/images/flag/Swaziland.jpg" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Tanzania"><img src="<?php echo base_url();?>resource/images/flag/Tanzania.png" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Thailand"><img src="<?php echo base_url();?>resource/images/flag/Thailand.png" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Trinidad and Tobago"><img src="<?php echo base_url();?>resource/images/flag/Trinidad and Tobago.png" height="30" width="50" /></a>
                        </div>

                        <div class="col-md-1">
                            <a href="#" data-toggle="tooltip" title="Uganda"><img src="<?php echo base_url();?>resource/images/flag/Uganda.png" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="United Kingdom"><img src="<?php echo base_url();?>resource/images/flag/United Kingdom.png" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="United States"><img src="<?php echo base_url();?>resource/images/flag/United States.jpg" height="30" width="50" /></a>

                            <a href="#" data-toggle="tooltip" title="Kiribati"><img src="<?php echo base_url();?>resource/images/flag/Kiribati.png" height="30" width="50" /></a>
                        </div>

                    </div>
                </div>
            </section>
    </div>
</section>