
<section id="comfirmation-mail">
    <div class="container">
        <div class="col-md-12">
            <center>
                <div style="overflow: hidden;">

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                      <li role="presentation" class="active"><a href="#confirmEmail" aria-controls="confirmEmail" role="tab" data-toggle="tab">Confirmation Email</a></li>
                      <li role="presentation"><a href="#userInfo" aria-controls="userInfo" role="tab" data-toggle="tab">User Information</a></li>
                      <li role="presentation"><a class="signupComplete anchor_disable confirmUserInfo" href="#conFirmUserInfo" aria-controls="conFirmUserInfo" role="tab" data-toggle="tab">Confirm Information</a></li>
                      <li role="presentation"><a class="signupComplete anchor_disable complete_submit" href="#complete" aria-controls="complete" role="tab" data-toggle="tab">Registration Complete</a></li>
                    </ul>

                    <!-- Tab panes -->
                    <div class="tab-content">
                        <span id="errorMsg"></span>
                        <div role="tabpanel" class="tab-pane active" id="confirmEmail">
                            <center>
                                <p class="confirm-mail-to">Confirmation Email sent to: <span><?php echo $prevemail; ?></span></p>
                                <img src="<?php echo base_url();?>images/img_sentmail.png" />
                                <p class="reg-not-complete">Registration is not completed yet.</p>
                                <img src="<?php echo base_url();?>images/confirmation-message.png" />
                                <p>Or click here to next or previous tab.</p>
                            </center>
                            <a class="btn btn-primary btnNext replace_text" >Next</a>
                        </div>
                      <div role="tabpanel" class="tab-pane" id="userInfo">
                            <p class="confirm-mail-to">Registration is 100% free, no credit card required, no hidden fees.</p>
                            <div class="registration-2 col-md-8">
                                <form class="form-horizontal" id="signUpcompleteForm" method="post">
                                    <label class="col-md-5 control-label">Country:</label>
                                    <div class="col-md-7">
                                        <select name="customer_country" class="form-control fcountry">
                                            <option value="Bangladesh">Bangladesh</option>
                                            <option value="Japan">Japan</option>
                                            <option value="India">India</option>
                                            <option value="England">England</option>
                                            <option value="China">China</option>
                                        </select><br/>
                                    </div>
                                    <label class="col-md-5 control-label">Email Address:</label>
                                    <div class="col-md-7">
                                        <input name="customer_email" maxlength="100" minlength="10" value="<?php echo $prevemail; ?>" type="email" class="form-control customer_email" placeholder="exampale@yahoo.com" readonly>  <br/>
                                    </div>

                                   <!-- <label class="col-md-5 control-label">Login ID:</label>
                                    <div class="col-md-7">
                                        <input name="login_id" type="text" class="form-control login_id" placeholder="Ex: john2016">
                                    </div>
                                    <p class="text-right com-2-ptag">*Used when logging in.</p>-->
                                    <label class="col-md-5 control-label">Password:</label>
                                    <div class="col-md-7">
                                        <input type="password" maxlength="15" minlength="6" name="password" class="form-control password" placeholder="Use at least 6 letters.">  <br/>
                                    </div>

                                    <label class="col-md-5 control-label">First Name:</label>
                                    <div class="col-md-7">
                                        <input type="text" maxlength="25" minlength="3" name="first_name" class="form-control first_name" placeholder="Ex: Willy ">
                                      <p class="text-right">*This name will appear on TST Japan Co., website.</p>
                                    </div>

                                    <label class="col-md-5 control-label">Last Name:</label>
                                    <div class="col-md-7">
                                        <input type="text" maxlength="25" minlength="2" name="last_name" class="form-control last_name" placeholder="Ex:  Smith">
                                        <p class="text-right">*This name will appear on TST Japan Co., website.</p>
                                    </div>
                                    <label class="col-md-5 control-label">Address:</label>
                                    <div class="col-md-7">
                                        <input type="text" maxlength="100" minlength="10" name="address" class="form-control address" placeholder="Road : #4, House : #75, Niketon, Gulshan, Dhaka"><br/>

                                    </div>
                                    <label class="col-md-5 control-label">Postal Code/Zip:</label>
                                    <div class="col-md-7">
                                        <input type="text" maxlength="6" minlength="4" name="postal_code" class="form-control postal_code" placeholder="1212"><br/>

                                    </div>
                                    <label class="col-md-5 control-label">Mobile No:</label>
                                    <div class="col-md-7">
                                        <input type="text" maxlength="15" minlength="8" name="mobile_no" class="form-control mobile_no" placeholder="1212"><br/>

                                    </div>
                                    <label class="col-md-5 control-label">Home Phone:</label>
                                    <div class="col-md-7">
                                        <input type="text" maxlength="15" minlength="6" name="home_phone" class="form-control home_phone" placeholder="1212"><br/>

                                    </div>
                                        <label class="col-sm-5 control-label">Date of birth:</label>
                                        <div class="col-sm-7">
                                            <div class="form-group date_of_birth">
                                                <div class='input-group date' id='datetimepicker9'>
                                                    <input type='text' name="customer_date_of_birth" class="form-control customer_date_of_birth" />
                                                    <span class="input-group-addon">
                                                        <span class="glyphicon glyphicon-calendar">
                                                        </span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <p class="text-right com-2-ptag">*You will need it when you reset the password.</p>


                                    <label class="col-md-8 control-label com-2-showpass">
                                        <input type="checkbox" name="special" class="special" id="blankCheckbox" value="1" aria-label="..."> Received Special Update
                                    </label><br/>
                                    <p class="col-md-10 com-2-terms">Please check the <a href="">Terms of Use</a> and click on the Confirm button to proceed.</p>
                                </form>
                               <div class="col-md-12">
                                   <a class="btn btn-primary btnPrevious" >Previous</a>
                                   <a class="btn btn-primary btnNext replace_text" >Next</a>
                               </div>
                            </div>
                            
                      </div>
                      <div role="tabpanel" class="tab-pane" id="conFirmUserInfo">
                          <p class="confirm-mail-to">Registration is 100% free, no credit card required, no hidden fees.</p>

                            <div class="registration-2 col-md-8">
                                <label class="col-md-5 control-label">Country:</label>
                                <div class="col-md-7">
                                    <input value="" type="text" class="form-control ccountry" placeholder="Bangladesh" readonly><br/>
                                </div>
                                <label class="col-md-5 control-label">Email Address:</label>
                                <div class="col-md-7">
                                    <input value="" type="text" class="form-control cemail" placeholder="exampale@yahoo.com" readonly><br/>
                                </div>
                                <label class="col-md-5 control-label">First Name: </label>
                                <div class="col-md-7">
                                  <input value="" type="text" class="form-control cfirstName" placeholder="Jhon" readonly><br/>
                                </div>
                                <label class="col-md-5 control-label">last Name: </label>
                                <div class="col-md-7">
                                  <input value="" type="text" class="form-control clastName" placeholder="Smith" readonly><br/>
                                </div>

                                <label class="col-md-5 control-label">Address:</label>
                                <div class="col-md-7">
                                    <input value="" type="text" class="form-control cAddress" readonly><br/>
                                </div>
                                <label class="col-md-5 control-label">Postal Code/Zip:</label>
                                <div class="col-md-7">
                                    <input value="" type="text" class="form-control cpostalCode" readonly><br/>
                                </div>
                                <label class="col-md-5 control-label">Mobile No:</label>
                                <div class="col-md-7">
                                    <input value="" type="text" class="form-control cmobile_no" readonly><br/>
                                </div>
                                <label class="col-md-5 control-label">Home Phone:</label>
                                <div class="col-md-7">
                                    <input value="" type="text" class="form-control chome_phone"  readonly><br/>
                                </div>
                                <label class="col-md-5 control-label">Date of birth:</label>
                                <div class="col-md-7">
                                    <input value="" type="text" class="form-control cdateOfBirth"  readonly><br/>
                                </div>
                                <label class="col-md-5 control-label">Password:</label>
                                <div class="col-md-7">
                                    <input value="" type="password" class="form-control cpassword" placeholder="........." readonly><br/>
                                </div>
                                 <div class="col-md-12">
                                     <a class="btn btn-primary btnPrevious" >Previous</a>
                                     <a class="btn btn-primary btnNext replace_text" >Next</a>
                                 </div>
                            </div>
                           
                      </div>
                      <div role="tabpanel" class="tab-pane" id="complete">
                          <p class="confirm-mail-to">Registration is 100% free, no credit card required, no hidden fees.</p>
                          <span class="registration_success_message"></span>
                            <p class="com-4-thank">Thank you for your Information!</p>
                            <p class="com-4-registration">To complete your registration please click finish button.</p>
                            <img src="<?php echo base_url(); ?>images/ready-to-start.png"/>
                            <div class="com-4-head">
                                    <i class="fa fa-star"></i> Popular vehicles in your country
                            </div>
                            <a class="btn btn-primary btnPrevious" >Previous</a>
                            <a class="btn btn-primary btnNext replace_text registrationComplete" >Finish</a>
                      </div>
                    </div>

                  </div>
                        
            </center>
        </div>	
    </div>
</section>
