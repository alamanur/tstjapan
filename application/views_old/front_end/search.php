<section id="usedcar-body">
    <div class="container">

        <div class="col-md-3" style="background: rgba(0,0,0,.05);">
            <div class="usedcar-left">
                <!--Search Box--->
                <?php
                echo $search;
                ?>
            </div>

            <div class="varticalmenu">
                <h3>Browse by Make</h3>
                <!--for vertical menu-->
                <?php
                echo $vertical_menu;
                ?>
            </div>
        </div>
        <div class="col-md-9">
            <section id="used-car-right">
                <div class="col-md-12 calculate"><!-- calculate div start -->
                    <h4>Calculate your Total Price</h4>
                    <p>Select your Destination</p>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="col-md-4 control-label">Country</label>
                            <div class="col-md-8">
                                <select class="form-control">
                                    <option>Bangladesh</option>
                                    <option>India</option>
                                    <option>USA</option>

                                </select><br/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-4 control-label">Nearest port</label>
                            <div class="col-md-8">
                                <select class="form-control">
                                    <option>Chittagong</option>
                                    <option>Mongla</option>

                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="checkbox form-group">
                            <label class="col-md-12 control-label">
                                <input type="checkbox" id="blankCheckbox" value="option1" aria-label="...">Insurance
                            </label>
                            <label class="col-md-12 control-label">
                                <input type="checkbox" id="blankCheckbox" value="option1" aria-label="...">Inspection <i class="fa fa-question-circle"></i>
                            </label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="calculation">
                            <i class="fa fa-calculator fa-2x"></i> <span>Calculate</span>
                        </div>
                    </div><!--calculate div end -->
                </div>
            </section>

            <section id="used-car-main-content">
                <div class="col-md-12">
                    <div class="row">
                       <!-- <div class="used-car-menu col-md-12">
                            <ul>
                                <a href="#"><li>Japan Stocks</li></a>
                                <a href="#"><li>All Stocks</li></a>
                                <a href="#"><li>Motorcycle & ATV</li></a>
                                <a href="#"><li>Parts</li></a>
                            </ul>
                        </div>-->
                        <div class="pagination col-md-12">
                            <div>
                                <?php //echo $car_list; ?>
                                <div class="panel-body">
                                    <table class="table table-striped table-bordered table-hover" id="carList">
                                        <thead>
                                        <tr>
                                            <th>Images</th>
                                            <th>Manufacture</th>
                                            <th>Mileage</th>
                                            <th>Price</th>
                                            <th>Estimate Price</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <?php
                                        //$query=$this->select_model->Select_Car_List();
                                        foreach ($search_query->result() as $row)
                                        {
                                            ?>
                                            <tr class="odd gradeX">
                                                <td class="text-center"><img src="<?php echo base_url();?>resource/images/car/<?php echo $row->feature_image; ?>" width="195"></td>
                                                <td class="center">
                                                    <a href="<?php echo base_url();?>en/car_details/<?php echo $row->product_id; ?>"><p style="font-size: 25px; color: #0088CC;"> <?php echo $row->name.' '.$row->model_name;?></p></a>
                                                    <p><?php echo $row->reference_no;?></p>
                                                </td>
                                                <td class="center"><?php echo $row->Mileage;?></td>
                                                <td class="center"><?php echo $row->price;?></td>
                                                <td class="center text-center"><button data-toggle="modal" data-target="#exampleModal" data-whatever="@mdo" class="btn btn-danger" >Negotiate</button> </td>
                                            </tr>
                                        <?php

                                        }
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>

    </div>
</section>
<!--Negotiation Model Start --->
<!--<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Negotiation Message</h4>
            </div>
            <div class="modal-body">
                <div class="form-group" id="message">
                    <input type="checkbox"value="I want to negotiate the best price"> I want to negotiate the best price <br>
                    <input type="checkbox" value="I want to know the shipping schedule"> I want to know the shipping schedule<br>
                    <input type="checkbox" value="I want to know about the condition of the car"> I want to know about the condition of the car
                </div>
                <form action="" method="get">

                    <div class="form-group">

                        <ul id="message-text"></ul>
                        <textarea class="form-control" name="message-text" id=""></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-danger">Send message</button>
                    </div>
                </form>
            </div>
            <div class="terms">
                <input type="checkbox" name="special_promotion"> I would like to receive information about special promotions from tstjapan.co.jp.<br>

                By clicking "Send" button, you agree to the following terms.<br/><br/>
                >> <a href=""> tstjapan.co.jp's Terms of Use</a><br/>
                >> <a href=""> Privacy Policy</a>
            </div>

        </div>
    </div>
</div>--><!--Negotiation Model End --->
<script type="text/javascript">
    //for advance search
    function toggle() {
        var ele = document.getElementById("toggleText");
        var text = document.getElementById("displayText");
        if(ele.style.display == "block") {
            ele.style.display = "none";
            text.innerHTML = "Advance Search";
        }
        else {
            ele.style.display = "block";
            text.innerHTML = "Basic Search";
        }
    }

    $('#myModal').on('shown.bs.modal', function () {
        $('#myInput').focus();
    });

    function updateTextArea() {
        var allVals = [];
        $('#c_b :checked').each(function() {
            allVals.push($(this).val());
        });
        $('#message-text').val(allVals);
    }
    $(function() {
        $('#message input').click(updateTextArea);
        updateTextArea();
    });

    /*$('#message input[checkbox]').change(function() {
     if (this.checked) {
     alert(11);
     $li = $('<li></li>');
     $li.text(this.value);
     $('#message-text').append($li);
     }
     else {
     $('li:contains('+this.value+')', '#message-text').remove();
     }
     });*/
</script>