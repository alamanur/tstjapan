<section id="signup">
    <div class="container">
        <div class="col-md-6 signup-left">
            <div class="text-center">
                <h3>Welcome to TST JAPAN </h3>
                <h1>Log in </h1>
                <div class="err_msg"></div>
                <div class="box-content"  >
                    <?php
                    //-----Display Success or Error message---
                    if(isset($feedback)){
                        echo $feedback;
                    }
                    //----Form Tag Start-------------
                    $attributes = array('class' => 'email', 'id' => 'myform');

                    echo form_open('', $attributes);
                    ?>
                </div>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa  fa-user"></i> </span>
                    <?php
                    $attributes=array(
                        'name'=>'txtEmailAddress',
                        'class'=>'form-control',
                        'placeholder'=>' Email Address',
                        'maxlength'   => '70',
                        'value' => set_value('txtEmailAddress')
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtEmailAddress');?></label>
                </div>
                <div class="input-group">
                    <span class="input-group-addon"><i class="fa fa-key"></i> </span>
                    <?php
                    $attributes=array(
                        'name'=>'txtPassword',
                        'class'=>'form-control',
                        'placeholder'=>'Write Password',
                        'maxlength'   => '12',
                        'value' => set_value('txtPassword')
                    );
                    echo form_password($attributes);
                    $currentUrl = "";
                    if(isset($_GET['currentUrl']))
                    {
                        $currentUrl = $_GET['currentUrl'];
                    }
                    ?>
                    <input type="hidden" name="currentUrl" class="currentUrl" value="<?php echo $currentUrl; ?>">
                </div>
                <a class="pull-left" href="">Forgot Password ?</a>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtPassword');?></label>
                </div>
                <?php
                $attribute=array(
                    'name'=>'btnSubmit',
                    'class'=>'btn btn-primary btn-lg ',
                    'value'=>'Sign in',
                    'id'=>'loginSubmit'
                );
                echo form_submit($attribute);//--Form Submit Button

                echo form_close();//--Form closing tag </form>
                ?>

                <p >Don't have an account? <a href="<?php echo base_url();?>en/sign_up/" >Sign Up</a></p>
            </div>
        </div>
        <div class="col-md-6 te">
            <div class="text-center">
                <img src="<?php echo base_url(); ?>resource/images/logo.png" /> is
                <p><b>TST JAPAN Co. Ltd., is Japan's largest online used car marketplace</b></p>
            </div>
            <div class="clearfix"></div>
            <img class="singin-side-img" src="<?php echo base_url(); ?>resource/images/sign_in_image.png" />
        </div>
    </div>
</section>