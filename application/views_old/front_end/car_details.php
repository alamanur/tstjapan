<section id="details-page">
    <div class="container">
        <div class="row">
            <div class="col-md-10">
                <div class="col-md-6"><!-- first div left start -->
                    <div class="details-left">

                        <h2><?php echo $row->manufacture_year.' '.$row->name.' '.$row->model_name;?></h2>
                        <img class="btnfShare" src="<?php echo base_url();?>resource/images/facebook-share-button.png" alt="Facebook Share button" />
                        <a href="<?php echo base_url();?>en/bookmarking/?product_id=<?php echo $row->product_id;?>" <i class="fa fa-star-o fa-lg fav pull-right favorite"> Add to My Favorite</i></a><br/><br/>

                        <div>
                            <a class="example-image-link" href="<?php echo base_url();?>resource/images/car/<?php echo $row->feature_image;?>" data-lightbox="example-set" data-title="Click the right half of the image to move forward."><img class="example-image first-image" src="<?php echo base_url();?>resource/images/car/<?php echo $row->feature_image;?>" alt="<?php echo $row->reference_no;?>"/></a>
                            <a class="example-image-link" href="<?php echo base_url();?>resource/images/details/2_b.jpg" data-lightbox="example-set" data-title="Or press the right arrow on your keyboard."><img class="example-image" src="<?php echo base_url();?>resource/images/details/2_s.jpg" alt="" /></a>
                            <a class="example-image-link" href="<?php echo base_url();?>resource/images/details/3_b.jpg" data-lightbox="example-set" data-title="The next image in the set is preloaded as you're viewing."><img class="example-image" src="<?php echo base_url();?>resource/images/details/3_s.jpg" alt="" /></a>
                            <a class="example-image-link" href="<?php echo base_url();?>resource/images/details/4_b.jpg" data-lightbox="example-set" data-title="Click anywhere outside the image or the X to the right to close."><img class="example-image" src="<?php echo base_url();?>resource/images/details/4_s.jpg" alt="" /></a>
                            <a class="example-image-link" href="<?php echo base_url();?>resource/images/details/2_b.jpg" data-lightbox="example-set" data-title="Or press the right arrow on your keyboard."><img class="example-image" src="<?php echo base_url();?>resource/images/details/2_s.jpg" alt="" /></a>
                            <a class="example-image-link" href="<?php echo base_url();?>resource/images/details/3_b.jpg" data-lightbox="example-set" data-title="The next image in the set is preloaded as you're viewing."><img class="example-image" src="<?php echo base_url();?>resource/images/details/3_s.jpg" alt="" /></a>
                            <a class="example-image-link" href="<?php echo base_url();?>resource/images/details/4_b.jpg" data-lightbox="example-set" data-title="Click anywhere outside the image or the X to the right to close."><img class="example-image" src="<?php echo base_url();?>resource/images/details/4_s.jpg" alt="" /></a>
                            <a class="example-image-link" href="<?php echo base_url();?>resource/images/details/2_b.jpg" data-lightbox="example-set" data-title="Or press the right arrow on your keyboard."><img class="example-image" src="<?php echo base_url();?>resource/images/details/2_s.jpg" alt="" /></a>
                            <a class="example-image-link" href="<?php echo base_url();?>resource/images/details/3_b.jpg" data-lightbox="example-set" data-title="The next image in the set is preloaded as you're viewing."><img class="example-image" src="<?php echo base_url();?>resource/images/details/3_s.jpg" alt="" /></a>
                            <a class="example-image-link" href="<?php echo base_url();?>resource/images/details/4_b.jpg" data-lightbox="example-set" data-title="Click anywhere outside the image or the X to the right to close."><img class="example-image" src="<?php echo base_url();?>resource/images/details/4_s.jpg" alt="" /></a>
                            <a class="example-image-link" href="<?php echo base_url();?>resource/images/details/2_b.jpg" data-lightbox="example-set" data-title="Or press the right arrow on your keyboard."><img class="example-image" src="<?php echo base_url();?>resource/images/details/2_s.jpg" alt="" /></a>
                            <a class="example-image-link" href="<?php echo base_url();?>resource/images/details/3_b.jpg" data-lightbox="example-set" data-title="The next image in the set is preloaded as you're viewing."><img class="example-image" src="<?php echo base_url();?>resource/images/details/3_s.jpg" alt="" /></a>
                            <a class="example-image-link" href="<?php echo base_url();?>resource/images/details/4_b.jpg" data-lightbox="example-set" data-title="Click anywhere outside the image or the X to the right to close."><img class="example-image" src="<?php echo base_url();?>resource/images/details/4_s.jpg" alt="" /></a>
                            <a class="example-image-link" href="<?php echo base_url();?>resource/images/details/2_b.jpg" data-lightbox="example-set" data-title="Or press the right arrow on your keyboard."><img class="example-image" src="<?php echo base_url();?>resource/images/details/2_s.jpg" alt="" /></a>
                            <a class="example-image-link" href="<?php echo base_url();?>resource/images/details/3_b.jpg" data-lightbox="example-set" data-title="The next image in the set is preloaded as you're viewing."><img class="example-image" src="<?php echo base_url();?>resource/images/details/3_s.jpg" alt="" /></a>
                            <a class="example-image-link" href="<?php echo base_url();?>resource/images/details/4_b.jpg" data-lightbox="example-set" data-title="Click anywhere outside the image or the X to the right to close."><img class="example-image" src="<?php echo base_url();?>resource/images/details/4_s.jpg" alt="" /></a>
                            <a class="example-image-link" href="<?php echo base_url();?>resource/images/details/4_b.jpg" data-lightbox="example-set" data-title="Click anywhere outside the image or the X to the right to close."><img class="example-image" src="<?php echo base_url();?>resource/images/details/4_s.jpg" alt="" /></a>
                        </div>
                    </div>
                </div><!-- first div left end -->

                <div class="col-md-6">
                    <div class="details-middle">
                        <div class="col-md-10">
                            Car Price (FOB):
                            <select class="detail-price">
                                <option>USD</option>
                                <!--<option>AED</option>
                                <option>AUD</option>
                                <option>CAD</option>
                                <option>EUR</option>-->
                            </select>
                            <span>US$ <?php echo $row->price;?></span>
                        </div>
                    </div>

                    <div class="col-md-12 details-calculate"><!-- calculate div start -->
                        <h4>1. Calculate your Total Price</h4>
                        <p>Select your Destination</p>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-4 control-label">Country</label>
                                <div class="col-md-8">
                                    <select class="form-control">
                                        <option>Bangladesh</option>
                                        <option>India</option>
                                        <option>USA</option>
                                    </select><br/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-4 control-label">Nearest port</label>
                                <div class="col-md-8">
                                    <select class="form-control">
                                        <option>Chittagong</option>
                                        <option>Mongla</option>

                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="checkbox form-group">
                                <label class="col-md-12 control-label">
                                    <input type="checkbox" id="blankCheckbox" value="option1" aria-label="...">Insurance
                                </label>
                                <label class="col-md-12 control-label">
                                    <input type="checkbox" id="blankCheckbox" value="option1" aria-label="...">Inspection <i class="fa fa-question-circle"></i>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="details-calculation">
                                <i class="fa fa-calculator fa-2x"></i> <span>Calculate</span>
                            </div>
                        </div><!--calculate div end -->

                        <div class="col-md-12">
                            <h4>2. Contact Seller</h4>
                            <div class="checkbox form-group">
                                <label class="col-md-12 control-label">
                                    <input type="checkbox" id="blankCheckbox" value="option1" aria-label="...">I want to negotiate the best price
                                </label>
                                <label class="col-md-12 control-label">
                                    <input type="checkbox" id="blankCheckbox" value="option1" aria-label="...">I want to know the shipping schedule
                                </label>
                                <label class="col-md-12 control-label">
                                    <input type="checkbox" id="blankCheckbox" value="option1" aria-label="...">I want to know about the condition of the car
                                </label>
                            </div>
                            <textarea class="form-control details-textarea" rows="3">I want to negotiate the best price</textarea><br/>
                            <button type="button" class="btn btn-primary btn-lg btn-block"><i class="fa fa-envelope fa-2x"> Send</i></button><br/>
                            <label class="col-md-12 control-label">
                                <input type="checkbox" id="blankCheckbox" value="option1" aria-label="..."> I would like to receive information about special promotions from tradecarview.
                            </label>
                            <p>
                                If you do not have a login ID, please<a href="#"> register</a> for free first.
                            </p>
                            <p>
                                By clicking "Send" button, you agree to the following terms.
                            </p>
                            <p>
                                <a href="#"> >> tstjapan.co.jp Terms of Use  </a>
                            </p>
                            <p>
                                <a href="#"> >> Privacy Policy </a>
                            </p>
                        </div>
                    </div>

                </div><!-- second div left end -->

                <div class="col-md-12">
                    <div class="details-bottom">
                        <div class="details-bottom-heading">Specific information</div>
                        <table class="table table-bordered">
                            <tbody>
                            <tr>
                                <td>VIN(Vehicle Identification Number)/Serial No.</td>
                                <td style="color:red"><?php echo $row->vin;?> Full VIN/Serial No. will be shown on Proforma Invoice and Invoice</td>
                                <td>Exterior Color</td>
                                <td><?php echo $row->exterior_color;?></td>
                            </tr>
                            <tr>
                                <td>BodyStyle</td>
                                <td><?php echo $row->body_style;?></td>
                                <td>Interior Color</td>
                                <td><?php echo $row->interior_color;?></td>
                            </tr>
                            <tr>
                                <td>Reference No</td>
                                <td><?php echo $row->reference_no;?></td>
                                <td>Expiry Date</td>
                                <td><?php echo $row->expiry_date;?></td>
                            </tr>
                            <tr>
                                <td>Door</td>
                                <td><?php echo $row->door;?></td>
                                <td>Condition</td>
                                <td><?php echo $row->condition;?></td>
                            </tr>
                            <tr>

                                <td>Options</td>
                                <td><?php echo $row->options;?></td>
                                <td>Drive Type</td>
                                <td ><?php echo $row->drive_type;?></td>
                            </tr>
                            <tr>
                                <td>Dimension</td>
                                <td><?php echo $row->dimension;?></td>
                                <td>Number Of Passengers</td>
                                <td><?php echo $row->no_of_passenger;?></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="details-right">
                    <div class="details-right-heading">Item Info</div>
                    <p>
                        Registration Year / Month
                        <?php echo $row->manufacture_year;?>/<?php echo $row->manufacture_month;?>
                    </p>
                    <p>
                        Mileage<br/>
                        <?php echo $row->Mileage;?>
                    </p>
                    <p>
                        Displacement<br/>
                        <?php echo $row->desplacement;?>
                    </p>
                    <p>
                        Steering<br/>
                        <?php echo $row->steering;?>
                    </p>
                    <p>
                        Transmission<br/>
                        <?php echo $row->transmission;?>
                    </p>
                    <p>
                        Fuel<br/>
                        <?php echo $row->fuel;?>
                    </p>
                    <p>
                        Model code<br/>
                        <?php echo $row->model_name;?>
                    </p>

                    <div class="details-right-heading">Need Help?</div>
                    <p>
                        <i class="fa fa-pencil-square-o fa-lg"></i>
                        Prevent yourself from scams
                    </p>
                    <p>
                        <i class="fa fa-ship fa-lg"></i>
                        Shipping Estimator
                    </p>
                    <p>
                        <i class="fa fa-columns fa-lg"></i>
                       <a href="<?php echo base_url();?>en/how_to_buy/">How to order</a>
                    </p>

                    <div class="details-right-heading1">for importing cars from Japan?</div>
                    <p>
                        Get quote from local importers in your country<br/>
                        <a href="#">My Local Importershilp Select importers</a>
                    </p>

                    <div class="details-right-heading">2003 Toyota Mark II</div>
                    <p>
                        <a href="">Detailed specification</a>
                    </p>
                </div>
            </div>

            <div class="col-md-12">
                <div class="sellers-info">
                    <div class="sellers-info-head">Seller's Information</div>
                    <div class="col-md-12">
                        <div class="sellers-left">
                            <h3><a href="#">METEOR CO., LTD.</a></h3>
                            <p>Your favorites vehicles available!</p>
                            <p>
                                METEOR Co.,Ltd. is the very best quality used vehicles supplier in Japan. We supply the Japanese, Europian and American used vehicles to our customers. And vehicles which we supply is great reputation as a best quality and best prices. Please contact us and let me know your requests, we will supply your requested vehicles as soon as possible.
                                If you have any questions regarding Japanese vehicles, contact E-mail us.
                            </p>
                        </div>

                        <div class="col-md-6">
                            <p>
                                <b>Country</b><br/>
                                Japan
                            </p>
                            <p>
                                <b>Address</b><br/>
                                NIKKYO BUILDING, IKEBUKURO, 2-65-3, TOSHIMA-KU, TOKYO, 171-0014
                            </p>
                            <p>
                                <b>Contact Person</b><br/>
                                14 sales staff waiting for you to serve
                            </p>
                            <p>
                                <b>Operating Hours</b><br/>
                                -------
                            </p>
                            <p>
                                <b>Language</b><br/>
                                English
                            </p>
                            <p>
                                <b>Total Annual Sales Volume</b><br/>
                                -------
                            </p>
                        </div>
                        <div class="col-md-6">
                            <p>
                                <b>Year Established</b><br/>
                                1995
                            </p>
                            <p>
                                <b>Ownership type</b><br/>
                                Corporation/Limited Liability Company
                            </p>
                            <p>
                                <b>Payment Terms</b><br/>
                                T/T  L/C  Other(Paypal)
                            </p>
                            <p>
                                <b>Business Classification</b><br/>
                                Exporter
                            </p>
                            <p>
                                <b>No.of Total Employees</b><br/>
                                11 - 50 People
                            </p>
                            <p>
                                <b>Above</b><br/>
                                -------
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</section>

