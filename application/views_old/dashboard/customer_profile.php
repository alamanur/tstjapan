   <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <div class="row no-margin">
            <div class="focus highlight">
                <h2 class="title">Customer Profile</h2>
            </div>
            <div class="below"></div>
        </div>

        <div class="row no-margin general">
            <div class="pull-left">
                <h4>General</h4>
            </div>

            <div class="pull-right">
                <a href="http://semita.com.bd/property/en/dashboard_general_setting/?customer_id=1 " class="btn btn-success btn-sm custom_btn">Edit</a>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table preview full" cellpadding="0" cellspacing="0">
                <tbody>
                <tr class="odd">
                    <td class="category">Status:</td>
                    <td class="value">Active</td>
                </tr>
                <tr class="odd">
                    <td class="category">First Name:</td>
                    <td class="value">Tarek</td>
                </tr>
                <tr class="even">
                    <td class="category">Last Name:</td>
                    <td class="value">Raihan</td>
                </tr>
                <tr class="odd">
                    <td class="category">Postal Code/Zip:</td>
                    <td class="value">12341</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="below"></div>

        <div class="row no-margin general">
            <div class="pull-left">
                <h4>Contact Infromation</h4>
            </div>

            <div class="pull-right">
                <a href="http://semita.com.bd/property/en/dashboard_Contact_Information_Settings/?customer_id=1 " class="btn btn-success btn-sm custom_btn">Edit</a>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table preview full" cellpadding="0" cellspacing="0">
                <tbody>
                <tr class="odd">
                    <td class="category">Email Address:</td>
                    <td class="value">tarekraihan@yahoo.com</td>
                </tr>
                <tr class="even">
                    <td class="category">Home Phone:</td>
                    <td class="value">222-333-4444</td>
                </tr>
                <tr class="odd">
                    <td class="category">Mobile Phone:</td>
                    <td class="value">01911222952</td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="below"></div>

        <div class="row no-margin general">
            <div class="pull-left">
                <h4>Password</h4>
            </div>

            <div class="pull-right">
                <a href="http://semita.com.bd/property/en/dashboard_password_setting/?customer_id=1 " class="btn btn-success btn-sm custom_btn">Edit</a>
            </div>
        </div>
        <p class="squeeze">Change the password for this account by clicking <strong>'Edit'</strong> in the top right hand corner of this box.</p>
        <div class="below"></div><br/><br/>

    </div>
