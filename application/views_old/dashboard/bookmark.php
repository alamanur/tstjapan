<div class="col-md-8">
<div class="row no-margin general">
    <div class="pull-left">
        <h4>My Favorite</h4>
    </div>
</div>
<div class="row no-margin general">
    <ul id="inventory">
        <?php
        $customer_id=$this->session->userdata('fuser_id');
        $query=$this->select_model->select_bookmark_product($customer_id);

        foreach ($query->result() as $row)
        {
        ?>
        <li class="item" id="">
            <div class="photo"><a href="#"><img style="width:185px; height:125px;" src="<?php echo base_url();?>/resource/images/car/<?php echo $row->feature_image; ?>"></a></div>
            <div class="tools">
                <h4 class="price_db"><a href="#">$<?php echo $row->price; ?></a></h4>
                <div class="icons">

                    <div class="zoom"><a href="#"><span title="View this listing" class="glyphicon glyphicon-search" aria-hidden="true"></span></a></div>

                    <div class="bookmarked"><a id="removebookmark_84527" href="#"><span title="remove Bookmark" class="glyphicon glyphicon-star text_yellow" aria-hidden="true"></span></a></div>

                </div>
                <div class="clear"></div>
            </div>

            <div class="address"><a href="<?php echo base_url();?>en/car_details/<?php echo $row->product_id;?>"><?php echo $row->name.' '.$row->model_name.' '.$row->manufacture_year;?></a></div>

        </li>
        <?php
        }
        ?>
    </ul>
    <div class="clear"></div>
    <div class="below"></div>

    </div>
</div>