<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
    <div class="row no-margin">
        <div class="focus highlight">
            <h2 class="title">Customer Profile</h2>
        </div>
        <div class="below"></div>
    </div>

    <div class="row no-margin general">
        <div class="pull-left">
            <h4>Change Password</h4>
        </div>
    </div>

    <form class="form-horizontal" id="changePasswordForm">
        <div class="feedback_message"></div>
        <div class="form-group">
            <label for="txtCurrentPassword" class="col-sm-4 control-label">Current Password</label>
            <div class="col-sm-8">
                <input type="password" name="txtCurrentPassword" class="form-control" id="txtCurrentPassword" placeholder="Current Password">
            </div>
        </div>
        <div class="form-group">
            <label for="txtNewPassword" class="col-sm-4 control-label">New Password</label>
            <div class="col-sm-8">
                <input type="password" name="txtNewPassword" class="form-control" id="txtNewPassword" placeholder="New Password">
            </div>
        </div>

        <div class="form-group">
            <label for="txtConfirmPassword" class="col-sm-4 control-label">Confirm Password</label>
            <div class="col-sm-8">
                <input type="password" name="txtConfirmPassword" class="form-control" id="txtConfirmPassword" placeholder="Confirm Password">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
                <button type="button" id="btnChangePassword" class="btn btn-primary">Change</button>
            </div>
        </div>
    </form>

</div>
