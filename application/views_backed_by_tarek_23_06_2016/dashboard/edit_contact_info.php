<?php
if($this->session->userdata('fuser_id'))
{
    $id=$this->session->userdata('fuser_id');
    $table='tst_user';
    $id_field='user_id';
    $row=$this->select_model->Select_Single_Row($id,$table,$id_field);
    /*print_r($row);
        die;*/
}else{
    $row['user_id']='';
    $row['email_address']='';
    $row['address']='';
    $row['mobile']='';
    $row['home_phone']='';
    $row['postal_code']='';
}
?>

<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
    <div class="row no-margin">
        <div class="focus highlight">
            <h2 class="title">Customer Profile</h2>
        </div>
        <div class="below"></div>
    </div>

    <div class="row no-margin general">
        <div class="pull-left">
            <h4>Edit Contact Information</h4>
        </div>
    </div>
    <form class="form-horizontal" id="edtiContactInformationForm">
        <div class="feedback_message"></div>
        <div class="form-group">
            <label for="inputEmail3" class="col-sm-4 control-label">Email</label>
            <div class="col-sm-8">
                <input type="hidden"  name="customer_id" value="<?php echo $row['user_id']; ?>">
                <input type="email" class="form-control" id="inputEmail3" name="customer_email" value="<?php echo $row['email_address']; ?>" placeholder="Email" maxlength="75" readonly>
            </div>
        </div>
        <div class="form-group">
            <label for="mobileNo" class="col-sm-4 control-label">Mobile No</label>
            <div class="col-sm-8">
                <input type="tel" class="form-control" id="mobileNo" name="mobile_no" value="<?php echo $row['mobile']; ?>" placeholder="Mobile no" maxlength="15">
            </div>
        </div>

        <div class="form-group">
            <label for="homePhone" class="col-sm-4 control-label">Home Phone</label>
            <div class="col-sm-8">
                <input type="tel" class="form-control" id="homePhone" name="home_phone" value="<?php echo $row['home_phone']; ?>" placeholder="Home phone" maxlength="15">
            </div>
        </div>

        <div class="form-group">
            <label for="address" class="col-sm-4 control-label">Home Phone</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="address" name="address" value="<?php echo $row['address']; ?>" placeholder=" Address" maxlength="150">
            </div>
        </div>

        <div class="form-group">
            <label for="postal_code" class="col-sm-4 control-label">Postal Code/Zip</label>
            <div class="col-sm-8">
                <input type="text" class="form-control" id="postal_code" name="postal_code" value="<?php echo $row['postal_code']; ?>" placeholder=" Postal code/zip" maxlength="6" minlength="4">
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-4 col-sm-8">
                <button type="button" id="btnContactInfo" class="btn btn-primary">Update</button>
            </div>
        </div>
    </form>
</div>