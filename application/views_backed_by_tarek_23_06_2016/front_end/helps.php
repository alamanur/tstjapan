
<div class="col-md-9">
    <div class="faq-help">
        <p>Frequently Asked Question</p>
        <div class="basic-help-child">
            <ul type="none">
                <a href="<?php echo base_url();?>en/help_details/"><li><i class="fa fa-share"></i> How can I be sure TST Japan dealers are legitimate?</li></a>
                <a href="#"><li><i class="fa fa-share"></i> Does the stated price include shipping charge?</li></a>
                <a href="#"><li><i class="fa fa-share"></i> Who do I pay to and how do I transfer the money?</li></a>
                <a href="#"><li><i class="fa fa-share"></i> How much is shipping charge to my port of destination?</li></a>
                <a href="#"><li><i class="fa fa-share"></i> I forgot my login ID</li></a>
                <a href="#"><li><i class="fa fa-share"></i> How do I edit/delete my profile information</li></a>
            </ul>
        </div>
    </div>
    <div class="basic-help">
        <p>About TST Japan</p>
        <div class="basic-help-child">
            <ul type="none">
                <a href="#"><li><i class="fa fa-share"></i> What is TST Japan?</li></a>
                <a href="#"><li><i class="fa fa-share"></i> Does TST Japan sell cars?</li></a>
                <a href="#"><li><i class="fa fa-share"></i> Who do I pay to and how do I transfer the money?</li></a>
            </ul>
        </div>
    </div><br/>
    <div class="basic-help">
        <p>Legitimacy of the Seller</p>
        <div class="basic-help-child">
            <ul type="none">
                <a href="#"><li><i class="fa fa-share"></i> Are all Sellers on TST Japan legitimate Seller?</li></a>
                <a href="#"><li><i class="fa fa-share"></i> Who are TST Japan dealers?</li></a>
                <a href="#"><li><i class="fa fa-share"></i> How can I be sure TST Japan dealers are legitimate?</li></a>
                <a href="#"><li><i class="fa fa-share"></i> Fraud/Scams Awareness</li></a>
                <a href="#"><li><i class="fa fa-share"></i> Fraud Cases</li></a>
                <a href="#"><li><i class="fa fa-share"></i> Report Fraud/Scams</li></a>
                <a href="#"><li><i class="fa fa-share"></i> Blacklisted Companies</li></a>
            </ul>
        </div>
    </div><br/>
    <div class="basic-help">
        <p>Buying</p>
        <div class="basic-help-child">
            <ul type="none">
                <a href="#"><li><i class="fa fa-share"></i> Outline</li></a>
                <a href="#"><li><i class="fa fa-share"></i> How to find car</li></a>
                <a href="#"><li><i class="fa fa-share"></i> Contacting Seller/Get quote</li></a>
                <a href="#"><li><i class="fa fa-share"></i> Does the stated price include shipping charge?</li></a>
                <a href="#"><li><i class="fa fa-share"></i> How much is shipping charge to my port of destination?</li></a>
                <a href="#"><li><i class="fa fa-share"></i> I want to know the condition of this car</li></a>
                <a href="#"><li><i class="fa fa-share"></i> Negotiation</li></a>
                <a href="#"><li><i class="fa fa-share"></i> Receiving Proforma Invoice</li></a>
                <a href="#"><li><i class="fa fa-share"></i> Place Your Order</li></a>
                <a href="#"><li><i class="fa fa-share"></i> Payment Notification</li></a>
                <a href="#"><li><i class="fa fa-share"></i> I visited the website but could not find any car of my desire</li></a>
                <a href="#"><li><i class="fa fa-share"></i> Who do I pay to and how do I transfer the money?</li></a>
                <a href="#"><li><i class="fa fa-share"></i> Transaction Status</li></a>
                <a href="#"><li><i class="fa fa-share"></i> Translation of the message</li></a>
            </ul>
        </div>
    </div><br/>
    <div class="basic-help">
        <p>Membership & account</p>
        <div class="basic-help-child">
            <ul type="none">
                <a href="#"><li><i class="fa fa-share"></i> How to register</li></a>
                <a href="#"><li><i class="fa fa-share"></i> I forgot my login ID</li></a>
                <a href="#"><li><i class="fa fa-share"></i> I forgot the password</li></a>
                <a href="#"><li><i class="fa fa-share"></i> I want to withdraw membership</li></a>
                <a href="#"><li><i class="fa fa-share"></i> I cannot log in or log out</li></a>
                <a href="#"><li><i class="fa fa-share"></i> I do not want to receive newsletter</li></a>
                <a href="#"><li><i class="fa fa-share"></i> Terms of Member's ID Agreement</li></a>
                <a href="#"><li><i class="fa fa-share"></i> How do I edit/delete my profile information</li></a>
            </ul>
        </div>
    </div><br/>
    <div class="basic-help">
        <p>Common trade terms</p>
        <div class="basic-help-child">
            <ul type="none">
                <a href="#"><li><i class="fa fa-share"></i> FOB（Free on board)</li></a>
                <a href="#"><li><i class="fa fa-share"></i> CIF (Cost Insurance and Freight)</li></a>
                <a href="#"><li><i class="fa fa-share"></i> C&F (Cost and Freight)</li></a>
                <a href="#"><li><i class="fa fa-share"></i> Bill of lading</li></a>
                <a href="#"><li><i class="fa fa-share"></i> Invoice</li></a>
                <a href="#"><li><i class="fa fa-share"></i> Proforma Invoice</li></a>
            </ul>
        </div>
    </div><br/>
    <div class="basic-help">
        <p>Tool Guide</p>
        <div class="basic-help-child">
            <ul type="none">
                <a href="#"><li><i class="fa fa-share"></i> Shipping Estimator</li></a>
                <a href="#"><li><i class="fa fa-share"></i> Car Ranking</li></a>
                <a href="#"><li><i class="fa fa-share"></i> Exchange Rate</li></a>
            </ul>
        </div>
    </div><br/>
    <div class="basic-help">
        <p>Import Regulation</p>
        <div class="basic-help-child">
            <ul type="none">
                <a href="#"><li><i class="fa fa-share"></i> Import Regulation</li></a>
            </ul>
        </div>
    </div><br/>
    <div class="basic-help">
        <p>Terms of use</p>
        <div class="basic-help-child">
            <ul type="none">
                <a href="#"><li><i class="fa fa-share"></i> Terms of use</li></a>
            </ul>
        </div>
    </div><br/>
    <div class="basic-help">
        <p>Privacy Policy</p>
        <div class="basic-help-child">
            <ul type="none">
                <a href="#"><li><i class="fa fa-share"></i> Privacy Policy</li></a>
            </ul>
        </div>
    </div><br/>
</div>
