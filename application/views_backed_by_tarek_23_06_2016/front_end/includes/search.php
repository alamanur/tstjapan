<section id="search-form"><!-- search-form section start-->
    <div class="col-md-12">
        <div class="row">
                <div class="search-form-body">
                    <form class="form-horizontal" method="get" d>
                        <h2 class="search-label">Search</h2>
                        <div class="form-group">
                            <label class="search-label">Make</label>
                            <div class="col-md-12">
                                <select name="txtMakeId" id="txtMakeId" class="form-control">
                                    <?php
                                    echo $this->select_model->Select_box($table='tbl_make');
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="search-label">Model</label>
                            <div class="col-md-12">
                                <select name="txtModel" id="txtModel" class="form-control">
                                    <option value="">Select One</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="search-label">FOB Price Range</label><br/>
                            <div class="col-md-6">
                                <select class="form-control" name="min_price">
                                    <option value="0">Any</option>
                                    <option value="1000">US $ 1,000</option>
                                    <option value="2000">US $ 2,000</option>
                                    <option value="5000">US $ 5,000</option>
                                    <option value="10000">US $ 10,000</option>
                                    <option value="20000">US $ 20,000</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <select class="form-control" name="max_price">
                                    <option value="0">Any</option>
                                    <option value="1000">US $ 1,000</option>
                                    <option value="2000">US $ 2,000</option>
                                    <option value="5000">US $ 5,000</option>
                                    <option value="10000">US $ 10,000</option>
                                    <option value="20000">US $ 20,000</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="search-label">Year/Month</label><br/>
                            <div class="col-sm-6">
                                <select name="manufactureYear" class="form-control" >
                                    <option value="0">Any</option>
                                    <?php
                                    $y= date("Y")+1;
                                    for ($i = 0;$i < 50;$i++)
                                    {
                                        $y -=1;
                                        echo "<option value='$y'>$y</option>";
                                    }
                                    ?>

                                </select>
                            </div>
                            <div class="col-sm-6">
                                <select name="manufactureMonth" class="form-control" >
                                    <option value="0">Any</option>
                                    <option value="January">January</option>
                                    <option value="February">February</option>
                                    <option value="March">March</option>
                                    <option value="April">April</option>
                                    <option value="May">May</option>
                                    <option value="June">June</option>
                                    <option value="July">July</option>
                                    <option value="August">August</option>
                                    <option value="September">September</option>
                                    <option value="October">October</option>
                                    <option value="November">November</option>
                                    <option value="December">December</option>
                                </select><br />
                            </div>
                            <div class="col-sm-6">
                                <select name="manufactureYear" class="form-control" >
                                    <option value="0">Any</option>
                                    <?php
                                    $y= date("Y")+1;
                                    for ($i = 0;$i < 50;$i++)
                                    {
                                        $y -=1;
                                        echo "<option value='$y'>$y</option>";
                                    }
                                    ?>

                                </select>
                            </div>
                            <div class="col-sm-6">
                                <select name="manufactureMonth" class="form-control" >
                                    <option value="0">Any</option>
                                    <option value="January">January</option>
                                    <option value="February">February</option>
                                    <option value="March">March</option>
                                    <option value="April">April</option>
                                    <option value="May">May</option>
                                    <option value="June">June</option>
                                    <option value="July">July</option>
                                    <option value="August">August</option>
                                    <option value="September">September</option>
                                    <option value="October">October</option>
                                    <option value="November">November</option>
                                    <option value="December">December</option>
                                </select><br />
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="search-label">Displacement</label><br/>
                            <div class="col-sm-6">
                                <select name="dp" class="form-control">
                                    <option value="0">Any</option>
                                    <option value="550">550</option>
                                    <option value="660">660</option>
                                    <option value="800">800</option>
                                    <option value="1000">1000</option>
                                    <option value="1100">1100(1.1L)</option>
                                    <option value="1200">1200(1.2L)</option>
                                    <option value="1300">1300(1.3L)</option>
                                    <option value="1400">1400(1.4L)</option>
                                    <option value="1500">1500(1.5L)</option>
                                    <option value="1600">1600(1.6L)</option>
                                    <option value="1700">1700(1.7L)</option>
                                    <option value="1800">1800(1.8L)</option>
                                    <option value="1900">1900(1.9L)</option>
                                    <option value="2000">2000(2.0L)</option>
                                    <option value="2100">2100(2.1L)</option>
                                    <option value="2200">2200(2.2L)</option>
                                    <option value="2300">2300(2.3L)</option>
                                    <option value="2400">2400(2.4L)</option>
                                    <option value="2500">2500(2.5L)</option>
                                    <option value="2600">2600(2.6L)</option>
                                    <option value="2700">2700(2.7L)</option>
                                    <option value="2800">2800(2.8L)</option>
                                    <option value="2900">2900(2.9L)</option>
                                    <option value="3000">3000(3.0L)</option>
                                    <option value="3500">3500(3.5L)</option>
                                    <option value="4000">4000(4.0L)</option>
                                    <option value="4500">4500(4.5L)</option>
                                    <option value="5000">5000(3.5L)</option>
                                    <option value="5500">5500(5.5L)</option>
                                    <option value="6000">6000(6.0L)</option>
                                </select>
                            </div>

                            <div class="col-sm-6">
                                <select name="dp" class="form-control">
                                    <option value="0">Any</option>
                                    <option value="550">550</option>
                                    <option value="660">660</option>
                                    <option value="800">800</option>
                                    <option value="1000">1000</option>
                                    <option value="1100">1100(1.1L)</option>
                                    <option value="1200">1200(1.2L)</option>
                                    <option value="1300">1300(1.3L)</option>
                                    <option value="1400">1400(1.4L)</option>
                                    <option value="1500">1500(1.5L)</option>
                                    <option value="1600">1600(1.6L)</option>
                                    <option value="1700">1700(1.7L)</option>
                                    <option value="1800">1800(1.8L)</option>
                                    <option value="1900">1900(1.9L)</option>
                                    <option value="2000">2000(2.0L)</option>
                                    <option value="2100">2100(2.1L)</option>
                                    <option value="2200">2200(2.2L)</option>
                                    <option value="2300">2300(2.3L)</option>
                                    <option value="2400">2400(2.4L)</option>
                                    <option value="2500">2500(2.5L)</option>
                                    <option value="2600">2600(2.6L)</option>
                                    <option value="2700">2700(2.7L)</option>
                                    <option value="2800">2800(2.8L)</option>
                                    <option value="2900">2900(2.9L)</option>
                                    <option value="3000">3000(3.0L)</option>
                                    <option value="3500">3500(3.5L)</option>
                                    <option value="4000">4000(4.0L)</option>
                                    <option value="4500">4500(4.5L)</option>
                                    <option value="5000">5000(3.5L)</option>
                                    <option value="5500">5500(5.5L)</option>
                                    <option value="6000">6000(6.0L)</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group search_checkbox">
                            <label class="col-md-12 control-label search-label">
                                <input type="checkbox" id="blankCheckbox" value="option1" aria-label="...">Left Hand Drive
                            </label>
                            <label class="col-md-12 control-label search-label">
                                <input type="checkbox" id="blankCheckbox" value="option1" aria-label="...">Right Hand Drive
                            </label>
                            <label class="col-md-12 control-label search-label">
                                <input type="checkbox" id="blankCheckbox" value="option1" aria-label="...">Special Price
                            </label>
                            <button class="btn btn-primary search-label" type="submit">Reset</button>
                        </div>

                        <!-- Advance Search -->
                        <div id="toggleText" style="display: none" class="form-group">
                            <div class="form-group">
                                <label class="col-md-12 control-label"> Body Style</label>
                                <div class="col-md-12">
                                    <select name="txtCategory" id="txtCategory" class="form-control">
                                        <?php
                                        echo $this->select_model->Select_car_category($table='tbl_category');
                                        ?>
                                    </select>
                                </div>

                            </div>

                            <div class="form-group">

                                <div class="col-md-12">
                                    <?php
                                    echo form_label('Transmission', 'txtTransmission');
                                    $options = array(
                                        'Automatic'  => 'Automatic',
                                        'Manual'    => 'Manual',
                                        'Automanual'   => 'Automanual',
                                        'Unspecified' => 'Unspecified',
                                        'CVT' => 'CVT',
                                    );
                                    $class = 'class = form-control';
                                    echo form_dropdown('txtTransmission', $options, 'Automatic',$class);
                                    ?>
                                </div>
                            </div>

                            <div  class="form-group">
                                <label for="inputEmail3" class="col-sm-12 col-md-12 control-label">Fuel</label>
                                <div class="col-md-12">
                                    <?php
                                    $options = array(
                                        'Any'  => 'Any',
                                        'Biodiesel'    => 'Biodiesel',
                                        'CNG'   => 'CNG',
                                        'Diesel'   => 'Diesel',
                                        'Electric'   => 'Electric',
                                        'Ethanol-FFV'   => 'Ethanol-FFV',
                                        'Gasoline/Petrol'   => 'Gasoline/Petrol',
                                        'Hybrid-electric'   => 'Hybrid-electric',
                                        'LPG'   => 'LPG',
                                        'Steam'   => 'Steam',
                                        'Other'   => 'Other',
                                    );
                                    $class = 'class = form-control';
                                    echo form_dropdown('txtFuel', $options, 'Gasoline/Petrol',$class);
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="inputEmail3" class="col-sm-12 col-md-12 control-label">Door</label>
                                <div class="col-md-12">
                                    <select class="form-control">
                                        <option>01</option>
                                        <option>02</option>
                                        <option>03</option>
                                        <option>04</option>
                                        <option>05</option>
                                    </select>
                                </div>
                            </div>
                            <div  class="form-group">
                                <label for="inputEmail3" class="col-sm-12 col-md-12 control-label">Color</label>
                                <div class="col-md-12">
                                    <?php
                                    $options = array(
                                        'Any'  => 'Any',
                                        'Beige'    => 'Beige',
                                        'Black'   => 'Black',
                                        'Blue'   => 'Blue',
                                        'Bronze'   => 'Bronze',
                                        'Brown'   => 'Brown',
                                        'Burgundy'   => 'Burgundy',
                                        'Champagne'   => 'Champagne',
                                        'Charcoal'   => 'Charcoal',
                                        'Cream'   => 'Cream',
                                        'Dark Blue'   => 'Dark Blue',
                                        'Gold'   => 'Gold',
                                        'Gray'   => 'Gray',
                                        'Green'   => 'Green',
                                        'Maroon'   => 'Maroon',
                                        'Off White'   => 'Off White',
                                        'Orange'   => 'Orange',
                                        'Other'   => 'Other',
                                        'Pearl'   => 'Pearl',
                                        'Pewter'   => 'Pewter',
                                        'Pink'   => 'Pink',
                                        'Purple'   => 'Purple',
                                        'Red'   => 'Red',
                                        'Silver'   => 'Silver',
                                        'Tan'   => 'Tan',
                                        'Red'   => 'Red',
                                        'Teal'   => 'Teal',
                                        'Titanium'   => 'Titanium',
                                        'Turquoise'   => 'Turquoise',
                                        'White'   => 'White',
                                        'Yellow'   => 'Yellow',
                                    );
                                    $class = 'class = form-control';
                                    echo form_dropdown('txtColor', $options, 'Any',$class);
                                    ?>
                                </div>
                            </div>
                            <div  class="form-group">
                                <label for="inputEmail3" class="col-sm-12 col-md-12 control-label">Drive</label>
                                <div class="col-md-12">
                                    <?php

                                    $options = array(
                                        '1'  => '2 While Drive',
                                        '2'    => '4 While Drive',
                                        '3'   => 'All While Drive',
                                    );
                                    $class = 'class = form-control';
                                    echo form_dropdown('txtOptions', $options, '1',$class);
                                    ?>
                                </div>
                            </div>

                            <div class=" form-group search-label">

                                <label class="col-sm-12 col-md-12 control-label">Condition</label>
                                <label class="col-sm-12 col-md-12 control-label">
                                    <input type="checkbox" id="blankCheckbox" value="option1" aria-label="...">Used Stocks
                                </label>
                                <label class="col-sm-12 col-md-12 control-label">
                                    <input type="checkbox" id="blankCheckbox" value="option1" aria-label="...">New Stocks
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="result-show">
                                    Show Results
                                    <i class="fa fa-angle-double-right"></i>
                                    <p>Over 300</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12 col-sm-12 btn-advance-search"><a id="displayText" href="javascript:toggle();">Advance Search</a></div>
                    </form>
                </div>
        </div>
    </div>
</section>