<div id='cssmenu'>
    <ul>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/honda-logo.png"><span> Honda</span></a>
            <ul>
                <?php
                    $query = $this->select_model->Select_model_by_id('4');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>

            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/bmw_logo.png"><span> BMW</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('1');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/mazda_logo_2.png"><span> Mazda</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('13');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/Mitsubishi_Motors.png"><span> Mitsubishi Motors</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('5');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/Mercedes.png"><span> Mercedes</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('14');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/Nissan_logo.png"><span> Nissan</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('3');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/toyota-emblem.png"><span> Toyota</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('2');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/Volkswagen_Logo.png"><span> Volkswagen</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('11');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/subaru.png"><span> Subaru</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('12');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/suzuki.png"><span> Suzuki</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('10');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/land-rover.png"><span> Land Rover</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('9');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/isuzu.png"><span> Isuzu</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('16');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/audi.png"><span> Audi</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('8');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/ford.png"><span> Ford</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('7');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/daihatsu.png"><span> Daihatsu</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('15');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/lexus.png"><span> Lexus</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('6');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
    </ul>
</div>
