<section id="signup">
    <div class="container">
        <div class="col-md-6 signup-left">
            <div class="text-center">
                <h3>Registration is 100% free, no credit card required, no hidden fees.</h3>
                <h1>Sign Up</h1>
                <p>Get started with TST JAPAN</p>
                <div class="err_msg"></div>
                <div class="box-content"  >
                    <?php
                    //-----Display Success or Error message---
                    if(isset($feedback)){
                        echo $feedback;
                    }
                    //----Form Tag Start-------------
                    $attributes = array('class' => 'email', 'id' => 'firstSignUpForm');

                    echo form_open('', $attributes);
                    ?>
                </div>
                <div class="form-group">
                    <?php
                    $attributes=array(
                        'name'=>'fsemail',
                        'class'=>'form-control',
                        'id'=>'fsemail',
                        'placeholder'=>'Write Email Address',
                        'maxlength'   => '70',
                        'value' => set_value('txtEmailAddress')
                    );
                    echo form_input($attributes);
                    ?>
                </div>
<!--                <div class="form-group">
                    <label class="red"><?php //echo form_error('txtEmailAddress');?></label>
                </div>-->
                <div class="form-group">
                    <?php
                    $attributes=array(
                        'name'=>'fscemail',
                        'class'=>'form-control',
                        'id'=>'fscemail',
                        'placeholder'=>'Confirm Email Address',
                        'maxlength'   => '70',
                        'value' => set_value('txtConfirmEmailAddress')
                    );
                    echo form_input($attributes);
                    ?>
                </div>
<!--                <div class="form-group">
                    <label class="red"><?php //echo form_error('txtConfirmEmailAddress');?></label>
                </div>-->
                <?php
                $attribute=array(
                    'name'=>'btnSubmit',
                    'class'=>'btn btn-primary btn-lg ',
                    'value'=>'Sign Up',
                    'id'=>'firstSignUp'
                );
                echo form_submit($attribute);//--Form Submit Button

                echo form_close();//--Form closing tag </form>
                ?>

                <p>Already have an account? <a href="<?php echo base_url();?>en/start/">Log in</a></p>
            </div>
        </div>
        <div class="col-md-6 te">
            <div class="text-center">
                <img src="<?php echo base_url(); ?>resource/images/logo.png" />
                <p><b>TST JAPAN Co. Ltd., is Japan's largest online used car marketplace</b></p>
            </div>
            <img class="singup-side-img" src="<?php echo base_url(); ?>resource/images/img_tcvservice.png" />
        </div>
    </div>
</section>