<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <h2>Create Product</h2>

            </div>
        </div>
        <!-- /. ROW  -->
        <hr/>
        <div class="row">
            <div class="col-md-12">
                <?php
                //-----Display Success or Error message---
                if(isset($feedback)){
                    echo $feedback;
                }
                ?>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-6 ">
                <div class="box-content"  >
                    <?php

                    //----Form Tag Start-------------
                    $attributes = array('class' => 'email', 'id' => 'myform');
                    echo form_open_multipart('backdoor/create_product');
                    ?>
                </div>

                <div class="form-group">
                    <label>Manufacturar Name</label>
                    <select name="txtMakeId" id="txtMakeId" class="form-control">
                        <?php
                        echo $this->select_model->Select_box($table='tbl_make');
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtMakeId');?></label>

                </div>

                <div class="form-group">
                    <label>Engine Type</label>
                    <select name="txtEngineType" id="txtEngineType" class="form-control">
                        <option value="">Select One</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtEngineType');?></label>
                </div>
                <div class="form-group">
                    <label>Vehicle Category</label>
                    <select name="txtCategory" id="txtCategory" class="form-control">
                        <?php
                        echo $this->select_model->Select_car_category($table='tbl_category');
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtCategory');?></label>
                </div>

                <div class="form-group">
                    <label>Seller</label>
                    <select name="txtSeller" id="txtSeller" class="form-control">
                        <?php
                        echo $this->select_model->Select_car_seller($table='tbl_company_information');
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtSeller');?></label>
                </div>


                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-6">
                            <label for="manufactureYear">Manufacture Year</label>
                            <select name="manufactureYear" class="form-control" >
                                <?php
                                $y= date("Y")+1;
                                for ($i = 0;$i < 50;$i++)
                                {
                                    $y -=1;
                                    echo "<option value='$y'>$y</option>";
                                }
                                ?>

                            </select>
                        </div>
                        <div class="col-xs-6">
                            <label for="manufactureMonth">Manufacture Month</label>
                            <select name="manufactureMonth" class="form-control" >
                                <option value="January">January</option>
                                <option value="February">February</option>
                                <option value="March">March</option>
                                <option value="April">April</option>
                                <option value="May">May</option>
                                <option value="June">June</option>
                                <option value="July">July</option>
                                <option value="August">August</option>
                                <option value="September">September</option>
                                <option value="October">October</option>
                                <option value="November">November</option>
                                <option value="December">December</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('manufactureMonth');?></label>
                </div>

                <div class="form-group">
                    <label>Price</label>
                    <?php
                    $attributes=array(
                    'name'=>'txtPrice',
                    'class'=>'form-control',
                    'maxlength'   => '12',
                    'placeholder'=>'EX : 1000 / 2500',
                    'value' => set_value('txtPrice'),
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtPrice');?></label>
                </div>

                <div class="form-group">

                    <label>Displacement</label>
                    <?php
                    $attributes=array(
                        'name'=>'txtDisplacement',
                        'class'=>'form-control',
                        'maxlength'   => '50',
                        'placeholder'=>'Ex :  2000 cc / 2500 cc',
                        'value' => set_value('txtDisplacement'),
                    );
                    echo form_input($attributes);
                    ?>
                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtDisplacement');?></label>
                </div>

                <div class="form-group">
                    <label>Steering</label>
                    <?php
                    $attributes=array(
                        'name'=>'txtSteering',
                        'class'=>'form-control',
                        'maxlength'   => '50',
                        'placeholder'=>'Ex : Right / Left',
                        'value' => set_value('txtSteering'),
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtSteering');?></label>
                </div>

                <div class="form-group">
                    <label>Condition</label>
                    <?php
                    $attributes=array(
                        'name'=>'txtCondition',
                        'class'=>'form-control',
                        'maxlength'   => '10',
                        'placeholder'=>'Ex : Used/ New',
                        'value' => set_value('txtCondition'),
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtCondition');?></label>
                </div>

                <div class="form-group">
                    <label>Made in</label>
                    <?php
                    $attributes=array(
                        'name'=>'txtMadeIn',
                        'class'=>'form-control',
                        'maxlength'   => '25',
                        'placeholder'=>'Ex : Japan / China',
                        'value' => set_value('txtMadeIn'),
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtMadeIn');?></label>
                </div>

                <div class="form-group">
                    <label>Fuel</label>
                    <?php
                    echo form_label('Fuel', 'txtFuel');
                    $options = array(
                        'Any'  => 'Any',
                        'Biodiesel'    => 'Biodiesel',
                        'CNG'   => 'CNG',
                        'Diesel'   => 'Diesel',
                        'Electric'   => 'Electric',
                        'Ethanol-FFV'   => 'Ethanol-FFV',
                        'Gasoline/Petrol'   => 'Gasoline/Petrol',
                        'Hybrid-electric'   => 'Hybrid-electric',
                        'LPG'   => 'LPG',
                        'CNG'   => 'CNG',
                        'Steam'   => 'Steam',
                        'Other'   => 'Other',
                    );
                    $class = 'class = form-control';
                    echo form_dropdown('txtFuel', $options, 'Any',$class);
                    ?>
                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtFuel');?></label>
                </div>

                <div class="form-group">
                    <?php
                    echo form_label('Mileage', 'txtMileage');

                    $attributes=array(
                        'name'=>'txtMileage',
                        'class'=>'form-control',
                        'maxlength'   => '25',
                        'placeholder'=>'Ex : 38,000 km ',
                        'value' => set_value('txtMileage'),
                    );
                    echo form_input($attributes);
                    ?>

                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtMileage');?></label>
                </div>


                <div class="form-group">

                    <?php
                    echo form_label('Options', 'txtOptions');
                    $attributes=array(
                        'name'=>'txtOptions',
                        'class'=>'form-control',
                        'placeholder'=>'Ex :Anti-Lock Brakes / Driver Airbag / Passenger Airbag ',
                        'value' => set_value('txtOptions'),
                    );
                    echo form_input($attributes);

                    ?>
                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtOptions');?></label>
                </div>


                <div class="checkbox">
                    <label><input name="txtIsAvailable" type="checkbox"> IsAvailable</label>
                </div>

                <div class="form-group">

                    <?php
                    echo form_label('Vehicle Image', 'txtImages');
                    $attributes=array(
                        'name'=>'txtImages',
                        'class'=>'form-control',
                        'maxlength'   => '50',
                        //'multiple'=>true,
                        'placeholder'=>'Write Options',
                        'value' => set_value('txtImages'),
                    );
                    echo form_upload($attributes);
                    ?>
                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtImages');?></label>
                </div>
            </div>


            <div class="col-md-6 col-sm-12 col-xs-6 ">


                <div class="form-group">
                    <label>Model Name</label>
                    <select name="txtModel" id="txtModelId" class="form-control">
                        <option value="">Select One</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtModel');?></label>
                </div>

                <div class="form-group">
                    <label>Body Style</label>
                    <select name="txtBodyStyle" id="txtBodyStyle" class="form-control">
                        <option value="">Select One</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtBodyStyle');?></label>
                </div>

                <div class="form-group">
                    <label>Transmission</label>
                    <select name="txtTransmission" id="txtTransmission" class="form-control">
                        <option value="">Select One</option>
                    </select>

                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtTransmission');?></label>
                </div>

                <div class="form-group">
                    <label>Doors</label>
                    <?php
                    $attributes=array(
                        'name'=>'txtDoor',
                        'class'=>'form-control',
                        'maxlength'   => '5',
                        'placeholder'=>'Number of doors Ex : 4 / 5',
                        'value' => set_value('txtDoor'),
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtDoor');?></label>
                </div>

                <div class="form-group">
                    <?php
                    echo form_label('Number of passenger', 'txtPassenger');
                    $attributes=array(
                        'name'=>'txtPassenger',
                        'class'=>'form-control',
                        'maxlength'   => '5',
                        'placeholder'=>'Number of passenger Ex : 4 / 5 ',
                        'value' => set_value('txtPassenger'),
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <?php
                    echo form_label('VIN (Vehicle Identification Number)/Serial No)', 'txtVin');
                    $attributes=array(
                        'name'=>'txtVin',
                        'class'=>'form-control',
                        'maxlength'   => '25',
                        'placeholder'=>' Ex : WAUZZZ8E03A169*** ',
                        'value' => set_value('txtVin'),
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtVin');?></label>
                </div>
                <div class="form-group">
                    <?php
                    echo form_label('Height (CM)', 'txtHeight');
                    $attributes=array(
                        'name'=>'txtHeight',
                        'class'=>'form-control',
                        'maxlength'   => '10',
                        'placeholder'=>'Ex : 144',
                        'value' => set_value('txtHeight'),
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtHeight');?></label>
                </div>
                <div class="form-group">
                    <?php
                    echo form_label('Width (cm)', 'txtWidth');
                    $attributes=array(
                        'name'=>'txtWidth',
                        'class'=>'form-control',
                        'maxlength'   => '10',
                        'placeholder'=>'Ex : 177',
                        'value' => set_value('txtWidth'),
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtWidth');?></label>
                </div>

                <div class="form-group">
                    <?php
                    echo form_label('Length (cm)', 'txtLength');
                    $attributes=array(
                        'name'=>'txtLength',
                        'class'=>'form-control',
                        'maxlength'   => '10',
                        'placeholder'=>'Ex : 550',
                        'value' => set_value('txtLength'),
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtLength');?></label>
                </div>

                <div class="form-group">
                    <?php
                    echo form_label('Price Per Cubic Meter (Dollar)', 'txtPricePerCubicMeter');
                    $attributes=array(
                        'name'=>'txtPricePerCubicMeter',
                        'class'=>'form-control',
                        'maxlength'   => '10',
                        'placeholder'=>'Ex : 10',
                        'value' => set_value('txtPricePerCubicMeter'),
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtPricePerCubicMeter');?></label>
                </div>

                <div class="form-group">
                    <?php
                    echo form_label('Exterior Color', 'txtExterior');

                        $options = array(
                            'Any'  => 'Any',
                            'Beige'    => 'Beige',
                            'Black'   => 'Black',
                            'Blue'   => 'Blue',
                            'Bronze'   => 'Bronze',
                            'Brown'   => 'Brown',
                            'Burgundy'   => 'Burgundy',
                            'Champagne'   => 'Champagne',
                            'Charcoal'   => 'Charcoal',
                            'Cream'   => 'Cream',
                            'Dark Blue'   => 'Dark Blue',
                            'Gold'   => 'Gold',
                            'Gray'   => 'Gray',
                            'Green'   => 'Green',
                            'Maroon'   => 'Maroon',
                            'Off White'   => 'Off White',
                            'Orange'   => 'Orange',
                            'Other'   => 'Other',
                            'Pearl'   => 'Pearl',
                            'Pewter'   => 'Pewter',
                            'Pink'   => 'Pink',
                            'Purple'   => 'Purple',
                            'Red'   => 'Red',
                            'Silver'   => 'Silver',
                            'Tan'   => 'Tan',
                            'Red'   => 'Red',
                            'Teal'   => 'Teal',
                            'Titanium'   => 'Titanium',
                            'Turquoise'   => 'Turquoise',
                            'White'   => 'White',
                            'Yellow'   => 'Yellow',
                        );
                        $class = 'class = form-control';
                        echo form_dropdown('txtExterior', $options, '30',$class);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtExterior');?></label>

                </div>

                <div class="form-group">
                    <?php
                    echo form_label('Interior Color', 'txtInterior');
                    $options = array(
                        'Any'  => 'Any',
                        'Beige'    => 'Beige',
                        'Black'   => 'Black',
                        'Blue'   => 'Blue',
                        'Bronze'   => 'Bronze',
                        'Brown'   => 'Brown',
                        'Burgundy'   => 'Burgundy',
                        'Champagne'   => 'Champagne',
                        'Charcoal'   => 'Charcoal',
                        'Cream'   => 'Cream',
                        'Dark Blue'   => 'Dark Blue',
                        'Gold'   => 'Gold',
                        'Gray'   => 'Gray',
                        'Green'   => 'Green',
                        'Maroon'   => 'Maroon',
                        'Off White'   => 'Off White',
                        'Orange'   => 'Orange',
                        'Other'   => 'Other',
                        'Pearl'   => 'Pearl',
                        'Pewter'   => 'Pewter',
                        'Pink'   => 'Pink',
                        'Purple'   => 'Purple',
                        'Red'   => 'Red',
                        'Silver'   => 'Silver',
                        'Tan'   => 'Tan',
                        'Red'   => 'Red',
                        'Teal'   => 'Teal',
                        'Titanium'   => 'Titanium',
                        'Turquoise'   => 'Turquoise',
                        'White'   => 'White',
                        'Yellow'   => 'Yellow',
                    );
                    $class = 'class = form-control';
                    echo form_dropdown('txtInterior', $options, '30',$class);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtInterior');?></label>
                </div>
                <div class="form-group">
                    <?php
                    echo form_label('Expire Date', 'txtExpireDate');
                    $attributes=array(
                        'name'=>'txtExpireDate',
                        'id'=>'expire_date',
                        'class'=>'form-control',
                        'maxlength'   => '10',
                        'placeholder'=>'Write Expire Date',
                        'value' => set_value('txtExpireDate'),
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtExpireDate');?></label>
                </div>
                <div class="form-group">
                    <?php
                    echo form_label('Reference Number', 'txtReferenceNo');
                    $attributes=array(
                        'name'=>'txtReferenceNo',
                        'class'=>'form-control',
                        'maxlength'   => '25',
                        'placeholder'=>'Write Reference Number',
                        'value' => set_value('txtReferenceNo'),
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtReferenceNo');?></label>
                </div>

                <div class="form-group">

                    <?php
                    echo form_label('Drive Type', 'txtDriveType');
                    $options = array(
                        '2 While Drive'  => '2 While Drive',
                        '4 While Drive'    => '4 While Drive',
                        'All While Drive'   => 'All While Drive',
                    );
                    $class = 'class = form-control';
                    echo form_dropdown('txtDriveType', $options,'4 While Drive' ,$class);

                    ?>
                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtDriveType');?></label>
                </div>


                <input type="submit" name="btnSubmit" class="btn btn-danger">
                <?php echo form_close();?>
            </div>
        </div>
    </div>
    <!-- /. ROW  -->
</div><!-- /. PAGE INNER  -->
</div><!-- /. PAGE WRAPPER  -->
