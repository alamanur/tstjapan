<?php

if(isset($_GET['transmission_id']))
{
    $id=$_GET['transmission_id'];
    $table='tbl_transmission';
    $id_field='id';
    $this->delete_model->Delete_Single_Row($id,$table,$id_field);
}

if(isset($_GET['id']))
{
    $id=$_GET['id'];
    $table='tbl_transmission';
    $id_field='id';
    $row=$this->select_model->Select_Single_Row($id,$table,$id_field);
    /*print_r($row);
        die;*/
}else{
    $row['transmission']='';
    $row['remarks']='';
    $row['id']='';
    $row['make_id']='';
    $row['model_id']='';

}
?>

<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <h2>Update Transmission</h2>

            </div>
        </div>
        <!-- /. ROW  -->
        <hr/>
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-4 ">
                <div class="form" >
                    <div class="box-content"  >
                        <?php
                        //-----Display Success or Error message---
                        if(isset($feedback)){
                            echo $feedback;
                        }
                        //----Form Tag Start-------------
                        $attributes = array('class' => 'email', 'id' => 'myform');

                        echo form_open('backdoor/edit_transmission', $attributes);
                        ?>
                    </div>
                    <div class="form-group">
                        <label>Manufacturar Name</label>
                        <input type="hidden" name="txtTransmissionId" value="<?php echo $row['id']; ?>">
                        <select name="txtMakeId" id="txtMakeId" class="form-control">
                            <option value="">Select One</option>
                            <?php
                            $result=$this->select_model->select_all('tbl_make',null,$id_field="make_id");
                            print_r($result);
                            foreach($result->result() as $row1){
                            ?>
                            <option value="<?php echo $row1->make_id;?>" <?php if(isset($row["make_id"]) && $row["make_id"]==$row1->make_id){echo "selected='select'";}?><?php echo set_select("txtMakeId",$row1->make_id)?>><?php echo $row1->name ; ?></option>';
                            <?php
                            }?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtMakeId');?></label>

                    </div>

                    <div class="form-group">
                        <label>Model Name</label>
                        <select name="txtModel" id="txtModel" class="form-control">
                            <option value="">Select One</option>
                            <?php
                            $result=$this->select_model->select_all('tbl_model',null,$id_field="model_id","make_id",$row['make_id']);
                            print_r($result);
                            foreach($result->result() as $row1){
                                ?>
                                <option value="<?php echo $row1->model_id;?>" <?php if(isset($row["model_id"]) && $row["model_id"]==$row1->model_id){echo "selected='select'";}?><?php echo set_select("txtModel",$row1->model_id)?>><?php echo $row1->model_name ; ?></option>';
                            <?php
                            }?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtModel');?></label>
                    </div>

                    <div class="form-group">
                        <label>Transmission</label>
                        <?php
                        $attributes=array(
                            'name'=>'txtTransmission',
                            'class'=>'form-control',
                            'placeholder'=>'Write Transmission',
                            'maxlength'   => '70',
                            'value' => $row["transmission"]
                        );
                        echo form_input($attributes);
                        ?>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtTransmission');?></label>
                    </div>
                    <div class="form-group">
                        <label>Remarks</label>
                        <input name="txtRemarks" class="form-control" maxlength="120" placeholder="Write Remarks" value="<?php if(isset($row["remarks"]) && $row["remarks"] != ""){echo $row["remarks"];}else{echo set_value('txtRemarks');} ?>">

                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtRemarks');?></label>
                    </div>
                    <?php
                    $attribute=array(
                        'name'=>'btnSubmit',
                        'class'=>'btn btn-danger ',
                        'value'=>'Update',

                    );
                    echo form_submit($attribute);//--Form Submit Button
                    echo form_close();//--Form closing tag </form>
                    ?>
                </div>
            </div>

            <div class="col-md-8col-sm-12 col-xs-8 ">
                <!-- Advanced Tables -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Transmission List
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="car_model">
                                <thead>
                                <tr>
                                    <th>SL No</th>
                                    <th>Manufacturar</th>
                                    <th>Model</th>
                                    <th>Body Style</th>
                                    <th>Remarks</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $query=$this->select_model->Select_transmission();

                                $sl=1;
                                foreach ($query->result() as $row)
                                {
                                    ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $sl; ?></td>
                                        <td class="center"><?php echo $row->name;?></td>
                                        <td class="center"><?php echo $row->model_name;?></td>
                                        <td class="center"><?php echo $row->transmission;?></td>
                                        <td class="center"><?php echo $row->remarks;?></td>
                                        <td class="center text-center"><a href="<?php echo base_url(); ?>backdoor/edit_transmission?id=<?php echo $row->id;?>"><i class="glyphicon glyphicon-edit "></a></td>
                                        <td class="center text-center"><a href="?transmission_id=<?php echo $row->id;?>" onclick="return confirm('Are you really want to delete this item')"><i class="glyphicon glyphicon-remove red "></a></td>

                                    </tr>
                                    <?php
                                    $sl++;
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <!--End Advanced Tables -->
            </div>
        </div>
    </div>
    <!-- /. ROW  -->
</div><!-- /. PAGE INNER  -->
</div><!-- /. PAGE WRAPPER  -->
