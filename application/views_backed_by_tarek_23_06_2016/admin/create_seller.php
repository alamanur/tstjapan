
<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <h2>Create Company Information</h2>

            </div>
        </div>
        <!-- /. ROW  -->
        <hr/>
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 ">
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="box-content"  >
                        <?php
                        //-----Display Success or Error message---
                        if(isset($feedback)){
                            echo $feedback;
                        }
                        ?>
                    </div>
                    <div class="form-group">
                        <label>Conpany Name</label>
                        <input type="text" class="form-control" name="txtCompanyName" value="<?php echo set_value('txtCompanyName'); ?>" placeholder="Enter Company Name" required="required"/>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtCompanyName');?></label>
                    </div>

                    <div class="form-group">
                        <label>Company Tag</label>
                        <input type="text" class="form-control" name="txtCompanyTag" value="<?php echo set_value('txtCompanyTag'); ?>" placeholder="Your favorites vehicles available!" required="required"/>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtCompanyTag');?></label>
                    </div>
                    <div class="form-group">
                        <label>Company Ownership Type</label>
                        <input type="text" class="form-control" name="txtOwnership" value="<?php echo set_value('txtOwnership'); ?>" placeholder=" Privet Ltd/ Partnership " required="required"/>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtOwnership');?></label>
                    </div>

                    <div class="form-group">
                        <label>Email Address</label>
                        <input type="email" class="form-control" name="txtEmailAddress" value="<?php echo set_value('txtEmailAddress'); ?>" placeholder="Ex: info@example.com" required="required"/>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtEmailAddress');?></label>
                    </div>

                    <div class="form-group">
                        <label>Phone No</label>
                        <input type="tel" class="form-control" name="txtPhone" value="<?php echo set_value('txtPhone'); ?>" placeholder="Enter Phone no" required="required"/>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtPhone');?></label>
                    </div>

                    <div class="form-group">
                        <label>Payment Terms</label>
                        <input type="text" class="form-control" name="txtPaymentTerms" value="<?php echo set_value('txtPaymentTerms'); ?>" placeholder="T/T L/C Other(Paypal)" required="required"/>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtPaymentTerms');?></label>
                    </div>

                    <div class="form-group">
                        <label>Established Year</label>
                        <select name="txtEstablishedYear" class="form-control" >
                            <option value="">Select One</option>
                            <?php
                            $y= date("Y")+1;
                            for ($i = 0;$i < 50;$i++)
                            {
                                $y -=1;
                                echo "<option value='$y' >$y</option>";
                            }
                            ?>

                        </select>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtEstablishedYear');?></label>
                    </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 ">
                    <div class="form-group">
                        <label>No of Employee</label>
                        <input type="text" class="form-control" name="txtNoOfEmployee" value="<?php echo set_value('txtNoOfEmployee'); ?>" placeholder=" 11 - 50 People" required="required"/>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtNoOfEmployee');?></label>
                    </div>

                    <div class="form-group">
                        <label>Business Classification</label>
                        <input type="text" class="form-control" name="txtBusinessClassification" value="<?php echo set_value('txtBusinessClassification'); ?>" placeholder=" Importer/Exporter" required="required"/>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtBusinessClassification');?></label>
                    </div>

                    <div class="form-group">
                        <label>Language</label>
                        <input type="text" class="form-control" name="txtLanguage" value="<?php echo set_value('txtLanguage'); ?>" placeholder=" English/Japanese" required="required"/>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtLanguage');?></label>
                    </div>

                    <div class="form-group">
                        <label>Country</label>
                        <select name="txtCountry" class="form-control"  >
                            <option value="">Select One</option>
                            <option value="Japan" <?php echo set_select('txtCountry', 'Japan'); ?> >Japan</option>
                            <option value="Bangladesh" <?php echo set_select('txtCountry', 'Bangladesh'); ?> >Bangladesh</option>
                            <option value="India" <?php echo set_select('txtCountry', 'India'); ?> >India</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtCountry');?></label>
                    </div>
                    <div class="form-group">

                        <label>Company Description</label>
                        <textarea class="form-control" rows="3" name="txtDescription" placeholder="Enter Description" required="required"><?php echo set_value('txtDescription'); ?></textarea>

                    </div>

                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtDescription');?></label>
                    </div>
                    <div class="form-group">
                        <label>Company Address</label>
                        <textarea class="form-control" rows="3" name="txtAddress"placeholder="Enter Description" required="required"><?php echo set_value('txtDescription'); ?></textarea>

                    </div>

                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtAddress');?></label>
                    </div>
                    <div class="checkbox">
                        <label><input name="txtIsActive" type="checkbox" value="1" checked> IsActive</label>
                    </div>

                    <div class="form-group">
                        <label>Company Logo</label>
                        <input type="file" class="form-control" name="txtLogo" placeholder="Select Logo" required="required"/>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtLogo');?></label>
                    </div>

                    <input type="submit" name="btnSubmit" value="Save" class="btn btn-danger" >
                </form>
            </div>
        </div>
    </div>
    <!-- /. ROW  -->
</div><!-- /. PAGE INNER  -->
</div><!-- /. PAGE WRAPPER  -->
