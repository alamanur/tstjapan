<?php
  if(isset($_GET['id'])){
        $id=$_GET['id'];
        $table='tbl_product';
        $id_field='product_id';
        $row=$this->select_model->Select_Single_Row_product($id,$table,$id_field);

//        echo $row[0]['product_id']; die;
//        print_r($row);
//        die;
    }else{

        $row[0]['product_id']='';
        $row[0]['make_id']='';
        $row[0]['model_id']='';
        $row[0]['category_id']='';
        $row[0]['manufacture_year']='';
        $row[0]['manufacture_month']='';
        $row[0]['price']='';
        $row[0]['desplacement']='';
        $row[0]['steering']='';
        $row[0]['condition']='';
        $row[0]['made_in']='';
        $row[0]['fuel']='';
        $row[0]['body_style_id']='';
        $row[0]['engine_type_id']='';
        $row[0]['door']='';
        $row[0]['drive_type']='';
        $row[0]['vin']='';
        $row[0]['Mileage']='';
        $row[0]['transmission_id']='';
        $row[0]['no_of_passenger']='';
        $row[0]['width']='';
        $row[0]['height']='';
        $row[0]['length']='';
        $row[0]['per_cubic_meter_price']='';
        $row[0]['exterior_color']='';
        $row[0]['interior_color']='';
        $row[0]['expiry_date']='';
        $row[0]['reference_no']='';
        $row[0]['options']='';
        $row[0]['status']='';
        $row[0]['seller_id']='';

    }
?>

<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <h2>Edit Product</h2>
                <?php

                if ($this->session->flashdata('errors')){ //change!
                    echo "<div class='error'>";
                    echo $this->session->flashdata('errors');
                    echo "</div>";
                }

                ?>
            </div>
        </div>
        <!-- /. ROW  -->
        <hr/>
        <div class="row">
            <div class="col-md-12">
                <?php
                //-----Display Success or Error message---
                if(isset($feedback)){
                    echo $feedback;
                }
                ?>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-6 ">
                <div class="box-content"  >
                    <?php

                    //----Form Tag Start-------------
                    $attributes = array('class' => 'email', 'id' => 'myform');
                    echo form_open_multipart('backdoor/edit_product');
                    ?>
                </div>

                <div class="form-group">
                    <label>Manufacturar Name</label>
                    <input type="hidden" value="<?php echo $row[0]['product_id']; ?>" name="txtProductId"/>
                    <select name="txtMakeId" id="txtMakeId" class="form-control">
                        <option value="">Select One</option>
                        <?php
                        $result=$this->select_model->select_all('tbl_make',null,$id_field="make_id");
                        print_r($result);
                        foreach($result->result() as $row1){
                            ?>
                            <option value="<?php echo $row1->make_id;?>" <?php if(isset($row[0]['make_id']) && $row[0]['make_id'] == $row1->make_id){echo "selected='select'";}?><?php echo set_select("txtMakeId",$row1->make_id)?>><?php echo $row1->name ; ?></option>';
                            <?php
                        }?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtMakeId');?></label>

                </div>

                <div class="form-group">
                    <label>Transmission</label>
                    <select name="txtTransmission" id="txtTransmission" class="form-control">
                        <option value="">Select One</option>
                        <?php
                        $result=$this->select_model->select_all('tbl_transmission',null,$id_field="id","model_id",$row[0]['model_id']);
                        //print_r($result);
                        foreach($result->result() as $row1){
                            ?>
                            <option value="<?php echo $row1->id;?>" <?php if(isset($row[0]['transmission_id']) && $row[0]['transmission_id'] == $row1->id){echo "selected='select'";}?><?php echo set_select("txtTransmission",$row1->id)?>><?php echo $row1->transmission ; ?></option>
                            <?php
                        }
                        ?>
                    </select>

                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtTransmission');?></label>
                </div>

                <div class="form-group">
                    <label>Vehicle Category</label>
                    <select name="txtCategory" id="txtCategory" class="form-control">
                        <option value="">Select One</option>
                        <?php
                        $result=$this->select_model->select_all('tbl_category',null,$id_field="category_id");
//                        print_r($result);
                        foreach($result->result() as $row1){
                            ?>
                            <option value="<?php echo $row1->category_id;?>" <?php if(isset($row[0]['category_id']) && $row[0]['category_id'] == $row1->category_id){echo "selected='select'";}?><?php echo set_select("txtCategory",$row1->category_id)?>><?php echo $row1->name ; ?></option>';
                            <?php
                        }?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtCategory');?></label>
                </div>

                <div class="form-group">
                    <label>Vehicle Seller</label>
                    <select name="txtSeller" id="txtSeller" class="form-control">
                        <option value="">Select One</option>
                        <?php
                        $result=$this->select_model->select_all('tbl_company_information',null,$id_field="company_id");
//                        print_r($result);
                        foreach($result->result() as $row1){
                            ?>
                            <option value="<?php echo $row1->company_id;?>" <?php if(isset($row[0]['seller_id']) && $row[0]['seller_id'] == $row1->company_id){echo "selected='select'";}?><?php echo set_select("txtSeller",$row1->company_id)?>><?php echo $row1->company_name ; ?></option>';
                            <?php
                        }?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtSeller');?></label>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-6">
                            <label for="manufactureYear">Manufacture Year</label>
                            <select name="manufactureYear" class="form-control" >
                                <?php
                                $y= date("Y")+1;
                                for ($i = 0;$i < 50;$i++)
                                {
                                    $y -=1;
                                 ?>
                                    <option value="<?php echo $y; ?>" <?php if(isset($row[0]['manufacture_year']) && $row[0]['manufacture_year'] == $y){echo "selected='select'";}?><?php echo set_select("manufactureYear",$y)?>><?php echo $y ; ?></option>';
                             <?php
                                }
                                ?>

                            </select>
                        </div>
                        <div class="col-xs-6">
                            <label for="manufactureMonth">Manufacture Month</label>
                            <select name="manufactureMonth" class="form-control" >
                                <option value="January" <?php if(isset($row[0]['manufacture_month']) && $row[0]['manufacture_month'] == $y){echo "selected";}?><?php echo set_select("manufactureMonth","January")?>>January</option>
                                <option value="February" <?php if(isset($row[0]['manufacture_month']) && $row[0]['manufacture_month'] == $y){echo "selected";}?><?php echo set_select("manufactureMonth","February")?>>February</option>
                                <option value="March" <?php if(isset($row[0]['manufacture_month']) && $row[0]['manufacture_month'] == $y){echo "selected";}?><?php echo set_select("manufactureMonth","March")?>>March</option>
                                <option value="April" <?php if(isset($row[0]['manufacture_month']) && $row[0]['manufacture_month'] == $y){echo "selected";}?><?php echo set_select("manufactureMonth","April")?>>April</option>
                                <option value="May" <?php if(isset($row[0]['manufacture_month']) && $row[0]['manufacture_month'] == $y){echo "selected";}?><?php echo set_select("manufactureMonth","May")?>>May</option>
                                <option value="June" <?php if(isset($row[0]['manufacture_month']) && $row[0]['manufacture_month'] == $y){echo "selected";}?><?php echo set_select("manufactureMonth","June")?>>June</option>
                                <option value="July" <?php if(isset($row[0]['manufacture_month']) && $row[0]['manufacture_month'] == $y){echo "selected";}?><?php echo set_select("manufactureMonth","July")?>>July</option>
                                <option value="August" <?php if(isset($row[0]['manufacture_month']) && $row[0]['manufacture_month'] == $y){echo "selected";}?><?php echo set_select("manufactureMonth","August")?>>August</option>
                                <option value="September" <?php if(isset($row[0]['manufacture_month']) && $row[0]['manufacture_month'] == $y){echo "selected";}?><?php echo set_select("manufactureMonth","September")?>>September</option>
                                <option value="October" <?php if(isset($row[0]['manufacture_month']) && $row[0]['manufacture_month'] == $y){echo "selected";}?><?php echo set_select("manufactureMonth","October")?>>October</option>
                                <option value="November" <?php if(isset($row[0]['manufacture_month']) && $row[0]['manufacture_month'] == $y){echo "selected";}?><?php echo set_select("manufactureMonth","November")?>>November</option>
                                <option value="December" <?php if(isset($row[0]['manufacture_month']) && $row[0]['manufacture_month'] == $y){echo "selected";}?><?php echo set_select("manufactureMonth","December")?>>December</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('manufactureMonth');?></label>
                </div>

                <div class="form-group">
                    <label>Price</label>
                    <input type="text"  name="txtPrice" class="form-control" maxlength="12" placeholder="Ex : 10000 / 25000" value="<?php if(form_error('txtPrice') != ""){echo set_value('txtPrice');}else{ echo $row[0]['price'];}?>">
                    <?php
/*                    $attributes=array(
                        'name'=>'txtPrice',
                        'class'=>'form-control',
                        'maxlength'   => '12',
                        'placeholder'=>'EX : 1000 / 2500',
                        'value' =>
                    );
                    echo form_input($attributes);
                    */?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtPrice');?></label>
                </div>

                <div class="form-group">

                    <label>Displacement</label>
                    <?php
                    $attributes=array(
                        'name'=>'txtDisplacement',
                        'class'=>'form-control',
                        'maxlength'   => '50',
                        'placeholder'=>'Ex :  2000 cc / 2500 cc',
                        'value' => $row[0]['desplacement']
                    );
                    echo form_input($attributes);
                    ?>
                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtDisplacement');?></label>
                </div>

                <div class="form-group">
                    <label>Steering</label>
                    <?php
                    $attributes=array(
                        'name'=>'txtSteering',
                        'class'=>'form-control',
                        'maxlength'   => '50',
                        'placeholder'=>'Ex : Right / Left',
                        'value' => $row[0]['steering'],
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtSteering');?></label>
                </div>

                <div class="form-group">
                    <label>Condition</label>
                    <?php
                    $attributes=array(
                        'name'=>'txtCondition',
                        'class'=>'form-control',
                        'maxlength'   => '10',
                        'placeholder'=>'Ex : Used/ New',
                        'value' => $row[0]['condition'],
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtCondition');?></label>
                </div>

                <div class="form-group">
                    <label>Made in</label>
                    <?php
                    $attributes=array(
                        'name'=>'txtMadeIn',
                        'class'=>'form-control',
                        'maxlength'   => '25',
                        'placeholder'=>'Ex : Japan / China',
                        'value' => $row[0]['made_in'],
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtMadeIn');?></label>
                </div>

                <div class="form-group">
                    <label>Fuel</label>
                    <?php
                    echo form_label('Fuel', 'txtFuel');
                    $options = array(
                        'Any'  => 'Any',
                        'Biodiesel'    => 'Biodiesel',
                        'CNG'   => 'CNG',
                        'Diesel'   => 'Diesel',
                        'Electric'   => 'Electric',
                        'Ethanol-FFV'   => 'Ethanol-FFV',
                        'Gasoline/Petrol'   => 'Gasoline/Petrol',
                        'Hybrid-electric'   => 'Hybrid-electric',
                        'LPG'   => 'LPG',
                        'CNG'   => 'CNG',
                        'Steam'   => 'Steam',
                        'Other'   => 'Other',
                    );
                    $class = 'class = form-control';
                    echo form_dropdown('txtFuel', $options, $row[0]['fuel'],$class);
                    ?>
                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtFuel');?></label>
                </div>

                <div class="form-group">
                    <?php
                    echo form_label('Mileage', 'txtMileage');

                    $attributes=array(
                        'name'=>'txtMileage',
                        'class'=>'form-control',
                        'maxlength'   => '25',
                        'placeholder'=>'Ex : 38,000 km ',
                        'value' => $row[0]['Mileage'],
                    );
                    echo form_input($attributes);
                    ?>

                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtMileage');?></label>
                </div>

                <div class="form-group">

                    <?php
                    echo form_label('Options', 'txtOptions');
                    $attributes=array(
                        'name'=>'txtOptions',
                        'class'=>'form-control',
                        'placeholder'=>'Ex :Anti-Lock Brakes / Driver Airbag / Passenger Airbag ',
                        'value' => $row[0]['options'],
                    );
                    echo form_input($attributes);

                    ?>
                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtOptions');?></label>
                </div>


                <div class="checkbox">
                    <label><input name="txtIsAvailable" value="1" type="checkbox" <?php echo(($row[0]['status']== 1)? "checked" : "");?>> IsAvailable</label>
                </div>

                <div class="form-group">

                    <?php
                    echo form_label('Vehicle Image', 'txtImages');
                    $attributes=array(
                        'name'=>'txtImages',
                        'class'=>'form-control',
                        'maxlength'   => '50',
                        //'multiple'=>true,
                        'placeholder'=>'Write Options',

                    );
                    echo form_upload($attributes);
                    ?>
                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtImages');?></label>
                </div>

            </div>


            <div class="col-md-6 col-sm-12 col-xs-6 ">
                <div class="form-group">
                    <label>Model Name</label>
                    <select name="txtModel" id="txtModelId" class="form-control">
                        <option value="">Select One</option>
                        <?php
                        $result=$this->select_model->select_all('tbl_model',null,$id_field="model_id","make_id",$row[0]['make_id']);
                        //print_r($result);
                        foreach($result->result() as $row1){
                            ?>
                            <option value="<?php echo $row1->model_id;?>" <?php if(isset($row[0]['model_id']) && $row[0]['model_id'] == $row1->model_id){echo "selected='select'";}?><?php echo set_select("txtModel",$row1->model_id)?>><?php echo $row1->model_name ; ?></option>
                       <?php
                        }
                        ?>
                    </select>
                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtModel');?></label>
                </div>

                <div class="form-group">
                    <label>Body Style</label>
                    <select name="txtBodyStyle" id="txtBodyStyle" class="form-control">
                        <option value="">Select One</option>
                        <?php
                        $result=$this->select_model->select_all('tbl_body_style',null,$id_field="id","model_id",$row[0]['model_id']);
                        //print_r($result);
                        foreach($result->result() as $row1){
                            ?>
                            <option value="<?php echo $row1->id;?>" <?php if(isset($row[0]['body_style_id']) && $row[0]['body_style_id'] == $row1->id){echo "selected='select'";}?><?php echo set_select("txtBodyStyle",$row1->id)?>><?php echo $row1->body_style ; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtBodyStyle');?></label>
                </div>
                <div class="form-group">
                    <label>Engine Type</label>
                    <select name="txtEngineType" id="txtEngineType" class="form-control">
                        <option value="">Select One</option>
                        <?php
                        $result=$this->select_model->select_all('tbl_engine_type',null,$id_field="id","model_id",$row[0]['model_id']);
                        //print_r($result);
                        foreach($result->result() as $row1){
                            ?>
                            <option value="<?php echo $row1->id;?>" <?php if(isset($row[0]['engine_type_id']) && $row[0]['engine_type_id'] == $row1->id){echo "selected='select'";}?><?php echo set_select("txtEngineType",$row1->id)?>><?php echo $row1->engine_type ; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtEngineType');?></label>
                </div>
                <div class="form-group">
                    <label>Doors</label>
                    <?php
                    $attributes=array(
                        'name'=>'txtDoor',
                        'class'=>'form-control',
                        'maxlength'   => '2',
                        'placeholder'=>'Number of doors Ex : 4 / 5',
                        'value' => $row[0]['door'],
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtDoor');?></label>
                </div>

                <div class="form-group">
                    <?php
                    echo form_label('Number of passenger', 'txtPassenger');
                    $attributes=array(
                        'name'=>'txtPassenger',
                        'class'=>'form-control',
                        'maxlength'   => '2',
                        'placeholder'=>'Number of passenger Ex : 4 / 5 ',
                        'value' => $row[0]['no_of_passenger'],
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <?php
                    echo form_label('VIN (Vehicle Identification Number)/Serial No)', 'txtVin');
                    $attributes=array(
                        'name'=>'txtVin',
                        'class'=>'form-control',
                        'maxlength'   => '25',
                        'placeholder'=>' Ex : WAUZZZ8E03A169*** ',
                        'value' => $row[0]['vin'],
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtVin');?></label>
                </div>
                <div class="form-group">
                    <?php
                    echo form_label('Height (CM)', 'txtHeight');
                    $attributes=array(
                        'name'=>'txtHeight',
                        'class'=>'form-control',
                        'maxlength'   => '10',
                        'placeholder'=>'Ex : 144',
                        'value' => $row[0]['height'],
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtHeight');?></label>
                </div>
                <div class="form-group">
                    <?php
                    echo form_label('Width (cm)', 'txtWidth');
                    $attributes=array(
                        'name'=>'txtWidth',
                        'class'=>'form-control',
                        'maxlength'   => '10',
                        'placeholder'=>'Ex : 177',
                        'value' => $row[0]['width'],
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtWidth');?></label>
                </div>

                <div class="form-group">
                    <?php
                    echo form_label('Length (cm)', 'txtLength');
                    $attributes=array(
                        'name'=>'txtLength',
                        'class'=>'form-control',
                        'maxlength'   => '10',
                        'placeholder'=>'Ex : 550',
                        'value' => $row[0]['length'],
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtLength');?></label>
                </div>

                <div class="form-group">
                    <?php
                    echo form_label('Price Per Cubic Meter (Dollar)', 'txtPricePerCubicMeter');
                    $attributes=array(
                        'name'=>'txtPricePerCubicMeter',
                        'class'=>'form-control',
                        'maxlength'   => '10',
                        'placeholder'=>'Ex : 10',
                        'value' => $row[0]['per_cubic_meter_price'],
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtPricePerCubicMeter');?></label>
                </div>
                <div class="form-group">
                    <?php
                    echo form_label('Exterior Color', 'txtExterior');

                    $options = array(
                        'Any'  => 'Any',
                        'Beige'    => 'Beige',
                        'Black'   => 'Black',
                        'Blue'   => 'Blue',
                        'Bronze'   => 'Bronze',
                        'Brown'   => 'Brown',
                        'Burgundy'   => 'Burgundy',
                        'Champagne'   => 'Champagne',
                        'Charcoal'   => 'Charcoal',
                        'Cream'   => 'Cream',
                        'Dark Blue'   => 'Dark Blue',
                        'Gold'   => 'Gold',
                        'Gray'   => 'Gray',
                        'Green'   => 'Green',
                        'Maroon'   => 'Maroon',
                        'Off White'   => 'Off White',
                        'Orange'   => 'Orange',
                        'Other'   => 'Other',
                        'Pearl'   => 'Pearl',
                        'Pewter'   => 'Pewter',
                        'Pink'   => 'Pink',
                        'Purple'   => 'Purple',
                        'Red'   => 'Red',
                        'Silver'   => 'Silver',
                        'Tan'   => 'Tan',
                        'Red'   => 'Red',
                        'Teal'   => 'Teal',
                        'Titanium'   => 'Titanium',
                        'Turquoise'   => 'Turquoise',
                        'White'   => 'White',
                        'Yellow'   => 'Yellow',
                    );
                    $class = 'class = form-control';
                    echo form_dropdown('txtExterior', $options, $row[0]['exterior_color'],$class);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtExterior');?></label>

                </div>

                <div class="form-group">
                    <?php
                    echo form_label('Interior Color', 'txtInterior');
                    $options = array(
                        'Any'  => 'Any',
                        'Beige'    => 'Beige',
                        'Black'   => 'Black',
                        'Blue'   => 'Blue',
                        'Bronze'   => 'Bronze',
                        'Brown'   => 'Brown',
                        'Burgundy'   => 'Burgundy',
                        'Champagne'   => 'Champagne',
                        'Charcoal'   => 'Charcoal',
                        'Cream'   => 'Cream',
                        'Dark Blue'   => 'Dark Blue',
                        'Gold'   => 'Gold',
                        'Gray'   => 'Gray',
                        'Green'   => 'Green',
                        'Maroon'   => 'Maroon',
                        'Off White'   => 'Off White',
                        'Orange'   => 'Orange',
                        'Other'   => 'Other',
                        'Pearl'   => 'Pearl',
                        'Pewter'   => 'Pewter',
                        'Pink'   => 'Pink',
                        'Purple'   => 'Purple',
                        'Red'   => 'Red',
                        'Silver'   => 'Silver',
                        'Tan'   => 'Tan',
                        'Red'   => 'Red',
                        'Teal'   => 'Teal',
                        'Titanium'   => 'Titanium',
                        'Turquoise'   => 'Turquoise',
                        'White'   => 'White',
                        'Yellow'   => 'Yellow',
                    );
                    $class = 'class = form-control';
                    echo form_dropdown('txtInterior', $options, $row[0]['interior_color'],$class);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtInterior');?></label>
                </div>
                <div class="form-group">
                    <?php
                    echo form_label('Expire Date', 'txtExpireDate');
                    $attributes=array(
                        'name'=>'txtExpireDate',
                        'id'=>'expire_date',
                        'class'=>'form-control',
                        'maxlength'   => '10',
                        'placeholder'=>'Write Expire Date',
                        'value' => $row[0]['expiry_date'],
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtExpireDate');?></label>
                </div>
                <div class="form-group">
                    <?php
                    echo form_label('Reference Number', 'txtReferenceNo');
                    $attributes=array(
                        'name'=>'txtReferenceNo',
                        'class'=>'form-control',
                        'maxlength'   => '25',
                        'placeholder'=>'Write Reference Number',
                        'value' => $row[0]['reference_no'],
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtReferenceNo');?></label>
                </div>

                <div class="form-group">

                    <?php
                    echo form_label('Drive Type', 'txtDriveType');
                    $options = array(
                        '2 While Drive'  => '2 While Drive',
                        '4 While Drive'    => '4 While Drive',
                        'All While Drive'   => 'All While Drive',
                    );
                    $class = 'class = form-control';
                    echo form_dropdown('txtDriveType', $options, $row[0]['drive_type'],$class);

                    ?>
                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtDriveType');?></label>
                </div>


                <input type="submit" name="btnSubmit" class="btn btn-danger">
                <?php echo form_close();?>
            </div>
        </div>
    </div>
    <!-- /. ROW  -->
</div><!-- /. PAGE INNER  -->
</div><!-- /. PAGE WRAPPER  -->
