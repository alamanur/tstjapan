<?php

if(isset($_GET['engine_type_id']))
{
    $id=$_GET['engine_type_id'];
    $table='tbl_engine_type';
    $id_field='id';
    $this->delete_model->Delete_Single_Row($id,$table,$id_field);
}
?>

<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <h2>Create Engine Type</h2>

            </div>
        </div>
        <!-- /. ROW  -->
        <hr/>
        <div class="row">
            <div class="col-md-4 col-sm-12 col-xs-4 ">
                <div class="form" >
                    <div class="box-content"  >
                        <?php
                        //-----Display Success or Error message---
                        if(isset($feedback)){
                            echo $feedback;
                        }
                        //----Form Tag Start-------------
                        $attributes = array('class' => 'email', 'id' => 'myform');

                        echo form_open('backdoor/engine_type', $attributes);
                        ?>
                    </div>
                    <div class="form-group">
                        <label>Manufacturar Name</label>
                        <select name="txtMakeId" id="txtMakeId" class="form-control">
                            <?php
                            echo $this->select_model->Select_box($table='tbl_make');
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtMakeId');?></label>

                    </div>

                    <div class="form-group">
                        <label>Model Name</label>
                        <select name="txtModel" id="txtModel" class="form-control">
                            <option value="">Select One</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtModel');?></label>
                    </div>

                    <div class="form-group">
                        <label>Engine Type</label>
                        <?php
                        $attributes=array(
                            'name'=>'txtEngineType',
                            'class'=>'form-control',
                            'placeholder'=>'Write Engine Type',
                            'maxlength'   => '70',
                            'value' => set_value('txtEngineType'),
                        );
                        echo form_input($attributes);
                        ?>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtEngineType');?></label>
                    </div>
                    <div class="form-group">
                        <label>Remarks</label>
                        <?php
                        $attributes=array(
                            'name'=>'txtRemarks',
                            'class'=>'form-control',
                            'placeholder'=>'Write Remarks',
                            'maxlength'   => '120',
                            'value' => set_value('txtRemarks'),
                        );
                        echo form_input($attributes);
                        ?>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtRemarks');?></label>
                    </div>
                    <?php
                    $attribute=array(
                        'name'=>'btnSubmit',
                        'class'=>'btn btn-danger ',
                        'value'=>'Submit',

                    );
                    echo form_submit($attribute);//--Form Submit Button
                    echo form_close();//--Form closing tag </form>
                    ?>
                </div>
            </div>

            <div class="col-md-8col-sm-12 col-xs-8 ">
                <!-- Advanced Tables -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Engine Type List
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="car_model">
                                <thead>
                                <tr>
                                    <th>SL No</th>
                                    <th>Manufacturar</th>
                                    <th>Model</th>
                                    <th>Engine Type</th>
                                    <th>Remarks</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $query=$this->select_model->Select_engine_type();

                                $sl=1;
                                foreach ($query->result() as $row)
                                {
                                    ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $sl; ?></td>
                                        <td class="center"><?php echo $row->name;?></td>
                                        <td class="center"><?php echo $row->model_name;?></td>
                                        <td class="center"><?php echo $row->engine_type;?></td>
                                        <td class="center"><?php echo $row->remarks;?></td>
                                        <td class="center text-center"><a href="<?php echo base_url(); ?>backdoor/edit_engine_type?id=<?php echo $row->id;?>"><i class="glyphicon glyphicon-edit "></a></td>
                                        <td class="center text-center"><a href="?engine_type_id=<?php echo $row->id;?>" onclick="return confirm('Are you really want to delete this item')"><i class="glyphicon glyphicon-remove red "></a></td>

                                    </tr>
                                    <?php
                                    $sl++;
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <!--End Advanced Tables -->
            </div>
        </div>
    </div>
    <!-- /. ROW  -->
</div><!-- /. PAGE INNER  -->
</div><!-- /. PAGE WRAPPER  -->
