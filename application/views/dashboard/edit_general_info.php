<?php
/**********************************************
 * Developer : Tarek Raihan                   *
 * Project : TSTJAPAN.CO.JP                   *
 * Script : To Edit General Info              *
 * Start Date :   13-06-2016                  *
 * Last Update : 15-06-2016                   *
 **********************************************/
if($this->session->userdata('fuser_id')) {
    $id=$this->session->userdata('fuser_id');
    $table='tst_user';
    $id_field='user_id';
    $result=$this->select_model->Select_Single_Row($id,$table,$id_field);
    /*print_r($row);
        die;*/
}else {
    $result['first_name']='';
    $result['last_name']='';
    $result['company_name']='';
}

?>
<div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
    <div class="row no-margin">
        <div class="focus highlight">
            <h2 class="title">Customer Profile</h2>
        </div>
        <div class="below"></div>
    </div>

    <div class="row no-margin general">
        <div class="pull-left">
            <h4>Edit General Information</h4>
        </div>
    </div>
    <div class="row no-margin general">
        <form class="form-horizontal" id="editGeneralInformationForm">
            <div class="feedback_message"></div>
            <div class="form-group">
                <label for="firstName" class="col-sm-4 control-label">First Name </label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="firstName" name="first_name" value="<?php echo $result['first_name'];?>" placeholder="First Name" maxlength="25">
                </div>
            </div>
            <div class="form-group">
                <label for="lastName" class="col-sm-4 control-label">Last Name</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="lastName" name="last_name" value="<?php echo $result['last_name'];?>" placeholder="Last Name" maxlength="25">
                </div>
            </div>
            <div class="form-group">
                <label for="companyName" class="col-sm-4 control-label">Company Name</label>
                <div class="col-sm-8">
                    <input type="text" class="form-control" id="companyName" name="company_name" value="<?php echo $result['company_name'];?>" placeholder="Company Name" maxlength="200">
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-4 col-sm-8">
                    <button type="button" id="btnGeneralInfoUpdate" class="btn btn-primary">Update</button>
                </div>
            </div>
        </form>
    </div>
</div>