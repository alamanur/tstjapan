<?php
if($this->session->userdata('fuser_id'))
{
    $id=$this->session->userdata('fuser_id');
    $table='tst_user';
    $id_field='user_id';
    $row=$this->select_model->Select_Single_Row($id,$table,$id_field);
    /*print_r($row);
        die;*/
}
?>

   <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
        <div class="row no-margin">
            <div class="focus highlight">
                <h2 class="title">Customer Profile</h2>
            </div>
            <div class="below"></div>
        </div>

        <div class="row no-margin general">
            <div class="pull-left">
                <h4>General</h4>
            </div>

            <div class="pull-right">
                <a href="<?php echo base_url();?>dashboard/edit_general_info" class="btn btn-success btn-sm custom_btn">Edit</a>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table preview full" cellpadding="0" cellspacing="0">
                <tbody>
                <tr class="odd">
                    <td class="category">Status:</td>
                    <td class="value"><?php echo (($row['status'] == '1')? 'Active' : 'Inactive');?></td>
                </tr>
                <tr class="even">
                    <td class="category">First Name:</td>
                    <td class="value"><?php echo $row['first_name']; ?></td>
                </tr>
                <tr class="odd">
                    <td class="category">Last Name:</td>
                    <td class="value"><?php echo $row['last_name']; ?></td>
                </tr>
                <tr class="even">
                    <td class="category">Company Name:</td>
                    <td class="value"><?php echo $row['company_name']; ?></td>
                </tr>

                </tbody>
            </table>
        </div>
        <div class="below"></div>

        <div class="row no-margin general">
            <div class="pull-left">
                <h4>Contact Infromation</h4>
            </div>

            <div class="pull-right">
                <a href="<?php echo base_url();?>dashboard/edit_contact_info/ " class="btn btn-success btn-sm custom_btn">Edit</a>
            </div>
        </div>

        <div class="table-responsive">
            <table class="table preview full" cellpadding="0" cellspacing="0">
                <tbody>
                <tr class="odd">
                    <td class="category">Email Address:</td>
                    <td class="value"><?php echo $row['email_address']; ?></td>
                </tr>
                <tr class="even">
                    <td class="category">Home Phone:</td>
                    <td class="value"><?php echo $row['home_phone']; ?></td>
                </tr>
                <tr class="odd">
                    <td class="category">Mobile Phone:</td>
                    <td class="value"><?php echo $row['mobile']; ?></td>
                </tr>
                <tr class="even">
                    <td class="category">Address:</td>
                    <td class="value"><?php echo $row['address']; ?></td>
                </tr>
                <tr class="odd">
                    <td class="category">Postal Code/Zip:</td>
                    <td class="value"><?php echo $row['postal_code']; ?></td>
                </tr>
                <tr class="even">
                    <td class="category">Country:</td>
                    <td class="value"><?php echo $row['country']; ?></td>
                </tr>
                </tbody>
            </table>
        </div>
        <div class="below"></div>

        <div class="row no-margin general">
            <div class="pull-left">
                <h4>Password</h4>
            </div>

            <div class="pull-right">
                <a href="<?php echo base_url();?>dashboard/change_password" class="btn btn-success btn-sm custom_btn">Edit</a>
            </div>
        </div>
        <p class="squeeze">Change the password for this account by clicking <strong>'Edit'</strong> in the top right hand corner of this box.</p>
        <div class="below"></div><br/><br/>

    </div>
