<?php
function convertNumber($number)
{
    list($integer, $fraction) = explode(".", (string) $number);

    $output = "";

    if ($integer{0} == "-")
    {
        $output = "negative ";
        $integer    = ltrim($integer, "-");
    }
    else if ($integer{0} == "+")
    {
        $output = "positive ";
        $integer    = ltrim($integer, "+");
    }

    if ($integer{0} == "0")
    {
        $output .= "zero";
    }
    else
    {
        $integer = str_pad($integer, 36, "0", STR_PAD_LEFT);
        $group   = rtrim(chunk_split($integer, 3, " "), " ");
        $groups  = explode(" ", $group);

        $groups2 = array();
        foreach ($groups as $g)
        {
            $groups2[] = convertThreeDigit($g{0}, $g{1}, $g{2});
        }

        for ($z = 0; $z < count($groups2); $z++)
        {
            if ($groups2[$z] != "")
            {
                $output .= $groups2[$z] . convertGroup(11 - $z) . (
                    $z < 11
                    && !array_search('', array_slice($groups2, $z + 1, -1))
                    && $groups2[11] != ''
                    && $groups[11]{0} == '0'
                        ? " and "
                        : ", "
                    );
            }
        }

        $output = rtrim($output, ", ");
    }

    if ($fraction > 0)
    {
        $output .= " point";
        for ($i = 0; $i < strlen($fraction); $i++)
        {
            $output .= " " . convertDigit($fraction{$i});
        }
    }

    return $output;
}

function convertGroup($index)
{
    switch ($index)
    {
        case 11:
            return " decillion";
        case 10:
            return " nonillion";
        case 9:
            return " octillion";
        case 8:
            return " septillion";
        case 7:
            return " sextillion";
        case 6:
            return " quintrillion";
        case 5:
            return " quadrillion";
        case 4:
            return " trillion";
        case 3:
            return " billion";
        case 2:
            return " million";
        case 1:
            return " thousand";
        case 0:
            return "";
    }
}

function convertThreeDigit($digit1, $digit2, $digit3)
{
    $buffer = "";

    if ($digit1 == "0" && $digit2 == "0" && $digit3 == "0")
    {
        return "";
    }

    if ($digit1 != "0")
    {
        $buffer .= convertDigit($digit1) . " hundred";
        if ($digit2 != "0" || $digit3 != "0")
        {
            $buffer .= " and ";
        }
    }

    if ($digit2 != "0")
    {
        $buffer .= convertTwoDigit($digit2, $digit3);
    }
    else if ($digit3 != "0")
    {
        $buffer .= convertDigit($digit3);
    }

    return $buffer;
}

function convertTwoDigit($digit1, $digit2)
{
    if ($digit2 == "0")
    {
        switch ($digit1)
        {
            case "1":
                return "ten";
            case "2":
                return "twenty";
            case "3":
                return "thirty";
            case "4":
                return "forty";
            case "5":
                return "fifty";
            case "6":
                return "sixty";
            case "7":
                return "seventy";
            case "8":
                return "eighty";
            case "9":
                return "ninety";
        }
    } else if ($digit1 == "1")
    {
        switch ($digit2)
        {
            case "1":
                return "eleven";
            case "2":
                return "twelve";
            case "3":
                return "thirteen";
            case "4":
                return "fourteen";
            case "5":
                return "fifteen";
            case "6":
                return "sixteen";
            case "7":
                return "seventeen";
            case "8":
                return "eighteen";
            case "9":
                return "nineteen";
        }
    } else
    {
        $temp = convertDigit($digit2);
        switch ($digit1)
        {
            case "2":
                return "twenty-$temp";
            case "3":
                return "thirty-$temp";
            case "4":
                return "forty-$temp";
            case "5":
                return "fifty-$temp";
            case "6":
                return "sixty-$temp";
            case "7":
                return "seventy-$temp";
            case "8":
                return "eighty-$temp";
            case "9":
                return "ninety-$temp";
        }
    }
}

function convertDigit($digit)
{
    switch ($digit)
    {
        case "0":
            return "zero";
        case "1":
            return "one";
        case "2":
            return "two";
        case "3":
            return "three";
        case "4":
            return "four";
        case "5":
            return "five";
        case "6":
            return "six";
        case "7":
            return "seven";
        case "8":
            return "eight";
        case "9":
            return "nine";
    }
}

/*$num = 500254.89;
$test = convertNumber($num);

echo $test;*/

?>
<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <title>TST Japan Co.Ltd.</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>resource/css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>resource/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>resource/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>resource/css/responsive.css">

    <style>
        body {
            -webkit-print-color-adjust: exact;
        }

        @media print {
            @page {
                size: A4;
            }

            .payment_table h3{
                font-size: 21px;
                text-align: center;
                font-family: 'Open Sans', sans-serif;
                text-decoration: underline;
                padding: 0;
            }
            .payment_text p{
                line-height: 2.1;
            }
            .invoice_lc p {
                font-size: 10px;
                font-weight: bold;
            }
            .custr_singature {
                margin: 70px 50px;
            }
            .payment_instruction{
                margin-left:10px;
            }

            .Clogo img{width: 120px;}
            .Cname img{width: 225px}
        }


    </style>

<body>
<section id="invoice">
    <div class="invoice_body">
        <div class="Clogo"><img src="<?php echo base_url();?>resource/images/logo.png" alt="Company Logo" /></div>
        <div class="Cname"><img src="<?php echo base_url();?>resource/images/company-name.png" alt="Company Name" /></div>
        <div class="clear"></div>
        <div class="invoice_text"><p>PROFORMA INVOICE</p></div>
        <div class="clear"></div>
        <div class="invoice_text2"><p>Invoice No. : TSTJ<?php echo date('Ymd').rand(1,50)?></p></div>
        <div class="invoice_text3"><p>Date: <?php echo date("d-F-Y"); ?></p></div>
        <div class="clear"></div>
        <div class="invoice_text4"><p>DESCRIPTION OF GOODS</p></div>
        <div class="invoice_text5"><p>(USED/RE-CONDITIONED MOTOR VEHICLES)</p></div>
        <div class="invoice_table">
            <table class="table table-bordered text-center">
                <thead>
                <tr>
                    <th>SL.</th>
                    <th>Model Year</th>
                    <th>Make</th>
                    <th>Model</th>
                    <th>Chassis</th>
                    <th>Unit Price</th>
                    <th>Freight</th>
                    <th>Amount(USD)</th>
                </tr>
                </thead>
                <tbody>
                <?php
                $dollar =$this->select_model->select_dollar_price();
                $customer_id=$this->session->userdata('fuser_id');
                $query=$this->select_model->select_order_list($customer_id);

                $customer = $this->select_model->Select_Single_Row($customer_id,'tst_user','user_id');
//                print_r($customer);die;

                $sl=1;
                $grand_total = 0;
                foreach ($query->result() as $row)
                {
                $unit_price = round((floatval($row->price) / floatval($dollar['dollar_price'])));
                $transport_cost =$this->select_model->select_transport_cost($row->country_id,$row->port_id);
                $freight ='';
                $freight = round(($transport_cost->per_cubic_meter_price * $row->total_dimension) + ($transport_cost->insurance_price + $transport_cost->insurance_price)) ;
                $total = $unit_price + $freight;
                    $grand_total += $total;
                ?>
                <tr>
                    <td><?php echo $sl; ?></td>
                    <td class="center"><?php echo $row->name;?></td>
                    <td class="center"><?php echo $row->model_name;?></td>
                    <td class="center"><?php echo $row->manufacture_year;?></td>
                    <td class="center"><?php echo $row->vin;?></td>
                    <td class="center">$<?php  echo $unit_price;?></td>
                    <td class="center">$<?php echo $freight;?></td>
                    <td class="center">$<?php echo $total;?></td>
                </tr>
                    <?php
                    $sl++;
                }
                ?>
                <tr>
                    <td colspan="6"></td>
                    <td> Total </td>
                    <td>$<?php echo $grand_total;?></td>
                </tr>
                </tbody>
            </table>
            <div class="invoice_text2"><p><b>Total Units: <?php echo $sl-1;?></b></p></div>
            <div class="invoice_text3"><p><b>Total Amount: <?php echo convertNumber($grand_total.'.00');?> Only (USD)</b></p></div>
            <div class="clear"></div>
        </div>
        <section id="invoice2">
            <section id="first_div">
                <div class="payment_table">
                    <table>
                        <tr>
                            <td style="50%"><h3>Payment instruction (important)</h3></td>
                            <td style="50%"> <h3>Bill To</h3></td>
                        </tr>
                        <tr>
                            <td>
                                <div class="row">
                                    <div class="col-md-12">

                                        <table class="payment_instruction">
                                            <tr>
                                                <td style="width:38%;overflow:hidden">A/C Name</td>
                                                <td style="width:5%;overflow:hidden">:</td>
                                                <th>TST JAPAN CO.LTD.</th>
                                            </tr>

                                            <tr>
                                                <td>Bank Address</td>
                                                <td>: </td>
                                                <th>5-12-4 HONCHO KOGANEICITY TOKYO,JAPAN</th>
                                            </tr>
                                            <tr>
                                                <td>Account No</td>
                                                <td>: </td>
                                                <th>3958772</th>
                                            </tr>
                                            <tr>
                                                <td>Account Types</td>
                                                <td>: </td>
                                                <th>Ordinary</th>
                                            </tr>
                                            <tr>
                                                <td>Swift</td>
                                                <td>: </td>
                                                <th>SMBCJPJT</th>
                                            </tr>
                                            <tr>
                                                <td>Branch Name</td>
                                                <td>: </td>
                                                <th>KOGANEI BRANCH</th>
                                            </tr>
                                            <tr>
                                                <td>Bank Name</td>
                                                <td>:</td>
                                                <th class="sumit_bank">SUMITOMO MITSUI BANKING CORPORATION</th>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </td>
                            <td>
                                <div class="row">
                                    <div class="col-md-12"><br/>

                                        <table class="payment_instruction">
                                            <tr>
                                                <td style="width:52%;overflow:hidden">Importer Name</td>
                                                <td style="width:7%;overflow:hidden">:</td>
                                                <th><?php echo ($customer['company_name'] !='') ? $customer['company_name'] :'N/A';?></th>
                                            </tr>
                                            <tr>
                                                <td>Address</td>
                                                <td>: </td>
                                                <th><?php echo $customer['address']; ?></th>
                                            </tr>
                                            <tr>
                                                <td>Counrty</td>
                                                <td>: </td>
                                                <th><?php echo $customer['country']; ?></th>
                                            </tr>
                                            <tr>
                                                <td>Phone</td>
                                                <td>: </td>
                                                <th><?php echo $customer['mobile']; ?></th>
                                            </tr>
                                            <tr>
                                                <td>Contact person</td>
                                                <td>: </td>
                                                <th><?php echo $customer['first_name'].' '.$customer['last_name']?></th>
                                            </tr>
                                            <tr>
                                                <td>Shipping Marks</td>
                                                <td>: </td>
                                                <th>RUNS AUTO/SHARIF</th>
                                            </tr>
                                            <tr>
                                                <td>H.S.CODE</td>
                                                <td>: </td>
                                                <th></th>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </section>
            <div class="page-break"></div>
            <div style="clear:both"></div>
            <section id="conditon">
                <div class="invoice_bor"> </div>
                <table>
                    <tr>
                        <td>
                            <div class="col-md-12 col-sm-12">
                                <P style="font-size: 10px">SHIPPED PER:TO BE ANNOUNCED</P>
                                <P style="font-size: 10px">FROM:ANY PORT JAPAN</P>
                            </div>
                        </td>
                        <td>
                            <div class="col-md-12 col-sm-12">
                                <P style="font-size: 10px">COUNTRY OF ORIGIN:JAPAN</P>
                                <P style="font-size: 10px">TO:CHITTAGONG,BANGLADESH</P>
                            </div>
                        </td>
                        <td>
                            <div class="col-md-12 col-sm-12">
                                <P style="font-size: 10px">SAILING ON OR ABOUT:TO BE ANNOUNCED</P>
                                <P style="font-size: 10px">TERMS:LC</P>
                            </div>
                        </td>
                    </tr>
                </table>
                <div style="clear:both"></div>
                <div class="invoice_pr"></div>
                <p><span class="condition_text">Conditions:</span>1.valid negotiation:15 days after shipment; 2.Transshipment:Allowed; 3.part shipment:Allowed</p>
            </section>
            <section id="seal">
                <div class="seal_authorized">
                    <table>
                        <tr>
                            <td style="width: 50%">
                                <div class="col-md-12">
                                    <h4>Authorized Signature & Seal:</h4><br/><br/>
                                    <h5>Aota Morshed</h5>
                                    <h5>TST.Japan Co.Ltd Company Seal</h5>
                                </div>
                            </td>
                            <td>
                                <div class="col-md-12 ">
                                    <h5 class="custr_singature">Customer Signature & Seal</h5>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </section>
            <div style="clear:both"></div>
            <section id="invoice_footer">
                <div class="seal_authorized_text">
                    <table>
                        <tr>
                            <td style="width: 50%;">
                                <div class="col-md-12">
                                    <h4>HEAD OFFICE</h4>
                                    <P>Tokyo-to,koganei-shi, HOncho 1 chome 11-16</P>
                                    <p>Sunrise 201,Musashi Koganei, Tokyo !</p><p> 184-0004,japan.</p>
                                </div>
                            </td>
                            <td style="width: 50%;">
                                <div class="col-md-12">
                                    <br/><p> Tel: +81-50-1478-9691,Cell: +81-90-6141-9061<p>
                                    <p>FAX: 81-50-1475-9691, email:info@tstjapan.co.jp</p>
                                    <p> web.www.tstjapan.co.jp</p>
                                </div>
                            </td>
                        </tr>
                    </table>
                    <!--<div style="clear:both"></div>
                    <div class="foter_bg"></div>-->
                </div>
            </section>
        </section>
    </div>
    <p><small>Powered By : Ingenious Technologies, Bangladesh.</small></p>
    </section>
</body>
</html>