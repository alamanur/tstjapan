<div class="col-md-8">
    <h2 class="notice_heading">There is no notice for you</h2>
    <div class="i-notice">
        <div class="i-notice-head">
            Important Notification
        </div>
        <p>
            There are important notification. Please click the link to check the details. <span>In the case of not checking the detail may cause the cancellation of the deal.</span>
        </p>
        <div class="col-md-5">
            <select class="form-control">
                <option>Unread / Progress</option>
                <option>Correspondence Completed</option>
                <option>Limit of Correspondence Expired</option>
                <option>All</option>
            </select>
        </div>
        <a href="#"><div class="btn btn-primary btn-sm">Search</div></a><br/>
        <a href="index.php"><p class="pull-right"><i class="fa fa-caret-right"></i> Check Detail Later</p></a>
        <p> No Important Notification</p>
    </div>
</div>