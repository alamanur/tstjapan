<?php
if(isset($_GET['image_id']))
{
    $id=$_GET['image_id'];
    $table='tbl_attachment';
    $id_field='id';
    $this->delete_model->Delete_Single_Row($id,$table,$id_field);
}
?>

<div id="page-wrapper" >
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <h2>Vehicle / Car Images List</h2>
            </div>
        </div>
        <!-- /. ROW  -->
        <div class="row">
            <div class="col-md-12">
                <!-- Advanced Tables -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Car Images List
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="carList">
                                <thead>
                                <tr>
                                    <th>SL No</th>
                                    <th>Make</th>
                                    <th>Model</th>
                                    <th>Vehicle ID</th>
                                    <th>Images</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $query=$this->select_model->Select_all_images();

                                $sl=1;
                                foreach ($query->result() as $row)
                                {
                                    ?>
                                    <tr class="odd gradeX">
                                        <td class="text-center"><?php echo $sl; ?></td>
                                        <td class="text-center"><?php echo $row->make_name; ?></td>
                                        <td class="text-center"><?php echo $row->model_name; ?></td>
                                        <td class="text-center"><?php echo $row->product_id; ?></td>
                                        <td class="text-center"><img src="<?php echo base_url();?>resource/images/car_images/<?php echo $row->attachment_name; ?>" width="100"></td>
                                        <td class="center text-center"><a href="<?php echo base_url(); ?>backdoor/vehicle_image_list?image_id=<?php echo $row->id;?>" onclick="return confirm('Are you really want to delete this item')"><i class="glyphicon glyphicon-remove red "></a></td>
                                    </tr>
                                    <?php
                                    $sl++;
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <!--End Advanced Tables -->
            </div>
        </div>
        <!-- /. ROW  -->
    </div>
    <!-- /. PAGE INNER  -->
</div><!-- /. PAGE WRAPPER  -->
