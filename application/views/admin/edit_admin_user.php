<?php
if(isset($_GET['id']))
{
    $id=$_GET['id'];
    $table='tst_admin_user';
    $id_field='admin_user_id';
    $row=$this->select_model->Select_Single_Row($id,$table,$id_field);
   /* print_r($row);
    die;*/
}else{
    $row['admin_user_id']='';
    $row['admin_first_name']='';
    $row['admin_last_name']='';
    $row['admin_email']='';
    $row['admin_role']='';
    $row['admin_phone']='';
    $row['status']='';
    $row['admin_address']='';
//    $row['current_password']='';

}
?>

<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">

            <div class="col-md-12">
                <h2>Update Admin User Info</h2>

            </div>
        </div>
        <!-- /. ROW  -->
        <hr/>
        <div class="col-md-12">
            <?php
            //-----Display Success or Error message---
            if(isset($feedback)){
                echo $feedback;
            }
            ?>
        </div>
        <div class="row">
        <div class="box-content"  >
           <form id="edit_admin_user" action="<?php echo base_url();?>backdoor/edit_admin_user" method="post" enctype="multipart/form-data">
        </div>
            <div class="col-md-6 col-sm-12 col-xs-6 ">

                <div class="form-group">
                    <?php
                    echo form_label('First Name', 'txtFirstName');
                    $attributes=array(
                        'name'=>'txtFirstName',
                        'class'=>'form-control',
                        'maxlength'   => '25',
                        'placeholder'=>'Enter First Name',
                        'value' => $row['admin_first_name'],
                    );
                    echo form_input($attributes);
                    ?>
                    <input type="hidden" name="txtAdminUserId" value="<?php echo $row['admin_user_id'];?> ">
                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtFirstName');?></label>
                </div>

                <div class="form-group">
                    <?php
                    echo form_label('Last Name', 'txtLastName');
                    $attributes=array(
                        'name'=>'txtLastName',
                        'class'=>'form-control',
                        'maxlength' => '25',
                        'placeholder'=>'Enter Last Name',
                        'value' => $row['admin_last_name'],
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtLastName');?></label>
                </div>

                <div class="form-group">
                    <?php
                    echo form_label('Email Address', 'txtEmailAddress');
                    $attributes=array(
                        'name'=>'txtEmailAddress',
                        'class'=>'form-control',
                        'maxlength'   => '70',
                        'placeholder'=>'Enter Email Address',
                        'value' =>$row['admin_email']
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtEmailAddress');?></label>
                </div>


                <div class="form-group">
                    <?php
                    echo form_label('Address', 'txtAddress');
                    $attributes=array(
                        'name'=>'txtAddress',
                        'class'=>'form-control',
                        'maxlength'   => '250',
                        'placeholder'=>'Write  Address',
                        'value' => $row['admin_address']
                    );
                    echo form_textarea($attributes);
                    ?>
                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtAddress');?></label>
                </div>
            </div>

        <div class="col-md-6 col-sm-12 col-xs-6 ">


            <div class="form-group">
                <?php
                echo form_label('Phone Number', 'txtPhone');
                $attributes=array(
                    'name'=>'txtPhone',
                    'class'=>'form-control',
                    'maxlength'   => '15',
                    'placeholder'=>'Write Phone Number',
                    'value' => $row['admin_phone']
                );
                echo form_input($attributes);
                ?>
            </div>
            <div class="form-group">
                <label class="red"><?php echo form_error('txtPhone');?></label>
            </div>

            <div class="form-group">
                <?php
                echo form_label('Password', 'txtPassword');
                $attributes=array(
                    'name'=>'txtPassword',
                    'class'=>'form-control',
                    'maxlength'   => '15',
                    'required' => 'required',
                    'placeholder'=>'Write Password',
                    'value' => set_value('txtPassword')
                );
                echo form_password($attributes);
                ?>
            </div>
            <div class="form-group">
                <label class="red"><?php echo form_error('txtPassword');?></label>
            </div>

            <div class="form-group">
                <?php
                echo form_label('Confirm Password', 'txtConfirmPassword');
                $attributes=array(
                    'name'=>'txtConfirmPassword',
                    'class'=>'form-control',
                    'maxlength'   => '15',
                    'required' => 'required',
                    'placeholder'=>'Confirm Password',
                    'value' => set_value('txtConfirmPassword'),
                );
                echo form_password($attributes);
                ?>
            </div>
            <div class="form-group">
                <label class="red"><?php echo form_error('txtConfirmPassword');?></label>
            </div>

            <div class="form-group">
                <label>Admin Role</label>

                <select pattern="[0-9]{1,3}"  name="txtAdminRole" id="txtAdminRole" data-rel="chosen" class="form-control">
                    <option value="">Select Admin Role</option>
                    <?php
                    $this->common_model->order_column = 'role_id';
                    $this->common_model->table_name = 'admin_user_role';
                    $query=$this->common_model->select_all();
                    foreach ($query->result() as $row1)
                    {
                        ?>
                        <option value="<?php echo $row1->role_id;?>"  <?php if(isset($row['admin_role']) && $row['admin_role'] == $row1->role_id){echo "selected";}?><?php echo set_select("txtAdminRole",$row1->role_id)?>><?php echo $row1->role_name;?></option>

                    <?php

                    }
                    ?>

                </select>

            </div>

            <div class="form-group">
                <?php
                echo form_label('Profile Picture', 'txtProfilePicture');
                $data = array(
                    'name'        => 'txtProfilePicture',
                    'id'          => 'txtProfilePicture',
                    'maxlength'   => '100',
                    'size'        => '50',
                    'class'       => 'form-control',
                    'placeholder'=> 'Select Profile Picture',
                    'value'=> set_value('txtProfilePicture')
                );

                echo form_upload($data);
                ?>
            </div>
            <div class="form-group">
                <label class="red"><?php echo form_error('txtProfilePicture');?></label>
            </div>

            <div class="checkbox">
                <label><input name="txtIsActive" <?php echo ($row['status'] == 1) ? 'checked' : '';?> value="1" type="checkbox"> IsActive</label>
            </div>
            <input type="submit" name="btnSubmit" class="btn btn-danger" value="Update">

        </div>
      </form>
    </div>
</div>
<!-- /. ROW  -->
</div><!-- /. PAGE INNER  -->
</div><!-- /. PAGE WRAPPER  -->
