<?php

if(isset($_GET['id']))
{
    $id=$_GET['id'];
    $table='tbl_model';
    $id_field='model_id';
    $row=$this->select_model->Select_Single_Row($id,$table,$id_field);
    /*print_r($result);
        die;*/
}else{
    $row['model_id']='';
    $row['model_name']='';
    $row['make_id']='';

}
?>

<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-6">
                <h2>Update Model</h2>
            </div>
            <div class="col-md-6">
                <a href="<?php echo base_url();?>backdoor/model_list/" class="btn btn-danger pull-right" style="margin-top: 18px;">Model List</a>
            </div>
        </div>
        <!-- /. ROW  -->
        <hr/>
        <div class="row">
            <div class="col-md-offset-3 col-md-6 col-sm-12 col-xs-4 ">
                        <div class="box-content"  >
                            <?php
                            //-----Display Success or Error message---
                            if(isset($feedback)){
                                echo $feedback;
                            }
                            //----Form Tag Start-------------
                            $attributes = array('class' => 'email', 'id' => 'myform');

                            echo form_open('backdoor/edit_model', $attributes);
                            ?>
                        </div>
                        <div class="form-group">
                            <label>Manufacturar Name</label>
                            <input type="hidden" name="txtModelId" value="<?php echo $row['model_id']; ?>">
                            <select name="txtMakeId" id="txtMakeId" class="form-control">
                                <option value="">Select One</option>
                                <?php
                                $result=$this->select_model->select_all('tbl_make',null,$id_field="make_id");
                                //print_r($result);
                                foreach($result->result() as $row1){
                                    ?>
                                    <option value="<?php echo $row1->make_id;?>" <?php if(isset($row["make_id"]) && $row["make_id"]==$row1->make_id){echo "selected='select'";}?><?php echo set_select("txtMakeId",$row1->make_id)?>><?php echo $row1->name ; ?></option>';
                                    <?php
                                }?>
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="red"><?php echo form_error('txtMakeId');?></label>

                        </div>

                        <div class="form-group">
                            <label>Model Name</label>
                            <?php

                            $attributes=array(
                                'name'=>'txtModelName',
                                'class'=>'form-control',
                                'placeholder'=>'Write Model Name',
                                'maxlength'   => '70',
                                'value' => $row['model_name']
                            );
                            echo form_input($attributes);
                            ?>
                        </div>
                        <div class="form-group">
                            <label class="red"><?php echo form_error('txtModelName');?></label>
                        </div>
                        <?php
                        $attribute=array(
                            'name'=>'btnSubmit',
                            'class'=>'btn btn-danger ',
                            'value'=>'Submit',

                        );
                        echo form_submit($attribute);//--Form Submit Button
                        echo form_close();//--Form closing tag </form>
                        ?>
                </div>

        </div>
    </div>
    <!-- /. ROW  -->
</div><!-- /. PAGE INNER  -->
</div><!-- /. PAGE WRAPPER  -->
