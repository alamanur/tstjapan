<?php

if(isset($_GET['id']))
{
    $id=$_GET['id'];
    $table='tbl_category';
    $id_field='category_id';
    $result=$this->select_model->Select_Single_Row($id,$table,$id_field);
    /*print_r($row);
        die;*/
}else{
    $result['category_id']='';
    $result['name']='';
    $result['remarks']='';
}

?>

<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-6">
                <h2>Update Vehicle Category</h2>
            </div>
            <div class="col-md-6">
                <a href="<?php echo base_url();?>backdoor/category_list/" class="btn btn-danger pull-right" style="margin-top: 18px;">Vehicle Category List</a>
            </div>
        </div>
        <!-- /. ROW  -->
        <hr/>
        <div class="row">
            <div class="col-md-offset-3 col-md-6 col-sm-12 col-xs-6 ">
                <div class="form" >
                    <div class="box-content"  >
                        <?php
                        //-----Display Success or Error message---
                        if(isset($feedback)){
                            echo $feedback;
                        }
                        //----Form Tag Start-------------
                        $attributes = array('class' => 'email', 'id' => 'myform');

                        echo form_open('backdoor/edit_vehicle_category', $attributes);
                        ?>
                    </div>
                    <div class="form-group">
                        <label>Vehicle Category</label>
                        <?php
                        $data = array(  //--For hidden text field--
                            'txtVehicleCategoryId'  => $result['category_id'],
                        );

                        echo form_hidden($data);

                        $attributes=array(
                            'name'=>'txtVehicleCategory',
                            'class'=>'form-control',
                            'maxlength'   => '40',
                            'placeholder'=>'Write Vehicle Category',
                            'value' => $result['name'],
                        );
                        echo form_input($attributes);
                        ?>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtVehicleCategory');?></label>

                    </div>

                    <div class="form-group">
                        <label>Remarks</label>
                        <?php
                        $attributes=array(
                            'name'=>'txtRemarks',
                            'class'=>'form-control',
                            'placeholder'=>'Write Remarks',
                            'maxlength'   => '100',
                            'value' => $result['remarks'],
                        );
                        echo form_input($attributes);
                        ?>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtRemarks');?></label>
                    </div>
                    <?php
                    $attribute=array(
                        'name'=>'btnSubmit',
                        'class'=>'btn btn-danger ',
                        'value'=>'Update',

                    );
                    echo form_submit($attribute);//--Form Submit Button
                    echo form_close();//--Form closing tag </form>
                    ?>
                </div>
            </div>

        </div>
    </div>
    <!-- /. ROW  -->
</div><!-- /. PAGE INNER  -->
</div><!-- /. PAGE WRAPPER  -->
