<?php
if(isset($_GET['id']))
{
    $id=$_GET['id'];
    $table='tbl_company_information';
    $id_field='company_id';
    $row=$this->select_model->Select_Single_Row($id,$table,$id_field);
    /*print_r($row);
        die;*/
}else{
    $row['company_id']='';
    $row['company_name']='';
    $row['company_tag']='';
    $row['ownership_type']='';
    $row['email_address']='';
    $row['payment_terms']='';
    $row['phone_no']='';
    $row['established_year']='';
    $row['no_of_employee']='';
    $row['business_classification']='';
    $row['language']='';
    $row['country']='';
    $row['description']='';
    $row['address']='';
    $row['status']='';

}
?>
<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-6">
                <h2>Update Company Information</h2>
            </div>
            <div class="col-md-6">
                <a href="<?php echo base_url();?>backdoor/seller_list/" class="btn btn-danger pull-right" style="margin-top: 18px;">Seller List</a>
            </div>
        </div>
        <!-- /. ROW  -->
        <hr/>
        <div class="row">
            <div class="col-md-6 col-sm-12 col-xs-12 ">
                <form action="" method="post" enctype="multipart/form-data">
                    <div class="box-content"  >
                        <?php
                        //-----Display Success or Error message---
                        if(isset($feedback)){
                            echo $feedback;
                        }
                        ?>
                    </div>
                    <div class="form-group">
                        <label>Conpany Name</label>
                        <input type="hidden" name="txtCompanyId" value="<?php if(isset($row["company_id"]) && $row["company_id"] != ""){echo $row["company_id"];}else{echo set_value('txtCompanyId');} ?>">
                        <input type="text" class="form-control" name="txtCompanyName"  value="<?php if(isset($row["company_name"]) && $row["company_name"] != ""){echo $row["company_name"];}else{echo set_value('txtCompanyName');} ?>" placeholder="Enter Company Name" required="required"/>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtCompanyName');?></label>
                    </div>

                    <div class="form-group">
                        <label>Company Tag</label>
                        <input type="text" class="form-control" name="txtCompanyTag"  value="<?php if(isset($row["company_tag"]) && $row["company_tag"] != ""){echo $row["company_tag"];}else{echo set_value('txtCompanyTag');} ?>"  placeholder="Your favorites vehicles available!" required="required"/>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtCompanyTag');?></label>
                    </div>
                    <div class="form-group">
                        <label>Company Ownership Type</label>
                        <input type="text" class="form-control" name="txtOwnership"  value="<?php if(isset($row["ownership_type"]) && $row["ownership_type"] != ""){echo $row["ownership_type"];}else{echo set_value('txtOwnership');} ?>"  placeholder=" Privet Ltd/ Partnership " required="required"/>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtOwnership');?></label>
                    </div>

                    <div class="form-group">
                        <label>Email Address</label>
                        <input type="email" class="form-control" name="txtEmailAddress" value="<?php if(isset($row["email_address"]) && $row["email_address"] != ""){echo $row["email_address"];}else{echo set_value('txtEmailAddress');} ?>" placeholder="Ex: info@example.com" required="required"/>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtEmailAddress');?></label>
                    </div>

                    <div class="form-group">
                        <label>Phone No</label>
                        <input type="tel" class="form-control" name="txtPhone"  value="<?php if(isset($row["phone_no"]) && $row["phone_no"] != ""){echo $row["phone_no"];}else{echo set_value('txtPhone');} ?>" placeholder="Enter Phone no" required="required"/>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtPhone');?></label>
                    </div>

                    <div class="form-group">
                        <label>Payment Terms</label>
                        <input type="text" class="form-control" name="txtPaymentTerms"  value="<?php if(isset($row["payment_terms"]) && $row["payment_terms"] != ""){echo $row["payment_terms"];}else{echo set_value('txtPaymentTerms');} ?>" placeholder="T/T L/C Other(Paypal)" required="required"/>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtPaymentTerms');?></label>
                    </div>

                    <div class="form-group">
                        <label>Established Year</label>
                        <select name="txtEstablishedYear" class="form-control" >
                            <option value="">Select One</option>
                            <?php
                            $y= date("Y")+1;
                            for ($i = 0;$i < 50;$i++)
                            {
                                $y -=1;
                                ?>
                                <option value="<?php echo $y; ?>" <?php if(isset($row['established_year']) && $row['established_year'] == $y){echo "selected='selected'";}?><?php echo set_select("txtEstablishedYear",$y)?>><?php echo $y ; ?></option>';
                                <?php
                            }
                            ?>

                        </select>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtEstablishedYear');?></label>
                    </div>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-12 ">
                <div class="form-group">
                    <label>No of Employee</label>
                    <input type="text" class="form-control" name="txtNoOfEmployee" value="<?php if(isset($row["no_of_employee"]) && $row["no_of_employee"] != ""){echo $row["no_of_employee"];}else{echo set_value('txtNoOfEmployee');} ?>"  placeholder=" 11 - 50 People" required="required"/>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtNoOfEmployee');?></label>
                </div>

                <div class="form-group">
                    <label>Business Classification</label>
                    <input type="text" class="form-control" name="txtBusinessClassification" value="<?php if(isset($row["business_classification"]) && $row["business_classification"] != ""){echo $row["business_classification"];}else{echo set_value('txtBusinessClassification');} ?>" placeholder=" Importer/Exporter" required="required"/>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtBusinessClassification');?></label>
                </div>

                <div class="form-group">
                    <label>Language</label>
                    <input type="text" class="form-control" name="txtLanguage"  value="<?php if(isset($row["language"]) && $row["language"] != ""){echo $row["language"];}else{echo set_value('txtLanguage');} ?>" placeholder=" English/Japanese" required="required"/>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtLanguage');?></label>
                </div>

                <div class="form-group">
                    <label>Country</label>
                    <select name="txtCountry" id="txtCountryId" class="form-control">
                        <option value="">Select One</option>
                        <?php
                        $result=$this->select_model->select_all('tbl_country');
                        //print_r($result);
                        foreach($result->result() as $row1){
                            ?>
                            <option value="<?php echo $row1->id;?>" <?php if(isset($row["country"]) && $row["country"]==$row1->id){echo "selected='selected'";}else{ echo set_select("txtCountry",$row1->id);} ?>><?php echo $row1->country_name ; ?></option>';
                            <?php
                        }?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtCountry');?></label>
                </div>
                <div class="form-group">

                    <label>Company Description</label>
                    <textarea class="form-control" rows="3" name="txtDescription" placeholder="Enter Description" required="required"><?php if(isset($row["description"]) && $row["description"] != ""){echo $row["description"];}else{echo set_value('txtDescription');} ?></textarea>

                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtDescription');?></label>
                </div>
                <div class="form-group">
                    <label>Company Address</label>
                    <textarea class="form-control" rows="3" name="txtAddress"placeholder="Enter Description" required="required"><?php if(isset($row["address"]) && $row["address"] != ""){echo $row["address"];}else{echo set_value('txtAddress');} ?></textarea>

                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtAddress');?></label>
                </div>
                <div class="checkbox">
                    <label><input name="txtIsActive" type="checkbox" value="1" checked> IsActive</label>
                </div>

                <div class="form-group">
                    <label>Company Logo</label>
                    <input type="file" class="form-control" name="txtLogo" placeholder="Select Logo"/>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtLogo');?></label>
                </div>

                <input type="submit" name="btnSubmit" value="Save" class="btn btn-danger" >
                </form>
            </div>
        </div>
    </div>
    <!-- /. ROW  -->
</div><!-- /. PAGE INNER  -->
</div><!-- /. PAGE WRAPPER  -->
