<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-6">
                <h2>Create Transportation Cost</h2>
            </div>
            <div class="col-md-6">
                <a href="<?php echo base_url();?>backdoor/transportation_cost_list/" class="btn btn-danger pull-right" style="margin-top: 18px;">Transport Cost List</a>
            </div>
        </div>
        <!-- /. ROW  -->
        <hr/>
        <div class="row">
            <div class=" col-md-offset-3 col-md-6 col-sm-12 col-xs-4 ">
                <div class="form" >
                    <form id="transportationCost" action="<?php echo base_url();?>backdoor/transportation_cost" method="post" >
                    <div class="box-content"  >
                        <?php
                        //-----Display Success or Error message---
                        if(isset($feedback)){
                            echo $feedback;
                        }
                        //----Form Tag Start-------------
                        ?>
                    </div>
                    <div class="form-group">
                        <label>Country</label>
                        <select name="txtCountry" id="txtCountryId" class="form-control">
                        <?php
                        echo $this->select_model->Select_country();
                        ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtCountry');?></label>

                    </div>

                    <div class="form-group">
                        <label>Nearest Port</label>
                        <select id="txtNearestPortId" name="txtNearestPort" class="form-control">
                            <option value="">Select One</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtNearestPort');?></label>
                    </div>

                    <div class="form-group">
                        <label>Per Cubic Meter Cost ($)</label>
                        <input type="text" name="txtPerCubicMeterCost" class="form-control" value="<?php echo set_value("txtPerCubicMeterCost") ;?>" placeholder="Ex : 10" required="required">

                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtPerCubicMeterCost');?></label>
                    </div>

                    <div class="form-group">
                        <label>Insurance Cost ($)</label>
                        <input type="text" name="txtInsurance" class="form-control" value="<?php echo set_value("txtInsurance") ;?>" placeholder="Ex : 10" required="required">
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtInsurance');?></label>
                    </div>

                    <div class="form-group">
                        <label>Inspection Cost </label>
                        <input type="text" name="txtInspection" class="form-control" value="<?php echo set_value("txtInspection") ;?>" placeholder="Ex : 10" required="required">
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtInspection');?></label>
                    </div>

                    <?php
                    $attribute=array(
                        'name'=>'btnSubmit',
                        'class'=>'btn btn-danger ',
                        'value'=>'Submit',

                    );
                    echo form_submit($attribute);//--Form Submit Button

                    ?>
                </div>
            </div>
            </form>
        </div>
    </div>
    <!-- /. ROW  -->
</div><!-- /. PAGE INNER  -->
</div><!-- /. PAGE WRAPPER  -->
