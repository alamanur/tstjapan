
<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-6">
                <h2>Create Engine Type</h2>
            </div>
            <div class="col-md-6">
                <a href="<?php echo base_url();?>backdoor/engine_type_list/" class="btn btn-danger pull-right" style="margin-top: 18px;">Engine Type List</a>
            </div>
        </div>
        <!-- /. ROW  -->
        <hr/>
        <div class="row">
            <div class="col-md-offset-3 col-md-6 col-sm-12 col-xs-4 ">
                <div class="form" >
                    <div class="box-content"  >
                        <?php
                        //-----Display Success or Error message---
                        if(isset($feedback)){
                            echo $feedback;
                        }
                        //----Form Tag Start-------------
                        $attributes = array('class' => 'email', 'id' => 'myform');

                        echo form_open('backdoor/engine_type', $attributes);
                        ?>
                    </div>
                    <div class="form-group">
                        <label>Manufacturar Name</label>
                        <select name="txtMakeId" id="txtMakeId" class="form-control">
                            <?php
                            echo $this->select_model->Select_box($table='tbl_make');
                            ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtMakeId');?></label>

                    </div>

                    <div class="form-group">
                        <label>Model Name</label>
                        <select name="txtModel" id="txtModelId" class="form-control">
                            <option value="">Select One</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtModel');?></label>
                    </div>

                    <div class="form-group">
                        <label>Engine Type</label>
                        <?php
                        $attributes=array(
                            'name'=>'txtEngineType',
                            'class'=>'form-control',
                            'placeholder'=>'Write Engine Type',
                            'maxlength'   => '70',
                            'value' => set_value('txtEngineType'),
                        );
                        echo form_input($attributes);
                        ?>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtEngineType');?></label>
                    </div>
                    <div class="form-group">
                        <label>Remarks</label>
                        <?php
                        $attributes=array(
                            'name'=>'txtRemarks',
                            'class'=>'form-control',
                            'placeholder'=>'Write Remarks',
                            'maxlength'   => '120',
                            'value' => set_value('txtRemarks'),
                        );
                        echo form_input($attributes);
                        ?>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtRemarks');?></label>
                    </div>
                    <?php
                    $attribute=array(
                        'name'=>'btnSubmit',
                        'class'=>'btn btn-danger ',
                        'value'=>'Submit',

                    );
                    echo form_submit($attribute);//--Form Submit Button
                    echo form_close();//--Form closing tag </form>
                    ?>
                </div>
            </div>

        </div>
    </div>
    <!-- /. ROW  -->
</div><!-- /. PAGE INNER  -->
</div><!-- /. PAGE WRAPPER  -->
