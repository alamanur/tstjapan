<?php
    if(isset($_GET['id']))
    {
        $id=$_GET['id'];
        $table='tbl_make';
        $id_field='make_id';
        $result=$this->select_model->Select_Single_Row($id,$table,$id_field);
        /*print_r($row);
            die;*/
    }else{
        $result['make_id']='';
        $result['name']='';
        $result['remarks']='';
    }

?>

<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-6">
                <h2>Update Manufacturar</h2>
            </div>
            <div class="col-md-6">
                <a href="<?php echo base_url();?>backdoor/make_list/" class="btn btn-danger pull-right" style="margin-top: 18px;">Manufacturar List</a>
            </div>
        </div>
        <!-- /. ROW  -->
        <hr/>
        <div class="row">
            <div class=" col-md-offset-3 col-md-6 col-sm-12 col-xs-6 ">
                <div class="form" >
                        <div class="box-content"  >
                            <?php
                            //-----Display Success or Error message---
                            if(isset($feedback)){
                                echo $feedback;
                            }
                            //----Form Tag Start-------------
                            $attributes = array('class' => 'email', 'id' => 'myform');

                            echo form_open('backdoor/edit_make', $attributes);
                            ?>
                        </div>
                        <div class="form-group">
                            <label>Manufacturar Name</label>
                            <?php
                            $data = array(  //--For hidden text field--
                                'txtMakeId'  => $result['make_id'],
                            );

                            echo form_hidden($data);

                            $attributes=array(
                                'name'=>'txtManufacturar',
                                'class'=>'form-control',
                                'maxlength'   => '40',
                                'placeholder'=>'Write Manufacturar Name',
                                'value' => $result['name'],
                            );
                            echo form_input($attributes);
                            ?>
                        </div>
                        <div class="form-group">
                            <label class="red"><?php echo form_error('txtManufacturar');?></label>

                        </div>

                        <div class="form-group">
                            <label>Remarks</label>
                            <?php
                            $attributes=array(
                                'name'=>'txtRemarks',
                                'class'=>'form-control',
                                'placeholder'=>'Write Remarks',
                                'maxlength'   => '100',
                                'value' => $result['remarks'],
                            );
                            echo form_input($attributes);
                            ?>
                        </div>
                        <div class="form-group">
                            <label class="red"><?php echo form_error('txtRemarks');?></label>
                        </div>
                        <?php
                        $attribute=array(
                            'name'=>'btnSubmit',
                            'class'=>'btn btn-danger ',
                            'value'=>'Update',

                        );
                        echo form_submit($attribute);//--Form Submit Button
                        echo form_close();//--Form closing tag </form>
                        ?>
                </div>
            </div>
        </div>
    </div>
    <!-- /. ROW  -->
</div><!-- /. PAGE INNER  -->
</div><!-- /. PAGE WRAPPER  -->
