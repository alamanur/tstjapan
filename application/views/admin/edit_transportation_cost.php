<?php

if(isset($_GET['id']))
{
    $id=$_GET['id'];
    $table='tbl_transportation_cost';
    $id_field='id';
    $row=$this->select_model->Select_Single_Row($id,$table,$id_field);
    /*print_r($row);
        die;*/
}else{
    $row['country_id']='';
    $row['port_id']='';
    $row['id']='';
    $row['per_cubic_meter_price']='';
    $row['insurance_price']='';
    $row['inspection_price']='';

}
?>

<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-6">
                <h2>Update Transportation Cost</h2>
            </div>
            <div class="col-md-6">
                <a href="<?php echo base_url();?>backdoor/transportation_cost_list/" class="btn btn-danger pull-right" style="margin-top: 18px;">Transport Cost List</a>
            </div>
        </div>
        <!-- /. ROW  -->
        <hr/>
        <div class="row">
            <div class="col-md-offset-3 col-md-6 col-sm-12 col-xs-4 ">
                <div class="form" >
                    <div class="box-content"  >
                        <?php
                        //-----Display Success or Error message---
                        if(isset($feedback)){
                            echo $feedback;
                        }
                        //----Form Tag Start-------------
                        $attributes = array('class' => 'email', 'id' => 'editTransportationCost');

                        echo form_open('backdoor/edit_transportation_cost', $attributes);
                        ?>
                    </div>
                    <div class="form-group">
                        <label>Country</label>
                        <input type="hidden" name="txtTransportationCostId" value="<?php echo $row['id']; ?>">
                        <select name="txtCountry" id="txtCountryId" class="form-control">
                            <option value="">Select One</option>
                            <?php
                            $result=$this->select_model->select_all('tbl_country');
                            //print_r($result);
                            foreach($result->result() as $row1){
                            ?>
                            <option value="<?php echo $row1->id;?>" <?php if(isset($row["country_id"]) && $row["country_id"]==$row1->id){echo "selected='selected'";}else{ echo set_select("txtCountry",$row1->id);} ?>><?php echo $row1->country_name ; ?></option>';
                            <?php
                            }?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtCountry');?></label>

                    </div>

                    <div class="form-group">
                        <label>Nearest Name</label>
                        <select id="txtNearestPortId" name="txtNearestPort" class="form-control">
                            <option value="">Select One</option>
                            <?php
                            $result=$this->select_model->select_all('tbl_country_port',null,$id_field="id","country_id",$row['country_id']);
//                            print_r($result);
                            foreach($result->result() as $row1){
                                ?>
                                <option value="<?php echo $row1->id;?>" <?php if(isset($row["port_id"]) && $row["port_id"]==$row1->id){echo "selected='selected'";}?><?php echo set_select("txtNearestPort",$row1->id)?>><?php echo $row1->port_name ; ?></option>';
                            <?php
                            }?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtNearestPort');?></label>
                    </div>

                    <div class="form-group">
                        <label>Per Cubic Meter Cost ($)</label>
                        <input type="text" name="txtPerCubicMeterCost" class="form-control" value="<?php if(isset($row["per_cubic_meter_price"]) && $row["per_cubic_meter_price"] != ""){echo $row["per_cubic_meter_price"];}else{ echo set_value("txtPerCubicMeterCost") ;} ?>" placeholder="Ex : 10" required="required">

                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtPerCubicMeterCost');?></label>
                    </div>

                    <div class="form-group">
                        <label>Insurance Cost ($)</label>
                        <input type="text" name="txtInsurance" class="form-control" value="<?php if(isset($row["insurance_price"]) && $row["insurance_price"] != ""){echo $row["insurance_price"];}else{ echo set_value("txtInsurance") ;}?>" placeholder="Ex : 10" required="required">
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtInsurance');?></label>
                    </div>

                    <div class="form-group">
                        <label>Inspection Cost </label>
                        <input type="text" name="txtInspection" class="form-control" value="<?php if(isset($row["inspection_price"]) && $row["inspection_price"] != ""){echo $row["inspection_price"];}else{ echo set_value("txtInspection") ;}?>" placeholder="Ex : 10" required="required">
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtInspection');?></label>
                    </div>
                    <?php
                    $attribute=array(
                        'name'=>'btnSubmit',
                        'class'=>'btn btn-danger ',
                        'value'=>'Update',

                    );
                    echo form_submit($attribute);//--Form Submit Button
                    echo form_close();//--Form closing tag </form>
                    ?>
                </div>
            </div>

        </div>
    </div>
    <!-- /. ROW  -->
</div><!-- /. PAGE INNER  -->
</div><!-- /. PAGE WRAPPER  -->
