
<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <h2>Create Admin User</h2>

            </div>
        </div>
        <!-- /. ROW  -->
        <hr/>
        <div class="row">
            <form action="<?php echo base_url();?>backdoor/admin_user" method="post" enctype="multipart/form-data">
            <div class="col-md-6 col-sm-12 col-xs-6 ">
                <div class="form" >

                        <div class="box-content"  >
                            <?php
                            //-----Display Success or Error message---
                            if(isset($feedback)){
                                echo $feedback;
                            }
                            ?>
                        </div>
                        <div class="form-group">
                            <label>First Name</label>
                            <input type="text" class="form-control" name="txtFirstName" placeholder="Enter First Name" required="required"/>
                        </div>
                        <div class="form-group">
                            <label class="red"><?php echo form_error('txtFirstName');?></label>
                        </div>


                        <div class="form-group">
                            <label>Email Address</label>
                            <input type="email" class="form-control" name="txtEmailAddress" placeholder="Enter Email Address" required="required"/>
                        </div>
                        <div class="form-group">
                            <label class="red"><?php echo form_error('txtEmailAddress');?></label>
                        </div>



                        <div class="form-group">
                            <label>Phone No</label>
                            <input type="tel" class="form-control" name="txtPhone" placeholder="Enter Phone no" />
                        </div>
                        <div class="form-group">
                            <label class="red"><?php echo form_error('txtPhone');?></label>
                        </div>

                        <div class="form-group">
                            <label>Password</label>
                            <input type="password" class="form-control" name="txtPassword" placeholder="Enter Password" required="required"/>
                        </div>
                        <div class="form-group">
                            <label class="red"><?php echo form_error('txtPassword');?></label>
                        </div>


                        <div class="form-group">
                            <label>Admin Role</label>

                                <select pattern="[0-9]{1,3}"  name="txtAdminRole" id="txtAdminRole" data-rel="chosen" class="form-control">
                                    <option value="">Select Admin Role</option>
                                    <?php
                                    $this->common_model->order_column = 'role_id';
                                    $this->common_model->table_name = 'admin_user_role';
                                    $query=$this->common_model->select_all();
                                    foreach ($query->result() as $row)
                                    {
                                        ?>
                                        <option value="<?php echo $row->role_id;?>"><?php echo $row->role_name;?></option>
                                    <?php

                                    }
                                    ?>

                                </select>

                        </div>
                </div>
            </div>

            <div class="col-md-6 col-sm-12 col-xs-6 ">

                <div class="form-group">
                    <label>Last Name</label>
                    <input type="text" class="form-control" name="txtLastName" placeholder="Enter Last Name" required="required"/>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtLastName');?></label>
                </div>

                <div class="form-group">

                    <label>Address</label>
                    <textarea class="form-control" rows="3" name="txtAddress" placeholder="Enter Address"></textarea>

                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtAddress');?></label>
                </div>

                <div class="form-group">
                    <label>Re Password</label>
                    <input type="password" class="form-control" name="txtRePassword" placeholder="Enter Re Password" required="required"/>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtRePassword');?></label>
                </div>

                <div class="form-group">
                    <label>Profile Picture</label>
                    <input type="file" class="form-control" name="txtProfilePicture" placeholder="Select Profile Picture"/>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtProfilePicture');?></label>
                </div>

                <div class="checkbox">
                    <label><input name="txtIsActive" value="1" type="checkbox"> IsActive</label>
                </div>
                <input type="submit" name="btnSubmit" value="Save" class="btn btn-danger" >

            </div>

        </div>
        </form>
    </div>
    <!-- /. ROW  -->
</div><!-- /. PAGE INNER  -->
</div><!-- /. PAGE WRAPPER  -->
