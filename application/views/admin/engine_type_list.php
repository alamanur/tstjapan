<?php

if(isset($_GET['engine_type_id']))
{
    $id=$_GET['engine_type_id'];
    $table='tbl_engine_type';
    $id_field='id';
    $this->delete_model->Delete_Single_Row($id,$table,$id_field);
}
?>

<div id="page-wrapper" >
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <h2>Engine Type List</h2>
            </div>
        </div>
        <!-- /. ROW  -->
        <div class="row">

            <div class="col-md-12 col-sm-12 col-xs-8 ">
                <!-- Advanced Tables -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Engine Type List
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="car_model">
                                <thead>
                                <tr>
                                    <th>SL No</th>
                                    <th>Manufacturar</th>
                                    <th>Model</th>
                                    <th>Engine Type</th>
                                    <th>Remarks</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $query=$this->select_model->Select_engine_type();

                                $sl=1;
                                foreach ($query->result() as $row)
                                {
                                    ?>
                                    <tr class="odd gradeX">
                                        <td><?php echo $sl; ?></td>
                                        <td class="center"><?php echo $row->name;?></td>
                                        <td class="center"><?php echo $row->model_name;?></td>
                                        <td class="center"><?php echo $row->engine_type;?></td>
                                        <td class="center"><?php echo $row->remarks;?></td>
                                        <td class="center text-center"><a href="<?php echo base_url(); ?>backdoor/edit_engine_type?id=<?php echo $row->id;?>"><i class="glyphicon glyphicon-edit "></a></td>
                                        <td class="center text-center"><a href="?engine_type_id=<?php echo $row->id;?>" onclick="return confirm('Are you really want to delete this item')"><i class="glyphicon glyphicon-remove red "></a></td>

                                    </tr>
                                    <?php
                                    $sl++;
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <!--End Advanced Tables -->
            </div>
        </div>
        <!-- /. ROW  -->
    </div>
    <!-- /. PAGE INNER  -->
</div><!-- /. PAGE WRAPPER  -->
