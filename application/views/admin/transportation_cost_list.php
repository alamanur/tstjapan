<?php
if(isset($_GET['cost_id']))
{
    $id=$_GET['cost_id'];
    $table='tbl_transportation_cost';
    $id_field='id';
    $this->delete_model->Delete_Single_Row($id,$table,$id_field);
}

?>
<div id="page-wrapper" >
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <h2>Transportation Cost List</h2>
            </div>
        </div>
        <!-- /. ROW  -->
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12 ">
                <!-- Advanced Tables -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Transportation Cost List
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="car_model">
                                <thead>
                                <tr>
                                    <th>SL No</th>
                                    <th>Country</th>
                                    <th>Nearest Port Name</th>
                                    <th>Price Per M<sup>3</sup> ($)</th>
                                    <th>Insurance Cost ($)</th>
                                    <th>Inspection Cost ($)</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $query=$this->select_model->Select_transportation_cost();

                                $sl=1;
                                foreach ($query->result() as $row)
                                {
                                    ?>
                                    <tr class="odd gradeX">
                                        <td class="text-center"><?php echo $sl; ?></td>
                                        <td class="center"><?php echo $row->country_name;?></td>
                                        <td class="center"><?php echo $row->port_name;?></td>
                                        <td class="center"><?php echo $row->per_cubic_meter_price;?></td>
                                        <td class="text-center"><?php echo $row->insurance_price;?></td>
                                        <td class="center"><?php echo $row->inspection_price;?></td>
                                        <td class="center text-center"><a href="<?php echo base_url(); ?>backdoor/edit_transportation_cost?id=<?php echo $row->id;?>"><i class="glyphicon glyphicon-edit "></a></td>
                                        <td class="center text-center"><a href="?cost_id=<?php echo $row->id;?>" onclick="return confirm('Are you really want to delete this item')"><i class="glyphicon glyphicon-remove red "></a></td>

                                    </tr>
                                    <?php
                                    $sl++;
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <!--End Advanced Tables -->
            </div>
        </div>
        <!-- /. ROW  -->
    </div>
    <!-- /. PAGE INNER  -->
</div><!-- /. PAGE WRAPPER  -->
