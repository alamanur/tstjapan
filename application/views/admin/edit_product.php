<?php
  if(isset($_GET['id'])){
        $id=$_GET['id'];
        $table='tbl_product';
        $id_field='product_id';
        $row=$this->select_model->Select_Single_Row_product($id,$table,$id_field);

//        echo $row[0]['product_id']; die;
//        print_r($row);
//        die;
    }else{

        $row[0]['product_id']='';
        $row[0]['make_id']='';
        $row[0]['model_id']='';
        $row[0]['category_id']='';
        $row[0]['manufacture_year']='';
        $row[0]['manufacture_month']='';
        $row[0]['price']='';
        $row[0]['desplacement']='';
        $row[0]['steering']='';
        $row[0]['condition']='';
        $row[0]['made_in']='';
        $row[0]['fuel']='';
        $row[0]['body_style_id']='';
        $row[0]['engine_type_id']='';
        $row[0]['door']='';
        $row[0]['drive_type']='';
        $row[0]['vin']='';
        $row[0]['Mileage']='';
        $row[0]['transmission_id']='';
        $row[0]['no_of_passenger']='';
        $row[0]['width']='';
        $row[0]['height']='';
        $row[0]['length']='';
        $row[0]['per_cubic_meter_price']='';
        $row[0]['exterior_color']='';
        $row[0]['interior_color']='';
        $row[0]['expiry_date']='';
        $row[0]['reference_no']='';
        $row[0]['options']='';
        $row[0]['status']='';
        $row[0]['seller_id']='';


    }
?>

<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <h2>Edit Product</h2>
                <?php

                if ($this->session->flashdata('errors')){ //change!
                    echo "<div class='error'>";
                    echo $this->session->flashdata('errors');
                    echo "</div>";
                }

                ?>
            </div>
        </div>
        <!-- /. ROW  -->
        <hr/>
        <div class="row">
            <div class="col-md-12">
                <?php
                //-----Display Success or Error message---
                if(isset($feedback)){
                    echo $feedback;
                }
                ?>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-6 ">
                <div class="box-content"  >
                    <?php

                    //----Form Tag Start-------------
                    $attributes = array('class' => 'email', 'id' => 'myform');
                    echo form_open_multipart('backdoor/edit_product');
                    ?>
                </div>

                <div class="form-group">
                    <label>Manufacturar Name</label>
                    <input type="hidden" value="<?php if(validation_errors() != ""){echo set_value('txtProductId');}else{ echo $row[0]['product_id'];}?>" name="txtProductId"/>
                    <select name="txtMakeId" id="txtMakeId" class="form-control">
                        <option value="">Select One</option>
                        <?php
                        $result=$this->select_model->select_all('tbl_make',null,$id_field="make_id");
                        print_r($result);
                        foreach($result->result() as $row1){
                            ?>
                            <option value="<?php echo $row1->make_id;?>" <?php if(isset($row[0]['make_id']) && $row[0]['make_id'] == $row1->make_id){echo "selected='select'";}?><?php echo set_select("txtMakeId",$row1->make_id)?>><?php echo $row1->name ; ?></option>';
                            <?php
                        }?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtMakeId');?></label>

                </div>

                <div class="form-group">
                    <label>Transmission</label>
                    <select name="txtTransmission" id="txtTransmission" class="form-control">
                        <option value="">Select One</option>
                        <?php
                        $result=$this->select_model->select_all('tbl_transmission',null,$id_field="id","model_id",$row[0]['model_id']);
                        //print_r($result);
                        foreach($result->result() as $row1){
                            ?>
                            <option value="<?php echo $row1->id;?>" <?php if(isset($row[0]['transmission_id']) && $row[0]['transmission_id'] == $row1->id){echo "selected='select'";}?><?php echo set_select("txtTransmission",$row1->id)?>><?php echo $row1->transmission ; ?></option>
                            <?php
                        }
                        ?>
                    </select>

                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtTransmission');?></label>
                </div>

                <div class="form-group">
                    <label>Vehicle Category</label>
                    <select name="txtCategory" id="txtCategory" class="form-control">
                        <option value="">Select One</option>
                        <?php
                        $result=$this->select_model->select_all('tbl_category',null,$id_field="category_id");
//                        print_r($result);
                        foreach($result->result() as $row1){
                            ?>
                            <option value="<?php echo $row1->category_id;?>" <?php if(isset($row[0]['category_id']) && $row[0]['category_id'] == $row1->category_id){echo "selected='select'";}?><?php echo set_select("txtCategory",$row1->category_id)?>><?php echo $row1->name ; ?></option>';
                            <?php
                        }?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtCategory');?></label>
                </div>

                <div class="form-group">
                    <label>Vehicle Seller</label>
                    <select name="txtSeller" id="txtSeller" class="form-control">
                        <option value="">Select One</option>
                        <?php
                        $result=$this->select_model->select_all('tbl_company_information',null,$id_field="company_id");
//                        print_r($result);
                        foreach($result->result() as $row1){
                            ?>
                            <option value="<?php echo $row1->company_id;?>" <?php if(isset($row[0]['seller_id']) && $row[0]['seller_id'] == $row1->company_id){echo "selected='select'";}?><?php echo set_select("txtSeller",$row1->company_id)?>><?php echo $row1->company_name ; ?></option>';
                            <?php
                        }?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtSeller');?></label>
                </div>

                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-6">
                            <label for="manufactureYear">Manufacture Year</label>
                            <select name="manufactureYear" class="form-control" >
                                <?php
                                $y= date("Y")+1;
                                for ($i = 0;$i < 50;$i++)
                                {
                                    $y -=1;
                                 ?>
                                    <option value="<?php echo $y; ?>" <?php if(isset($row[0]['manufacture_year']) && $row[0]['manufacture_year'] == $y){echo "selected='selected'";}?><?php echo set_select("manufactureYear",$y)?>><?php echo $y ; ?></option>';
                             <?php
                                }
                                ?>

                            </select>
                        </div>
                        <div class="col-xs-6">
                            <label for="manufactureMonth">Manufacture Month</label>
                            <select name="manufactureMonth" class="form-control" >
                                <option value="January" <?php if(isset($row[0]['manufacture_month']) && $row[0]['manufacture_month'] == "January"){echo "selected";}else{ echo set_select("manufactureMonth","January"); }?>>January</option>
                                <option value="February" <?php if(isset($row[0]['manufacture_month']) && $row[0]['manufacture_month'] == "February"){echo "selected";}else{ echo set_select("manufactureMonth","February"); }?>>February</option>
                                <option value="March" <?php if(isset($row[0]['manufacture_month']) && $row[0]['manufacture_month'] == "March"){echo "selected";}else{ echo set_select("manufactureMonth","March"); }?>>March</option>
                                <option value="April" <?php if(isset($row[0]['manufacture_month']) && $row[0]['manufacture_month'] == "April"){echo "selected";}else{ echo set_select("manufactureMonth","April"); }?>>April</option>
                                <option value="May" <?php if(isset($row[0]['manufacture_month']) && $row[0]['manufacture_month'] == "May"){echo "selected";}else{ echo set_select("manufactureMonth","May"); }?>>May</option>
                                <option value="June" <?php if(isset($row[0]['manufacture_month']) && $row[0]['manufacture_month'] == "June"){echo "selected";}else{ echo set_select("manufactureMonth","June"); }?>>June</option>
                                <option value="July" <?php if(isset($row[0]['manufacture_month']) && $row[0]['manufacture_month'] == "July"){echo "selected";}else{ echo set_select("manufactureMonth","July"); }?>>July</option>
                                <option value="August" <?php if(isset($row[0]['manufacture_month']) && $row[0]['manufacture_month'] == "August"){echo "selected";}else{ echo set_select("manufactureMonth","August"); }?>>August</option>
                                <option value="September" <?php if(isset($row[0]['manufacture_month']) && $row[0]['manufacture_month'] == "September"){echo "selected";}?><?php echo set_select("manufactureMonth","September")?>>September</option>
                                <option value="October" <?php if(isset($row[0]['manufacture_month']) && $row[0]['manufacture_month'] == "October"){echo "selected";}else{ echo set_select("manufactureMonth","October"); }?>>October</option>
                                <option value="November" <?php if(isset($row[0]['manufacture_month']) && $row[0]['manufacture_month'] == "November"){echo "selected";}else{ echo set_select("manufactureMonth","November"); }?>>November</option>
                                <option value="December" <?php if(isset($row[0]['manufacture_month']) && $row[0]['manufacture_month'] == "December"){echo "selected";}else{ echo set_select("manufactureMonth","December"); }?>>December</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('manufactureMonth');?></label>
                </div>

                <div class="form-group">
                    <label>Price</label>
                    <input type="text"  name="txtPrice" class="form-control" maxlength="12" placeholder="Ex : 10000 / 25000" value="<?php if(form_error('txtPrice') != ""){echo set_value('txtPrice');}else{ echo $row[0]['price'];}?>">
                    <?php
/*                    $attributes=array(
                        'name'=>'txtPrice',
                        'class'=>'form-control',
                        'maxlength'   => '12',
                        'placeholder'=>'EX : 1000 / 2500',
                        'value' =>
                    );
                    echo form_input($attributes);
                    */?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtPrice');?></label>
                </div>

                <div class="form-group">

                    <label>Displacement</label>
                    <select name="txtDisplacement" class="form-control" >
                        <option value="0" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] == 0){echo "selected";}else{ echo set_select('txtDisplacement', '0');} ?>>Any</option>
                        <option value="550" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] == '550'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '550');}?>>550 CC</option>
                        <option value="660" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] == '660'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '660');} ?>>660 CC</option>
                        <option value="800" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] == '800'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '800');} ?>>800 CC</option>
                        <option value="1000" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] == '1000'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '1000');} ?>>1000 CC</option>
                        <option value="1100" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] =='1100'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '1100');} ?>>1100 CC</option>
                        <option value="1200" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] =='1200'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '1200');} ?>>1200 CC</option>
                        <option value="1300" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] =='1300'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '1300');} ?>>1300 CC</option>
                        <option value="1400" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] =='1400'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '1400');} ?>>1400 CC</option>
                        <option value="1500" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] =='1500'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '1500');} ?>>1500 CC</option>
                        <option value="1600" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] =='1600'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '1600');} ?>>1600 CC</option>
                        <option value="1700" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] == '1700'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '1700');} ?>>1700 CC</option>
                        <option value="1800" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] =='1800'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '1800');} ?>>1800 CC</option>
                        <option value="1900" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] == '1900'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '1900');} ?>>1900 CC</option>
                        <option value="2000" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] == '2000'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '2000');} ?>>2000 CC</option>
                        <option value="2100" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] == '2100'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '2100');} ?>>2100 CC</option>
                        <option value="2200" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] == '2200'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '2200');} ?>>2200 CC</option>
                        <option value="2300" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] == '2300'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '2300');} ?>>2300 CC</option>
                        <option value="2400" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] == '2400'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '2400');} ?>>2400 CC</option>
                        <option value="2500" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] == '2500'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '2500');} ?>>2500 CC</option>
                        <option value="2600" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] == '2600'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '2600');} ?>>2600 CC</option>
                        <option value="2700" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] == '2700'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '2700');} ?>>2700 CC</option>
                        <option value="2800" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] == '2800'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '2800');} ?>>2800 CC</option>
                        <option value="2900" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] == '2900'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '2900');} ?> >2900 CC</option>
                        <option value="3000" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] =='3000'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '3000');} ?>>3000 CC</option>
                        <option value="3100" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] =='3100'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '3100');} ?>>3100 CC</option>
                        <option value="3200" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] =='3200'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '3200');} ?>>3200 CC</option>
                        <option value="3300" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] =='3300'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '3300');} ?>>3300 CC</option>
                        <option value="3500" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] == '3500'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '3500');} ?>>3500 CC</option>
                        <option value="4000" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] == '4000'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '4000');} ?>>4000 CC</option>
                        <option value="4100" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] == '4100'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '4100');} ?>>4100 CC</option>
                        <option value="4200" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] == '4200'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '4200');} ?>>4200 CC</option>
                        <option value="4500" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] == '4500'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '4500');} ?>>4500(4.5L)</option>
                        <option value="5000" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] == '5000'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '5000');} ?>>5000 CC</option>
                        <option value="5500" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] == '5500'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '5500');} ?>>5500 CC</option>
                        <option value="6000" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] == '6000'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '6000');} ?>>6000 CC</option>
                        <option value="10000" <?php if(isset($row[0]['desplacement']) && $row[0]['desplacement'] == '10000'){echo "selected='selected'";}else{ echo set_select('txtDisplacement', '10000');} ?>>6000 CC</option>
                    </select>
                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtDisplacement');?></label>
                </div>

                <div class="form-group">
                    <label>Steering</label>
                    <select name="txtSteering" class="form-control" >
                        <option value="Right" <?php if(isset($row[0]['steering']) && $row[0]['steering'] == 'Right'){echo "selected";}else{ echo set_select('txtSteering', 'Right');} ?>>Right</option>
                        <option value="Left" <?php if(isset($row[0]['steering']) && $row[0]['steering'] == 'Left'){echo "selected='selected'";}else{ echo set_select('txtSteering', 'Left');}?>>Left</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtSteering');?></label>
                </div>

                <div class="form-group">
                    <label>Condition</label>
                    <select name="txtCondition" class="form-control" >
                        <option value="New" <?php if(isset($row[0]['condition']) && $row[0]['condition'] == 'New'){echo "selected";}else{ echo set_select('txtCondition', 'New');} ?>>New</option>
                        <option value="Used" <?php if(isset($row[0]['condition']) && $row[0]['condition'] == 'Used'){echo "selected='selected'";}else{ echo set_select('txtCondition', 'Used');}?>>Used</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtCondition');?></label>
                </div>

                <div class="form-group">
                    <label>Made in</label>
                    <select name="txtCountry" id="txtCountry" class="form-control">
                        <option value="">Select One</option>
                        <?php
                        $result=$this->select_model->select_all('tbl_country');
                        //print_r($result);
                        foreach($result->result() as $row1){
                            ?>
                            <option value="<?php echo $row1->id;?>" <?php if(isset($row[0]['made_in']) && $row[0]['made_in'] == $row1->id){echo "selected='selected'";}?><?php echo set_select("txtMadeIn",$row1->id)?>><?php echo $row1->country_name ; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtCountry');?></label>
                </div>

                <div class="form-group">
                    <label>Fuel</label>
                    <select name="txtFuel" class="form-control" >
                        <option value="" <?php if(isset($row[0]['fuel']) && $row[0]['fuel'] == ''){echo "selected";}else{ echo set_select('txtFuel', '');} ?>>Select One</option>
                        <option value="Biodiesel" <?php if(isset($row[0]['fuel']) && $row[0]['fuel'] == 'Biodiesel'){echo "selected";}else{ echo set_select('txtFuel', 'Biodiesel'); }?>>Biodiesel</option>
                        <option value="CNG" <?php if(isset($row[0]['fuel']) && $row[0]['fuel'] == 'CNG'){echo "selected";}else{ echo set_select('txtFuel', 'CNG');} ?>>CNG</option>
                        <option value="Diesel" <?php if(isset($row[0]['fuel']) && $row[0]['fuel'] == 'Diesel'){echo "selected";}else{ echo set_select('txtFuel', 'Diesel');} ?>>Diesel</option>
                        <option value="Electric" <?php if(isset($row[0]['fuel']) && $row[0]['fuel'] == 'Electric'){echo "selected";}else{ echo set_select('txtFuel', 'Electric');} ?>>Electric</option>
                        <option value="Ethanol-FFV" <?php if(isset($row[0]['fuel']) && $row[0]['fuel'] == 'Ethanol-FFV'){echo "selected";}else{ echo set_select('txtFuel', 'Ethanol-FFV');} ?>>Ethanol-FFV</option>
                        <option value="Gasoline/Petrol" <?php if(isset($row[0]['fuel']) && $row[0]['fuel'] == 'Gasoline/Petrol'){echo "selected";}else{ echo set_select('txtFuel', 'Gasoline/Petrol');} ?>>Gasoline/Petrol </option>
                        <option value="Hybrid-electric" <?php if(isset($row[0]['fuel']) && $row[0]['fuel'] == 'Hybrid-electric'){echo "selected";}else{ echo set_select('txtFuel', 'Hybrid-electric');} ?>>Hybrid-electric</option>
                        <option value="LPG" <?php if(isset($row[0]['fuel']) && $row[0]['fuel'] == 'LPG'){echo "selected";}else{ echo set_select('txtFuel', 'LPG');} ?>>LPG</option>
                        <option value="CNG" <?php if(isset($row[0]['fuel']) && $row[0]['fuel'] == 'CNG'){echo "selected";}else{ echo set_select('txtFuel', 'CNG');} ?>>CNG</option>
                        <option value="Steam" <?php if(isset($row[0]['fuel']) && $row[0]['fuel'] == 'Steam'){echo "selected";}else{ echo set_select('txtFuel', 'Steam');} ?>>Steam</option>
                        <option value="Other" <?php if(isset($row[0]['fuel']) && $row[0]['fuel'] == 'Other'){echo "selected";}else{ echo set_select('txtFuel', 'Other');} ?>>Other</option>

                    </select>
                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtFuel');?></label>
                </div>

                <div class="form-group">
                    <?php
                    echo form_label('Mileage', 'txtMileage');

                    $attributes=array(
                        'name'=>'txtMileage',
                        'class'=>'form-control',
                        'maxlength'   => '25',
                        'placeholder'=>'Ex : 38,000 km ',
                        'value' => $row[0]['Mileage'],
                    );
                    echo form_input($attributes);
                    ?>

                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtMileage');?></label>
                </div>

                <div class="form-group">

                    <?php
                    echo form_label('Options', 'txtOptions');
                    $attributes=array(
                        'name'=>'txtOptions',
                        'class'=>'form-control',
                        'placeholder'=>'Ex :Anti-Lock Brakes / Driver Airbag / Passenger Airbag ',
                        'value' => $row[0]['options'],
                    );
                    echo form_input($attributes);

                    ?>
                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtOptions');?></label>
                </div>


                <div class="checkbox">
                    <label><input name="txtIsAvailable" value="1" type="checkbox" <?php echo(($row[0]['status']== 1)? "checked" : "");?>> IsAvailable</label>
                </div>

                <div class="form-group">

                    <?php
                    echo form_label('Vehicle Image', 'txtImages');
                    $attributes=array(
                        'name'=>'txtImages',
                        'class'=>'form-control',
                        'maxlength'   => '50',
                        //'multiple'=>true,
                        'placeholder'=>'Write Options',

                    );
                    echo form_upload($attributes);
                    ?>
                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtImages');?></label>
                </div>

            </div>


            <div class="col-md-6 col-sm-12 col-xs-6 ">
                <div class="form-group">
                    <label>Model Name</label>
                    <select name="txtModel" id="txtModelId" class="form-control">
                        <option value="">Select One</option>
                        <?php
                        $result=$this->select_model->select_all('tbl_model',null,$id_field="model_id","make_id",$row[0]['make_id']);
                        //print_r($result);
                        foreach($result->result() as $row1){
                            ?>
                            <option value="<?php echo $row1->model_id;?>" <?php if(isset($row[0]['model_id']) && $row[0]['model_id'] == $row1->model_id){echo "selected='select'";}?><?php echo set_select("txtModel",$row1->model_id)?>><?php echo $row1->model_name ; ?></option>
                       <?php
                        }
                        ?>
                    </select>
                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtModel');?></label>
                </div>

                <div class="form-group">
                    <label>Body Style</label>
                    <select name="txtBodyStyle" id="txtBodyStyle" class="form-control">
                        <option value="">Select One</option>
                        <?php
                        $result=$this->select_model->select_all('tbl_body_style',null,$id_field="id","model_id",$row[0]['model_id']);
                        //print_r($result);
                        foreach($result->result() as $row1){
                            ?>
                            <option value="<?php echo $row1->id;?>" <?php if(isset($row[0]['body_style_id']) && $row[0]['body_style_id'] == $row1->id){echo "selected='select'";}?><?php echo set_select("txtBodyStyle",$row1->id)?>><?php echo $row1->body_style ; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtBodyStyle');?></label>
                </div>
                <div class="form-group">
                    <label>Engine Type</label>
                    <select name="txtEngineType" id="txtEngineType" class="form-control">
                        <option value="">Select One</option>
                        <?php
                        $result=$this->select_model->select_all('tbl_engine_type',null,$id_field="id","model_id",$row[0]['model_id']);
                        //print_r($result);
                        foreach($result->result() as $row1){
                            ?>
                            <option value="<?php echo $row1->id;?>" <?php if(isset($row[0]['engine_type_id']) && $row[0]['engine_type_id'] == $row1->id){echo "selected='select'";}?><?php echo set_select("txtEngineType",$row1->id)?>><?php echo $row1->engine_type ; ?></option>
                            <?php
                        }
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtEngineType');?></label>
                </div>
                <div class="form-group">
                    <label>Doors</label>
                    <select name="txtDoor" class="form-control" >
                        <option value="" <?php if(isset($row[0]['door']) && $row[0]['door'] == ''){echo "selected='select'";}else{echo set_select('txtDoor', ' '); }?>>Select One</option>
                        <option value="1" <?php if(isset($row[0]['door']) && $row[0]['door'] == '1'){echo "selected='select'";}else{ echo set_select('txtDoor', '1');} ?>>1 Door</option>
                        <option value="2" <?php if(isset($row[0]['door']) && $row[0]['door'] == '2'){echo "selected='select'";}else{ echo set_select('txtDoor', '2');} ?>>2 Doors</option>
                        <option value="3" <?php if(isset($row[0]['door']) && $row[0]['door'] == '3'){echo "selected='select'";}else{ echo set_select('txtDoor', '3');} ?>>3 Doors</option>
                        <option value="4" <?php if(isset($row[0]['door']) && $row[0]['door'] == '4'){echo "selected='select'";}else{ echo set_select('txtDoor', '4');} ?>>4 Doors</option>
                        <option value="5" <?php if(isset($row[0]['door']) && $row[0]['door'] == '5'){echo "selected='select'";}else{ echo set_select('txtDoor', '5');} ?>>5 Doors</option>
                        <option value="6" <?php if(isset($row[0]['door']) && $row[0]['door'] == '6'){echo "selected='select'";}else{ echo set_select('txtDoor', '6');} ?>>6 Doors</option>
                        <option value="7" <?php if(isset($row[0]['door']) && $row[0]['door'] == '7'){echo "selected='select'";}else{ echo set_select('txtDoor', '7');} ?>>7 Doors</option>
                        <option value="8" <?php if(isset($row[0]['door']) && $row[0]['door'] == '8'){echo "selected='select'";}else{ echo set_select('txtDoor', '8');} ?>>8 Doors</option>
                        <option value="9" <?php if(isset($row[0]['door']) && $row[0]['door'] == '9'){echo "selected='select'";}else{ echo set_select('txtDoor', '9');} ?>>9 Doors</option>
                        <option value="10" <?php if(isset($row[0]['door']) && $row[0]['door'] == '10'){echo "selected='select'";}else{ echo set_select('txtDoor', '10');} ?>>10 Doors</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtDoor');?></label>
                </div>

                <div class="form-group">
                    <?php
                    echo form_label('Number of passenger', 'txtPassenger');
                    $attributes=array(
                        'name'=>'txtPassenger',
                        'class'=>'form-control',
                        'maxlength'   => '2',
                        'placeholder'=>'Number of passenger Ex : 4 / 5 ',
                        'value' => $row[0]['no_of_passenger'],
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <?php
                    echo form_label('VIN (Vehicle Identification Number)/Serial No)', 'txtVin');
                    $attributes=array(
                        'name'=>'txtVin',
                        'class'=>'form-control',
                        'maxlength'   => '25',
                        'placeholder'=>' Ex : WAUZZZ8E03A169*** ',
                        'value' => $row[0]['vin'],
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtVin');?></label>
                </div>
                <div class="form-group">
                    <?php
                    echo form_label('Height (CM)', 'txtHeight');
                    $attributes=array(
                        'name'=>'txtHeight',
                        'class'=>'form-control',
                        'maxlength'   => '10',
                        'placeholder'=>'Ex : 144',
                        'value' => $row[0]['height'],
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtHeight');?></label>
                </div>
                <div class="form-group">
                    <?php
                    echo form_label('Width (cm)', 'txtWidth');
                    $attributes=array(
                        'name'=>'txtWidth',
                        'class'=>'form-control',
                        'maxlength'   => '10',
                        'placeholder'=>'Ex : 177',
                        'value' => $row[0]['width'],
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtWidth');?></label>
                </div>

                <div class="form-group">
                    <?php
                    echo form_label('Length (cm)', 'txtLength');
                    $attributes=array(
                        'name'=>'txtLength',
                        'class'=>'form-control',
                        'maxlength'   => '10',
                        'placeholder'=>'Ex : 550',
                        'value' => $row[0]['length'],
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtLength');?></label>
                </div>
                <div class="form-group">
                    <label for="txtExterior">Exterior Color</label>
                    <select name="txtExterior" id="txtExterior" class="form-control">
                        <option value="Any" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Any'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Any');} ?>>Any</option>
                        <option value="Beige" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Beige'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Beige');} ?>>Beige</option>
                        <option value="Black" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Black'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Black');} ?>>Black</option>
                        <option value="Blue" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Blue'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Blue');} ?>>Blue</option>
                        <option value="Bronze" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Bronze'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Bronze');} ?>>Bronze</option>
                        <option value="Brown" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Brown'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Brown');} ?>>Brown</option>
                        <option value="Burgundy" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Burgundy'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Burgundy');} ?>>Burgundy</option>
                        <option value="Champagne" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Champagne'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Champagne');} ?>>Champagne</option>
                        <option value="Charcoal" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Charcoal'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Charcoal');} ?>>Charcoal</option>
                        <option value="Cream" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Cream'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Cream');} ?>>Cream</option>
                        <option value="Dark Blue" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Dark Blue'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Dark Blue');} ?>>Dark Blue</option>
                        <option value="Gold" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Gold'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Gold');} ?>>Gold</option>
                        <option value="Gray" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Gray'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Gray');} ?>>Gray</option>
                        <option value="Green" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Green'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Green');} ?>>Green</option>
                        <option value="Maroon" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Maroon'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Maroon');} ?>>Maroon</option>
                        <option value="Off White" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Off White'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Off White');} ?>>Off White</option>
                        <option value="Orange" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Orange'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Orange');} ?>>Orange</option>
                        <option value="Other" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Other'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Other');} ?>>Other</option>
                        <option value="Pearl" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Pearl'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Pearl');} ?>>Pearl</option>
                        <option value="Pewter" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Pewter'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Pewter');} ?>>Pewter</option>
                        <option value="Pink" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Pink'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Pink');} ?>>Pink</option>
                        <option value="Purple" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Purple'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Purple');} ?>>Purple</option>
                        <option value="Red" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Red'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Red');} ?>>Red</option>
                        <option value="Silver" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Silver'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Silver');} ?>>Silver</option>
                        <option value="Tan" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Tan'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Tan');} ?>>Tan</option>
                        <option value="Teal" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Teal'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Teal');} ?>>Teal</option>
                        <option value="Titanium" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Titanium'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Titanium');} ?>>Titanium</option>
                        <option value="Turquoise" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Turquoise'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Turquoise');} ?>>Turquoise</option>
                        <option value="White" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'White'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'White');} ?>>White</option>
                        <option value="Yellow" <?php if(isset($row[0]['exterior_color']) && $row[0]['exterior_color'] == 'Yellow'){echo "selected='selected'";}else{ echo set_select('txtExterior', 'Yellow');} ?>>Yellow</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtExterior');?></label>

                </div>

                <div class="form-group">
                    <label for="txtInterior">Interior Color</label>
                    <select name="txtInterior" id="txtInterior" class="form-control">
                        <option value="Any" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Any'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Any');} ?>>Any</option>
                        <option value="Beige" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Beige'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Beige');} ?>>Beige</option>
                        <option value="Black" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Black'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Black');} ?>>Black</option>
                        <option value="Blue" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Blue'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Blue');} ?>>Blue</option>
                        <option value="Bronze" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Bronze'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Bronze');} ?>>Bronze</option>
                        <option value="Brown" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Brown'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Brown');} ?>>Brown</option>
                        <option value="Burgundy" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Burgundy'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Burgundy');} ?>>Burgundy</option>
                        <option value="Champagne" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Champagne'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Champagne');} ?>>Champagne</option>
                        <option value="Charcoal" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Charcoal'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Charcoal');} ?>>Charcoal</option>
                        <option value="Cream" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Cream'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Cream');} ?>>Cream</option>
                        <option value="Dark Blue" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Dark Blue'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Dark Blue');} ?>>Dark Blue</option>
                        <option value="Gold" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Gold'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Gold');} ?>>Gold</option>
                        <option value="Gray" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Gray'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Gray');} ?>>Gray</option>
                        <option value="Green" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Green'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Green');} ?>>Green</option>
                        <option value="Maroon" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Maroon'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Maroon');} ?>>Maroon</option>
                        <option value="Off White" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Off White'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Off White');} ?>>Off White</option>
                        <option value="Orange" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Orange'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Orange');} ?>>Orange</option>
                        <option value="Other" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Other'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Other');} ?>>Other</option>
                        <option value="Pearl" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Pearl'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Pearl');} ?>>Pearl</option>
                        <option value="Pewter" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Pewter'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Pewter');} ?>>Pewter</option>
                        <option value="Pink" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Pink'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Pink');} ?>>Pink</option>
                        <option value="Purple" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Purple'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Purple');} ?>>Purple</option>
                        <option value="Red" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Red'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Red');} ?>>Red</option>
                        <option value="Silver" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Silver'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Silver');} ?>>Silver</option>
                        <option value="Tan" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Tan'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Tan');} ?>>Tan</option>
                        <option value="Teal" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Teal'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Teal');} ?>>Teal</option>
                        <option value="Titanium" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Titanium'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Titanium');} ?>>Titanium</option>
                        <option value="Turquoise" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Turquoise'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Turquoise');} ?>>Turquoise</option>
                        <option value="White" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'White'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'White');} ?>>White</option>
                        <option value="Yellow" <?php if(isset($row[0]['interior_color']) && $row[0]['interior_color'] == 'Yellow'){echo "selected='selected'";}else{ echo set_select('txtInterior', 'Yellow');} ?>>Yellow</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtInterior');?></label>
                </div>
                <div class="form-group">
                    <?php
                    echo form_label('Expire Date', 'txtExpireDate');
                    $attributes=array(
                        'name'=>'txtExpireDate',
                        'id'=>'expire_date',
                        'class'=>'form-control',
                        'maxlength'   => '10',
                        'placeholder'=>'Write Expire Date',
                        'value' => $row[0]['expiry_date'],
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtExpireDate');?></label>
                </div>
                <div class="form-group">
                    <?php
                    echo form_label('Reference Number', 'txtReferenceNo');
                    $attributes=array(
                        'name'=>'txtReferenceNo',
                        'class'=>'form-control',
                        'maxlength'   => '25',
                        'placeholder'=>'Write Reference Number',
                        'value' => $row[0]['reference_no'],
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtReferenceNo');?></label>
                </div>

                <div class="form-group">

                    <?php
                    echo form_label('Drive Type', 'txtDriveType');
                    $options = array(
                        '2 While Drive'  => '2 While Drive',
                        '4 While Drive'    => '4 While Drive',
                        'All While Drive'   => 'All While Drive',
                    );
                    $class = 'class = form-control';
                    echo form_dropdown('txtDriveType', $options, $row[0]['drive_type'],$class);

                    ?>
                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtDriveType');?></label>
                </div>


                <input type="submit" name="btnSubmit" class="btn btn-danger">
                <?php echo form_close();?>
            </div>
        </div>
    </div>
    <!-- /. ROW  -->
</div><!-- /. PAGE INNER  -->
</div><!-- /. PAGE WRAPPER  -->
