<section id="search-form"><!-- search-form section start-->
    <div class="col-md-12">
        <div class="row">
                <div class="search-form-body">
                    <form class="form-horizontal" method="post" action="<?php echo base_url().'en/used_car'; ?>">
                        <h2 class="search-label">Search</h2>
                        <div class="form-group">
                            <label class="search-label">Make</label>
                            <div class="col-md-12">
                                <select name="txtMakeId" id="txtMakeId" class="form-control">
                                    <?php
                                    echo $this->select_model->Select_box($table='tbl_make');
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="search-label">Model</label>
                            <div class="col-md-12">
                                <select name="txtModel" id="txtModel" class="form-control">
                                    <option value="">Select One</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="search-label">FOB Price Range</label><br/>
                            <div class="col-md-6">
                                <select class="form-control" name="min_price" id="min_price">
                                    <option value="">Any</option>
                                    <option value="1000">US $ 1,000</option>
                                    <option value="2000">US $ 2,000</option>
                                    <option value="5000">US $ 5,000</option>
                                    <option value="10000">US $ 10,000</option>
                                    <option value="20000">US $ 20,000</option>
                                </select>
                            </div>
                            <div class="col-md-6">
                                <select class="form-control" name="max_price" id="max_price">
                                    <option value="">Any</option>
                                    <option value="1000">US $ 1,000</option>
                                    <option value="2000">US $ 2,000</option>
                                    <option value="5000">US $ 5,000</option>
                                    <option value="10000">US $ 10,000</option>
                                    <option value="20000">US $ 20,000</option>
                                    <option value="40000">US $ 40,000</option>
                                    <option value="80000">US $ 80,000</option>
                                </select>
                            </div>
                        </div>


                        <div class="form-group">
                            <label class="search-label">Manufacture Year</label><br/>
                            <div class="col-sm-6">
                                <select name="manufactureYearfrom" class="form-control" id="manufactureYearto" >
                                    <option value="">Any</option>
                                    <?php
                                    $y= date("Y")+1;
                                    for ($i = 0;$i < 50;$i++)
                                    {
                                        $y -=1;
                                        echo "<option value='$y'>$y</option>";
                                    }
                                    ?>

                                </select>
                            </div>

                            <div class="col-sm-6">
                                <select name="manufactureYearto" class="form-control" id="manufactureYearto" >
                                    <option value="">Any</option>
                                    <?php
                                    $y= date("Y")+1;
                                    for ($i = 0;$i < 50;$i++)
                                    {
                                        $y -=1;
                                        echo "<option value='$y'>$y</option>";
                                    }
                                    ?>

                                </select>
                            </div>

                        </div>

                        <div class="form-group">
                            <label class="search-label">Displacement/CC</label><br/>
                            <div class="col-sm-6">
                                <select name="dpfrom" id="dpfrom" class="form-control">
                                    <option value="">Any</option>
                                    <option value="550">550 CC</option>
                                    <option value="660">660 CC</option>
                                    <option value="800">800 CC</option>
                                    <option value="1000">1000 CC</option>
                                    <option value="1100">1100 CC</option>
                                    <option value="1200">1200 CC</option>
                                    <option value="1300">1300 CC</option>
                                    <option value="1400">1400 CC</option>
                                    <option value="1500">1500 CC</option>
                                    <option value="1600">1600 CC</option>
                                    <option value="1700">1700 CC</option>
                                    <option value="1800">1800 CC</option>
                                    <option value="1900">1900 CC</option>
                                    <option value="2000">2000 CC</option>
                                    <option value="2100">2100 CC</option>
                                    <option value="2200">2200 CC</option>
                                    <option value="2300">2300 CC</option>
                                    <option value="2400">2400 CC</option>
                                    <option value="2500">2500 CC)</option>
                                    <option value="2600">2600 CC</option>
                                    <option value="2700">2700 CC</option>
                                    <option value="2800">2800 CC</option>
                                    <option value="2900">2900 CC</option>
                                    <option value="3000">3000 CC</option>
                                    <option value="3500">3500 CC</option>
                                    <option value="4000">4000 CC</option>
                                    <option value="4100">4100 CC</option>
                                    <option value="4200">4200 CC</option>
                                    <option value="4300">4300 CC</option>
                                    <option value="4500">4500 CC</option>
                                    <option value="5000">5000 CC</option>
                                    <option value="5500">5500 CC</option>
                                    <option value="6000">6000 CC</option>
                                    <option value="10000">10000 CC</option>
                                </select>
                            </div>

                            <div class="col-sm-6">
                                <select name="dpto" id="dpto" class="form-control">
                                    <option value="">Any</option>
                                    <option value="550">550 CC</option>
                                    <option value="660">660 CC</option>
                                    <option value="800">800 CC</option>
                                    <option value="1000">1000 CC</option>
                                    <option value="1100">1100 CC</option>
                                    <option value="1200">1200 CC</option>
                                    <option value="1300">1300 CC</option>
                                    <option value="1400">1400 CC</option>
                                    <option value="1500">1500 CC</option>
                                    <option value="1600">1600 CC</option>
                                    <option value="1700">1700 CC</option>
                                    <option value="1800">1800 CC</option>
                                    <option value="1900">1900 CC</option>
                                    <option value="2000">2000 CC</option>
                                    <option value="2100">2100 CC</option>
                                    <option value="2200">2200 CC</option>
                                    <option value="2300">2300 CC</option>
                                    <option value="2400">2400 CC</option>
                                    <option value="2500">2500 CC)</option>
                                    <option value="2600">2600 CC</option>
                                    <option value="2700">2700 CC</option>
                                    <option value="2800">2800 CC</option>
                                    <option value="2900">2900 CC</option>
                                    <option value="3000">3000 CC</option>
                                    <option value="3500">3500 CC</option>
                                    <option value="4000">4000 CC</option>
                                    <option value="4100">4100 CC</option>
                                    <option value="4200">4200 CC</option>
                                    <option value="4300">4300 CC</option>
                                    <option value="4500">4500 CC</option>
                                    <option value="5000">5000 CC</option>
                                    <option value="5500">5500 CC</option>
                                    <option value="6000">6000 CC</option>
                                    <option value="10000">10000 CC</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group search_checkbox">
                            <label class="col-md-12 control-label search-label">
                                <input type="checkbox" id="steering_left" value="Left" aria-label="...">Left Hand Drive
                            </label>
                            <label class="col-md-12 control-label search-label">
                                <input type="checkbox" id="steering_right" value="Right" aria-label="...">Right Hand Drive<br/><br/>
                            </label>
                            <button class="btn btn-primary search-label" type="reset">Reset</button> <input type="submit" class="btn btn-primary btn-md" value="Search">
                        </div>

                        <!-- Advance Search -->
                        <div id="toggleText" style="display: none" class="form-group">
                            <div class="form-group">
                                <label class="col-md-12 control-label"> Body Type</label>
                                <div class="col-md-12">
                                    <select name="txtCategory" id="txtCategory" class="form-control">
                                        <option value="0">Select One</option>
                                        <?php
                                        echo $this->select_model->Select_car_category($table='tbl_category');
                                        ?>
                                    </select>
                                </div>

                            </div>

                            <div class="form-group">

                                <div class="col-md-12">
                                  <label >Transmission</label>
                                    <select id="txtTransmissionId" name="txtTransmission" class="form-control">
                                        <option value="0">Select One</option>
                                    </select>
                                </div>
                            </div>

                            <div  class="form-group">
                                <label for="txtFuel" class="col-sm-12 col-md-12 control-label">Fuel</label>
                                <div class="col-md-12">
                                    <?php
                                    $options = array(
                                        '0'  => 'Any',
                                        'Biodiesel'    => 'Biodiesel',
                                        'CNG'   => 'CNG',
                                        'Diesel'   => 'Diesel',
                                        'Electric'   => 'Electric',
                                        'Ethanol-FFV'   => 'Ethanol-FFV',
                                        'Gasoline/Petrol'   => 'Gasoline/Petrol',
                                        'Hybrid-electric'   => 'Hybrid-electric',
                                        'LPG'   => 'LPG',
                                        'Steam'   => 'Steam',
                                        'Other'   => 'Other',
                                    );
                                    $class = 'class = form-control id=txtFuel';
                                    echo form_dropdown('txtFuel', $options, '0',$class);
                                    ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="doors" class="col-sm-12 col-md-12 control-label">Door</label>
                                <div class="col-md-12">
                                    <select class="form-control" id="doors" name="doors">
                                        <option value="0">Select One</option>
                                        <option value="1">01 Door</option>
                                        <option value="2">02 Doors</option>
                                        <option value="3">03 Doors</option>
                                        <option value="4">04 Doors</option>
                                        <option value="5">05 Doors</option>
                                        <option value="6">06 Doors</option>
                                        <option value="7">07 Doors</option>
                                        <option value="8">08 Doors</option>
                                        <option value="9">09 Doors</option>
                                        <option value="10">10 Doors</option>
                                    </select>
                                </div>
                            </div>
                            <div  class="form-group">
                                <label for="txtColor" class="col-sm-12 col-md-12 control-label">Color</label>
                                <div class="col-md-12">
                                    <?php
                                    $options = array(
                                        '0'  => 'Any',
                                        'Beige'    => 'Beige',
                                        'Black'   => 'Black',
                                        'Blue'   => 'Blue',
                                        'Bronze'   => 'Bronze',
                                        'Brown'   => 'Brown',
                                        'Burgundy'   => 'Burgundy',
                                        'Champagne'   => 'Champagne',
                                        'Charcoal'   => 'Charcoal',
                                        'Cream'   => 'Cream',
                                        'Dark Blue'   => 'Dark Blue',
                                        'Gold'   => 'Gold',
                                        'Gray'   => 'Gray',
                                        'Green'   => 'Green',
                                        'Maroon'   => 'Maroon',
                                        'Off White'   => 'Off White',
                                        'Orange'   => 'Orange',
                                        'Other'   => 'Other',
                                        'Pearl'   => 'Pearl',
                                        'Pewter'   => 'Pewter',
                                        'Pink'   => 'Pink',
                                        'Purple'   => 'Purple',
                                        'Red'   => 'Red',
                                        'Silver'   => 'Silver',
                                        'Tan'   => 'Tan',
                                        'Red'   => 'Red',
                                        'Teal'   => 'Teal',
                                        'Titanium'   => 'Titanium',
                                        'Turquoise'   => 'Turquoise',
                                        'White'   => 'White',
                                        'Yellow'   => 'Yellow',
                                    );
                                    $class = 'class = form-control id=txtColor';
                                    echo form_dropdown('txtColor', $options, '0',$class);
                                    ?>
                                </div>
                            </div>
                            <div  class="form-group">
                                <label for="txtOptions" class="col-sm-12 col-md-12 control-label">Drive</label>
                                <div class="col-md-12">
                                    <?php

                                    $options = array(
                                        '0' => 'Select One',
                                        '1'  => '2 While Drive',
                                        '2'    => '4 While Drive',
                                        '3'   => 'All While Drive',
                                    );
                                    $class = 'class = form-control id=txtOptions';
                                    echo form_dropdown('txtOptions', $options, '0',$class);
                                    ?>
                                </div>
                            </div>

                            <div class=" form-group search-label">

                                <label class="col-sm-12 col-md-12 control-label">Condition</label>
                                <label class="col-sm-12 col-md-12 control-label">
                                    <input type="checkbox" id="condition_used" value="Used" aria-label="...">Used Stocks
                                </label>
                                <label class="col-sm-12 col-md-12 control-label">
                                    <input type="checkbox" id="condition_new" value="New" aria-label="...">New Stocks
                                </label>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">

                            </div>
                        </div>
                       <!-- <div class="col-md-12 col-sm-12 btn-advance-search"><a id="displayText" href="javascript:toggle();">Advance Search</a></div>-->
                    </form>
                </div>
        </div>
    </div>
</section>