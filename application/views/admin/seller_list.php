<?php

if(isset($_GET['id']))
{
    $id=$_GET['id'];
    $table='tbl_company_information';
    $id_field='company_id';
    $this->delete_model->Delete_Single_Row($id,$table,$id_field);
}
?>

<div id="page-wrapper" >
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <h2>Seller List</h2>
            </div>
        </div>
        <!-- /. ROW  -->
        <div class="row">
            <div class="col-md-12">
                <!-- Advanced Tables -->
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Seller List
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="carList">
                                <thead>
                                <tr>
                                    <th>SL No</th>
                                    <th>Logo</th>
                                    <th>Seller Name</th>
                                    <th>Contact_No</th>
                                    <th>Email Address</th>
                                    <th>Address</th>
                                    <th>Edit</th>
                                    <th>Delete</th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $query=$this->select_model->Select_seller_list();

                                $sl=1;
                                foreach ($query->result() as $row)
                                {
                                    ?>
                                    <tr class="odd gradeX">
                                        <td class="text-center"><?php echo $sl; ?></td>
                                        <td class="text-center"><img src="<?php echo base_url();?>resource/images/company/<?php echo $row->logo_name; ?>" width="100"></td>
                                        <td class="text-center"><?php echo $row->company_name; ?></td>
                                        <td class="center"><?php echo $row->phone_no;?></td>
                                        <td class="center"><?php echo $row->email_address;?></td>
                                        <td class="center"><?php echo $row->address;?></td>
                                        <td class="center text-center"><a href="<?php echo base_url(); ?>backdoor/edit_seller_info?id=<?php echo $row->company_id;?>"><i class="glyphicon glyphicon-edit "></a></td>

                                        <td class="center text-center"><a href="<?php echo base_url(); ?>backdoor/seller_list?id=<?php echo $row->company_id;?>" onclick="return confirm('Are you really want to delete this item')"><i class="glyphicon glyphicon-remove red "></a></td>
                                    </tr>
                                    <?php
                                    $sl++;
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
                <!--End Advanced Tables -->
            </div>
        </div>
        <!-- /. ROW  -->
    </div>
    <!-- /. PAGE INNER  -->
</div><!-- /. PAGE WRAPPER  -->
