<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-6">
                <h2>Create Vehicle Category</h2>
            </div>
            <div class="col-md-6">
                <a href="<?php echo base_url();?>backdoor/category_list/" class="btn btn-danger pull-right" style="margin-top: 18px;">Vehicle Category List</a>
            </div>
        </div>
        <!-- /. ROW  -->
        <hr/>
        <div class="row">
            <div class=" col-md-offset-3 col-md-6 col-sm-12 col-xs-6 ">
                <div class="form" >
                        <div class="box-content"  >
                            <?php
                            //-----Display Success or Error message---
                            if(isset($feedback)){
                                echo $feedback;
                            }
                            //----Form Tag Start-------------
                            $attributes = array('class' => 'email', 'id' => 'myform');

                            echo form_open('backdoor/vehicle_category', $attributes);
                            ?>
                        </div>
                        <div class="form-group">
                            <label>Vehicle Category</label>
                            <?php
                            $attributes=array(
                                'name'=>'txtVehicleCategory',
                                'class'=>'form-control',
                                'maxlength'   => '25',
                                'placeholder'=>'Write Vehicle Category',
                                'value' => set_value('txtVehicleCategory'),
                            );
                            echo form_input($attributes);
                            ?>
                        </div>
                        <div class="form-group">
                            <label class="red"><?php echo form_error('txtVehicleCategory');?></label>
                        </div>

                        <div class="form-group">
                            <label>Remarks</label>
                            <?php
                            $attributes=array(
                                'name'=>'txtRemarks',
                                'class'=>'form-control',
                                'placeholder'=>'Write Remarks',
                                'maxlength'   => '100',
                                'value' => set_value('txtRemarks'),
                            );
                            echo form_input($attributes);
                            ?>
                        </div>
                        <div class="form-group">
                            <label class="red"><?php echo form_error('txtRemarks');?></label>
                        </div>
                        <?php
                        $attribute=array(
                            'name'=>'btnSubmit',
                            'class'=>'btn btn-danger ',
                            'value'=>'Submit',
                        );
                        echo form_submit($attribute);//--Form Submit Button
                        echo form_close();//--Form closing tag </form>
                        ?>
                </div>
            </div>

        </div>
    </div>
    <!-- /. ROW  -->
</div><!-- /. PAGE INNER  -->
</div><!-- /. PAGE WRAPPER  -->
