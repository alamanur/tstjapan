<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-12">
                <h2>Create Product</h2>

            </div>
        </div>
        <!-- /. ROW  -->
        <hr/>
        <div class="row">
            <div class="col-md-12">
                <?php
                //-----Display Success or Error message---
                if(isset($feedback)){
                    echo $feedback;
                }
                ?>
            </div>
            <div class="col-md-6 col-sm-12 col-xs-6 ">
                <div class="box-content"  >
                    <?php

                    //----Form Tag Start-------------
                    $attributes = array('class' => 'email', 'id' => 'myform');
                    echo form_open_multipart('backdoor/create_product');
                    ?>
                </div>

                <div class="form-group">
                    <label>Manufacturar Name</label>
                    <select name="txtMakeId" id="txtMakeId" class="form-control">
                        <?php
                        echo $this->select_model->Select_box($table='tbl_make');
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtMakeId');?></label>

                </div>

                <div class="form-group">
                    <label>Engine Type</label>
                    <select name="txtEngineType" id="txtEngineType" class="form-control">
                        <option value="">Select One</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtEngineType');?></label>
                </div>
                <div class="form-group">
                    <label>Vehicle Category</label>
                    <select name="txtCategory" id="txtCategory" class="form-control">
                        <?php
                        echo $this->select_model->Select_car_category();
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtCategory');?></label>
                </div>

                <div class="form-group">
                    <label>Seller</label>
                    <select name="txtSeller" id="txtSeller" class="form-control">
                        <?php
                        echo $this->select_model->Select_car_seller($table='tbl_company_information');
                        ?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtSeller');?></label>
                </div>


                <div class="form-group">
                    <div class="row">
                        <div class="col-xs-6">
                            <label for="manufactureYear">Manufacture Year</label>
                            <select name="manufactureYear" class="form-control" >
                                <?php
                                $y= date("Y")+1;
                                for ($i = 0;$i < 50;$i++)
                                {
                                    $y -=1;
                                    echo "<option value='$y' ".set_select('manufactureYear', 'January').">$y</option>";
                                }
                                ?>

                            </select>
                        </div>
                        <div class="col-xs-6">
                            <label for="manufactureMonth">Manufacture Month</label>
                            <select name="manufactureMonth" class="form-control" >
                                <option value="January" <?php echo set_select('manufactureMonth', 'January'); ?>>January</option>
                                <option value="February" <?php echo set_select('manufactureMonth', 'February'); ?>>February</option>
                                <option value="March" <?php echo set_select('manufactureMonth', 'March'); ?>>March</option>
                                <option value="April" <?php echo set_select('manufactureMonth', 'April'); ?>>April</option>
                                <option value="May" <?php echo set_select('manufactureMonth', 'May'); ?>>May</option>
                                <option value="June" <?php echo set_select('manufactureMonth', 'June'); ?>>June</option>
                                <option value="July" <?php echo set_select('manufactureMonth', 'July'); ?>>July</option>
                                <option value="August" <?php echo set_select('manufactureMonth', 'August'); ?>>August</option>
                                <option value="September" <?php echo set_select('manufactureMonth', 'September'); ?>>September</option>
                                <option value="October" <?php echo set_select('manufactureMonth', 'October'); ?>>October</option>
                                <option value="November" <?php echo set_select('manufactureMonth', 'November'); ?>>November</option>
                                <option value="December" <?php echo set_select('manufactureMonth', 'December'); ?>>December</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('manufactureMonth');?></label>
                </div>

                <div class="form-group">
                    <label>Price</label>
                    <?php
                    $attributes=array(
                    'name'=>'txtPrice',
                    'class'=>'form-control',
                    'maxlength'   => '12',
                    'placeholder'=>'EX : 1000 / 2500',
                    'value' => set_value('txtPrice'),
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtPrice');?></label>
                </div>

                <div class="form-group">

                    <label>Displacement/ CC</label>
                    <select name="txtDisplacement" class="form-control" >
                        <option value="0" <?php echo set_select('txtDisplacement', '0', TRUE); ?>>Any</option>
                        <option value="550" <?php echo set_select('txtDisplacement', '550'); ?>>550</option>
                        <option value="660" <?php echo set_select('txtDisplacement', '660'); ?>>660</option>
                        <option value="800" <?php echo set_select('txtDisplacement', '800'); ?>>800</option>
                        <option value="1000" <?php echo set_select('txtDisplacement', '1000'); ?>>1000</option>
                        <option value="1100" <?php echo set_select('txtDisplacement', '1100'); ?>>1100 CC</option>
                        <option value="1200" <?php echo set_select('txtDisplacement', '1200'); ?>>1200 CC</option>
                        <option value="1300" <?php echo set_select('txtDisplacement', '1300'); ?>>1300 CC</option>
                        <option value="1400" <?php echo set_select('txtDisplacement', '1400'); ?>>1400 CC)</option>
                        <option value="1500" <?php echo set_select('txtDisplacement', '1500'); ?>>1500 CC</option>
                        <option value="1600" <?php echo set_select('txtDisplacement', '1600'); ?>>1600 CC</option>
                        <option value="1700" <?php echo set_select('txtDisplacement', '1700'); ?>>1700 CC</option>
                        <option value="1800" <?php echo set_select('txtDisplacement', '1800'); ?>>1800 CC</option>
                        <option value="1900" <?php echo set_select('txtDisplacement', '1900'); ?>>1900 CC</option>
                        <option value="2000" <?php echo set_select('txtDisplacement', '2000'); ?>>2000 CC</option>
                        <option value="2100" <?php echo set_select('txtDisplacement', '2100'); ?>>2100 CC</option>
                        <option value="2200" <?php echo set_select('txtDisplacement', '2200'); ?>>2200 CC</option>
                        <option value="2300" <?php echo set_select('txtDisplacement', '2300'); ?>>2300 CC</option>
                        <option value="2400" <?php echo set_select('txtDisplacement', '2400'); ?>>2400 CC</option>
                        <option value="2500" <?php echo set_select('txtDisplacement', '2500'); ?>>2500 CC</option>
                        <option value="2600" <?php echo set_select('txtDisplacement', '2600'); ?>>2600 CC</option>
                        <option value="2700" <?php echo set_select('txtDisplacement', '2700'); ?>>2700 CC</option>
                        <option value="2800" <?php echo set_select('txtDisplacement', '2800'); ?>>2800 CC</option>
                        <option value="2900" <?php echo set_select('txtDisplacement', '2900'); ?> >2900 CC</option>
                        <option value="3000" <?php echo set_select('txtDisplacement', '3000'); ?>>3000 CC</option>
                        <option value="3100" <?php echo set_select('txtDisplacement', '3100'); ?>>3100 CC</option>
                        <option value="3200" <?php echo set_select('txtDisplacement', '3200'); ?>>3200 CC</option>
                        <option value="3300" <?php echo set_select('txtDisplacement', '3300'); ?>>3300 CC</option>
                        <option value="3500" <?php echo set_select('txtDisplacement', '3500'); ?>>3500 CC</option>
                        <option value="4000" <?php echo set_select('txtDisplacement', '4000'); ?>>4000 CC</option>
                        <option value="4100" <?php echo set_select('txtDisplacement', '4100'); ?>>4100 CC</option>
                        <option value="4200" <?php echo set_select('txtDisplacement', '4200'); ?>>4200 CC</option>
                        <option value="4500" <?php echo set_select('txtDisplacement', '4500'); ?>>4500 CC</option>
                        <option value="5000" <?php echo set_select('txtDisplacement', '5000'); ?>>5000 CC</option>
                        <option value="5500" <?php echo set_select('txtDisplacement', '5500'); ?>>5500 CC</option>
                        <option value="6000" <?php echo set_select('txtDisplacement', '6000'); ?>>6000 CC</option>
                        <option value="10000" <?php echo set_select('txtDisplacement', '10000'); ?>>10000 CC</option>
                    </select>
                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtDisplacement');?></label>
                </div>

                <div class="form-group">
                    <label>Steering</label>
                    <select name="txtSteering" class="form-control" >
                        <option value="Right" <?php echo set_select('txtSteering', 'Right',true); ?>>Right</option>
                        <option value="Left" <?php echo set_select('txtSteering', 'Left'); ?>>Left</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtSteering');?></label>
                </div>

                <div class="form-group">
                    <label>Condition</label>
                    <select name="txtCondition" class="form-control" >
                        <option value="New" <?php echo set_select('txtCondition', 'New',true); ?>>New</option>
                        <option value="Used" <?php echo set_select('txtCondition', 'Used'); ?> >Used</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtCondition');?></label>
                </div>

                <div class="form-group">
                    <label>Made in</label>

                    <select name="txtCountry" id="txtCountry" class="form-control">
                        <?php
                        echo $this->select_model->Select_country();
                        ?>
                    </select>

                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtCountry');?></label>
                </div>

                <div class="form-group">
                    <label>Fuel</label>
                    <select name="txtFuel" class="form-control" >
                        <option value="" <?php echo set_select('txtFuel', ''); ?>>Select One</option>
                        <option value="Biodiesel" <?php echo set_select('txtFuel', 'Biodiesel'); ?>>Biodiesel</option>
                        <option value="CNG" <?php echo set_select('txtFuel', 'CNG'); ?>>CNG</option>
                        <option value="Diesel" <?php echo set_select('txtFuel', 'Diesel'); ?>>Diesel</option>
                        <option value="Electric" <?php echo set_select('txtFuel', 'Electric'); ?>>Electric</option>
                        <option value="Ethanol-FFV" <?php echo set_select('txtFuel', 'Ethanol-FFV'); ?>>Ethanol-FFV</option>
                        <option value="Gasoline/Petrol" <?php echo set_select('txtFuel', 'Gasoline/Petrol', TRUE); ?>>Gasoline/Petrol </option>
                        <option value="Hybrid-electric" <?php echo set_select('txtFuel', 'Hybrid-electric'); ?>>Hybrid-electric</option>
                        <option value="LPG" <?php echo set_select('txtFuel', 'LPG'); ?>>LPG</option>
                        <option value="CNG" <?php echo set_select('txtFuel', 'CNG'); ?>>CNG</option>
                        <option value="Steam" <?php echo set_select('txtFuel', 'Steam'); ?>>Steam</option>
                        <option value="Other" <?php echo set_select('txtFuel', 'Other'); ?>>Other</option>

                    </select>
                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtFuel');?></label>
                </div>

                <div class="form-group">
                    <?php
                    echo form_label('Mileage', 'txtMileage');

                    $attributes=array(
                        'name'=>'txtMileage',
                        'class'=>'form-control',
                        'maxlength'   => '25',
                        'placeholder'=>'Ex : 38,000 km ',
                        'value' => set_value('txtMileage'),
                    );
                    echo form_input($attributes);
                    ?>

                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtMileage');?></label>
                </div>


                <div class="form-group">

                    <?php
                    echo form_label('Options', 'txtOptions');
                    $attributes=array(
                        'name'=>'txtOptions',
                        'class'=>'form-control',
                        'placeholder'=>'Ex :Anti-Lock Brakes / Driver Airbag / Passenger Airbag ',
                        'value' => set_value('txtOptions'),
                    );
                    echo form_input($attributes);

                    ?>
                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtOptions');?></label>
                </div>


                <div class="checkbox">
                    <label><input name="txtIsAvailable" type="checkbox" value="1"> IsAvailable</label>
                </div>

                <div class="form-group">

                    <?php
                    echo form_label('Vehicle Image', 'txtImages');
                    $attributes=array(
                        'name'=>'txtImages',
                        'class'=>'form-control',
                        'maxlength'   => '50',
                        //'multiple'=>true,
                        'placeholder'=>'Write Options',
                        'value' => set_value('txtImages'),
                    );
                    echo form_upload($attributes);
                    ?>
                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtImages');?></label>
                </div>
            </div>


            <div class="col-md-6 col-sm-12 col-xs-6 ">


                <div class="form-group">
                    <label>Model Name</label>
                    <select name="txtModel" id="txtModelId" class="form-control">
                        <option value="">Select One</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtModel');?></label>
                </div>

                <div class="form-group">
                    <label>Body Style</label>
                    <select name="txtBodyStyle" id="txtBodyStyle" class="form-control">
                        <option value="">Select One</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtBodyStyle');?></label>
                </div>

                <div class="form-group">
                    <label>Transmission</label>
                    <select name="txtTransmission" id="txtTransmission" class="form-control">
                        <option value="">Select One</option>
                    </select>

                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtTransmission');?></label>
                </div>

                <div class="form-group">
                    <label>Doors</label>
                    <select name="txtDoor" class="form-control" >
                        <option value="" <?php echo set_select('txtDoor', ' ', TRUE); ?>>Select One</option>
                        <option value="1" <?php echo set_select('txtDoor', '1'); ?>>1 Door</option>
                        <option value="2" <?php echo set_select('txtDoor', '2'); ?>>2 Doors</option>
                        <option value="3" <?php echo set_select('txtDoor', '3'); ?>>3 Doors</option>
                        <option value="4" <?php echo set_select('txtDoor', '4'); ?>>4 Doors</option>
                        <option value="5" <?php echo set_select('txtDoor', '5'); ?>>5 Doors</option>
                        <option value="6" <?php echo set_select('txtDoor', '6'); ?>>6 Doors</option>
                        <option value="7" <?php echo set_select('txtDoor', '7'); ?>>7 Doors</option>
                        <option value="8" <?php echo set_select('txtDoor', '8'); ?>>8 Doors</option>
                        <option value="9" <?php echo set_select('txtDoor', '9'); ?>>9 Doors</option>
                        <option value="10" <?php echo set_select('txtDoor', '10'); ?>>10 Doors</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtDoor');?></label>
                </div>

                <div class="form-group">
                    <?php
                    echo form_label('Number of passenger', 'txtPassenger');
                    $attributes=array(
                        'name'=>'txtPassenger',
                        'class'=>'form-control',
                        'maxlength'   => '5',
                        'placeholder'=>'Number of passenger Ex : 4 / 5 ',
                        'value' => set_value('txtPassenger'),
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <?php
                    echo form_label('VIN (Vehicle Identification Number)/Serial No)', 'txtVin');
                    $attributes=array(
                        'name'=>'txtVin',
                        'class'=>'form-control',
                        'maxlength'   => '25',
                        'placeholder'=>' Ex : WAUZZZ8E03A169*** ',
                        'value' => set_value('txtVin'),
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtVin');?></label>
                </div>
                <div class="form-group">
                    <?php
                    echo form_label('Height (CM)', 'txtHeight');
                    $attributes=array(
                        'name'=>'txtHeight',
                        'class'=>'form-control',
                        'maxlength'   => '10',
                        'placeholder'=>'Ex : 144',
                        'value' => set_value('txtHeight'),
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtHeight');?></label>
                </div>
                <div class="form-group">
                    <?php
                    echo form_label('Width (cm)', 'txtWidth');
                    $attributes=array(
                        'name'=>'txtWidth',
                        'class'=>'form-control',
                        'maxlength'   => '10',
                        'placeholder'=>'Ex : 177',
                        'value' => set_value('txtWidth'),
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtWidth');?></label>
                </div>

                <div class="form-group">
                    <?php
                    echo form_label('Length (cm)', 'txtLength');
                    $attributes=array(
                        'name'=>'txtLength',
                        'class'=>'form-control',
                        'maxlength'   => '10',
                        'placeholder'=>'Ex : 550',
                        'value' => set_value('txtLength'),
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtLength');?></label>
                </div>

                <div class="form-group">
                    <label for="txtExterior">Exterior Color</label>
                    <select name="txtExterior" id="txtExterior" class="form-control">
                        <option value="Any" <?php echo set_select('txtExterior', 'Any'); ?>>Any</option>
                        <option value="Beige" <?php echo set_select('txtExterior', 'Beige'); ?>>Beige</option>
                        <option value="Black" <?php echo set_select('txtExterior', 'Black'); ?>>Black</option>
                        <option value="Blue" <?php echo set_select('txtExterior', 'Blue'); ?>>Blue</option>
                        <option value="Bronze" <?php echo set_select('txtExterior', 'Bronze'); ?>>Bronze</option>
                        <option value="Brown" <?php echo set_select('txtExterior', 'Brown'); ?>>Brown</option>
                        <option value="Burgundy" <?php echo set_select('txtExterior', 'Burgundy'); ?>>Burgundy</option>
                        <option value="Champagne" <?php echo set_select('txtExterior', 'Champagne'); ?>>Champagne</option>
                        <option value="Charcoal" <?php echo set_select('txtExterior', 'Charcoal'); ?>>Charcoal</option>
                        <option value="Cream" <?php echo set_select('txtExterior', 'Cream'); ?>>Cream</option>
                        <option value="Dark Blue" <?php echo set_select('txtExterior', 'Dark Blue'); ?>>Dark Blue</option>
                        <option value="Gold" <?php echo set_select('txtExterior', 'Gold'); ?>>Gold</option>
                        <option value="Gray" <?php echo set_select('txtExterior', 'Gray'); ?>>Gray</option>
                        <option value="Green" <?php echo set_select('txtExterior', 'Green'); ?>>Green</option>
                        <option value="Maroon" <?php echo set_select('txtExterior', 'Maroon'); ?>>Maroon</option>
                        <option value="Off White" <?php echo set_select('txtExterior', 'Off White'); ?>>Off White</option>
                        <option value="Orange" <?php echo set_select('txtExterior', 'Orange'); ?>>Orange</option>
                        <option value="Other" <?php echo set_select('txtExterior', 'Other'); ?>>Other</option>
                        <option value="Pearl" <?php echo set_select('txtExterior', 'Pearl'); ?>>Pearl</option>
                        <option value="Pewter" <?php echo set_select('txtExterior', 'Pewter'); ?>>Pewter</option>
                        <option value="Pink" <?php echo set_select('txtExterior', 'Pink'); ?>>Pink</option>
                        <option value="Purple" <?php echo set_select('txtExterior', 'Purple'); ?>>Purple</option>
                        <option value="Red" <?php echo set_select('txtExterior', 'Red'); ?>>Red</option>
                        <option value="Silver" <?php echo set_select('txtExterior', 'Silver'); ?>>Silver</option>
                        <option value="Tan" <?php echo set_select('txtExterior', 'Tan'); ?>>Tan</option>
                        <option value="Teal" <?php echo set_select('txtExterior', 'Teal'); ?>>Teal</option>
                        <option value="Titanium" <?php echo set_select('txtExterior', 'Titanium'); ?>>Titanium</option>
                        <option value="Turquoise" <?php echo set_select('txtExterior', 'Turquoise'); ?>>Turquoise</option>
                        <option value="White" <?php echo set_select('txtExterior', 'White'); ?>>White</option>
                        <option value="Yellow" <?php echo set_select('txtExterior', 'Yellow'); ?>>Yellow</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtExterior');?></label>

                </div>

                <div class="form-group">
                    <label for="txtInterior">Interior Color</label>
                    <select name="txtInterior" id="txtInterior" class="form-control">
                        <option value="Any" <?php echo set_select('txtInterior', 'Any'); ?>>Any</option>
                        <option value="Beige" <?php echo set_select('txtInterior', 'Beige'); ?>>Beige</option>
                        <option value="Black" <?php echo set_select('txtInterior', 'Black'); ?>>Black</option>
                        <option value="Blue" <?php echo set_select('txtInterior', 'Blue'); ?>>Blue</option>
                        <option value="Bronze" <?php echo set_select('txtInterior', 'Any'); ?>>Bronze</option>
                        <option value="Brown" <?php echo set_select('txtInterior', 'Brown'); ?>>Brown</option>
                        <option value="Burgundy" <?php echo set_select('txtInterior', 'Burgundy'); ?>>Burgundy</option>
                        <option value="Champagne" <?php echo set_select('txtInterior', 'Champagne'); ?>>Champagne</option>
                        <option value="Charcoal" <?php echo set_select('txtInterior', 'Charcoal'); ?>>Charcoal</option>
                        <option value="Cream" <?php echo set_select('txtInterior', 'Cream'); ?>>Cream</option>
                        <option value="Dark Blue" <?php echo set_select('txtInterior', 'Dark Blue'); ?>>Dark Blue</option>
                        <option value="Gold" <?php echo set_select('txtInterior', 'Gold'); ?>>Gold</option>
                        <option value="Gray" <?php echo set_select('txtInterior', 'Gray'); ?>>Gray</option>
                        <option value="Green" <?php echo set_select('txtInterior', 'Green'); ?>>Green</option>
                        <option value="Maroon" <?php echo set_select('txtInterior', 'Maroon'); ?>>Maroon</option>
                        <option value="Off White" <?php echo set_select('txtInterior', 'Off White'); ?>>Off White</option>
                        <option value="Orange" <?php echo set_select('txtInterior', 'Orange'); ?>>Orange</option>
                        <option value="Other" <?php echo set_select('txtInterior', 'Other'); ?>>Other</option>
                        <option value="Pearl" <?php echo set_select('txtInterior', 'Pearl'); ?>>Pearl</option>
                        <option value="Pewter" <?php echo set_select('txtInterior', 'Pewter'); ?>>Pewter</option>
                        <option value="Pink" <?php echo set_select('txtInterior', 'Pink'); ?>>Pink</option>
                        <option value="Purple" <?php echo set_select('txtInterior', 'Purple'); ?>>Purple</option>
                        <option value="Red" <?php echo set_select('txtInterior', 'Red'); ?>>Red</option>
                        <option value="Silver" <?php echo set_select('txtInterior', 'Silver'); ?>>Silver</option>
                        <option value="Tan" <?php echo set_select('txtInterior', 'Tan'); ?>>Tan</option>
                        <option value="Teal" <?php echo set_select('txtInterior', 'Teal'); ?>>Teal</option>
                        <option value="Titanium" <?php echo set_select('txtInterior', 'Titanium'); ?>>Titanium</option>
                        <option value="Turquoise" <?php echo set_select('txtInterior', 'Turquoise'); ?>>Turquoise</option>
                        <option value="White" <?php echo set_select('txtInterior', 'White'); ?>>White</option>
                        <option value="Yellow" <?php echo set_select('txtInterior', 'Yellow'); ?>>Yellow</option>
                    </select>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtInterior');?></label>
                </div>
                <div class="form-group">
                    <?php
                    echo form_label('Expire Date', 'txtExpireDate');
                    $attributes=array(
                        'name'=>'txtExpireDate',
                        'id'=>'expire_date',
                        'class'=>'form-control',
                        'maxlength'   => '10',
                        'placeholder'=>'Write Expire Date',
                        'value' => set_value('txtExpireDate'),
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtExpireDate');?></label>
                </div>
                <div class="form-group">
                    <?php
                    echo form_label('Reference Number', 'txtReferenceNo');
                    $attributes=array(
                        'name'=>'txtReferenceNo',
                        'class'=>'form-control',
                        'maxlength'   => '25',
                        'placeholder'=>'Write Reference Number',
                        'value' => set_value('txtReferenceNo'),
                    );
                    echo form_input($attributes);
                    ?>
                </div>
                <div class="form-group">
                    <label class="red"><?php echo form_error('txtReferenceNo');?></label>
                </div>

                <div class="form-group">
                    <label for="txtDriveType">Drive Type</label>
                    <select name="txtDriveType" id="txtDriveType" class="form-control">
                        <option value="2 While Drive" <?php echo set_select('txtDriveType', '2 While Drive'); ?>>2 While Drive</option>
                        <option value="4 While Drive" <?php echo set_select('txtDriveType', '4 While Drive'); ?>>4 While Drive</option>
                        <option value="All While Drive" <?php echo set_select('txtDriveType', 'All While Drive'); ?>>All While Drive</option>

                    </select>
                </div>

                <div class="form-group">
                    <label class="red"><?php echo form_error('txtDriveType');?></label>
                </div>


                <input type="submit" name="btnSubmit" class="btn btn-danger">
                <?php echo form_close();?>
            </div>
        </div>
    </div>
    <!-- /. ROW  -->
</div><!-- /. PAGE INNER  -->
</div><!-- /. PAGE WRAPPER  -->
