<?php

if(isset($_GET['id']))
{
    $id=$_GET['id'];
    $row=$this->select_model->Select_single_product_basic_info($id);
    /*print_r($result);
        die;*/
}else{
    $row['make_name']='';
    $row['model_name']='';
    $row['product_id']='';

}
?>
<div id="page-wrapper">
    <div id="page-inner">
        <div class="row">
            <div class="col-md-6">
                <h2>Add Product Images</h2>
            </div>
            <div class="col-md-6">
                <a href="<?php echo base_url();?>backdoor/vehicle_image_list" class="btn btn-danger pull-right" style="margin-top: 18px;">Image List</a>
            </div>
        </div>
        <!-- /. ROW  -->
        <hr/>
        <div class="row">
            <div class=" col-md-12 col-sm-12 col-xs-4 ">
                <form id="addProductImages" name="addProductImages" method="post" action="<?php echo base_url();?>backdoor/add_product_images" enctype="multipart/form-data">
                <div class="form" >
                    <div class="box-content"  >
                        <?php
                        //-----Display Success or Error message---
                        if(isset($feedback)){
                            echo $feedback;
                        }
                        //----Form Tag Start-------------

                        ?>
                    </div>
                    <div class="form-group">
                        <label>Manufacturar Name</label>
                        <input type="text" name="txtMakeId" value="<?php echo $row['make_name'];?>" readonly class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtMakeId');?></label>

                    </div>

                    <div class="form-group">
                        <label>Model Name</label>
                        <input type="text" name="txtModel" value="<?php echo $row['model_name'];?>" readonly class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtModel');?></label>
                    </div>

                    <div class="form-group">
                        <label>Car ID</label>
                        <input type="text" name="txtCarId" value="<?php echo $row['product_id'];?>" readonly class="form-control">
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('txtCarId');?></label>
                    </div>

                    <div class="form-group">
                        <label class="control-label">Select Images</label>
                        <input id="images" name="images[]" type="file" multiple class="file-loading"  data-allowed-file-extensions='["jpg", "JPG", "png","PNG","jpeg","JPEG"]'>
                    </div>
                    <div class="form-group">
                        <label class="red"><?php echo form_error('file');?></label>
                    </div>
                    <?php
                    $attribute=array(
                        'name'=>'btnSubmit',
                        'class'=>'btn btn-danger ',
                        'value'=>'Submit',

                    );
                    echo form_submit($attribute);//--Form Submit Button

                    ?>
                </div>
            </div>

            </form>
        </div>
    </div>
    <!-- /. ROW  -->
</div><!-- /. PAGE INNER  -->
</div><!-- /. PAGE WRAPPER  -->