<section id="testimonials">
    <div class="container">
        <div class="row">
            <div class="satisfaction-1 col-md-12">
                <p><span>94%</span> That's recent TST japan co. Customer satisfaction rate</p>
            </div>
            <div class="col-md-6">
                <div class="satisfaction-2">
                    <span>Most of the customers are satisfied</span>
                    <p>with TST Japna co. service !</p>
                    <img src="<?php echo base_url(); ?>resource/images/satisfaction_1.png" alt="Customer image"/>
                </div>
            </div>
            <div class="col-md-6">
                <div class="satisfaction-2">
                    <span>Buying with TST Japna co. is</span>
                    <p>cheap and worry-free</p>
                    <img src="<?php echo base_url(); ?>resource/images/satisfaction_2.png" />
                </div>
            </div>
            <div class="satisfaction-3">
                <div class="col-md-12 smess">
                    <p>You can search your related customer's voice and read what customers say about TSTJAPAN.CO.JP.</p>
                </div>
                <div class="col-md-8">
                    <div class="s-1 text-right">
                        <p>29 Nov 2015</p>
                    </div>
                    <div class="s-2">
                        <img class="s-2-img" src="<?php echo base_url(); ?>resource/images/flag/bangladesh.png" />
                        <span> Bangladesh</span>
                        <span class="pull-right">See Bangladesh Local Page <i class="fa fa-angle-double-right fa-lg pull-right"></i></span>
                        <div class="s-3">
                            <img src="<?php echo base_url(); ?>resource/images/03s.jpg" />

                            <p><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> Level of Satisfaction 3.0</p>
                            <p>TST japan co. facilitates effective communication between buyer and seller, and provides a safe means of payment for cars purchased. They keep the customer fully aware of the whole process of negotiation, payment, shipping and documentation.</p>
                        </div>
                    </div>

                    <div class="s-1 text-right">
                        <p>29 Nov 2015</p>
                    </div>
                    <div class="s-2">
                        <img class="s-2-img" src="<?php echo base_url(); ?>resource/images/flag/bangladesh.png" />
                        <span> Bangladesh</span>
                        <span class="pull-right">See Bangladesh Local Page <i class="fa fa-angle-double-right fa-lg pull-right"></i></span>
                        <div class="s-3">
                            <img src="<?php echo base_url(); ?>resource/images/03s.jpg" />

                            <p><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> Level of Satisfaction 3.0</p>
                            <p>TST japan co. facilitates effective communication between buyer and seller, and provides a safe means of payment for cars purchased. They keep the customer fully aware of the whole process of negotiation, payment, shipping and documentation.</p>
                        </div>
                    </div>

                    <div class="s-1 text-right">
                        <p>29 Nov 2015</p>
                    </div>
                    <div class="s-2">
                        <img class="s-2-img" src="<?php echo base_url(); ?>resource/images/flag/bangladesh.png" />
                        <span> Bangladesh</span>
                        <span class="pull-right">See Bangladesh Local Page <i class="fa fa-angle-double-right fa-lg pull-right"></i></span>
                        <div class="s-3">
                            <img src="<?php echo base_url(); ?>resource/images/03s.jpg" />

                            <p><i class="fa fa-star"></i> <i class="fa fa-star"></i> <i class="fa fa-star"></i> Level of Satisfaction 3.0</p>
                            <p>TST japan co. facilitates effective communication between buyer and seller, and provides a safe means of payment for cars purchased. They keep the customer fully aware of the whole process of negotiation, payment, shipping and documentation.</p>
                        </div>
                    </div>
            </div>

            </div>
        </div>
    </div>
</section>