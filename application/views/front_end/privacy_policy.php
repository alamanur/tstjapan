<div class="col-md-9">
    <div class="termsuse_head">
        <p>Privacy Policy</p>
    </div>
    <div class="article_head">
        <p>Overview</p>
    </div>
    <div class="termsuse_text2">
        <p><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> TST.JAPAN Co. Ltd. (“TST.JAPAN ”) fully understands your concern over protecting privacy and the way of our use of your personal information. Whether you visit our website, TST.JAPAN , to utilize our services, or you simply browse through our website, we assure you of protecting your personal information.</p>
    </div>
    <div class="article_head">
        <p>What This Privacy Policy Covers</p>
    </div>
    <div class="termsuse_text2">
        <p><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> This policy covers how TST.JAPAN treats personal information that TST.JAPAN  collects and receives. Personal information is information about you that is personally identifiable like your name, address, email address, or phone number, and that is not otherwise publicly available.</p>
        <p><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> By visiting our Site, you are deemed to have agreed to the terms and conditions of this Privacy Policy. If you do not agree, please do not use or access our Site.</p>
    </div>
    <div class="article_head">
        <p>Membership Eligibility</p>
    </div>
    <div class="termsuse_text2">
        <p><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> Our services are available only to, and may only be used by individuals who can form legally binding contracts under applicable law. Without limiting the foregoing, our services are not available to children, i.e. persons under the age of 16.</p>
    </div>
    <div class="article_head">
        <p>Information Collection and Use</p>
    </div>
    <div class="termsuse_text2">
        <p><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> TST.JAPAN collects personal information when you register with the Site, when you use the Site’s services or when you visit pages of the Site.</p>
        <p><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> When you register, we ask for information such as your name, phone number, email address, birth date, zip code. Once you register with the Site and Log in to our services, you are not anonymous to us.          </p>
        <p><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> TST.JAPAN uses information for the following general purposes: to fulfill your requests for services, improve our services, contact you, conduct research, customer support, sales promotion and provide anonymous reporting for internal and external clients.</p>
    </div>
    <div class="article_head">
        <p>Information Sharing and Disclosure</p>
    </div>
    <div class="termsuse_text2">
        <p><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> TST.JAPAN  does not rent, sell, or share personal information about you with other people or non-affiliated companies except to provide services you've requested, when we have your permission, or under the following circumstances:</p>
        <p><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> We provide the information to trusted partners who work with TST.JAPAN under confidentiality agreements.</p>
        <p><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> We respond to subpoenas, court orders, or legal process, or to establish or exercise our legal rights or defend against legal claims.</p>
        <p><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> We believe it is necessary to share information in order to investigate, prevent, or take action regarding illegal activities, suspected fraud, situations involving potential threats to the physical safety of any person, violations of TST.JAPAN's terms of use, or as otherwise required by law.</p>
        <p><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> TST.JAPAN uses information for the following general purposes: to fulfill your requests for services, improve our services, contact you, conduct research, and provide anonymous reporting for internal and external clients.</p>
    </div>
    <div class="article_head">
        <p>Cookies</p>
    </div>
    <div class="termsuse_text2">
        <p><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> TST.JAPAN may set and access trade TST.JAPAN cookies on your computer.</p>
        <p><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> We use data collection devices called "cookies" on certain pages of the Site to help analyze the Site page flow, measure promotional effectiveness, and promote trust and safety. "Cookies" are small files placed on your hard drive that assist us in providing our services. We offer certain features that are only available through the use of a "cookie". We also use cookies to allow you to enter your password less frequently during connecting with the Site. Cookies can also help us provide information that is targeted to your interests. You are always free to decline our cookies if your browser permits, although in that case you may not be able to use certain features on the Site and you may be required to reenter your password more frequently during connecting with the Site.</p>
    </div>
    <div class="article_head">
        <p>Right to Edit and Delete Your Personal Information</p>
    </div>
    <div class="termsuse_text2">
        <p><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> You can edit or delete your personal information at any time by either contacting us or accessing your settings in the Site.</p>
        <p><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> We reserve the right to send you certain communications relating to the Site service, such as service announcements and administrative messages that are considered part of TST.JAPAN’s  account, without offering you the opportunity to opt-out of receiving them.</p>
    </div>
    <div class="article_head">
        <p>Confidentiality and Security</p>
    </div>
    <div class="termsuse_text2">
        <p><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> We limit access to personal information about you to our employees who we believe reasonably need to come into contact with that information to provide products or services to you or in order to do their jobs.</p>
        <p><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> We have physical, electronic, and procedural safeguards that comply with regulations in Japan to protect personal information about you.</p>
    </div>
    <div class="article_head">
        <p>Changes or Revision to this Privacy Policy</p>
    </div>
    <div class="termsuse_text2">
        <p><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> TST.JAPAN reserves the right to update this policy at any time by revising the terms and conditions herein. Users are responsible for regularly reviewing these terms and conditions. Continued use of the Site following any such changes shall constitute the users' acceptance of such changes.</p>
    </div>
    <div class="article_head">
        <p>Questions and Suggestions</p>
    </div>
    <div class="termsuse_text2">
        <p><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i> If you have any questions regarding privacy, security, opting-out of e-mail offers or understanding how your personal information is being used, please send e-mail at info@tstjapan.co.jp. If you do not receive acknowledgement of your inquiry in a prompt fashion or within 5 days, or your inquiry has not been properly addressed, you may contact us withinfo@tstjapan.co.jp</p>
    </div>
</div>