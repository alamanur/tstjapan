<div class="col-md-9">
        <div class="termsuse_head">
            <p>Terms of Member's ID Agreement</p>
        </div>
        <div class="termsuse_text">
            <p>
                Before you register with our website and use all the service
                that is provided by TST.JAPAN CO. LTD (''TST.JAPAN''),
                make sure you read the Terms of Use and the Terms of Member’s ID Agreement stated below:
                This Terms of Member's ID Agreement (the ''Agreement'')
                describes the terms and conditions applicable to your use of TST.
                JAPAN’'s website which is identified by the uniform resource locator,
                http://www.tstjapan.co.jp/ and http://www.tstjapan.co.jp/ ( ''website'').
                This Agreement is entered into between you as the user of the website (''User'') and TST.JAPAN’.
            </p>
        </div>
        <div class="article_head">
            <p>1. Person Who Registers with the Website</p>
        </div>
        <div class="termsuse_text2">
            <ol class="nested_list2">
                <li>1.1 Person who registers with the website must personally register. The substitution's registration is not permitted.</li>
                <li>1.2 User shall represent and warrant that the information they offer to TST.JAPAN’ as a true statement is true, accurate, current and complete, and shall not give any false information when they register.</li>
                <li>1.3 User under 16 years cannot register in Member's ID service. (except　with TST.JAPAN’'s　permission).</li>
                <li>1.4 User shall register a user name that is not used by another User. TST.JAPAN’ may delete a member's user name, based on the member's using history or this Agreement, despite of deliberation or fault.</li>
                <li>1.5 When there is any change in the User's information offered　to TST.JAPAN’, User shall report such change to TST.JAPAN’ promptly.</li>
            </ol>
        </div>
        <div class="article_head">
            <p>2. ID and Password</p>
        </div>
        <div class="termsuse_text2">
            <ol>
                <li>TST.JAPAN provides the Service to Users through the Site, the Affiliate Sites, or through other methods in order to support the realization of an enriched car life and the sale and purchase of Users’ cars.</li>
                <li>The types, contents, and details of the service are as provided and posted on the Site by TST.JAPAN.</li>
                <li>TST.JAPAN shall post the environment necessary or recommended in order to use the Service on the Site. Users shall maintain this usage environment at their own expense and responsibility.</li>
            </ol>
        </div>
        <div class="article_head">
            <p>3. Account Settings Information</p>
        </div>
        <div class="termsuse_text2">
            <p>User can change their profile information after login, and choose ''Account Settings'', to change registered information and it is User's personal responsibility.</p>
        </div>
        <div class="article_head">
            <p>4. Delete Account (Withdrawal)</p>
        </div>
        <div class="termsuse_text2">
            <p>User can choose “Delete Account” to withdraw their membership and the automated emails will be stopped. (If you registered to receive mail magazine from TST.JAPAN’, it will be stopped after your withdrawal of your user account.)</p>
        </div>
        <div class="article_head">
            <p>5. Damage for Delay, Interruption, and Discontinuance of Internet</p>
        </div>
        <div class="termsuse_text2">
            <p>TST.JAPAN’ does not take any responsibility for the damage that occurs because of the delay, interruption, and discontinuance of providing internet service.</p>
        </div>
        <div class="article_head">
            <p>6. Individual Information Registered</p>
        </div>
        <div class="termsuse_text2">
            <p>TST.JAPAN’ uses User's information for fulfilling User's requests for necessary service, improving our services, contacting Users, conducting research, customer support, sales promotion and providing anonymous reporting for internal and external clients, but does not disclose registered individual information to any third party without User's agreement or permission. Notwithstanding of this provision, if there is an indication claim under law,　TST.JAPAN’ will respond to legal requirements based upon applicable law.</p>
        </div>
        <div class="article_head">
            <p>7. Regulations concerning the Use of Membership</p>
        </div>
        <div class="termsuse_text2">
            <p>TST.JAPAN’ reserves the right to restrict the use of service (For instance, the capacity of the disk and the access time, the contribution frequency and the material etc.). TST.JAPAN’ doesn't assume the responsibility for the contents that are deleted or not saved in User's listing pages or BBS in the communication. TST.JAPAN’ reserves the right to delete member's ID that has not been used for a certain period of time.</p>
        </div>
        <div class="article_head">
            <p>8. De-registration and Stoppage of Member's ID</p>
        </div>
        <div class="termsuse_text2">
            <p>If User corresponds to any of the following events, TST.JAPAN’ will stop the service and delete the membership immediately without notifying the User beforehand. TST.JAPAN’ reserves the right to refuse their use of service in the future. In such case, TST.JAPAN’ shall not be liable to User for the disadvantages or damages that may be caused from deleting the membership of User.</p>
            <ol class="nested_list">
                <li>8.1 When TST.JAPAN’ figures that User's registration is not the User oneself.</li>
                <li>8.2 When the registered information is false.</li>
                <li>8.3 When User violate this Agreement or other agreement of TST.JAPAN’, and when User acts against law or does anything illegally.</li>
                <li>8.4 When User disagrees with the Agreement.</li>
                <li>8.5 Other case that TST.JAPAN’ figures not proper.</li>
            </ol>
        </div>
        <div class="article_head">
            <p>9. Prohibition</p>
        </div>
        <div class="termsuse_text2">
            <p>As Member's ID user, the following activities are not allowed:</p>
            <ol class="nested_list">
                <li>9.1 Give false information when register or edit profile information.</li>
                <li>9.2 Use Member's ID and password illegally.</li>
                <li>9.3 Use the service and/or the access of service provided by TST.JAPAN’ for commercial purpose (Irrespective of the forms of use, reproduction, the duplicate, copy, sales, and re-sales, etc.).</li>
                <li>9.4 Violate copyright, trademark right, design, and all other intellectual property rights of TST.JAPAN’, its partner companies, manufacturers, shops or third party.</li>
                <li>9.5 Violate privacy of TST.JAPAN’, our partner companies, manufacturers, shops and third party.</li>
                <li>9.6 Slander or abuse TST.JAPAN’, our partner companies, manufacturers, shops and third party (Including slandering act that doesn't correspond to defamation)</li>
                <li>9.7 Disadvantaging or cause any damage to TST.JAPAN’, our partner companies, manufacturers, shops and third party.</li>
                <li>9.8 All acts of obstructing operation of this service.</li>
                <li>9.9 Use some name or nickname that may introduce oneself as other person, organization, or corporation without the representation right, giving a misunderstanding or a remarkable unpleasantness to other User, or third party.</li>
                <li>9.10 Own several IDs by one person /company</li>
                <li>9.11 Violate law and act of impairing customs of goodness or public order.</li>
            </ol>
        </div>
        <div class="article_head">
            <p>10. Validity of this Agreement on User Whose Registration is Cancelled</p>
        </div>
        <div class="termsuse_text2">
            <p>Even after User's registration has been cancelled, User shall assume the obligations of this Agreement with regard to the activities during registration.</p>
        </div>
        <div class="article_head">
            <p>11.	Indemnity</p>
        </div>
        <div class="termsuse_text2">
            <p>When User caused any damage to TST.JAPAN’, other Users, or the third party by an act against this Agreement or any illegal act, User shall indemnify for such damage.</p>
        </div>
        <div class="article_head">
            <p>12.	Discharge</p>
        </div>
        <div class="termsuse_text2">
            <p>Under no circumstances shall TST.JAPAN’ be held liable for an delay or failure or disruption of the services resulting directly or indirectly from acts of nature, forces or causes beyond its reasonable control, including without limitation, internet failures, computer, telecommunications or any other equipment failures, electrical power failures, strikes, labor disputes, riots, insurrections, civil disturbances, shortages of labor or materials, fires, flood, storms, explosions, war, governmental actions, orders of domestic or foreign courts or tribunals or non-performance of third parties, except when there is the gross negligence caused by TST.JAPAN’.</p>
        </div>
        <div class="article_head">
            <p>13.	End of Service</p>
        </div>
        <div class="termsuse_text2">
            <ol class="nested_list">
                <li>13.1 TST.JAPAN’ reserves the right to end the service without notifying User concerned beforehand.</li>
                <li>13.2 TST.JAPAN’ doesn't assume the responsibility of disadvantage that User may have from ending this service.</li>
            </ol>
        </div>
        <div class="article_head">
            <p>14.	Dispute Settlement</p>
        </div>
        <div class="termsuse_text2">
            <p>The User shall settle the dispute by their own responsibility when the dispute is caused with other User, the affiliated companies, or third parties of this service. TST.JAPAN’ shall not assume the responsibility for settling dispute concerned.</p>
        </div>
        <div class="article_head">
            <p>15.	Copyright and Trademark</p>
        </div>
        <div class="termsuse_text2">
            <p>The copyright of all information and contents that have been described by TST.JAPAN’ in the website shall belong to TST.JAPAN’ or the third party that obtains permission from TST.JAPAN’.  The act of using, reproducing, and modifying without prior permission from TST.JAPAN’ is prohibited.  The intellectual property right (trademark, logo, and servicemark, etc.) on the website is TST.JAPAN’'s registered trademark or trademark in the procedure　for the registry.</p>
        </div>
        <div class="article_head">
            <p>16.	Governing Law, Jurisdiction and Revision</p>
        </div>
        <div class="termsuse_text2">
            <ol class="nested_list">
                <li>16.1 This Agreement is construed based on laws of Japan and shall be governed by the laws of Japan.</li>
                <li>16.2 The Tokyo District Court has exclusive jurisdiction as a court of first instance regarding any dispute concerning this Agreement.</li>
                <li>16.3 TST.JAPAN’ may revise this Agreement when necessary, and User shall be bound by the revised version accordingly</li>
            </ol>
        </div>
</div>