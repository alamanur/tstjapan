<div class="col-md-9">
        <div class="termsuse_head">
            <p>Terms of use</p>
        </div>
        <div class="termsuse_text">
            <p>
                TST.JAPAN Corporation (“TST.JAPAN”) provides the following regulations
                (these “Regulations”)regarding usage of all services provided by TST.JAPAN (the “Service”)
                through the website operated by TST.JAPAN, TST.JAPAN.co.jp, tradeTST.JAPAN.com (the “Site”)
                and websites　operated by TST.JAPAN’s affiliates (the “Affiliate Sites”). Those who use the Service(the “Users”)
                shall understand, agree to, and comply with each of the provisions　of the Regulations.
                If you do not agree to accept the Regulations, please refrain from perusing the
                Site and using the Service. If a User accepts the Regulations by clicking on the　
                accept button on the Site or the Affiliate Sites, or by some other method without
                such clicking requirement, and if　the User uses the Service, the User will as a matter
                of course be deemed to have　accepted the Regulations.

            </p>
        </div>
        <div class="article_head">
            <p>Article 1 (Scope of these Regulations)</p>
        </div>
        <div class="termsuse_text2">
            <ol>
                <li>All of other terms of use, special agreements, rules etc. provided by TST.JAPAN regarding the Service are deemed to be applied as one unit with these Regulations (the “Rules　and Regulations”). In such case, if the contents of these Regulations differ from　the contents of other terms of use, special agreements, or rules etc., the provisions　of such other terms of use, special agreements, or rules etc. will take precedence. Unless　particularly stated otherwise, terms defined in these Regulations have the same　meaning in the terms of use, special agreements, and rules etc.</li>
                <li>Even if the User uses the Service through an Affiliate Site, the Rules and Regulations will be deemed to be applied between the User and TST.JAPAN. In this case, information the User sends to the Affiliate Site will be deemed to have been sent to TST.JAPAN.</li>
                <li>TST.JAPAN may change the contents of the Rules and Regulations. In this case, TST.JAPAN　shall display the changed contents on the Site and subsequently the User will be　bound by the changed Rules and Regulations at the earlier of the point when a User　uses the Service for the first time or the point when the notification period provided　by TST.JAPAN has passed.</li>
            </ol>
        </div>
        <div class="article_head">
            <p>Article 2 (Provision of the Service)</p>
        </div>
        <div class="termsuse_text2">
            <ol>
                <li>TST.JAPAN provides the Service to Users through the Site, the Affiliate Sites, or through other methods in order to support the realization of an enriched car life and the sale and purchase of Users’ cars.</li>
                <li>The types, contents, and details of the service are as provided and posted on the Site by TST.JAPAN.</li>
                <li>TST.JAPAN shall post the environment necessary or recommended in order to use the Service on the Site. Users shall maintain this usage environment at their own expense and responsibility.</li>
            </ol>
        </div>
        <div class="article_head">
            <p>Article 3 (Consideration for the Service)</p>
        </div>
        <div class="termsuse_text2">
            <ol>
                <li>Consideration for the Service is free of charge except if otherwise provided.</li>
                <li>If TST.JAPAN provides a service for a fee it shall provide that information in the Rules and Regulations and shall post the amount of the fee, the method of payment, and other necessary information on the Site.</li>
            </ol>
        </div>
        <div class="article_head">
            <p>Article 4 (User Registration and Authentication)</p>
        </div>
        <div class="termsuse_text2">
            <ol>
                <li>For the parts of the service that require User registration as prescribed by TST.JAPAN, Users shall complete User registration in accordance with the procedures prescribed by TST.JAPAN.</li>
                <li>Users shall confirm and warrant to TST.JAPAN that all of the information they submit, as a true statement, to TST.JAPAN at the time of User registration is accurate, true, and up-to-date. Further, if a change occurs after User registration the User shall promptly change their registration in accordance with the procedures prescribed by TST.JAPAN.</li>
                <li>TST.JAPAN may refuse an application for User registration at its discretion. In such case, the User may not make any claim or objection and TST.JAPAN does not have any obligations, such as to explain the reason for refusal.</li>
                <li>TST.JAPAN shall give registered Users (“Registered Users”) an ID, password, and other authentication key (“Authentication Key”), or if the Authentication Key is set bythe Registered User itself, the Registered User shall strictly manage the Authentication Key and shall not disclose, divulge, or allow another person to use the Authentication Key.</li>
                <li>TST.JAPAN may treat all communications conducted correctly using the Registered User’s Authentication Key as being deemed to have been conducted by the Registered User itself or by a person given the appropriate authority by the Registered User. In such case, TST.JAPAN is not liable for any damage that occurs to the Registered User, even if it occurs due to misuse of the Authentication Key or due to another reason.</li>
                <li>Registered Users may cancel their User registration in accordance with TST.JAPAN’s prescribed procedures at any time.</li>
                <li>Registered Users are deemed to consent to the receipt of e-mail magazines and other electronic mail from TST.JAPAN (including advertisements from TST.JAPAN and third parties,“TST.JAPAN Mail”) through the User registration. Registered User can refuse to receive TST.JAPAN Mail by cancelling User Registration or another method provided by TST.JAPAN.</li>
            </ol>
        </div>
        <div class="article_head">
            <p>Article 5 (Contact Information)</p>
        </div>
        <div class="termsuse_text2">
            <ol>
                <li>1.	If TST.JAPAN judges that notice is required to be made to a Registered User, it will make the notice to the registered address using electronic mail, postal mail, telephone, fax, or another appropriate method. In this case the notice will be deemed to have arrived when it would normally have arrived, even if it does not arrive or arrives late.</li>
                <li>2.	Questions and enquiries about the Service should be directed to TST.JAPAN by electronic mail or postal mail. TST.JAPAN does not accept enquiries made by telephone, directly visiting TST.JAPAN, or any other method.</li>
            </ol>
        </div>
        <div class="article_head">
            <p>Article 6 (Handling User Information)</p>
        </div>
        <div class="termsuse_text2">
            <ol>
                <li>TST.JAPAN handles the personal information of Users appropriately, in accordance with the Act on the Protection of Personal Information and the ( “Privacy Policy”) provided by TST.JAPAN and posted on the Site.</li>
                <li>In addition to Article 6.1, if TST.JAPAN handles a User’s business secrets it shall handle them with the due care of a good manager in accordance with the spirit of the Service.</li>
            </ol>
        </div>
        <div class="article_head">
            <p>Article 7 (Intellectual Property Rights)</p>
        </div>
        <div class="termsuse_text2">
            <ol>
                <li>Trademarks such as TST.JAPAN (written in English or Japanese) are TST.JAPAN’s trademarks or registered trademarks in Japan and other countries.</li>
                <li>The copyrights, image rights, or other rights for all written materials, photographs, video, and other content on posted on the Site or in TST.JAPAN Mail (“TST.JAPAN Web Content”) belong to TST.JAPAN or an approved third party, and do not belong to the Users. Unless otherwise stated by TST.JAPAN, Users are only permitted to peruse these contents in the methods prescribed by TST.JAPAN and copying, redistributing, or using the contents in any other way, or changing, or creating derivative works using the contents is prohibited.</li>
                <li>All copyrights or other rights such as for software and files attached to TST.JAPAN Mail or able to be downloaded or used on the Site belong to TST.JAPAN or an approved third party, and do not belong to the Users. Unless otherwise stated by TST.JAPAN, Users are only permitted assignable usage rights regarding the software, to peruse the Site, or within the minimum required scope in order to use the Service for personal use, and not for profit. TST.JAPAN may cancel the license at any time and in such case the User shall immediately suspend use of the software and delete it from all memory devices managed by the User.</li>
                <li>Copyrights for messages or files etc. contributed to the Site by Users (“Messages”)(including the rights set out in Articles 27 and 28 of the Copyright Act) are transferred to and belong to TST.JAPAN as a result of the contribution. Further, Users license the use of Messages by TST.JAPAN (including use for the Service and for advertisements, publication, or other commercial use) and the User shall not exercise an author’s personal rights regarding the use. Further, by contributing Messages to the Site, Users represent and warrant that they have all the rights required to use the Messages and to license TST.JAPAN to use them (including copyright for the Messages, and the consent of any individuals that could be identified from inclusion in the subject or model for the Messages, or from the information in the Messages).</li>
            </ol>
        </div>
        <div class="article_head">
            <p>Article 8 (Transactions and Communication with Other Operators)</p>
        </div>
        <div class="termsuse_text2">
            <ol>
                <li>If anyone other than TST.JAPAN, whether an operator other than TST.JAPAN (regardless of the existence of any capital ties, tie-ups, or contractual relationships with TST.JAPAN.), another User, or another corporation or individual (“Other Operators”),directly provide Users with information within the Service, such as regarding products or transaction conditions, the Service is limited to the provision of the information provided by the Other Operators to Users as it is, and all confirmation and judgment of the contents of the information is conducted at the liability of the Users themselves. TST.JAPAN does not make any warranties or bear any responsibility regarding whether the information is accurate, up-to-date, true, lawful, or compatible for a purpose or otherwise.</li>
                <li>If Users conduct transactions with Other Operators through the Service, TST.JAPAN will not make any warranties, endorse, act as an agent, mediate, intercede, or conduct any canvassing regarding the transaction between the User and the Other Operator. Further all confirmation of the contents of the transaction and judgment regarding the execution of the transaction is conducted at the liability of the Users themselves. TST.JAPAN does not make any warranties or bear any responsibility regarding the actual existence of the Other Operators, their identity or other attributes, whether they have authority, likelihood of performance of obligations, or performance or non-performance, the transaction’s effectiveness, compatibility, actual existence of products, or whether or not the products have flaws.</li>
                <li>If a User contacts a specified or unspecified Other Operator through the service and exchanges Messages or otherwise communicates with them, the User shall make judgments regarding whether or not to disclose information about the User themselves or the User’s assets etc. to the other party, or whether files provided by the other party contain harmful programs etc., at their own liability.</li>
                <li>Any disputes between the User and the Other Operators regarding transactions, communication etc. will be resolved at the expense and liability of the User.</li>
                <li>The User acknowledges and agrees that TST.JAPAN, within the limitation of applicable law, monitors the User’s communications with the Other Operators for the purpose of ensuring Use’s compliance with its obligations under the Regulation, and that TST.JAPAN may restrict, delete, or prohibit such communications, if TST.JAPAN decides it is necessary to do so, based on its sole discretion.</li>
            </ol>
        </div>
        <div class="article_head">
            <p>Article 9 (Links and Advertisements)</p>
        </div>
        <div class="termsuse_text2">
            <ol>
                <li>TST.JAPAN sometimes posts links from the TST.JAPAN Web Content to other sites. Even in this case TST.JAPAN will not make any warranties, endorse, act as an agent, mediate, intercede, or conduct any canvassing, and does not bear any responsibility regarding the information and services etc. provided at the linked site. Further, whether authorized or not, the same applies for sites that link to the Site.</li>
                <li>TST.JAPAN sometimes posts advertisements for Other Operators on the Site. Even in this case TST.JAPAN will not make any warranties, endorse, act as an agent, mediate, intercede, or conduct any canvassing, and does not bear any responsibility regarding the products and services etc. provided by the advertiser.</li>
            </ol>
        </div>
        <div class="article_head">
            <p>Article 10 (TST.JAPAN’s Obligations and Liability)</p>
        </div>
        <div class="termsuse_text2">
            <ol>
                <li>TST.JAPAN may suspend or terminate part or all of the Service without prior notice due to system failure, software or hardware breakdown, fault, malfunction, or failure of telecommunication lines.</li>
                <li>The information provided through the Service and communications and other exchanges may be delayed or interrupted as a result of TST.JAPAN or Other Operators not being open for business, the occurrence of any of the events set out in Article 10.1,or for other reasons.</li>
                <li>The information, data, software, products, and services provided by TST.JAPAN through the service may include inaccuracies or faults. Further, TST.JAPAN may add to, change, or delete all or part of this information etc. without prior warning.</li>
                <li>TST.JAPAN will take security measures at the level it judges reasonable regarding the server and other network equipment managed by TST.JAPAN, but it is possible that incidents such as unlawful access, information leakage, or distribution of harmful programs could occur, in spite of these measures. Further, as TST.JAPAN does not take security measures regarding information that travels over the Internet or other open networks unless specifically stated, and since even if security measures are taken they could be overridden, it is possible that information could be stolen, falsified etc.</li>
                <li>TST.JAPAN does not bear any obligation to protect information posted on the site by Users and may arrange, move, or delete the information as appropriate.</li>
                <li>TST.JAPAN does not bear any liability regarding damage suffered by Users resulting from the events set out in each of the above items.</li>
                <li>TST.JAPAN does not bear any liability regarding damage suffered by Users resulting from the parts of the service that are provided free of charge. Further, even ifa User suffers damage resulting from TST.JAPAN’s negligence in a part of the servicethat is provided for a fee, TST.JAPAN’s liability will be limited to the amount of payment actually received regarding the service that was the direct cause of the occurrence of damage, whether or not any cause for liability exists, such as non-performance of obligations, defect warranty, or illegal acts, excluding damage arising due to special circumstances and lost profits damage.</li>
            </ol>
        </div>
        <div class="article_head">
            <p>Article 11 (Prohibited Acts)</p>
        </div>
        <div class="termsuse_text2">
            <ol>
                <li>Users shall not conduct any of the actions that fall under the following items in their use of the Service:
                    <ol class="nested_list">
                        <li>1.1 breaching the copyrights, trademark rights, or other intellectual property rights, privacy rights, image rights, or other rights of another person, damaging the honor, credibility, or assets of another person, or actions that contribute to this;</li>
                        <li>1.2 exposing information, or know how etc. that is kept confidential by another person;</li>
                        <li>1.3 actions whereby the User behaves threateningly, provocatively, or insultingly to another party, or otherwise causes mental anguish;</li>
                        <li>1.4 forcing another person to enter into an association, activity, or organization, or to furnish transactions, profits etc., or to provide a service, or actions that request such things even though the other person has refused;</li>
                        <li>1.5 registering or posting information which is untrue, or that contains mistakes, or actions that could possibly cause another person to misunderstand the User’s identity, products, contents of the service or transaction conditions;</li>
                        <li>1.6 regarding transactions conducted with Other Operators through the Service, actions that delay performance of obligations, makes performance impossible, or imperfect or flawed performance;</li>
                        <li>1.7 collecting, stockpiling, altering, or deleting another person’s information;</li>
                        <li>1.8 using the Service under the guise of another person, having multiple people use the same account, or an individual establishing several accounts;</li>
                        <li>1.9 unauthorized access or attempting to use unauthorized access, sending computer viruses, back-door or other unauthorized commands, programs, data, etc. to another person’s computer, or leaving harmful computer programs, etc. in a position whereby another person could receive them;</li>
                        <li>1.10 actions that exceed the scope of normal use and place a burden on the server;</li>
                        <li>1.11 using, gathering, or processing the information provided in the Service by a method other than the method provided by TST.JAPAN, whether legal or illegal, and whether or not it infringes upon rights, or using the Service by a method other than the method provided by TST.JAPAN, for profit or for commercial purposes;</li>
                        <li>1.12 posting information considerably lacking in quality, information for which the meaning is unclear, or other Messages that deviate from the purpose of the Service, or repeatedly posting Messages with the same or similar content;</li>
                        <li>1.13 actions that damage the credibility of TST.JAPAN, the Site, or the Service, or actions that demean the reputation of TST.JAPAN, the Site, or the Service;</li>
                        <li>1.14 actions other than the items set out above that violate laws and ordinances, public standards, or the Rules and Regulations, actions that impede the operation of the Service, and actions particularly provided by TST.JAPAN and posted on the Site.</li>
                    </ol>
                </li>
                <li>TST.JAPAN is not obliged to monitor whether or not the actions set out in the items in Article 11.1 are being conducted in respect of the Site or the Service. Further, TST.JAPAN is not liable for any damage suffered by a User as a result of another User conducting the actions set out in the items in Article 11.1.</li>
                <li>TST.JAPAN may request cooperation from Users regarding the submission of materials, or obtaining information in order to investigate whether or not the actions setout in the items in Article 11.1 have taken place and the details thereof, and Users shall cooperate with such requests; provided, however, that TST.JAPAN is not obliged to conduct such investigations.</li>
            </ol>
        </div>
        <div class="article_head">
            <p>Article 12 (Termination of Use)</p>
        </div>
        <div class="termsuse_text2">
            <ol>
                <li>TST.JAPAN may, at its discretion, take any or several of the measures set out below in respect of a particular User without any notice; provided, however, that TST.JAPAN has no obligation to take such measures:
                    <ol class="nested_list">
                        <li>1.1 suspension or restriction of all or part of the Service.</li>
                        <li>1.2 refusal or restriction of access to the Site;</li>
                        <li>1.3 cancellation of User registration and subsequent refusal of User registration;</li>
                        <li>1.4 amendment or deletion of all or part of messages submitted by a User;</li>
                        <li>1.5 cooperation with criminal or other investigations by investigation agencies and administrative agencies; and</li>
                        <li>1.6 any other measures TST.JAPAN judges appropriate.</li>
                    </ol>
                </li>
                <li>Users may not make any claims or objections regarding the measures in Article 12.1 and TST.JAPAN does not bear any obligation or responsibility such as to explain its reasons for taking the measures.</li>
            </ol>
        </div>
        <div class="article_head">
            <p>Article 13 (Damages)</p>
        </div>
        <div class="termsuse_text2">
            <p>If a User breaches the representations and warranties it made in respect of these Rules and Regulations or TST.JAPAN, or if TST.JAPAN suffers damage due to a User’s willful misconduct or neglect, the User shall compensate TST.JAPAN for all damage suffered by TST.JAPAN (including legal fees).</p>
        </div>
        <div class="article_head">
            <p>Article 14 (Entire Agreement and Severability)</p>
        </div>
        <div class="termsuse_text2">
            <ol>
                <li>If part of the provisions of these Rules and Regulations are judged invalid or unenforceable, the provision will be deemed to have been replaced with an effective and enforceable provision, the details of which are as close as possible to the purpose of the original provision. Further, in such case, the other provisions of these Rules and Regulations will survive and will not be influenced in any way.</li>
                <li>These Rules and Regulations constitute the entire agreement between the User and TST.JAPAN regarding the service and the Site and take precedence over all previous or current communications or suggestions made either electronically, in writing, or verbally.</li>
            </ol>
        </div>
        <div class="article_head">
            <p>Article 15 (Target Regions, Governing Law, and Jurisdiction)</p>
        </div>
        <div class="termsuse_text2">
            <ol>
                <li>The Service is only provided for residents of the target regions provided by TST.JAPAN and posted on the Site and is not aimed at residents of any other countries or regions. Use of the Service from outside the target regions is prohibited.</li>
                <li>The governing law for these Rules and Regulations, the Site, and the Service is the law of Japan.</li>
                <li>The Tokyo District Court has exclusive jurisdiction as a court of first instance regarding any dispute concerning these Rules and Regulations, the Site, or the Service.</li>
            </ol>
        </div>
</div>