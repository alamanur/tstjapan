<section id="error_404">
    <div class="container">
        <div class="inner-page wp_page no_sidebar">
            <div class="error-message col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <h2 class="error padding-10 margin-bottom-30 padding-top-none"><i class="fa fa-exclamation-circle exclamation margin-right-50"></i>404</h2>
                <em>File not found.</em>
            </div>


            <div class="clearfix"></div>
        </div>
    </div>
</section>