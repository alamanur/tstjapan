<section id="footer">
    <div class="container">
        <div class="col-md-12">
            <div class="footer-menu">
                <!--<ul>
                    <a href="#"><li>buy</li></a>
                    <a href="#"><li>sell</li></a>
                    <a href="index.php?page=8"><li>about</li></a>
                    <a href="#"><li>vehicle specifications</li></a>
                    <a href="#"><li>site map</li></a>
                    <a href="#"><li>help</li></a>
                </ul>-->
            </div>
        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="footer-menu2 text-left">
                        <p>Developed By : <a href="http://ingtechbd.com/" target="_blank">Ingenious Technologies</a> </p>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="foote-menu2 text-right">
                        <ul>
                            <a href="<?php echo base_url();?>en/terms_of_use/"><li>Terms of use</a></li>
                            <a href="<?php echo base_url();?>en/privacy_policy/"><li>Privacy Policy</a></li>
                            <a href="<?php echo base_url();?>en/terms_of_agreement/"><li>Terms of agreement</a></li>
                        </ul>
                        <p>Copyright ©2015 TST Japan co.ltd. Corporation All rights reserved.</p>
                        <p>TST Japan co.ltd. operates with Permission from Tokyo Public Safety Commission.Reg.</p>
                        <p>Address: 84-0004, Koganai- shi , Honcho 1-11-16 Room No.201 Tokyo, Japan.</p>
                        <p>Email : tstjapan.co@gmail.com, tst.japan@yahoo.com, info@tstjapan.co.jp</p>
                        <p>Tel: 090-6141-9061 Fax: 050-1478-9691</p>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>
<!--Negotiation Model Start --->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="exampleModalLabel">Negotiation Message</h4>
            </div>
            <div class="modal-body">
                <div id="negotiation_message"></div>

                <div class="form-group" id="message">
                    <input type="checkbox" value="I want to negotiate the best price" checked > I want to negotiate the best price <br>
                    <input type="checkbox" value="I want to know the shipping schedule"> I want to know the shipping schedule<br>
                    <input type="checkbox" value="I want to know about the condition of the car"> I want to know about the condition of the car
                </div>
                <form name="negotiationForm" id="negotiationForm" action="" method="post">

                    <div class="form-group">


                        <input type="hidden" name="negotiation_car_id" id="negotiation_car_id">
                        <textarea class="form-control" name="negotiationMessage" id="myTextArea1" rows="6" maxlength="250" minlength="20" required></textarea>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-danger" id="send_negotiation">Send message</button>
                    </div>

            </div>
            <div class="terms">
                <input type="radio" value="1" name="special_promotion"> I would like to receive information about special promotions from tstjapan.co.jp.<br>
                </form>
                By clicking "Send message" button, you agree to the following terms.<br/><br/>
                >> <a href="<?php echo base_url();?>en/terms_of_use/"> tstjapan.co.jp's Terms of Use</a><br/>
                >> <a href="<?php echo base_url();?>en/privacy_policy/"> Privacy Policy</a>
            </div>

        </div>
    </div>
</div><!--Negotiation Model End --->

<!-- Modal Success or Error modal for Negotiation Modal-->
<div class="modal fade" id="messageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-body">
                <p id="message_feedback"></p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!--<script type="text/javascript" src="<?php /*echo base_url();*/?>resource/js/jquery.js"></script>-->
<!--<script type="text/javascript" src="<?php /*echo base_url();*/?>resource/jquery.paginate"></script>-->
<script type="text/javascript" src="<?php echo base_url();?>resource/js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>resource/js/lightbox-plus-jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>resource/js/bootstrap.min.js"></script>



<!-- DATA TABLE SCRIPTS -->
<script src="<?php echo base_url(); ?>resource/assets/js/dataTables/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>resource/assets/js/dataTables/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url();?>resource/js/myScript.js"></script>

<script>
    $(document).ready(function(){
        var currentUrl = window.location;

        $("#txtMakeId").change(function(){
            var make=$("#txtMakeId").val();
            //alert(company_id);
            $.ajax({
                type:"POST",
                url:"<?php echo base_url();?>backdoor/get_model/",
                data:{make_id:make},
                success: function(response){
                    // alert(response);
                    $("#txtModel").html(response);
                }
            })
        });
        $("#txtCountryId").change(function(){
            var country_id=$(this).val();
            //alert(company_id);
            $.ajax({
                type:"POST",
                url:"<?php echo base_url();?>en/ajax_get_nearest_port/",
                data:{country_id:country_id},
                success: function(response){
                    // alert(response);
                    $("#txtNearestPortId").html(response);
                }
            })
        });

        $("#txtNearestPortId").change(function(){
            var port_id=$(this).val();
            //alert(company_id);
            $.ajax({
                type:"POST",
                url:"<?php echo base_url();?>en/ajax_get_transport_cost/",
                data:{port_id:port_id},
                dataType: "json",
                success: function(response){

                    $('#per_cubic_meter_price').val(response.per_cubic_meter_price);
                    $('#insurance_price').val(response.insurance_price);
                    $('#inspection_price').val(response.inspection_price);
                    console.log(response);
                }
            })
        });

        $("#calculate").on("click",function(){//carFobPrice
            if($('#txtNearestPortId').val() != '' && $('#txtNearestPortId').val() !=0){

                calculate_cif_price();
            }else {
//                alert("please select your destination")
                var response ='<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert">&times;</button>Please Select Your port ! </div>';
                $('#message_feedback').html(response);
                $('#messageModal').modal('show');
            }
        });

        $("#placeOrder").on("click",function(){//carFobPrice

            if($('#txtNearestPortId').val() != '' && $('#txtNearestPortId').val() !=0){
                var  formData = $(this).data();
                var car_id = formData.car_id;
                var country_id=$('#txtCountryId').val();
                var port_id=$('#txtNearestPortId').val();

                var data = 'car_id='+car_id+'&country_id='+country_id+'&port_id='+port_id;
//                alert(data);
                $.ajax({
                        url: '<?php echo base_url();?>en/ajax_add_order/',
                        type: 'post',
                        data: data,
                        dataType: 'json'
                    })
                    .done(function(response) {
//                        alert(response.process );
                        if(response.process == false){
//                            alert(response);
                            window.location.href = "<?php echo base_url();?>en/start?currentUrl="+currentUrl;
                        }else if(response.process == true && response.slug == 'success'){
                            var feedback ='<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert">&times;</button>'+ response.message +'<br/> Please go to <a href="<?php echo base_url();?>dashboard/order_list" >dashboard</a> to generate Invoice</div>';
                            $('#message_feedback').html(feedback);
                            $('#messageModal').modal('show');
//                            window.location.href = "<?php //echo base_url();?>//";
                        }else{
//                            alert(response);
                            var feedback ='<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert">&times;</button>'+ response.message +'</div>';
                            $('#message_feedback').html(feedback);
                            $('#messageModal').modal('show');
                        }
                    });

            }else {
//                alert("please select your destination")
                var response ='<div class="alert alert-info"><button type="button" class="close" data-dismiss="alert">&times;</button>Please Select Your port ! </div>';
                $('#message_feedback').html(response);
                $('#messageModal').modal('show');
            }
        });

        function calculate_cif_price(){

            var per_cubic_price = (Math.round($("#carMCubic").val()) * Math.round(parseInt($("#per_cubic_meter_price").val())));
            var CIFPirce = Math.round(parseInt($("#carFobPrice").text()))+ per_cubic_price;

            if($("#addInsurance").is(':checked')){
                CIFPirce += Math.round(parseInt($("#insurance_price").val()));
            }
            if($("#addInspection").is(':checked')){
                CIFPirce += Math.round(parseInt($("#inspection_price").val()));
            }
            $("#cifPrice").text('$ '+CIFPirce);
        }
        $("#txtModel").change(function(){
            var model_id=$(this).val();
            //alert(company_id);
            $.ajax({
                type:"POST",
                url:"<?php echo base_url();?>backdoor/ajax_get_transmission/",
                data:{model_id:model_id},
                success: function(response){
                    // alert(response);
                    $("#txtTransmissionId").html(response);
                }
            })
        });
        //-- Car List DataTable
        $('#carList').dataTable({
            "lengthMenu": [ 13, 25, 50, 75, 100 ]
        });
        $('#car_model').dataTable();


        $('input[type=checkbox]').change(function () {
            updateTextArea();
        });

        updateTextArea();

        $('.negotiation').on('click',function(){
            var  formData = $(this).data();
            var car_id = formData.car_id;
            $("#negotiation_car_id").val(car_id);
        })


        $('#print_invoice').on('click',function(){
//            console.log(reservation_id);
            $.ajax({
                    type: 'post',
                    url: '<?php echo base_url();?>dashboard/invoice',
                    data: '',
                    dataType: 'html'
                })
                .done(function(data) {
                    console.log(data);
                    var win = window.open();
                    win.window.open();
                    win.document.write(data);
//                    win.document.write('hi');
                    win.document.close();
                    var printInterval = setInterval(function(){
                        win.print();
                        clearInterval(printInterval)
                    },1000)

                });
        })
    });

    function updateTextArea() {
        var text = "";
        $('input[type=checkbox]:checked').each( function() {
           //alert($(this).val());
            text += $(this).val() + " \n";
        });

        $('#myTextArea1').val( text );
    }
    $(".message_id").change(function(){
        updateMessage();
    });
    function  updateMessage(){
        var text = "";
        $('.message_id').each(function(){
            text +=  $(this).val() + " \n";
        });
        $("#messageText").val(text);
    }

</script>
</body>
</html>