<!doctype html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="description" content="Japan used cars exporter TST JAPAN provides a large selection of Japanese used cars to buy directly from Japan. TST JAPAN also exports new and used tuning parts." />
    <meta name="keywords" content="japan used cars, japanese used car exporter, japanese used car, japanese used cars, japan used car, buy japanese used cars, IST, Regiusace Van" />
    <meta name="author" content="Tarek Raihan, Phone:+88-01911-222952">
    <meta property="og:image" content="http://tstjapan.co.jp/resource/images/car/20160809103145000000.JPG" />
    <title>TST Japan Co.Ltd.</title>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>resource/css/font-awesome.min.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>resource/css/my_font.css"/>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>resource/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>resource/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>resource/css/menu.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>resource/css/style.css">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>resource/css/responsive.css">
    <!--<link rel="stylesheet" type="text/css" href="<?php /*echo base_url();*/?>resource/css/pagenation.css" media="screen">-->
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>resource/css/lightbox.css">
    <!-- DataTable-->
    <link href="<?php echo base_url(); ?>resource/assets/js/dataTables/dataTables.bootstrap.css" rel="stylesheet" />
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
</head>

<body>
<script>
    var base_url = '<?php echo base_url(); ?>';
</script>
<section id="banner">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <div class="company-name-logo">
                    <a href="<?php echo base_url();?>"><img src="<?php echo base_url();?>resource/images/logo.png" /></a>
                    <img src="<?php echo base_url();?>resource/images/company-name.png" />
                </div>
            </div>
            <div class="col-md-7 banner-middle">
                <div class="col-md-6">
                    <label class="col-md-4 control-label">Language</label>
                    <div class="col-md-5">
                        <select>
                            <option>English</option>

                        </select>
                    </div>
                    <div class="singup-login col-md-12">
                        <?php
                            if($this->session->userdata('fusername')){
                        ?>
                        <a href="<?php echo base_url();?>dashboard/customer_profile" class="btn btn-primary btn-sm"><?php echo $this->session->userdata('fusername'); ?> <?php echo $this->session->userdata('fuser_last_name'); ?></a>
                            <a class="btn btn-primary btn-sm" href="<?php echo base_url() . 'dashboard/logout'; ?>">Logout</a>
                            <a  href="<?php echo base_url(); ?>" class="btn btn-primary btn-sm">Home Page</a>
                            <?php
                            }else{
                            ?>
                            <a href="<?php echo base_url(); ?>en/sign_up/" class="btn btn-primary btn-sm">Sign Up</a>
                            <a href="<?php echo base_url(); ?>en/start/" class="btn btn-primary btn-sm">Log in</a>
                            <?php
                            }
                            ?>

                </div>
            </div>
                <div class="col-md-6">
                    <div class="banner-right text-right">
                        <div class="time">
                            <i class="fa fa-clock-o"> Japan Time:</i>
                            <span>
                                <?php
                                date_default_timezone_set('Asia/Tokyo');
                                print(Date("F d, Y, h:i a"));
                                ?>(JST)
                            </span>
                        </div>
                        <div class="social_link">
                            <div class="link">
                                <a href="<?php echo base_url();?>dashboard/favorite/" ><i class="fa fa-star-o fa-lg fav"> Favorite</i></a>
                                <ul>
                                    <li><a href="#" target="_blank"><img src="<?php echo base_url();?>resource/images/pinstar.jpg" style="max-height:30px; width:auto; border:none"></a></li>
                                    <li><a href="#" target="_blank"><img src="<?php echo base_url();?>resource/images/google+.jpg" style="max-height:30px; width:auto; border:none"></a></li>
                                    <li><a href="#" target="_blank"><img src="<?php echo base_url();?>resource/images/twitter.jpg" style="max-height:30px; width:auto; border:none"></a></li>
                                    <li><a href="https://www.facebook.com/" target="_blank"><img src="<?php echo base_url();?>resource/images/facebook.jpg" style="max-height:30px; width:auto; border:none"></a></li>
                                </ul>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>