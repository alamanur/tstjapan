<section id="footer">
    <div class="container">
        <div class="col-md-12">
            <div class="footer-menu">
               <!-- <ul>
                    <a href="#"><li>buy</li></a>
                    <a href="#"><li>sell</li></a>
                    <a href="index.php?page=8"><li>about</li></a>
                    <a href="#"><li>vehicle specifications</li></a>
                    <a href="#"><li>site map</li></a>
                    <a href="#"><li>help</li></a>
                </ul>-->
            </div>
        </div>
        <div class="col-md-12">
            <div class="foote-menu2 text-right">
                <div class="foote-menu2 text-right">
                    <ul>
                        <a href="<?php echo base_url();?>en/terms_of_use/"><li>Terms of use</a></li>
                        <a href="<?php echo base_url();?>en/privacy_policy/"><li>Privacy Policy</a></li>
                        <a href="<?php echo base_url();?>en/terms_of_agreement/"><li>Terms of agreement</a></li>
                    </ul>
                    <p>Copyright ©2015 TST Japan co.ltd. Corporation All rights reserved.</p>
                    <p>TST Japan co.ltd. operates with Permission from Tokyo Public Safety Commission.Reg.</p>
                    <p>Address: 84-0004, Koganai- shi , Honcho 1-11-16 Room No.201 Tokyo, Japan.</p>
                    <p>Email : tstjapan.co@gmail.com, tst.japan@yahoo.com, info@tstjapan.co.jp</p>
                    <p>Tel: 090-6141-9061 Fax: 050-1478-9691</p>
                </div>
            </div>
        </div>
    </div>
</section>

<!--<script type="text/javascript" src="<?php /*echo base_url();*/?>resource/js/jquery.js"></script>-->
<!--<script type="text/javascript" src="<?php /*echo base_url();*/?>resource/jquery.paginate"></script>-->
<script type="text/javascript" src="<?php echo base_url();?>resource/js/jquery-1.11.3.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>resource/js/moment.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>resource/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url();?>resource/js/bootstrap-datetimepicker.js"></script>
<!--<script type="text/javascript" src="--><?php //echo base_url();?><!--resource/js/script-font-end.js"></script>-->
<!--
<script type="text/javascript" src="<?php //echo base_url();?>resource/js/lightbox-plus-jquery.min.js"></script>-->

<!-- DATA TABLE SCRIPTS -->
<script src="<?php echo base_url(); ?>resource/assets/js/dataTables/jquery.dataTables.js"></script>
<script src="<?php echo base_url();?>resource/assets/js/dataTables/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url();?>resource/js/myScript.js"></script>

<script>
    $(document).ready(function(){
        //alert("hhis");
        $("#txtMakeId").change(function(){
            var make=$("#txtMakeId").val();
            //alert(company_id);
            $.ajax({
                type:"POST",
                url:"<?php echo base_url();?>backdoor/get_model/",
                data:{make_id:make},
                success: function(response){
                    // alert(response);
                    $("#txtModel").html(response);
                }
            })
        });
        //-- Car List DataTable
        $('#carList').dataTable({
            "lengthMenu": [ 13, 25, 50, 75, 100 ]
        });
        $('#car_model').dataTable();

        $('#datetimepicker9').datetimepicker({
            viewMode: 'years',
            format: 'DD-MM-YYYY',
            minDate: '1945',
            maxDate : '2005'
        });
    });

</script>
</body>
</html>