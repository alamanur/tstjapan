<section id="menu">
    <div class="container">
        <nav class="navbar navbar-default">
            <div class="container-fluid">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                    <ul class="nav navbar-nav navbar-left">
                        <li><a href="<?php echo base_url();?>en/">home</a></li>
                        <li><a href="<?php echo base_url();?>en/used_car/">used car stock </a></li>
                        <li><a href="<?php echo base_url();?>en/review/">review</a></li>
                        <li><a href="<?php echo base_url();?>en/how_to_buy/">how to buy</a></li>
                        <li><a href="<?php echo base_url();?>en/helps/">help</a></li>
                        <li><a href="">about us</a></li>
                    </ul>
                </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
        </nav>
    </div>
</section>