<div id='cssmenu'>
    <ul>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/honda-logo.png"><span> Honda</span></a>
            <ul>
                <?php
                    $query = $this->select_model->Select_model_by_id('4');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>

            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/bmw_logo.png"><span> BMW</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('1');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/mazda_logo_2.png"><span> Mazda</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('13');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/Mitsubishi_Motors.png"><span> Mitsubishi Motors</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('5');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/Mercedes.png"><span> Mercedes</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('14');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/Nissan_logo.png"><span> Nissan</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('3');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/toyota-emblem.png"><span> Toyota</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('2');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/Volkswagen_Logo.png"><span> Volkswagen</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('11');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/subaru.png"><span> Subaru</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('12');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/suzuki.png"><span> Suzuki</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('10');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/land-rover.png"><span> Land Rover</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('9');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/isuzu.png"><span> Isuzu</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('16');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/audi.png"><span> Audi</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('8');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/ford.png"><span> Ford</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('7');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/daihatsu.png"><span> Daihatsu</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('15');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/lexus.png"><span> Lexus</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('6');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/Chrysler_Group_logo.png"><span> Chrysler</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('17');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/car_logo.png"><span> Citroën</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('18');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/Dodge-Ram-Logo-Transparent.png"><span> Dodge</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('19');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/fiat-logo-icon-png.png"><span> Fiat</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('20');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/Holden-Logo.png"><span> Holden</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('21');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/HSVLogo.png"><span> HSV</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('22');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/Hyundai_car_logo.png"><span> Hyundai</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('23');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/jeep_car_logo_.png"><span> Jeep</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('24');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/kia_motors_america.png"><span> KIA</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('25');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/Peugeot.png"><span> Peugeot</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('26');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/porsche_logo.png"><span> Porsche</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('27');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/proton-logo.png"><span> Proton</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('28');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/Skoda.png"><span> Skoda</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('30');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/ssangyong.png"><span> Ssangyong</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('31');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
        <li class='active has-sub'><a href='#'><img class="menulogo" src="<?php echo base_url(); ?>resource/images/logo/Volvo.png"><span> Volvo</span></a>
            <ul>
                <?php
                $query = $this->select_model->Select_model_by_id('32');
                foreach ($query->result() as $row) {
                    $name = str_replace(' ', '_', strtolower(trim($row->name)));
                    ?>
                    <li><a href='<?php echo base_url();?>en/car/<?php echo $name; ?>/<?php echo $row->model_id; ?>'><span><?php echo $row->model_name; ?></span><span class="badge pull-right"><?php echo $row->no; ?></span></a></li>
                <?php
                }
                ?>
            </ul>
        </li>
    </ul>
</div>
