<section id="help_details">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-3">
                        <div class="basic-help">
                            <p>Basic</p>
                            <div class="basic-help-child">
                                <ul type="none">
                                    <a href="#"><li>Help Top</li></a>
                                    <a href="#"><li>About TST Japan Co.</li></a>
                                    <a href="#"><li>Legitimacy of the Seller</li></a>
                                    <a href="#"><li>Buying</li></a>
                                    <a href="#"><li>Membership & account</li></a>
                                </ul>
                            </div>
                        </div>
                        <br/>
                        <div class="basic-help">
                            <p>Others</p>
                            <div class="basic-help-child">
                                <ul type="none">
                                    <a href="#"><li>Common trade terms</li></a>
                                    <a href="#"><li>Tool Guide</li></a>
                                    <a href="#"><li>Import Regulation</li></a>
                                    <a href="#"><li>Terms of use</li></a>
                                    <a href="#"><li>Privacy Policy</li></a>
                                </ul>
                            </div>
                        </div>
                        <br/>
                        <div class="basic-help">
                            <p>Others</p>
                            <div class="basic-help-child">
                                <ul type="none">
                                    <a href="#"><li>Customer Support</li></a>
                                </ul>
                            </div>
                        </div>
                    </div>