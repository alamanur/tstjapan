<?php
class my404 extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
       /* $this->output->set_status_header('404');
        $data['title'] = 'error_404'; // View name
        $this->load->view('my404',$data);//loading in my template*/

        $data['slide'] = $this->load->view('front_end/includes/slide', '', true);
        $data['vertical_menu'] = $this->load->view('front_end/includes/vertical_menu', '', true);
        $this->load->view('front_end/includes/header');
        $this->load->view('front_end/includes/header_menu',$data);
        $this->load->view('front_end/my404');
        $this->load->view('front_end/includes/footer');
    }
}
