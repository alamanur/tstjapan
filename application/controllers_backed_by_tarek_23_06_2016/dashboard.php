<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function index()
    {
        $this->common_model->frontUserCheck();
        $this->load->view('front_end/includes/header');
        $this->load->view('dashboard/dashboard_left_menu');
        $this->load->view('dashboard/dashboard');
        $this->load->view('dashboard/dashboard_footer');
        $this->load->view('front_end/includes/footer_index');
    }

    public function notice()
    {
        $this->common_model->frontUserCheck();
        $this->load->view('front_end/includes/header');
        $this->load->view('dashboard/dashboard_left_menu');
        $this->load->view('dashboard/notice');
        $this->load->view('dashboard/dashboard_footer');
        $this->load->view('front_end/includes/footer_index');
    }

    public function favorite()
    {
        $this->common_model->frontUserCheck();
        $this->load->view('front_end/includes/header');
        $this->load->view('dashboard/dashboard_left_menu');
        $this->load->view('dashboard/bookmark');
        $this->load->view('dashboard/dashboard_footer');
        $this->load->view('front_end/includes/footer_index');
    }

    public function customer_profile()
    {
        $this->common_model->frontUserCheck();
        $this->load->view('front_end/includes/header');
        $this->load->view('dashboard/dashboard_left_menu');
        $this->load->view('dashboard/customer_profile');
        $this->load->view('dashboard/dashboard_footer');
        $this->load->view('front_end/includes/footer_index');
    }

    public function edit_general_info()
    {
        $this->common_model->frontUserCheck();

        $this->load->view('front_end/includes/header');
        $this->load->view('dashboard/dashboard_left_menu');
        $this->load->view('dashboard/edit_general_info');
        $this->load->view('dashboard/dashboard_footer');
        $this->load->view('front_end/includes/footer_index');
    }

    public function ajax_update_general_info(){
        $this->form_validation->set_rules('first_name','First Name','trim|required|max_length[25]');
        $this->form_validation->set_rules('last_name','Last Name','trim|required|max_length[25]');
        if ($this->form_validation->run() == FALSE)
        {
            $html  =    '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>';
            $html .=    validation_errors();
            $html .=    '</div>';
            echo $html;
        }
        else
        {
            $this->common_model->data=array(
                'first_name'=> htmlentities($this->input->post('first_name')),
                'last_name'=> htmlentities($this->input->post('last_name'))
            );
            $this->common_model->table_name=("tst_user");
            $this->common_model->where = array('user_id' => $this->session->userdata('fuser_id'));
            $result = $this->common_model->update();
            if($result)
            {
                $html  =    '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>';
                $html .=    'Data Successfully saved ! ';
                $html .=    '</div>';
                echo $html;
            }

        }

    }

    public function logout()
    {
        $this->session->unset_userdata('fuser_id');
        $this->session->unset_userdata('fusername');
        $this->session->unset_userdata('fuser_last_name');
        $this->session->unset_userdata('fuser_email');
        redirect(base_url()."en/start");
    }

    public function edit_contact_info()
    {
        $this->common_model->frontUserCheck();

        $this->load->view('front_end/includes/header');
        $this->load->view('dashboard/dashboard_left_menu');
        $this->load->view('dashboard/edit_contact_info');
        $this->load->view('dashboard/dashboard_footer');
        $this->load->view('front_end/includes/footer_index');
    }

    public function ajax_update_contact_info(){
        $this->form_validation->set_rules('customer_email','Customer Email','trim|required|max_length[80]');
        $this->form_validation->set_rules('mobile_no','Mobile','trim|required|min_length[7]|max_length[15]');
        $this->form_validation->set_rules('home_phone','Home Phone','trim|required|min_length[7]|max_length[15]');
        $this->form_validation->set_rules('address','Address','trim|required|max_length[150]');
        $this->form_validation->set_rules('postal_code','Postal','trim|required|min_length[4]|max_length[6]');
        if ($this->form_validation->run() == FALSE)
        {
            $html  =    '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>';
            $html .=    validation_errors();
            $html .=    '</div>';
            echo $html;
        }
        else
        {
            if($this->session->userdata('fuser_email') == $this->input->post('customer_email') ){
                $this->common_model->data=array(
                    'address'=> htmlentities($this->input->post('address')),
                    'mobile'=> htmlentities($this->input->post('mobile_no')),
                    'home_phone'=> htmlentities($this->input->post('home_phone')),
                    'postal_code'=> htmlentities($this->input->post('postal_code'))
                );
                $this->common_model->table_name=("tst_user");
                $this->common_model->where = array('user_id' => $this->input->post('customer_id'));
                $result = $this->common_model->update();
                if($result)
                {
                    $html  =    '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>';
                    $html .=    'Data Successfully saved ! ';
                    $html .=    '</div>';
                    echo $html;
                }
            }else{
                $html  =    '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>';
                $html .=    'It is strictly prohibited to change email address  ! ';
                $html .=    '</div>';
                echo $html;
            }

        }

    }

    public function change_password(){
        $this->common_model->frontUserCheck();
        $this->load->view('front_end/includes/header');
        $this->load->view('dashboard/dashboard_left_menu');
        $this->load->view('dashboard/edit_customer_password');
        $this->load->view('dashboard/dashboard_footer');
        $this->load->view('front_end/includes/footer_index');
    }

    public function ajax_change_password(){

        $this->form_validation->set_rules('txtCurrentPassword','Current Password','trim|required');
        $this->form_validation->set_rules('txtNewPassword','New Password','trim|required|min_length[6]|max_length[12]');
        $this->form_validation->set_rules('txtConfirmPassword','Confirm Password','trim|required|min_length[6]|max_length[12]|matches[txtNewPassword]');
        if ($this->form_validation->run() == FALSE)
        {
            $html  =    '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>';
            $html .=    validation_errors();
            $html .=    '</div>';
            echo $html;
        }
        else
        {
            if($this->session->userdata('fuser_password') == md5($this->input->post('txtCurrentPassword')) ){
                $this->common_model->data=array(
                    'current_password'=> md5($this->input->post('txtNewPassword')),
                    'old_password'=>$this->session->userdata('fuser_password')
                );
                $this->common_model->table_name="tst_user";
                $this->common_model->where = array('user_id' => $this->session->userdata('fuser_id'));
                $result = $this->common_model->update();
                if($result)
                {
                    $html  = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>';
                    $html .= 'Data Successfully saved ! ';
                    $html .= '</div>';
                    echo $html;
                }
            }else{
                $html  =    '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>';
                $html .=    'Current Password not Match ! ';
                $html .=    '</div>';
                echo $html;
            }

        }

    }

    public function ajax_remove_favorite(){
        $car_id = $this->input->post('car_id');
        $this->common_model->data=array(
            'status'=> 0
        );
        $this->common_model->table_name ="tbl_favorite";
        $this->common_model->where = array('customer_id' => $this->session->userdata('fuser_id'), 'product_id' => $car_id);
        $result = $this->common_model->update();
        if($result)
        {
            $html  = '<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button>';
            $html .= 'Data Successfully removed ! ';
            $html .= '</div>';
            echo $html;
        }

    }
}