<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class Backdoor extends CI_Controller
{
    /**********************************************
     * Developer : Tarek Raihan                   *
     * Developer Email : tarekraihan@yahoo.com    *
     * Project : TSTJAPAN.CO.JP                   *
     * Script : Main  controller                   *
     * Start Date : 10-11-2015                    *
     * Last Update : 10-11-2015                   *
     **********************************************/

    public function index()
    {
        if (!$this->session->userdata('admin_email')) {

            $data['title'] = "Login";
            $this->load->view('admin/login', $data);

        }
    }

    public function dashboard()
    {
        if ($this->session->userdata('admin_email')) {
            $data['title'] = "TST - Dashboard";
            $this->load->view('admin/includes/admin_header', $data);
            $this->load->view('admin/index');
            $this->load->view('admin/includes/admin_footer');
        }else {
            redirect('backdoor/index');
        }
    }

    public function admin_user_role($msg = '')
    {
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor/index');
        } else {

            if ($msg == 'success') {
                $data['feedback'] = '<div class="text-center alert alert-success">Successfully Save !!</div>';
            } else if ($msg == 'error') {
                $data['feedback'] = '<div class=" text-center alert alert-danger">Problem to Insert !!</div>';
            }
            $this->form_validation->set_rules('txtAdminRole', 'Admin Role', 'trim|required|min_length[4]|max_length[20]|xss_clean|is_unique[admin_user_role.role_name]');
            if ($this->form_validation->run() == FALSE) {
                $data['title'] = "Admin User Role";
                $this->load->view('admin/includes/admin_header', $data);
                $this->load->view('admin/admin_user_role');
                $this->load->view('admin/includes/admin_footer');
            } else {
                $date = date('Y-m-d h:i:s');

                $this->common_model->data = array('role_name' => $this->input->post('txtAdminRole'), 'created' => $date);
                $this->common_model->table_name = 'admin_user_role';
                $result = $this->common_model->insert();

                if($result){
                    redirect('backdoor/admin_user_role/success');
                } else {
                    redirect('backdoor/admin_user_role/error');
                }

            }
        }

    }

    function alpha_dash_space($str)
    {
        return (!preg_match("/^([-a-zA-Z_ .-])+$/i", $str)) ? FALSE : TRUE;
    }

    function phone_number($str)
    {
        return (!preg_match("/^([+0-9])+$/i", $str)) ? FALSE : TRUE;
    }

    public function edit_admin_user_role($msg = '')
    {
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor');
        } else {

            if ($msg == 'success') {
                $data['feedback'] = '<div class="text-center alert alert-success">Successfully Update !!</div>';
            } else if ($msg == 'error') {
                $data['feedback'] = '<div class=" text-center alert alert-danger">Problem to Update !!</div>';
            }
            $this->form_validation->set_rules('txtEditAdminRole', 'Admin Role', 'trim|required|callback_alpha_dash_space|min_length[4]|max_length[20]|xss_clean');
            if ($this->form_validation->run() == FALSE) {
                $data['title'] = "Edit Admin User Role";
                $this->load->view('admin/includes/admin_header', $data);
                $this->load->view('admin/edit_admin_user_role');
                $this->load->view('admin/includes/admin_footer');
            } else {
                $date = date('Y-m-d h:i:s');

                $this->common_model->data = array('role_name' => $this->input->post('txtEditAdminRole'), 'modified' => $date);
                $this->common_model->table_name = 'admin_user_role';
                $this->common_model->where = array('role_id' => $this->input->post('txtRoleId'));
                $result = $this->common_model->update();

                if ($result) {
                    redirect('backdoor/edit_admin_user_role/success');
                } else {
                    redirect('backdoor/edit_admin_user_role/error');
                }

            }
        }

    }

    public function admin_user($msg = '')
    {
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor');
        } else {

            if ($msg == 'success') {
                $data['feedback'] = '<div class="text-center alert alert-success">Successfully Save !!</div>';
            } else if ($msg == 'error') {
                $data['feedback'] = '<div class=" text-center alert alert-danger">Problem to Insert !!</div>';
            }
            $this->form_validation->set_rules('txtFirstName', 'First Name', 'trim|required|callback_alpha_dash_space|min_length[3]|max_length[20]|xss_clean');
            $this->form_validation->set_rules('txtLastName', 'Last Name', 'trim|required|callback_alpha_dash_space|min_length[3]|max_length[20]|xss_clean');
            $this->form_validation->set_rules('txtEmailAddress', 'Email Address', 'trim|valid_email|required|min_length[5]|max_length[80]|xss_clean|is_unique[tst_admin_user.admin_email]');
            $this->form_validation->set_rules('txtAddress', 'Address', 'trim|required|min_length[5]|max_length[100]|xss_clean');
            $this->form_validation->set_rules('txtPhone', 'Phone', 'trim|required|callback_phone_number|min_length[7]|max_length[15]|xss_clean');
            $this->form_validation->set_rules('txtPassword', 'Password', 'trim|required|min_length[4]|max_length[15]|xss_clean|matches[txtRePassword]');
            $this->form_validation->set_rules('txtRePassword', 'Re Password', 'trim|required|min_length[4]|max_length[15]');
            if ($this->form_validation->run() == FALSE) {
                $data['title'] = "Create Admin User";
                $this->load->view('admin/includes/admin_header', $data);
                $this->load->view('admin/create_admin_user');
                $this->load->view('admin/includes/admin_footer');
            } else {
                //$upload_result = $this->do_upload('./resource/images/admin/', 'txtProfilePicture');
                /* print_r($upload_result);
                 die;*/
                if($_FILES['txtProfilePicture']['size'] != 0) {

                    $upload_result = $this->do_upload('./resource/images/admin/', 'txtProfilePicture');
                }

                $date = date('Y-m-d h:i:s');
                $this->common_model->data = array(
                    'admin_first_name' => htmlentities($this->input->post('txtFirstName')),
                    'admin_last_name' => htmlentities($this->input->post('txtLastName')),
                    'admin_email' => htmlentities($this->input->post('txtEmailAddress')),
//                    'username' => htmlentities($this->input->post('username')),
                    'admin_address' => htmlentities($this->input->post('txtAddress')),
                    'admin_phone' => $this->input->post('txtPhone'),
                    'admin_role' => $this->input->post('txtAdminRole'),
                    'current_password' => md5($this->input->post('txtPassword')),
                    'status' => $this->input->post('txtIsActive'),
                    //'profile_picture' => $upload_result['file_name'],
                    'created' => $date
                );
                if(!empty($upload_result['file_name'])){
                    $this->common_model->data = array(
                        'profile_picture' => $upload_result['file_name']
                    );

                }
                $this->common_model->table_name = 'tst_admin_user';
                $result = $this->common_model->insert();

                if ($result) {
                    redirect('backdoor/admin_user/success');
                } else {
                    redirect('backdoor/admin_user/error');
                }

            }
        }

    }

    public function edit_admin_user($msg = '')
    {
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor');
        } else {


            if ($msg == 'success') {
                $data['feedback'] = '<div class="text-center alert alert-success">Successfully Save !!</div>';
            } else if ($msg == 'error') {
                $data['feedback'] = '<div class=" text-center alert alert-danger">Problem to Insert !!</div>';
            }
            $this->form_validation->set_rules('txtFirstName', 'First Name', 'trim|required|callback_alpha_dash_space|min_length[3]|max_length[20]|xss_clean');
            $this->form_validation->set_rules('txtLastName', 'Last Name', 'trim|required|callback_alpha_dash_space|min_length[3]|max_length[20]|xss_clean');
            $this->form_validation->set_rules('txtEmailAddress', 'Email Address', 'trim|valid_email|required|min_length[5]|max_length[80]|xss_clean');
            $this->form_validation->set_rules('txtAddress', 'Address', 'trim|min_length[5]|max_length[100]|xss_clean');
            $this->form_validation->set_rules('txtPhone', 'Phone', 'trim|required|callback_phone_number|min_length[7]|max_length[15]|xss_clean');
            $this->form_validation->set_rules('txtPassword', 'Password', 'trim|required|min_length[4]|max_length[15]|xss_clean|matches[txtConfirmPassword]');
            $this->form_validation->set_rules('txtConfirmPassword', 'Confirm Password', 'trim|required|min_length[4]|max_length[15]');
            if ($this->form_validation->run() == FALSE) {
                $data['title'] = "Create Admin User";
                $this->load->view('admin/includes/admin_header', $data);
                $this->load->view('admin/edit_admin_user');
                $this->load->view('admin/includes/admin_footer');
            } else {
//                print_r($this->input->post());die;
                if($_FILES['txtProfilePicture']['size'] != 0) {

                    $upload_result = $this->do_upload('./resource/images/admin/', 'txtProfilePicture');
                }

                $status=0;
                    if($this->input->post('txtIsActive')== 1){
                        $status=1;
                    }
                $date = date('Y-m-d h:i:s');
                $this->common_model->data = array(
                    'admin_first_name' => htmlentities($this->input->post('txtFirstName')),
                    'admin_last_name' => htmlentities($this->input->post('txtLastName')),
                    'admin_email' => htmlentities($this->input->post('txtEmailAddress')),
                    'admin_address' => htmlentities($this->input->post('txtAddress')),
                    'admin_phone' => $this->input->post('txtPhone'),
                    'admin_role' => $this->input->post('txtAdminRole'),
                    'current_password' => md5($this->input->post('txtPassword')),
                    'status' => ($this->input->post('txtIsActive')== 1) ? '1': '0',
//                    'profile_picture' => $upload_result['file_name'],
                    'modified' => $date
                );
                if(!empty($upload_result['file_name'])){
                    $this->common_model->data = array(
                        'profile_picture' => $upload_result['file_name']
                    );

                }
                $this->common_model->table_name = 'tst_admin_user';
                $this->common_model->where = array('admin_user_id' => $this->input->post('txtAdminUserId'));
                $result = $this->common_model->update();
                if ($result) {
                    redirect('backdoor/edit_admin_user/success');
                } else {
                    redirect('backdoor/edit_admin_user/error');
                }

            }
        }

    }

    public function user_list()
    {
        if ($this->session->userdata('admin_email')) {
            $data['title'] = "User List";
            $this->load->view('admin/includes/admin_header', $data);
            $this->load->view('admin/user_list');
            $this->load->view('admin/includes/admin_footer');
        }else {
            redirect('backdoor/index');
        }
    }


    public function do_upload($path, $field = '')
    {
        $date = date('Ymdhisu');
        $this->load->library('upload');
        $config['upload_path'] = $path;
        $config['allowed_types'] = 'gif|jpg|png|jpeg';
        $config['max_size'] = '4096';
        $config['file_name'] = $date;

        $this->upload->initialize($config);

        if (!$this->upload->do_upload($field)) {
            return $this->upload->display_errors();
        } else {
            return $this->upload->data();
        }
    }

    public function make($msg = '')
    {
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor');
        } else {

            if ($msg == 'success') {
                $data['feedback'] = '<div class="text-center alert alert-success">Successfully Save !!</div>';
            } else if ($msg == 'error') {
                $data['feedback'] = '<div class=" text-center alert alert-danger">Problem to Insert !!</div>';
            }
            $this->form_validation->set_rules('txtManufacturar', 'Manufacturar Name', 'trim|required|min_length[2]|max_length[20]|xss_clean|is_unique[tbl_make.name]');

            if ($this->form_validation->run() == FALSE) {
                $data['title'] = "Create Manufacturar";
                $this->load->view('admin/includes/admin_header', $data);
                $this->load->view('admin/make');
                $this->load->view('admin/includes/admin_footer');
            } else {
                $date = date('Y-m-d h:i:s');
                $this->common_model->data = array(
                    'name' => $this->input->post('txtManufacturar'),
                    'remarks' => $this->input->post('txtRemarks'),
                    'created_by' => $this->input->post('admin_user_id'),
                    'created' => $date
                );
                $this->common_model->table_name = 'tbl_make';
                $result = $this->common_model->insert();

                if ($result) {
                    redirect('backdoor/make/success');
                } else {
                    redirect('backdoor/make/error');
                }

            }
        }

    }

    public function make_list($msg=''){
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor');
        } else {
            $data['title'] = "Make List";
            $this->load->view('admin/includes/admin_header', $data);
            $this->load->view('admin/make_list');
            $this->load->view('admin/includes/admin_footer');
        }
    }

    public function edit_make($msg = '')
    {
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor');
        } else {

            if ($msg == 'success') {
                $data['feedback'] = '<div class="text-center alert alert-success">Successfully Update !!</div>';
            } else if ($msg == 'error') {
                $data['feedback'] = '<div class=" text-center alert alert-danger">Problem to Update !!</div>';
            }
            $this->form_validation->set_rules('txtManufacturar', 'Manufacturar Name', 'trim|required|min_length[2]|max_length[20]|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $data['title'] = "Edit Manufacturar";
                $this->load->view('admin/includes/admin_header', $data);
                $this->load->view('admin/edit_make');
                $this->load->view('admin/includes/admin_footer');
            } else {
                $date = date('Y-m-d h:i:s');
                $this->common_model->data = array(
                    'name' => $this->input->post('txtManufacturar'),
                    'remarks' => $this->input->post('txtRemarks'),
                    'modified' => $date
                );
                $this->common_model->table_name = 'tbl_make';
                $this->common_model->where = array('make_id' => $this->input->post('txtMakeId'));
                $result = $this->common_model->update();
                if ($result) {
                    redirect('backdoor/edit_make/success');
                } else {
                    redirect('backdoor/edit_make/error');
                }

            }
        }

    }

    public function model($msg = '')
    {
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor');
        } else {

            if ($msg == 'success') {
                $data['feedback'] = '<div class="text-center alert alert-success">Successfully Save !!</div>';
            } else if ($msg == 'error') {
                $data['feedback'] = '<div class=" text-center alert alert-danger">Problem to Insert !!</div>';
            }
            $this->form_validation->set_rules('txtModelName', 'Model Name', 'trim|required|min_length[2]|max_length[70]|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $data['title'] = "Create Model";
                $this->load->view('admin/includes/admin_header', $data);
                $this->load->view('admin/model');
                $this->load->view('admin/includes/admin_footer');
            } else {
                $date = date('Y-m-d h:i:s');
                $this->common_model->data = array(
                    'model_name' => htmlentities($this->input->post('txtModelName')),
                    'make_id' => $this->input->post('txtMakeId'),
                    'created_by' => $this->session->userdata('admin_user_id'),
                    'created' => $date,
                );
                $this->common_model->table_name = 'tbl_model';
                $result = $this->common_model->insert();

                if ($result) {
                    redirect('backdoor/model/success');
                } else {
                    redirect('backdoor/model/error');
                }

            }
        }

    }

    public function model_list($msg=''){
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor');
        } else {
            $data['title'] = "Model List";
            $this->load->view('admin/includes/admin_header', $data);
            $this->load->view('admin/model_list');
            $this->load->view('admin/includes/admin_footer');
        }
    }

    public function edit_model($msg = '')
    {
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor');
        } else {

            if ($msg == 'success') {
                $data['feedback'] = '<div class="text-center alert alert-success">Successfully Updated !!</div>';
            } else if ($msg == 'error') {
                $data['feedback'] = '<div class=" text-center alert alert-danger">Problem to Update !!' . mysql_error() . '</div>';
            }
            $this->form_validation->set_rules('txtModelName', 'Model Name', 'trim|required|min_length[2]|max_length[70]|xss_clean');
            $this->form_validation->set_rules('txtMakeId', 'Select Manufacturar', 'trim|required|is_natural_no_zero|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $data['title'] = "Update Model";
                $this->load->view('admin/includes/admin_header', $data);
                $this->load->view('admin/edit_model');
                $this->load->view('admin/includes/admin_footer');
            } else {
                $date = date('Y-m-d h:i:s');
                $this->common_model->data = array(
                    'model_name' => htmlentities($this->input->post('txtModelName')),
                    'make_id' => $this->input->post('txtMakeId'),
                    'created_by' => $this->session->userdata('admin_user_id'),
                    'modified' => $date,
                );
                $this->common_model->table_name = 'tbl_model';
                $this->common_model->where = array('model_id' => $this->input->post('txtModelId'));
                $result = $this->common_model->update();

                if ($result) {
                    redirect('backdoor/edit_model/success');
                } else {
                    redirect('backdoor/edit_model/error');
                }

            }
        }

    }

    public function body_style($msg = '')
    {
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor');
        } else {

            if ($msg == 'success') {
                $data['feedback'] = '<div class="text-center alert alert-success">Successfully Save !!</div>';
            } else if ($msg == 'error') {
                $data['feedback'] = '<div class=" text-center alert alert-danger">Problem to Insert !!</div>';
            }
            $this->form_validation->set_rules('txtBodyStyle', 'BodyStyle', 'trim|required|min_length[2]|max_length[70]|xss_clean');
            $this->form_validation->set_rules('txtModel', 'Model', 'trim|required');
            $this->form_validation->set_rules('txtMakeId', 'MakeId', 'trim|required');
            $this->form_validation->set_rules('txtRemarks', 'Remarks', 'trim|max_length[150]');

            if ($this->form_validation->run() == FALSE) {
                $data['title'] = "Create Body Style";
                $this->load->view('admin/includes/admin_header', $data);
                $this->load->view('admin/body_style');
                $this->load->view('admin/includes/admin_footer');
            } else {
                $date = date('Y-m-d h:i:s');
                $this->common_model->data = array(
                    'body_style' => htmlentities($this->input->post('txtBodyStyle')),
                    'make_id' => $this->input->post('txtMakeId'),
                    'model_id' => $this->input->post('txtModel'),
                    'remarks' => $this->input->post('txtRemarks'),
                    'created_by' => $this->session->userdata('admin_user_id'),
                    'created' => $date,
                );
                $this->common_model->table_name = 'tbl_body_style';
                $result = $this->common_model->insert();

                if ($result) {
                    redirect('backdoor/body_style/success');
                } else {
                    redirect('backdoor/body_style/error');
                }

            }
        }

    }

    public function body_style_list($msg=''){
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor');
        } else {
            $data['title'] = "Body Style List";
            $this->load->view('admin/includes/admin_header', $data);
            $this->load->view('admin/body_style_list');
            $this->load->view('admin/includes/admin_footer');
        }
    }

    public function edit_body_style($msg = '')
    {
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor');
        } else {

            if ($msg == 'success') {
                $data['feedback'] = '<div class="text-center alert alert-success">Successfully Updated !!</div>';
            } else if ($msg == 'error') {
                $data['feedback'] = '<div class=" text-center alert alert-danger">Problem to Update !!' . mysql_error() . '</div>';
            }
            $this->form_validation->set_rules('txtBodyStyle', 'BodyStyle', 'trim|required|min_length[2]|max_length[70]|xss_clean');
            $this->form_validation->set_rules('txtModel', 'Model', 'trim|required');
            $this->form_validation->set_rules('txtMakeId', 'MakeId', 'trim|required');
            $this->form_validation->set_rules('txtRemarks', 'Remarks', 'trim|max_length[150]');
            if ($this->form_validation->run() == FALSE) {
                $data['title'] = "Update Body style";
                $this->load->view('admin/includes/admin_header', $data);
                $this->load->view('admin/edit_body_style');
                $this->load->view('admin/includes/admin_footer');
            } else {
                $this->common_model->data = array(
                    'body_style' => htmlentities($this->input->post('txtBodyStyle')),
                    'make_id' => $this->input->post('txtMakeId'),
                    'model_id' => $this->input->post('txtModel'),
                    'remarks' => $this->input->post('txtRemarks'),
                    'modified_by' => $this->session->userdata('admin_user_id'),
                );
                $this->common_model->table_name = 'tbl_body_style';
                $this->common_model->where = array('id' => $this->input->post('txtBodyStyleId'));
                $result = $this->common_model->update();

                if ($result) {
                    redirect('backdoor/edit_body_style/success');
                } else {
                    redirect('backdoor/edit_body_style/error');
                }

            }
        }

    }

    public function transmission($msg = '')
    {
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor/index');
        } else {

            if ($msg == 'success') {
                $data['feedback'] = '<div class="text-center alert alert-success">Successfully Save !!</div>';
            } else if ($msg == 'error') {
                $data['feedback'] = '<div class=" text-center alert alert-danger">Problem to Insert !!</div>';
            }
            $this->form_validation->set_rules('txtTransmission', 'Transmission', 'trim|required|min_length[2]|max_length[70]|xss_clean');
            $this->form_validation->set_rules('txtModel', 'Model', 'trim|required');
            $this->form_validation->set_rules('txtMakeId', 'MakeId', 'trim|required');
            $this->form_validation->set_rules('txtRemarks', 'Remarks', 'trim|max_length[150]|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $data['title'] = "Create Transmission";
                $this->load->view('admin/includes/admin_header', $data);
                $this->load->view('admin/transmission');
                $this->load->view('admin/includes/admin_footer');
            } else {
                $date = date('Y-m-d h:i:s');
                $this->common_model->data = array(
                    'transmission' => htmlentities($this->input->post('txtTransmission')),
                    'make_id' => $this->input->post('txtMakeId'),
                    'model_id' => $this->input->post('txtModel'),
                    'remarks' => $this->input->post('txtRemarks'),
                    'created_by' => $this->session->userdata('admin_user_id'),
                    'created' => $date,
                );
                $this->common_model->table_name = 'tbl_transmission';
                $result = $this->common_model->insert();

                if ($result) {
                    redirect('backdoor/transmission/success');
                } else {
                    redirect('backdoor/transmission/error');
                }

            }
        }

    }

    public function transmission_list($msg=''){
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor');
        } else {
            $data['title'] = "Transmission List";
            $this->load->view('admin/includes/admin_header', $data);
            $this->load->view('admin/transmission_list');
            $this->load->view('admin/includes/admin_footer');
        }
    }

    public function edit_transmission($msg = '')
    {
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor');
        } else {

            if ($msg == 'success') {
                $data['feedback'] = '<div class="text-center alert alert-success">Successfully Updated !!</div>';
            } else if ($msg == 'error') {
                $data['feedback'] = '<div class=" text-center alert alert-danger">Problem to Update !! </div>';
            }
            $this->form_validation->set_rules('txtTransmission', 'Transmission', 'trim|required|min_length[2]|max_length[70]|xss_clean');
            $this->form_validation->set_rules('txtModel', 'Model', 'trim|required');
            $this->form_validation->set_rules('txtMakeId', 'MakeId', 'trim|required');
            $this->form_validation->set_rules('txtRemarks', 'Remarks', 'trim|max_length[120]|xss_clean');
            if ($this->form_validation->run() == FALSE) {
                $data['title'] = "Update Transmission";
                $this->load->view('admin/includes/admin_header', $data);
                $this->load->view('admin/edit_transmission');
                $this->load->view('admin/includes/admin_footer');
            } else {
                $this->common_model->data = array(
                    'transmission' => htmlentities($this->input->post('txtTransmission')),
                    'make_id' => $this->input->post('txtMakeId'),
                    'model_id' => $this->input->post('txtModel'),
                    'remarks' => $this->input->post('txtRemarks'),
                    'modified_by' => $this->session->userdata('admin_user_id'),
                );
                $this->common_model->table_name = 'tbl_transmission';
                $this->common_model->where = array('id' => $this->input->post('txtTransmissionId'));
                $result = $this->common_model->update();

                if ($result) {
                    redirect('backdoor/edit_transmission/success');
                } else {
                    redirect('backdoor/edit_transmission/error');
                }

            }
        }

    }

    public function engine_type($msg = '')
    {
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor/index');
        } else {

            if ($msg == 'success') {
                $data['feedback'] = '<div class="text-center alert alert-success">Successfully Save !!</div>';
            } else if ($msg == 'error') {
                $data['feedback'] = '<div class=" text-center alert alert-danger">Problem to Insert !!</div>';
            }
            $this->form_validation->set_rules('txtEngineType', 'Engine Type', 'trim|required|min_length[2]|max_length[70]|xss_clean');
            $this->form_validation->set_rules('txtModel', 'Model', 'trim|required');
            $this->form_validation->set_rules('txtMakeId', 'MakeId', 'trim|required');
            $this->form_validation->set_rules('txtRemarks', 'Remarks', 'trim|max_length[150]|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $data['title'] = "Create Engine Type";
                $this->load->view('admin/includes/admin_header', $data);
                $this->load->view('admin/engine_type');
                $this->load->view('admin/includes/admin_footer');
            } else {
                $date = date('Y-m-d h:i:s');
                $this->common_model->data = array(
                    'engine_type' => htmlentities($this->input->post('txtEngineType')),
                    'make_id' => $this->input->post('txtMakeId'),
                    'model_id' => $this->input->post('txtModel'),
                    'remarks' => $this->input->post('txtRemarks'),
                    'created_by' => $this->session->userdata('admin_user_id'),
                    'created' => $date,
                );
                $this->common_model->table_name = 'tbl_engine_type';
                $result = $this->common_model->insert();

                if ($result) {
                    redirect('backdoor/engine_type/success');
                } else {
                    redirect('backdoor/engine_type/error');
                }

            }
        }

    }

    public function engine_type_list($msg=''){
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor');
        } else {
            $data['title'] = "Engine Type List";
            $this->load->view('admin/includes/admin_header', $data);
            $this->load->view('admin/engine_type_list');
            $this->load->view('admin/includes/admin_footer');
        }
    }

    public function edit_engine_type($msg = '')
    {
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor');
        } else {

            if ($msg == 'success') {
                $data['feedback'] = '<div class="text-center alert alert-success">Successfully Updated !!</div>';
            } else if ($msg == 'error') {
                $data['feedback'] = '<div class=" text-center alert alert-danger">Problem to Update !! </div>';
            }
            $this->form_validation->set_rules('txtEngineType', 'Engine Type', 'trim|required|min_length[2]|max_length[70]|xss_clean');
            $this->form_validation->set_rules('txtModel', 'Model', 'trim|required');
            $this->form_validation->set_rules('txtMakeId', 'MakeId', 'trim|required');
            $this->form_validation->set_rules('txtRemarks', 'Remarks', 'trim|max_length[120]|xss_clean');
            if ($this->form_validation->run() == FALSE) {
                $data['title'] = "Update Engine Type";
                $this->load->view('admin/includes/admin_header', $data);
                $this->load->view('admin/edit_engine_type');
                $this->load->view('admin/includes/admin_footer');
            } else {
                $this->common_model->data = array(
                    'engine_type' => htmlentities($this->input->post('txtEngineType')),
                    'make_id' => $this->input->post('txtMakeId'),
                    'model_id' => $this->input->post('txtModel'),
                    'remarks' => $this->input->post('txtRemarks'),
                    'modified_by' => $this->session->userdata('admin_user_id'),
                );
                $this->common_model->table_name = 'tbl_engine_type';
                $this->common_model->where = array('id' => $this->input->post('txtEngineTypeId'));
                $result = $this->common_model->update();

                if ($result) {
                    redirect('backdoor/edit_engine_type/success');
                } else {
                    redirect('backdoor/edit_engine_type/error');
                }

            }
        }

    }

    public function admin_details()
    {
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor');
        } else {
            $data['admin_id'] = $this->uri->segment(3);
            $data['title'] = "Admin Details";
            $this->load->view('admin/includes/admin_header', $data);
            $this->load->view('admin/admin_details');
            $this->load->view('admin/includes/admin_footer');
        }
    }

    public function vehicle_category($msg = '')
    {
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor');
        } else {

            if ($msg == 'success') {
                $data['feedback'] = '<div class="text-center alert alert-success">Successfully Save !!</div>';
            } else if ($msg == 'error') {
                $data['feedback'] = '<div class=" text-center alert alert-danger">Problem to Insert !!</div>';
            }
            $this->form_validation->set_rules('txtVehicleCategory', 'Vehicle Category', 'trim|required|min_length[2]|max_length[25]|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $data['title'] = "Create Vehicle Category";
                $this->load->view('admin/includes/admin_header', $data);
                $this->load->view('admin/vehicle_category');
                $this->load->view('admin/includes/admin_footer');
            } else {
                $date = date('Y-m-d h:i:s');
                $this->common_model->data = array(
                    'name' => htmlentities($this->input->post('txtVehicleCategory')),
                    'remarks' => htmlentities($this->input->post('txtRemarks')),
                    'created_by' => $this->session->userdata('admin_user_id'),
                    'created' => $date,
                );

                $this->common_model->table_name = 'tbl_category';
                $result = $this->common_model->insert();

                if ($result) {
                    redirect('backdoor/vehicle_category/success');
                } else {
                    redirect('backdoor/vehicle_category/error');
                }

            }
        }

    }

    public function category_list($msg=''){
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor');
        } else {
            $data['title'] = "Category List";
            $this->load->view('admin/includes/admin_header', $data);
            $this->load->view('admin/category_list');
            $this->load->view('admin/includes/admin_footer');
        }
    }

    public function edit_vehicle_category($msg = '')
    {
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor');
        } else {

            if ($msg == 'success') {
                $data['feedback'] = '<div class="text-center alert alert-success">Successfully Update !!</div>';
            } else if ($msg == 'error') {
                $data['feedback'] = '<div class=" text-center alert alert-danger">Problem to Update !!</div>';
            }
            $this->form_validation->set_rules('txtVehicleCategory', 'Vehicle Category', 'trim|required|min_length[2]|max_length[25]|xss_clean');

            if ($this->form_validation->run() == FALSE) {
                $data['title'] = "Create Vehicle Category";
                $this->load->view('admin/includes/admin_header', $data);
                $this->load->view('admin/edit_vehicle_category');
                $this->load->view('admin/includes/admin_footer');
            } else {
                $date = date('Y-m-d h:i:s');
                $this->common_model->data = array(
                    'name' => htmlentities($this->input->post('txtVehicleCategory')),
                    'remarks' => htmlentities($this->input->post('txtRemarks')),
                    'modified' => $date,
                );

                $this->common_model->table_name = 'tbl_category';
                $this->common_model->where = array('category_id' => $this->input->post('txtVehicleCategoryId'));
                $result = $this->common_model->update();

                /* $this->common_model->table_name='tbl_category';
                 $result=$this->common_model->insert();*/

                if ($result) {
                    redirect('backdoor/edit_vehicle_category/success');
                } else {
                    redirect('backdoor/edit_vehicle_category/error');
                }

            }
        }

    }

    public function create_product($msg = '')
    {
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor');
        } else {

            if ($msg == 'success') {
                $data['feedback'] = '<div class="text-center alert alert-success">Successfully Save !!</div>';
            } else if ($msg == 'error') {
                $data['feedback'] = '<div class=" text-center alert alert-danger">Problem to Insert !!</div>';
            }


            $this->form_validation->set_rules('txtMakeId', 'Manufacture ', 'trim|required|xss_clean');
            $this->form_validation->set_rules('txtModel', 'Model', 'trim|required|xss_clean');
            $this->form_validation->set_rules('txtBodyStyle', 'Body Style', 'trim|xss_clean');
            $this->form_validation->set_rules('txtTransmission', 'Transmission', 'trim|required');
            $this->form_validation->set_rules('txtEngineType', 'Engine Type', 'trim|required');
            $this->form_validation->set_rules('txtCategory', 'Category', 'trim|required|xss_clean');
            $this->form_validation->set_rules('manufactureYear', 'Manufacture Year', 'trim|required');
            $this->form_validation->set_rules('manufactureMonth', 'Manufacture Month', 'trim|required|xss_clean');
            $this->form_validation->set_rules('txtPrice', 'Price', 'trim|required|min_length[2]|max_length[15]|xss_clean');
            $this->form_validation->set_rules('txtDisplacement', 'Displacement', 'trim|required|max_length[50]|xss_clean');
            $this->form_validation->set_rules('txtSteering', 'Steering', 'trim|required|max_length[15]|xss_clean');
            $this->form_validation->set_rules('txtCondition', 'Condition', 'trim|required|max_length[15]|xss_clean');
            $this->form_validation->set_rules('txtCountry', 'Made In', 'trim|required|max_length[25]|xss_clean');
            $this->form_validation->set_rules('txtFuel', 'Fuel', 'trim||max_length[29]|xss_clean|required');
            $this->form_validation->set_rules('txtMileage', 'Mileage', 'trim|max_length[25]|xss_clean|required');
            $this->form_validation->set_rules('txtDoor', 'Door', 'trim|is_natural|xss_clean|required');
            $this->form_validation->set_rules('txtPassenger', 'Passenger', 'trim|is_natural|xss_clean|required');
//            $this->form_validation->set_rules('txtPricePerCubicMeter', 'price per C3', 'trim|xss_clean|max_length[10]|required');
            $this->form_validation->set_rules('txtHeight', 'Height', 'trim|xss_clean|max_length[10]|required');
            $this->form_validation->set_rules('txtWidth', 'Width', 'trim|xss_clean|max_length[10]|required');
            $this->form_validation->set_rules('txtLength', 'Length', 'trim|xss_clean|max_length[10]|required');
            $this->form_validation->set_rules('txtExterior', 'Exterior', 'trim|max_length[50]|xss_clean|required');
            $this->form_validation->set_rules('txtInterior', 'Interior', 'trim|max_length[50]|xss_clean |required');
            $this->form_validation->set_rules('txtExpireDate', 'Expire Date', 'trim|exact_length[10]|xss_clean');
            $this->form_validation->set_rules('txtReferenceNo', 'Reference No', 'trim|max_length[25]|xss_clean');
            $this->form_validation->set_rules('txtOptions', 'Options', 'trim|xss_clean|required');
            $this->form_validation->set_rules('txtDriveType', 'Drive Type', 'trim|xss_clean|required');
            $this->form_validation->set_rules('txtVin', 'VIN', 'trim|xss_clean|required');
            $this->form_validation->set_rules('txtSeller', 'Seller', 'trim|xss_clean|required');
            if ($this->form_validation->run() == FALSE) {
                $data['title'] = "Create Product";
                $this->load->view('admin/includes/admin_header', $data);
                $this->load->view('admin/create_product');
                $this->load->view('admin/includes/admin_footer');
            } else {
                $data = $this->input->post();
//                print_r($data);die;
                $upload_result = $this->do_upload('./resource/images/car/', 'txtImages');
                /* print_r($upload_result);
                 die;*/
                $date = date('Y-m-d h:i:s');
                $dimension = round(($data['txtHeight']*$data['txtWidth']*$data['txtLength'])*0.000001,2);
//                echo $dimension; die;
                $this->common_model->data = array(
                    'make_id' =>$this->input->post('txtMakeId'),
                    'model_id' =>$this->input->post('txtModel'),
                    'category_id' =>$this->input->post('txtCategory'),
                    'manufacture_year' => htmlentities($this->input->post('manufactureYear')),
                    'manufacture_month' => htmlentities($this->input->post('manufactureMonth')),
                    'desplacement' => htmlentities($this->input->post('txtDisplacement')),
                    'condition' => htmlentities($this->input->post('txtCondition')),
                    'steering' => htmlentities($this->input->post('txtSteering')),
                    'price' => htmlentities($this->input->post('txtPrice')),
                    'made_in' => htmlentities($this->input->post('txtCountry')),
                    'fuel' => htmlentities($this->input->post('txtFuel')),
                    'body_style_id' => htmlentities($this->input->post('txtBodyStyle')),
                    'engine_type_id' => htmlentities($this->input->post('txtEngineType')),
                    'Mileage' => htmlentities($this->input->post('txtMileage')),
                    'transmission_id' => htmlentities($this->input->post('txtTransmission')),
                    'door' => htmlentities($this->input->post('txtDoor')),
                    'drive_type' => htmlentities($this->input->post('txtDriveType')),
                    'vin' => htmlentities($this->input->post('txtVin')),
                    'no_of_passenger' => htmlentities($this->input->post('txtPassenger')),
                    'dimension' => htmlentities($data['txtLength']."cm X ".$data['txtWidth']."cm X ".$data['txtHeight']."cm = ".$dimension),
                    'total_dimension'=>$dimension,
                    'exterior_color' => htmlentities($this->input->post('txtExterior')),
                    'length' => htmlentities($this->input->post('txtLength')),
                    'height' => htmlentities($this->input->post('txtHeight')),
                    'width' => htmlentities($this->input->post('txtWidth')),
//                    'per_cubic_meter_price' => htmlentities($this->input->post('txtPricePerCubicMeter')),
                    'interior_color' => htmlentities($this->input->post('txtInterior')),
                    'expiry_date' => date( "Y-m-d", strtotime($this->input->post('txtExpireDate'))),
                    'reference_no' => htmlentities($this->input->post('txtReferenceNo')),
                    'options' => htmlentities($this->input->post('txtOptions')),
                    'status' => $this->input->post('txtIsAvailable'),
                    'feature_image' => $upload_result['file_name'],
                    'created' => $date,
                    'seller_id' => htmlentities($this->input->post('txtSeller'))
                );
                $this->common_model->table_name = 'tbl_product';
                $result = $this->common_model->insert();

                if ($result) {
                    redirect('backdoor/create_product/success');
                } else {
                    redirect('backdoor/create_product/error');
                }

            }
        }

    }

    function get_model(){
        $make_id=$this->input->post('make_id');
        //$query = $this->db->query("SELECT * FROM tbl_states WHERE country_id=".$country_id);

        $this->common_model->order_column = 'model_id';
        $this->common_model->table_name = 'tbl_model';
        $this->common_model->where = array('make_id'=>$make_id);
        $query=$this->common_model->select_all();

        $option = "<option value=''> Select One</option>";
        foreach($query->result() as $model){
            $option .= "<option value='".$model->model_id."'>".$model->model_name."</option>";
        }
        echo $option;

    }


    function ajax_get_body_style(){
        $model_id=$this->input->post('model_id');
        //$query = $this->db->query("SELECT * FROM tbl_states WHERE country_id=".$country_id);

        $this->common_model->order_column = 'id';
        $this->common_model->table_name = 'tbl_body_style';
        $this->common_model->where = array('model_id'=>$model_id);
        $query=$this->common_model->select_all();

        $option = "<option value=''> Select One</option>";
        foreach($query->result() as $body_style){
            $option .= "<option value='".$body_style->id."'>".$body_style->body_style."</option>";
        }
        echo $option;

    }

    function ajax_get_transmission(){
        $model_id=$this->input->post('model_id');
        //$query = $this->db->query("SELECT * FROM tbl_states WHERE country_id=".$country_id);

        $this->common_model->order_column = 'id';
        $this->common_model->table_name = 'tbl_transmission';
        $this->common_model->where = array('model_id'=>$model_id);
        $query=$this->common_model->select_all();

        $option = "<option value=''> Select One</option>";
        foreach($query->result() as $transmission){
            $option .= "<option value='".$transmission->id."'>".$transmission->transmission."</option>";
        }
        echo $option;

    }

    function ajax_get_engine_type(){
        $model_id=$this->input->post('model_id');
        //$query = $this->db->query("SELECT * FROM tbl_states WHERE country_id=".$country_id);

        $this->common_model->order_column = 'id';
        $this->common_model->table_name = 'tbl_engine_type';
        $this->common_model->where = array('model_id'=>$model_id);
        $query=$this->common_model->select_all();

        $option = "<option value=''> Select One</option>";
        foreach($query->result() as $engine){
            $option .= "<option value='".$engine->id."'>".$engine->engine_type."</option>";
        }
        echo $option;

    }



    public function vehicle_list($msg=''){
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor');
        } else {

            if ($msg == 'success') {
                $data['feedback'] = '<div class="text-center alert alert-success">Successfully Save !!</div>';
            }else if ($msg == 'error') {
                $data['feedback'] = '<div class=" text-center alert alert-danger">Problem to Insert !!</div>';
            }

            $data['title'] = "Vehicle List";
            $this->load->view('admin/includes/admin_header', $data);
            $this->load->view('admin/vehicle_list');
            $this->load->view('admin/includes/admin_footer');
        }
    }

    public function edit_product($msg=''){
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor/index');
        } else {


            if ($msg == 'success') {
                $data['feedback'] = '<div class="text-center alert alert-success">Successfully Update !!</div>';
            } else if ($msg == 'error') {
                $data['feedback'] = '<div class=" text-center alert alert-danger">Problem to Update !!</div>';
            }
                $this->form_validation->set_rules('txtMakeId', 'Manufacture ', 'trim|required|xss_clean');
                $this->form_validation->set_rules('txtModel', 'Model', 'trim|required|xss_clean');
                $this->form_validation->set_rules('txtCategory', 'Category', 'trim|required|xss_clean');
                $this->form_validation->set_rules('manufactureYear', 'Manufacture Year', 'trim|required');
                $this->form_validation->set_rules('manufactureMonth', 'Manufacture Month', 'trim|required|xss_clean');
                $this->form_validation->set_rules('txtPrice', 'Price', 'trim|required|min_length[2]|max_length[15]|xss_clean');
                $this->form_validation->set_rules('txtDisplacement', 'Displacement', 'trim|max_length[50]|xss_clean');
                $this->form_validation->set_rules('txtSteering', 'Steering', 'trim|max_length[15]|xss_clean');
                $this->form_validation->set_rules('txtCondition', 'Condition', 'trim|required|max_length[15]|xss_clean');
                $this->form_validation->set_rules('txtCountry', 'Made In', 'trim|required|max_length[25]|xss_clean');
                $this->form_validation->set_rules('txtFuel', 'Fuel', 'trim||max_length[29]|xss_clean|required');
                $this->form_validation->set_rules('txtBodyStyle', 'Body Style', 'trim|required|required|xss_clean');
                $this->form_validation->set_rules('txtTransmission', 'Transmission', 'trim|xss_clean|required');
                $this->form_validation->set_rules('txtEngineType', 'Engine Type', 'trim|xss_clean|required');
                $this->form_validation->set_rules('txtMileage', 'Mileage', 'trim|max_length[25]|xss_clean|required');
                $this->form_validation->set_rules('txtDoor', 'Door', 'trim|is_natural|xss_clean|required');
                $this->form_validation->set_rules('txtPassenger', 'Passenger', 'trim|is_natural|xss_clean|required');
//                $this->form_validation->set_rules('txtPricePerCubicMeter', 'price per C3', 'trim|xss_clean|max_length[10]|required');
                $this->form_validation->set_rules('txtHeight', 'Height', 'trim|xss_clean|max_length[10]|required');
                $this->form_validation->set_rules('txtWidth', 'Width', 'trim|xss_clean|max_length[10]|required');
                $this->form_validation->set_rules('txtLength', 'Length', 'trim|xss_clean|max_length[10]|required');
                $this->form_validation->set_rules('txtExterior', 'Exterior', 'trim|max_length[50]|xss_clean|required');
                $this->form_validation->set_rules('txtInterior', 'Interior', 'trim|max_length[50]|xss_clean |required');
                $this->form_validation->set_rules('txtExpireDate', 'Expire Date', 'trim|exact_length[10]|xss_clean');
                $this->form_validation->set_rules('txtReferenceNo', 'Reference No', 'trim|max_length[25]|xss_clean');
                $this->form_validation->set_rules('txtOptions', 'Options', 'trim|xss_clean|required');
                $this->form_validation->set_rules('txtDriveType', 'Drive Type', 'trim|xss_clean|required');
                $this->form_validation->set_rules('txtVin', 'VIN', 'trim|xss_clean|required');
                $this->form_validation->set_rules('txtSeller', 'Seller', 'trim|xss_clean|required');

                if ($this->form_validation->run() == FALSE) {
                   $data['title'] = "Edit Vehicle";
                    $this->load->view('admin/includes/admin_header', $data);
                    $this->load->view('admin/edit_product');
                    $this->load->view('admin/includes/admin_footer');
                    /*$this->session->set_flashdata('errors', validation_errors());
                    redirect($this->session->flashdata('redirect'),refress);*/


                } else {
                    if($_FILES['txtImages']['size'] != 0) {

                        $upload_result = $this->do_upload('./resource/images/car/', 'txtImages');
                    }


                    $data = $this->input->post();
                    $dimension = round(($data['txtHeight']*$data['txtWidth']*$data['txtLength'])*0.000001,2);
                    /* print_r($upload_result);
                     die;*/
                    $date = date('Y-m-d h:i:s');
                    $this->common_model->data = array(
                        'make_id' =>$this->input->post('txtMakeId'),
                        'model_id' =>$this->input->post('txtModel'),
                        'category_id' =>$this->input->post('txtCategory'),
                        'manufacture_year' => htmlentities($this->input->post('manufactureYear')),
                        'manufacture_month' => htmlentities($this->input->post('manufactureMonth')),
                        'desplacement' => htmlentities($this->input->post('txtDisplacement')),
                        'condition' => htmlentities($this->input->post('txtCondition')),
                        'steering' => htmlentities($this->input->post('txtSteering')),
                        'price' => htmlentities($this->input->post('txtPrice')),
                        'made_in' => htmlentities($this->input->post('txtCountry')),
                        'fuel' => htmlentities($this->input->post('txtFuel')),
                        'body_style_id' => $this->input->post('txtBodyStyle'),
                        'Mileage' => htmlentities($this->input->post('txtMileage')),
                        'transmission_id' => $this->input->post('txtTransmission'),
                        'engine_type_id' => $this->input->post('txtEngineType'),
                        'door' => htmlentities($this->input->post('txtDoor')),
                        'drive_type' => htmlentities($this->input->post('txtDriveType')),
                        'vin' => htmlentities($this->input->post('txtVin')),
                        'no_of_passenger' => htmlentities($this->input->post('txtPassenger')),
                        'dimension' => htmlentities($data['txtLength']."cm X ".$data['txtWidth']."cm X ".$data['txtHeight']."cm = ".$dimension),
                        'total_dimension'=>$dimension,
                        'exterior_color' => htmlentities($this->input->post('txtExterior')),
                        'length' => htmlentities($this->input->post('txtLength')),
                        'height' => htmlentities($this->input->post('txtHeight')),
                        'width' => htmlentities($this->input->post('txtWidth')),
//                        'per_cubic_meter_price' => htmlentities($this->input->post('txtPricePerCubicMeter')),
                        'interior_color' => htmlentities($this->input->post('txtInterior')),
                        'expiry_date' => date( "Y-m-d", strtotime($this->input->post('txtExpireDate'))),
                        'reference_no' => htmlentities($this->input->post('txtReferenceNo')),
                        'options' => htmlentities($this->input->post('txtOptions')),
                        'status' => $this->input->post('txtIsAvailable'),
                        'seller_id' => htmlentities($this->input->post('txtSeller')),
                        'created' => $date,
                    );
                    if(!empty($upload_result['file_name'])){
                        $this->common_model->data = array(
                            'feature_image' => $upload_result['file_name']
                        );

                    }


                    /*print_r($this->common_model->data);
                    die;*/
                    $this->common_model->table_name = 'tbl_product';
                    $this->common_model->where = array('product_id' => $this->input->post('txtProductId'));
                    $result = $this->common_model->update();

                    /* $this->common_model->table_name='tbl_category';
                     $result=$this->common_model->insert();*/

                    if ($result) {
                        redirect('backdoor/edit_product/success');
                    } else {
                        redirect('backdoor/edit_product/error');
                    }

                }

        }


    }

    public function create_seller($msg = '')
    {

        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor/index');
        } else {

            if ($msg == 'success') {
                $data['feedback'] = '<div class="text-center alert alert-success">Successfully Save !!</div>';
            } else if ($msg == 'error') {
                $data['feedback'] = '<div class=" text-center alert alert-danger">Problem to Insert !!</div>';
            }
            $this->form_validation->set_rules('txtCompanyName', 'Company Name', 'trim|required|callback_alpha_dash_space|min_length[3]|max_length[120]');
            $this->form_validation->set_rules('txtCompanyTag', 'Tag', 'trim|required|min_length[3]|max_length[120]');
            $this->form_validation->set_rules('txtOwnership', 'Ownership', 'trim|required');
            $this->form_validation->set_rules('txtEmailAddress', 'Email Address', 'trim|valid_email|required|min_length[5]|max_length[80]|is_unique[tbl_company_information.email_address]');
            $this->form_validation->set_rules('txtPaymentTerms', 'Payment Terms', 'trim|required|min_length[5]|max_length[100]');
            $this->form_validation->set_rules('txtEstablishedYear', 'Established Year', 'trim|required|exact_length[4]|xss_clean');
            $this->form_validation->set_rules('txtAddress', 'Address', 'trim|required|min_length[5]|max_length[200]|xss_clean');
            $this->form_validation->set_rules('txtDescription', 'Description', 'trim|required|min_length[5]|max_length[1000]|xss_clean');
            $this->form_validation->set_rules('txtNoOfEmployee', 'No of Employee', 'trim|required|min_length[5]|max_length[30]|xss_clean');
            $this->form_validation->set_rules('txtBusinessClassification', 'Business Classification', 'trim|required|min_length[5]|max_length[30]|xss_clean');
            $this->form_validation->set_rules('txtLanguage', 'Language', 'trim|required|min_length[2]|max_length[30]|xss_clean');
            $this->form_validation->set_rules('txtCountry', 'Country', 'trim|required');
            $this->form_validation->set_rules('txtPhone', 'Phone', 'trim|required|callback_phone_number|min_length[7]|max_length[15]|xss_clean');

            if ($this->form_validation->run() == FALSE) {

                $data['title'] = "Create Seller";
                $this->load->view('admin/includes/admin_header', $data);
                $this->load->view('admin/create_seller');
                $this->load->view('admin/includes/admin_footer');
            } else {
                $upload_result = $this->do_upload('./resource/images/company/', 'txtLogo');

                $date = date('Y-m-d h:i:s');
                $this->common_model->data = array(
                    'company_name' => htmlentities($this->input->post('txtCompanyName')),
                    'company_tag' => htmlentities($this->input->post('txtCompanyTag')),
                    'ownership_type' => htmlentities($this->input->post('txtOwnership')),
                    'email_address' => htmlentities($this->input->post('txtEmailAddress')),
                    'phone_no' => htmlentities($this->input->post('txtPhone')),
                    'established_year' => htmlentities($this->input->post('txtEstablishedYear')),
                    'no_of_employee' => htmlentities($this->input->post('txtNoOfEmployee')),
                    'business_classification' =>htmlentities($this->input->post('txtBusinessClassification')),
                    'language' => htmlentities($this->input->post('txtLanguage')),
                    'country' => htmlentities($this->input->post('txtCountry')),
                    'payment_terms' => htmlentities($this->input->post('txtPaymentTerms')),
                    'description' => htmlentities($this->input->post('txtDescription')),
                    'address' => htmlentities($this->input->post('txtAddress')),
                    'status' => $this->input->post('txtIsActive'),
                    'logo_name' => $upload_result['file_name'],
                    'created' => $date,
                    'created_by' => $this->session->userdata('admin_user_id')
                );
                $this->common_model->table_name = 'tbl_company_information';
                $result = $this->common_model->insert();

                if ($result) {
                    redirect('backdoor/create_seller/success');
                } else {
                    redirect('backdoor/create_seller/error');
                }

            }
        }


    }

    public function edit_seller_info($msg = '')
    {

        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor/index');
        } else {

            if ($msg == 'success') {
                $data['feedback'] = '<div class="text-center alert alert-success">Successfully Save !!</div>';
            } else if ($msg == 'error') {
                $data['feedback'] = '<div class=" text-center alert alert-danger">Problem to Insert !!</div>';
            }
            $this->form_validation->set_rules('txtCompanyName', 'Company Name', 'trim|required|callback_alpha_dash_space|min_length[3]|max_length[120]');
            $this->form_validation->set_rules('txtCompanyTag', 'Tag', 'trim|required|min_length[3]|max_length[120]');
            $this->form_validation->set_rules('txtOwnership', 'Ownership', 'trim|required');
            $this->form_validation->set_rules('txtEmailAddress', 'Email Address', 'trim|valid_email|required|min_length[5]|max_length[80]');
            $this->form_validation->set_rules('txtPaymentTerms', 'Payment Terms', 'trim|required|min_length[5]|max_length[100]');
            $this->form_validation->set_rules('txtEstablishedYear', 'Established Year', 'trim|required|exact_length[4]|xss_clean');
            $this->form_validation->set_rules('txtAddress', 'Address', 'trim|required|min_length[5]|max_length[200]|xss_clean');
            $this->form_validation->set_rules('txtDescription', 'Description', 'trim|required|min_length[5]|max_length[1000]|xss_clean');
            $this->form_validation->set_rules('txtNoOfEmployee', 'No of Employee', 'trim|required|min_length[5]|max_length[30]|xss_clean');
            $this->form_validation->set_rules('txtBusinessClassification', 'Business Classification', 'trim|required|min_length[5]|max_length[30]|xss_clean');
            $this->form_validation->set_rules('txtLanguage', 'Language', 'trim|required|min_length[2]|max_length[30]|xss_clean');
            $this->form_validation->set_rules('txtCountry', 'Country', 'trim|required');
            $this->form_validation->set_rules('txtPhone', 'Phone', 'trim|required');

            if ($this->form_validation->run() == FALSE) {

                $data['title'] = "Update Seller";
                $this->load->view('admin/includes/admin_header', $data);
                $this->load->view('admin/edit_seller_info');
                $this->load->view('admin/includes/admin_footer');
            } else {
                if($_FILES['txtLogo']['size'] != 0) {
                    $upload_result = $this->do_upload('./resource/images/company/', 'txtLogo');
                }

                $this->common_model->data = array(
                    'company_name' => htmlentities($this->input->post('txtCompanyName')),
                    'company_tag' => htmlentities($this->input->post('txtCompanyTag')),
                    'ownership_type' => htmlentities($this->input->post('txtOwnership')),
                    'email_address' => htmlentities($this->input->post('txtEmailAddress')),
                    'phone_no' => htmlentities($this->input->post('txtPhone')),
                    'established_year' => htmlentities($this->input->post('txtEstablishedYear')),
                    'no_of_employee' => htmlentities($this->input->post('txtNoOfEmployee')),
                    'business_classification' =>htmlentities($this->input->post('txtBusinessClassification')),
                    'language' => htmlentities($this->input->post('txtLanguage')),
                    'country' => htmlentities($this->input->post('txtCountry')),
                    'payment_terms' => htmlentities($this->input->post('txtPaymentTerms')),
                    'description' => htmlentities($this->input->post('txtDescription')),
                    'address' => htmlentities($this->input->post('txtAddress')),
                    'status' => $this->input->post('txtIsActive'),
                    'modified_by' => $this->session->userdata('admin_user_id')
                );

                if(!empty($upload_result['file_name'])){
                    $this->common_model->data = array(
                        'logo_name' => $upload_result['file_name']
                    );

                }

                $this->common_model->table_name = 'tbl_company_information';
                $this->common_model->where = array('company_id' => $this->input->post('txtCompanyId'));
                $result = $this->common_model->update();
                if ($result) {
                    redirect('backdoor/edit_seller_info/success');
                } else {
                    redirect('backdoor/edit_seller_info/error');
                }

            }
        }


    }


    public function transportation_cost($msg = '')
    {
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor');
        } else {

            if ($msg == 'success') {
                $data['feedback'] = '<div class="text-center alert alert-success">Successfully Save !!</div>';
            } else if ($msg == 'error') {
                $data['feedback'] = '<div class=" text-center alert alert-danger">Problem to Insert !!</div>';
            }
            $this->form_validation->set_rules('txtPerCubicMeterCost', 'Per Cubic Meter Cost', 'trim|required|max_length[10]|xss_clean');
            $this->form_validation->set_rules('txtInspection', 'Inspection', 'trim|required|max_length[10]|xss_clean');
            $this->form_validation->set_rules('txtInsurance', 'Insurance', 'trim|required|max_length[10]|xss_clean');
            $this->form_validation->set_rules('txtCountry', 'Country', 'trim|required');
            $this->form_validation->set_rules('txtNearestPort', 'Nearest Port', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $data['title'] = "Create Transportation Cost";
                $this->load->view('admin/includes/admin_header', $data);
                $this->load->view('admin/transportation_cost');
                $this->load->view('admin/includes/admin_footer');
            } else {
                $date = date('Y-m-d h:i:s');
                $this->common_model->data = array(
                    'per_cubic_meter_price' => htmlentities($this->input->post('txtPerCubicMeterCost')),
                    'insurance_price' => htmlentities($this->input->post('txtInsurance')),
                    'inspection_price' => htmlentities($this->input->post('txtInspection')),
                    'country_id' => $this->input->post('txtCountry'),
                    'port_id' => $this->input->post('txtNearestPort'),
                    'created_by' => $this->session->userdata('admin_user_id'),
                    'created' => $date,
                );
                $this->common_model->table_name = 'tbl_transportation_cost';
                $result = $this->common_model->insert();

                if ($result) {
                    redirect('backdoor/transportation_cost/success');
                } else {
                    redirect('backdoor/transportation_cost/error');
                }

            }
        }

    }

    public function transportation_cost_list($msg=''){
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor');
        } else {
            $data['title'] = "Transport cost List";
            $this->load->view('admin/includes/admin_header', $data);
            $this->load->view('admin/transportation_cost_list');
            $this->load->view('admin/includes/admin_footer');
        }
    }
    public function seller_list($msg=''){
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor');
        } else {
            $data['title'] = "Seller List";
            $this->load->view('admin/includes/admin_header', $data);
            $this->load->view('admin/seller_list');
            $this->load->view('admin/includes/admin_footer');
        }
    }
    public function vehicle_image_list($msg=''){
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor');
        } else {
            $data['title'] = "Seller List";
            $this->load->view('admin/includes/admin_header', $data);
            $this->load->view('admin/image_list');
            $this->load->view('admin/includes/admin_footer');
        }
    }

    public function edit_transportation_cost($msg = '')
    {
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor');
        } else {

            if ($msg == 'success') {
                $data['feedback'] = '<div class="text-center alert alert-success">Successfully Updated !!</div>';
            } else if ($msg == 'error') {
                $data['feedback'] = '<div class=" text-center alert alert-danger">Problem to Update !!' . mysql_error() . '</div>';
            }
            $this->form_validation->set_rules('txtPerCubicMeterCost', 'Per Cubic Meter Cost', 'trim|required|max_length[10]|xss_clean');
            $this->form_validation->set_rules('txtInspection', 'Inspection', 'trim|required|max_length[10]|xss_clean');
            $this->form_validation->set_rules('txtInsurance', 'Insurance', 'trim|required|max_length[10]|xss_clean');
            $this->form_validation->set_rules('txtCountry', 'Country', 'trim|required');
            $this->form_validation->set_rules('txtNearestPort', 'Nearest Port', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $data['title'] = "Update Transport Cost";
                $this->load->view('admin/includes/admin_header', $data);
                $this->load->view('admin/edit_transportation_cost');
                $this->load->view('admin/includes/admin_footer');
            } else {
                $this->common_model->data = array(
                    'per_cubic_meter_price' => htmlentities($this->input->post('txtPerCubicMeterCost')),
                    'insurance_price' => htmlentities($this->input->post('txtInsurance')),
                    'inspection_price' => htmlentities($this->input->post('txtInspection')),
                    'country_id' => $this->input->post('txtCountry'),
                    'port_id' => $this->input->post('txtNearestPort'),
                    'modified_by' => $this->session->userdata('admin_user_id'),
                );
                $this->common_model->table_name = 'tbl_transportation_cost';
                $this->common_model->where = array('id' => $this->input->post('txtTransportationCostId'));
                $result = $this->common_model->update();

                if ($result) {
                    redirect('backdoor/edit_transportation_cost/success');
                } else {
                    redirect('backdoor/edit_transportation_cost/error');
                }

            }
        }

    }


    public function dollar_price($msg = '')
    {
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor');
        } else {

            if ($msg == 'success') {
                $data['feedback'] = '<div class="text-center alert alert-success">Successfully Update !!</div>';
            } else if ($msg == 'error') {
                $data['feedback'] = '<div class=" text-center alert alert-danger">Problem to Update !!</div>';
            }
            $this->form_validation->set_rules('txtDollarPrice', 'Dollar Price', 'trim|required');

            if ($this->form_validation->run() == FALSE) {
                $data['title'] = "Dollar Price";
                $this->load->view('admin/includes/admin_header', $data);
                $this->load->view('admin/doller_price');
                $this->load->view('admin/includes/admin_footer');
            } else {

                $this->common_model->data = array(
                    'dollar_price' => $this->input->post('txtDollarPrice'),
                    'remarks' => $this->input->post('txtRemarks'),
                );
                $this->common_model->table_name = 'tbl_dollar_price';
                $this->common_model->where = array('id' => $this->input->post('txtDollarPriceId'));
                $result = $this->common_model->update();
                if ($result) {
                    redirect('backdoor/dollar_price/success');
                } else {
                    redirect('backdoor/dollar_price/error');
                }

            }
        }

    }

    public function add_product_images($msg=''){
        if (!$this->session->userdata('admin_email')) {
            redirect('backdoor/index');
        } else {

            if ($msg == 'success') {
                $data['feedback'] = '<div class="text-center alert alert-success">Successfully Save !!</div>';
            } else if ($msg == 'error') {
                $data['feedback'] = '<div class=" text-center alert alert-danger">Problem to Insert !!</div>';
            }else if($msg == 'error_image'){
                $data['feedback'] = '<div class=" text-center alert alert-danger">Please select minimum one Image !!</div>';
            }
            $this->form_validation->set_rules('txtMakeId', 'Make', 'trim|required');
            $this->form_validation->set_rules('txtModel', 'Model', 'trim|required');
            $this->form_validation->set_rules('txtCarId', 'Product ID', 'trim|required');
            if ($this->form_validation->run() == FALSE) {
                $data['title'] = "Add Images";
                $this->load->view('admin/includes/admin_header', $data);
                $this->load->view('admin/add_product_images');
                $this->load->view('admin/includes/admin_footer');
            } else {
                if(count($_FILES['images']['size']) != 0) {
                    $files = $_FILES;

                    $cpt = count($_FILES['images']['name']);
                    $product_id = $this->input->post('txtCarId');
                    $result = array();

                    for($i=0; $i<$cpt; $i++)
                     {
                        $_FILES['images']['name']= $files['images']['name'][$i];
                         $_FILES['images']['type']= $files['images']['type'][$i];
                         $_FILES['images']['tmp_name']= $files['images']['tmp_name'][$i];
                         $_FILES['images']['error']= $files['images']['error'][$i];
                         $_FILES['images']['size']= $files['images']['size'][$i];

                        $upload_result=$this->do_upload('./resource/images/car_images/','images');
                        /*echo "<pre>";
                        echo count($_FILES['images']['size'])."<br/>";
                        print_r($upload_result);
                        echo '</pre>';
                        die;*/

                        $this->common_model->data = array('product_id' => $product_id, 'attachment_name' => $upload_result['file_name']);
                        $this->common_model->table_name = 'tbl_attachment';
                        $result[] = $this->common_model->insert();
                    }
                    if ($result) {
                        redirect('backdoor/add_product_images/success');
                    } else {
                        redirect('backdoor/add_product_images/error');
                    }

                }else {
                    redirect('backdoor/add_product_images/error_image');
                }

            }
        }

    }

}//--End of Controller