$(function(){
    $("#loginSubmit").click(function(e){
        e.preventDefault();
        var currentUrl = $('.currentUrl').val();
        $.ajax({
            type: "post",
            url: base_url+"en/fuserlogin",
            data: $('#myform').serialize(),
            beforeSend: function(){
//                $('.inmodalloading').attr('src', base_url+'images/loading.gif');
//                $(".inmodalloading").show();
            },
            success: function(response){
                if(response == 'done'){
                    if(currentUrl != "")
                    {
                        window.location.href = currentUrl;
                    }
                    else
                    {
                        window.location.href = base_url+"dashboard";
                    }
                }
                else
                {
                    $(".err_msg").html(response);
                }
            }
        });
    });
});

$(function(){
    $("#firstSignUp").click(function(e){
        e.preventDefault();
        //var currentUrl = $('.currentUrl').val();
        $.ajax({
            type: "post",
            url: base_url+"en/firstSignUpForm",
            data: $('#firstSignUpForm').serialize(),
            beforeSend: function(){
//                $('.inmodalloading').attr('src', base_url+'images/loading.gif');
//                $(".inmodalloading").show();
            },
            success: function(response){
                $(".err_msg").html(response);
                $("#fsemail").val('');
                $("#fscemail").val('');
            }
        });
    });
});

$(function(){
   $('.btnNext').click(function(){
        $('.nav-tabs > .active').next('li').find('a').trigger('click');
      });

        $('.btnPrevious').click(function(){
        $('.nav-tabs > .active').prev('li').find('a').trigger('click');
      });
});


$(function(){

    function show_error (id_name, msg) {
            var html_jsval  =    '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>';
                    html_jsval +=    msg;
                    html_jsval +=    '</div>';	
            $("#"+id_name).html(html_jsval);
            return false;
    }		
    function check_complete(classname, message) 
    {
            if(classname.indexOf('anchor_disable')>"0")
            {
                    // alert(message);
                    show_error ("errorMsg", message ) ;
                    return true;	
            }
    }
    $('.signupComplete').click(function(event) 
    {
        var classname = $(this).attr('class');
        var first_name = $('.first_name').val();
        var last_name = $('.last_name').val();
        var address = $('.address').val();
        var postal_code = $('.postal_code').val();
        var password = $('.password').val();
        var mobile_no = $('.mobile_no').val();

        if(password == ''){
            if( check_complete( classname, "Password can not be empty in user information tab." ) )
            {
                return false;
            }
        }else if(first_name == ''){
            if( check_complete( classname, "First Name can not be empty in user information tab." ) )
            {
                    return false;
            }
        }
        else if(last_name == ''){
            if( check_complete( classname, "Last Name can not be empty in user information tab." ) )
            {
                return false;
            }
        }else if(postal_code == ''){
                if( check_complete( classname, "Postal Code can not be empty in user information tab." ) )
                {
                    return false;
                }
        }else if(address == ''){
                if( check_complete( classname, "Address can not be empty in user information tab." ) )
                {
                    return false;
                }
        }else if(mobile_no == ''){
                if( check_complete( classname, "Mobile can not be empty in user information tab." ) )
                {
                    return false;
                }
        }
        else
        {
            $('#errorMsg').html('');
        }

    });           
});

$(function(){
    $('.registrationComplete').click(function(){
        $.ajax({
            type: "post",
            url: base_url+"en/CompleteRegistration",
            data: $('#signUpcompleteForm').serialize(),
            beforeSend: function(){
//                $('.inmodalloading').attr('src', base_url+'images/loading.gif');
//                $(".inmodalloading").show();
            },
            success: function(response){
                $('.registration_success_message').html(response);
            }
        });
    });
});

$(function(){
    $('.confirmUserInfo').click(function(){
        var customer_country = $('.customer_country').val();
        var customer_email = $('.customer_email').val();
        var password = $('.password').val();
        var first_name = $('.first_name').val();
        var last_name = $('.last_name').val();
        var customer_date_of_birth = $('.customer_date_of_birth').val();
        var address = $('.address').val();
        var postal_code = $('.postal_code').val();
        var mobile_no = $('.mobile_no').val();
        var home_phone = $('.home_phone').val();

        $('.ccountry').val(customer_country);
        $('.cemail').val(customer_email);
        $('.cfirstName').val(first_name);
        $('.clastName').val(last_name);
        $('.cAddress').val(address);
        $('.cpassword').val(password);
        $('.cdateOfBirth').val(customer_date_of_birth);
        $('.cpostalCode').val(postal_code);
        $('.cmobile_no').val(mobile_no);
        $('.chome_phone').val(home_phone);

    });
});


$(function(){
    $('#send_negotiation').click(function(){
        $.ajax({
            type: "post",
            url: base_url+"en/send_negotiation",
            data: $('#negotiationForm').serialize(),
            beforeSend: function(){
//                $('.inmodalloading').attr('src', base_url+'images/loading.gif');
//                $(".inmodalloading").show();
            },
            success: function(response){
                if(response != 'not_login'){
                    $('#exampleModal').modal('toggle');
                    $('#message_feedback').html(response);
                    $('#messageModal').modal('show');
                }else{
                    //window.location.replace(window.location.hostname+'/en/start');
                    window.location.href ='http://tstjapan.co.jp/en/start/';
                }
            }
        });
    });
});