# # TST JAPAN # 

This is a customize admin panel using CodeIgniter 2.x version and Bootstrap 3

1.0 [Tarek][03-12-2015] : Create Login script

1.01 [Tarek][06-12-2015] : Create Admin User Role insert, update, delete script

1.02 [Tarek][06-12-2015] : Create Admin User  insert, update, delete script

1.03 [Tarek][07-12-2015] : Create Manufacturar and Model  insert, update, delete script

1.04 [Tarek][14-12-2015] : Create Product Ajax call by Manufacture change script

1.05 [Tarek][24-12-2015] : Include font end 

1.06 [Tarek][24-01-2016] : Add Sign up and login
 
1.07 [Tarek][26-01-2016] : Complete menu and its search
 
1.08 [Tarek][29-01-2017] : Complete search by price and search by category
 
1.09 [Tarek][30-01-2016] : Complete car Details view

1.10 [Tarek][30-01-2016] : Include Header in Dashboard

1.11 [Tarek][31-01-2016] : Update product Info


