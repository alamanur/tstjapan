$(function(){
    $("#loginSubmit").click(function(e){
        e.preventDefault();
        var currentUrl = $('.currentUrl').val();
        $.ajax({
            type: "post",
            url: base_url+"en/fuserlogin",
            data: $('#myform').serialize(),
            beforeSend: function(){
//                $('.inmodalloading').attr('src', base_url+'images/loading.gif');
//                $(".inmodalloading").show();
            },
            success: function(response){
                if(response == 'done'){
                    if(currentUrl != "")
                    {
                        window.location.href = currentUrl;
                    }
                    else
                    {
                        window.location.href = base_url;
                    }
                }
                else
                {
                    $(".err_msg").html(response);
                }
            }
        });
    });
});

$(function(){
    $("#firstSignUp").click(function(e){
        e.preventDefault();
        //alert();
        var currentUrl = $('.currentUrl').val();
        $.ajax({
            type: "post",
            url: base_url+"en/firstSignUpForm",
            data: $('#firstSignUpForm').serialize(),
            beforeSend: function(){
//                $('.inmodalloading').attr('src', base_url+'images/loading.gif');
//                $(".inmodalloading").show();
            },
            success: function(response){
                $(".err_msg").html(response);
                $("#fsemail").val('');
                $("#fscemail").val('');
            }
        });
    });
});

$(function(){
   $('.btnNext').click(function(){
        $('.nav-tabs > .active').next('li').find('a').trigger('click');
      });

        $('.btnPrevious').click(function(){
        $('.nav-tabs > .active').prev('li').find('a').trigger('click');
      });
});


$(function(){

    function show_error (id_name, msg) {
            var html_jsval  =    '<div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button>';
                    html_jsval +=    msg;
                    html_jsval +=    '</div>';	
            $("#"+id_name).html(html_jsval);
            return false;
    }		
    function check_complete(classname, message) 
    {
            if(classname.indexOf('anchor_disable')>"0")
            {
                    // alert(message);
                    show_error ("errorMsg", message ) ;
                    return true;	
            }
    }
    $('.signupComplete').click(function(event) 
    {
        var classname = $(this).attr('class');
        var first_name = $('.first_name').val();
        var last_name = $('.last_name').val();
        var address = $('.address').val();
        var postal_code = $('.postal_code').val();
        var password = $('.password').val();
        var mobile_no = $('.mobile_no').val();

        if(password == ''){
            if( check_complete( classname, "Password can not be empty in user information tab." ) )
            {
                return false;
            }
        }else if(first_name == ''){
            if( check_complete( classname, "First Name can not be empty in user information tab." ) )
            {
                    return false;
            }
        }
        else if(last_name == ''){
            if( check_complete( classname, "Last Name can not be empty in user information tab." ) )
            {
                return false;
            }
        }else if(postal_code == ''){
                if( check_complete( classname, "Postal Code can not be empty in user information tab." ) )
                {
                    return false;
                }
        }else if(address == ''){
                if( check_complete( classname, "Address can not be empty in user information tab." ) )
                {
                    return false;
                }
        }else if(mobile_no == ''){
                if( check_complete( classname, "Mobile can not be empty in user information tab." ) )
                {
                    return false;
                }
        }
        else
        {
            $('#errorMsg').html('');
        }

    });           
});

$(function(){
    $('.registrationComplete').click(function(){
        $.ajax({
            type: "post",
            url: base_url+"en/CompleteRegistration",
            data: $('#signUpcompleteForm').serialize(),
            beforeSend: function(){
//                $('.inmodalloading').attr('src', base_url+'images/loading.gif');
//                $(".inmodalloading").show();
            },
            success: function(response){
                $('.registration_success_message').html(response);
            }
        });
    });
});

$(function(){
    $('.confirmUserInfo').click(function(){
        var customer_country = $('.customer_country').val();
        var customer_email = $('.customer_email').val();
        var password = $('.password').val();
        var first_name = $('.first_name').val();
        var last_name = $('.last_name').val();
        var customer_date_of_birth = $('.customer_date_of_birth').val();
        var address = $('.address').val();
        var postal_code = $('.postal_code').val();
        var mobile_no = $('.mobile_no').val();
        var home_phone = $('.home_phone').val();

        $('.ccountry').val(customer_country);
        $('.cemail').val(customer_email);
        $('.cfirstName').val(first_name);
        $('.clastName').val(last_name);
        $('.cAddress').val(address);
        $('.cpassword').val(password);
        $('.cdateOfBirth').val(customer_date_of_birth);
        $('.cpostalCode').val(postal_code);
        $('.cmobile_no').val(mobile_no);
        $('.chome_phone').val(home_phone);

    });
});


$(function(){
    $('#send_negotiation').click(function(){
        $.ajax({
            type: "post",
            url: base_url+"en/send_negotiation",
            data: $('#negotiationForm').serialize(),
            beforeSend: function(){
//                $('.inmodalloading').attr('src', base_url+'images/loading.gif');
//                $(".inmodalloading").show();
            },
            success: function(response){
                if(response != 'not_login'){

                    //alert(response);
                    $('#exampleModal').modal('toggle');
                    $('#message_feedback').html(response);
                    $('#messageModal').modal('show');

                }else{
                    //console.log(response);
                    //$('.negotiation_message').html(response);
                    //window.location.href= 'http://localhost/git/car/en/start';
                    window.location.href= 'http://tstjapan.co.jp/en/start';
                }
            }
        });
    });
});

$(function(){
    $('#btnContactInfo').click(function(){
        //alert();
        $.ajax({
            type: "post",
            url: base_url+"dashboard/ajax_update_contact_info",
            data: $('#edtiContactInformationForm').serialize(),
            beforeSend: function(){
//                $('.inmodalloading').attr('src', base_url+'images/loading.gif');
//                $(".inmodalloading").show();
            },
            success: function(response){
                $(".feedback_message").html(response);
            }
        });
    });
});
$(function(){
    $('#btnGeneralInfoUpdate').click(function(){
        //alert();
        $.ajax({
            type: "post",
            url: base_url+"dashboard/ajax_update_general_info",
            data: $('#editGeneralInformationForm').serialize(),
            beforeSend: function(){
//                $('.inmodalloading').attr('src', base_url+'images/loading.gif');
//                $(".inmodalloading").show();
            },
            success: function(response){
                $(".feedback_message").html(response);
            }
        });
    });
});

$(function(){
    $('#btnContactInfo').click(function(){
        //alert();
        $.ajax({
            type: "post",
            url: base_url+"dashboard/ajax_update_contact_info",
            data: $('#edtiContactInformationForm').serialize(),
            beforeSend: function(){
//                $('.inmodalloading').attr('src', base_url+'images/loading.gif');
//                $(".inmodalloading").show();
            },
            success: function(response){
                $(".feedback_message").html(response);
            }
        });
    });
});

$(function(){
    $('#btnChangePassword').click(function(){

        //alert();
        $.ajax({
            type: "post",
            url: base_url+"dashboard/ajax_change_password",
            data: $('#changePasswordForm').serialize(),
            beforeSend: function(){
//                $('.inmodalloading').attr('src', base_url+'images/loading.gif');
//                $(".inmodalloading").show();
            },
            success: function(response){
                $(".feedback_message").html(response);
            }
        });
    });

    $(".removeFavorite").on("click",function(){
        var formData = $(this).data();
        var car_id = formData.car_id;
        //console.log(car_id);
        $.ajax({
            type : "post",
            url : base_url +"dashboard/ajax_remove_favorite",
            data : {car_id : car_id},
            success : function(res){
                window.location.reload();
               // $(".feedback_message").html(res);
            }
        });
    })
});

$(function(){
    $('.result-show').click(function(){
        loadData();
    });
});

function loadData(){
    //loading_show();

//            var mainarray = new Array();

    var param = '';
    var make =$('#txtMakeId').val();
    if(make != '' && make !=0){
        param += "make_id="+make;
    }

    var model =$('#txtSerchModel').val();
    if(model != '' && model !=0){
        param += "&Model_id="+model;
    }
    var min_price = $('#min_price').val();
    if(min_price != '' && min_price != 0){
        param += "&min_price="+min_price;
    }
    var max_price =$('#max_price').val();
    if(max_price != '' && max_price !=0){
        param += "&max_price="+max_price;
    }
    var startManufactureYear =$('#startManufactureYear').val();
    if(startManufactureYear != '' && startManufactureYear !=0){
        param += "&startManufactureYear="+startManufactureYear;
    }
    var endManufactureYear =$('#endManufactureYear').val();
    if(endManufactureYear != '' && endManufactureYear !=0){
        param += "&endManufactureYear="+endManufactureYear;
    }
    var start_displacement =$('#start_displacement').val();
    if(start_displacement != '' && start_displacement !=0){
        param += "&start_displacement="+start_displacement;
    }
    var end_displacement =$('#end_displacement').val();
    if(end_displacement != '' && end_displacement !=0){
        param += "&end_displacement="+end_displacement;
    }

    var steering_left =$('#steering_left').val();
    if($("#steering_left").is(':checked')){
        param += "&steering_left="+steering_left;
    }

    var steering_right =$('#steering_right').val();
    if($("#steering_right").is(':checked')){
        param += "&steering_right="+steering_right;
    }
    var car_category =$('#txtCategory').val();
    if(car_category != '' && car_category != 0){
        param += "&car_category="+car_category;
    }
    var transmission_id =$('#txtTransmissionId').val();
    if(transmission_id != '' && transmission_id != 0){
        param += "&transmission_id="+transmission_id;
    }
    var fuel =$('#txtFuel').val();
    if(fuel != '' && fuel != 0){
        param += "&fuel="+fuel;
    }
    var doors =$('#doors').val();
    if(doors != '' && doors != 0){
        param += "&doors="+doors;
    }
    var color =$('#txtColor').val();
    if(color != '' && color != 0){
        param += "&color="+color;
    }
    var option =$('#txtOptions').val();
    if(option != '' && option != 0){
        param += "&option="+option;
    }


    var condition_used =$('#condition_used').val();
    if($("#condition_used").is(':checked')){
        param += "&condition_used="+condition_used;
    }

    var condition_new = $('#condition_new').val();
    if($("#condition_new").is(':checked')){
        param += "&condition_new="+condition_new;
    }

    console.log(param);
    $.ajax
    ({
        type: "POST",
        url: "<?php echo base_url();?>en/ajax_get_car",
        data: param,
        cache: false,
        success: function(msg)
        {

            //loading_hide();
             console.log(msg);

            //$("#SearchCard").html(msg);

        }
    });
}